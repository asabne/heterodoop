package org.apache.hadoop.mapred.lib;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Partitioner;


public class StringPartitioner implements Partitioner<Text, Text> {

  public void configure(JobConf job) {}

  public int getPartition(Text key, Text value,
                          int numReduceTasks) {
    String sKey = key.toString();
    //String[] splits=sKey.split("\t");  //Split the key on tab
    //int year = Integer.parseInt(splits[1]);  //The year is the second field
    //return year % numReduceTasks;  //Return the year mod number of reduce tasks as the partitioner number to send the record to.

    char c = sKey.charAt(0);
    return c % numReduceTasks;
  }
}
