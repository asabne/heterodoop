import sys
from random import randint
import argparse

#2000000*2 ==> 126MB input
numLines = 2000000*2
#numLines = 18000

parser = argparse.ArgumentParser()
parser.add_argument("rlength", help="enter 1 if the length should be random between 1 and 16",
                    type=int)
parser.add_argument('--lines', type=int,default=numLines, help='Number of lines to generate')
args = parser.parse_args()

length = 16;


numLines = args.lines
for j in range(numLines):  
  if args.rlength == 1:
    length = randint(1,16);
  for i in range(length):
    sys.stdout.write(str(randint(1,5)))
    sys.stdout.write(",")
  sys.stdout.write("\n")
