#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char ch ;
	char word[4095];
	int opInd = 0;
	int i;
	int start = 0;
	int count;
	int val;
	float prev = -1;
	float bin;
	
	count = 0;
	while(scanf("%s", word) == 1) {
		scanf("%*[ \n\t]%d", &val);
		bin = atof(word);
		
		if(bin == prev ) {
			count += val;
		}
		else {
			if(prev != -1)
				printf("%.1f\t%d\n", prev, count);
			prev = bin;
			count = val;
		}
	}
	
	if(prev != -1)
		printf("%.1f\t%d\n", prev, count);
		
	return 0;
}
