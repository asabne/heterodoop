#include <stdio.h>
#include <stdlib.h>


int main() {
	char *line;
	
	int read;
	int one;
	int ratingInt;
	size_t nbytes = 100000;
	line = (char*) malloc(nbytes*sizeof(char));
	
	#pragma mapreduce mapper key(ratingInt) value(one) keylength(1) vallength(1) kvpairs(16)
	{
    while ( (read = getline(&line, &nbytes, stdin)) != -1) {
      //printf("%s", line);
      int count = 0;
      char rating[5];
      int ratingSum = 0;
      one = 1;
      while ( count < read - 1 )
      {
        int start = 0;
        while(line[count] != ',' && line[count] != '\0' && line[count] != '\n' ) {
          rating[start] = line[count];
          start++;
          count++;
        }
        rating[start] = '\0';
        if(start) {
          ratingInt = atoi(rating);
          printf("%d\t%d\n", ratingInt, one);
        }
        count++;
      }
    }
  }
  free(line);
	return 0;
	
}
