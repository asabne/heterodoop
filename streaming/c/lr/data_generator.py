import sys
import string
from random import randint
import argparse
import os
#numLines = 1024*1024
numLines=1024*128
numRows = 32;
numCols = 12;
limit=128


parser = argparse.ArgumentParser()
parser.add_argument('--lines', type=int,default=numLines, help='Number of lines to generate')

args = parser.parse_args()

numLines = args.lines

for j in range(numLines):  
  for k in range(numRows):  
    val = randint(-limit,limit);
    sys.stdout.write(str(val));
    sys.stdout.write('_');
    for m in range(numCols): 
      val = randint(-limit,limit);
      sys.stdout.write(str(val));
      sys.stdout.write(',');
  sys.stdout.write("\n")


