#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//These dimensions are of x
#define ROWS 32
#define COLS 12
#define NUMREDUCERS 16

#define BUFSIZE ( (COLS*COLS + COLS) * 12)
int main() {
	char *line;
	int read;
	int i,j;
	char temp[30];
  char dataBuffer[BUFSIZE];
	int reducerNum;
  size_t nbytes = 10000;
	line = (char*) malloc(nbytes*sizeof(char));
	
  srand(time(0));
  reducerNum = rand()% NUMREDUCERS;
	
  //kvpairs == numReducers
	#pragma mapreduce mapper key(reducerNum) value(dataBuffer) keylength(1) vallength(1872) kvpairs(2)
	{
		while ( (read = getline(&line, &nbytes, stdin)) != -1) {
			char temp[20];
			int ptr = 0 ;
      int xMat[ROWS][COLS];
      int y[ROWS];
      int xTxMat[COLS][COLS];
      int xTy[COLS];
      int dataSetNum=0;
      int k;
      int m;
      int p;
			int count = 0;
			long residue;
			int key;
      
      for(dataSetNum=0; dataSetNum < ROWS; dataSetNum++) {
        ptr = 0;
        while(line[count] != '_') {
          temp[ptr++] = line[count++];
        }
        temp[ptr] = '\0';
        y[dataSetNum] = atoi(temp);
        count++;
        
        for(k=0; k <COLS; k++) {
          ptr = 0;
          while(line[count] != ',') {
            temp[ptr++] = line[count++];
          }
          temp[ptr] = '\0';
          xMat[dataSetNum][k] = atoi(temp);
          count++;
        }
        
        //calculate xTxMat
        for(k=0; k <COLS; k++) {
          int sum2=0;
          for(m=0; m < COLS; m++) {
            int sum =0;
            
            for(p=0; p < ROWS; p++) {
              sum += xMat[p][k]*xMat[p][m];
            }
            xTxMat[k][m] = sum;
            
            sum2 += xMat[m][k]*y[m];
          }
          xTy[k] = sum2;
        }
      }
      /*
      for(i=0;i<CHUNK; i++) {
        for(j=0;j<CHUNK; j++) {
          printf("x[%d][%d] %d\n", i, j, xMat[i][j]);
        }
         printf("y[%d] %d\n", i, y[i]);
      }
      */
      /*
      for(i=0;i<CHUNK; i++) {
        for(j=0;j<CHUNK; j++) {
          printf("xTx[%d][%d] %d\n", i, j, xTxMat[i][j]);
        }
         printf("xTy[%d] %d\n", i, xTy[i]);
      }
      */
    ptr = 0;
    for(k=0; k <COLS; k++) {
        for(m=0; m < COLS; m++) {
        key = xTxMat[k][m];
        residue = 1;
        if(key < 0) {
          key = -key;
          dataBuffer[ptr++] = 45;		
        }
        residue = 1;
        while( residue*10  < key) {
          residue = residue*10;
        }
        while( residue >= 1) {
          int digit = key / residue;
          key = key - digit*residue;  
          dataBuffer[ptr++] = 48 + digit;	
          residue = residue/10;
        }
        dataBuffer[ptr++]= ',';
      }
    }
    
    for(m=0; m < COLS; m++) {
      key = xTy[m];
      residue = 1;
      if(key < 0) {
        key = -key;
        dataBuffer[ptr++] = 45;		
      }
      residue = 1;
      while( residue*10  < key) {
        residue = residue*10;
      }
      while( residue >= 1) {
        int digit = key / residue;
        key = key - digit*residue;  
        dataBuffer[ptr++] = 48 + digit;	
        residue = residue/10;
      }
      dataBuffer[ptr++]= ',';
    }
    dataBuffer[ptr++]= '\0';
    
    if(xTy[0] < 0) {
      reducerNum = (-xTy[0])%NUMREDUCERS;
    } else {
      reducerNum = xTy[0]%NUMREDUCERS;
    }
    
    printf("%d\t%s\n", reducerNum, dataBuffer);
    //printf("%d\n", ptr);
	}
  }
	free(line);
	return 0;
	
}
