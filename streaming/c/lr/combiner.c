#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#define COLS 12

#define BUFSIZE ( (COLS*COLS + COLS) * 12)

int main() {
	
  int prevReducerNum;
  int reducerNum;
  char dataBuffer[BUFSIZE];
  char dataOutBuffer[BUFSIZE];
  int x[COLS][COLS];
  int y[COLS];
  
  //printf("Alloc done\n");
  #pragma mapreduce combiner key(prevReducerNum) value(dataOutBuffer) keyin(reducerNum) valuein(dataBuffer) keylength(1) vallength(1872)
  {
    int i;
    int j;
    int k;
    int m;
    int ptr;
    int dataSetNum;
    int key;
    int count;
    char temp[20];
    long residue;
    prevReducerNum = -1;

    while(scanf("%d %s", &reducerNum, dataBuffer) == 2) {
      //printf("In while loop with reducerNum %d data %s\n", reducerNum, dataBuffer);
      //emit KV
      if(reducerNum != prevReducerNum) {
        if(prevReducerNum != -1) {
          ptr = 0;
          for(k=0; k <COLS; k++) {
              for(m=0; m < COLS; m++) {
              key = x[k][m];
              residue = 1;
              if(key < 0) {
                key = -key;
                dataOutBuffer[ptr++] = 45;		
              }
              residue = 1;
              while( residue*10  < key) {
                residue = residue*10;
              }
              while( residue >= 1) {
                int digit = key / residue;
                key = key - digit*residue;  
                dataOutBuffer[ptr++] = 48 + digit;	
                residue = residue/10;
              }
              dataOutBuffer[ptr++]= ',';
            }
            
          }
          
          for(m=0; m < COLS; m++) {
            key = y[m];
            residue = 1;
            if(key < 0) {
              key = -key;
              dataOutBuffer[ptr++] = 45;		
            }
            residue = 1;
            while( residue*10  < key) {
              residue = residue*10;
            }
            while( residue >= 1) {
              int digit = key / residue;
              key = key - digit*residue;  
              dataOutBuffer[ptr++] = 48 + digit;	
              residue = residue/10;
            }
            dataOutBuffer[ptr++]= ',';
          }
          dataOutBuffer[ptr++]= '\0';
          printf("%d\t%s\n", prevReducerNum, dataOutBuffer);
          
        }
        //set prevReducerNum
        prevReducerNum = reducerNum;
        //reset x and y
        for(k=0; k < COLS; k++) {
          for(m=0; m < COLS; m++) {
            x[k][m] = 0;
          }
          y[k]=0;
        }
      }
      
      //do combine
      count = 0;
      for(dataSetNum=0; dataSetNum < COLS; dataSetNum++) {
        for(k=0; k <COLS; k++) {
          ptr = 0;
          while(dataBuffer[count] != ',') {
            temp[ptr++] = dataBuffer[count++];
          }
          temp[ptr] = '\0';
          x[dataSetNum][k] += atoi(temp);
          count++;
        }
        
        ptr = 0;
        while(dataBuffer[count] != ',') {
          temp[ptr++] = dataBuffer[count++];
        }
        temp[ptr] = '\0';
        y[dataSetNum] += atoi(temp);
        count++;
      }
       
    }
    
    //emit last KV
    ptr = 0;
    for(k=0; k <COLS; k++) {
        for(m=0; m < COLS; m++) {
          //printf("Key is %d\n", x[k][m]);
          key = x[k][m];
          residue = 1;
          if(key < 0) {
            key = -key;
            dataOutBuffer[ptr++] = 45;		
          }
          while( residue*10  < key) {
            residue = residue*10;
          }
          while( residue >= 1) {
            int digit = key / residue;
            key = key - digit*residue;  
            dataOutBuffer[ptr++] = 48 + digit;	
            residue = residue/10;
          }
          dataOutBuffer[ptr++]= ',';
        }
      //printf("In first loop\n");
    }
    
    for(m=0; m < COLS; m++) {
      key = y[m];
      residue = 1;
      if(key < 0) {
        key = -key;
        dataOutBuffer[ptr++] = 45;		
      }
      while( residue*10  < key) {
        residue = residue*10;
      }
      while( residue >= 1) {
        int digit = key / residue;
        key = key - digit*residue;  
        dataOutBuffer[ptr++] = 48 + digit;	
        residue = residue/10;
      }
      dataOutBuffer[ptr++]= ',';
    }
    dataOutBuffer[ptr++]= '\0';
    printf("%d\t%s\n", prevReducerNum, dataOutBuffer);
        
  }
  return 0;
}
