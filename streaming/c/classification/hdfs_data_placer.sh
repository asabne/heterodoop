#! /bin/bash

outputDir=$1
node=$3
host=`hostname`
opFile=$outputDir/$host

#this is the same as in the data generation script (8192*8)
numMoviesPerFile=65536


d=0
if [ -z "$5" ]
	then
		d=0;
	else	
		d=$5;
fi

baseId=`expr $node \* $numMoviesPerFile \* $2`
for (( c=1; c<=$2; c++ ))
do
	p=`expr $d + $c`
	#if [ "$baseId" -eq 0 ]
	#then
		#python ./data_generator.py $baseId --initFile=$4 > $opFile	
	#else
		#python ./data_generator.py $baseId > $opFile 
	#fi

	#hadoop fs -put $opFile input/$host-$p
	./gen_place.sh $baseId $opFile $host $p &
	
	m=`expr $c % 12`
	if [[ "$m" -eq 0 ]]
	then
		running=`ps -A | grep python`
		while [ -n "$running" ]
		do
			running=`ps -A | grep python`
		done
	fi

	baseId=`expr $baseId + $numMoviesPerFile`


done

#wait for all files to be placed
running=`ps -A | grep python`
while [ -n "$running" ]
do
	running=`ps -A | grep python`
done

#sleep for a second; to ensure that all files get in the HDFS
sleep 1

