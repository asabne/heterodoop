import sys
import os
from random import randint
import argparse
numLines = 8192*8
reviewsPerMovie = 256
rareReviews = 1024
numCentroids=16;
filename = ''

#this is 2^30
totalReviewers = 1073741824
parser = argparse.ArgumentParser()
parser.add_argument("baseId", help="base movie id to start with",
                    type=int)
parser.add_argument('--lines', type=int,default=numLines, help='Number of lines to generate')
parser.add_argument("--initFile", help="Provide the filepath for the final file",
                    type=str, default=filename)
parser.add_argument('--numCentroids', type=int,default=numCentroids, help='Number of lines to generate')

args = parser.parse_args()

baseId = args.baseId
length = 16;

filename = args.initFile
numCentroids = args.numCentroids

f = 0

if filename != '' :
  #delete if the file exists already
  if os.path.isfile(filename):
    os.remove(filename)
  f = open(filename, 'w')

numLines = args.lines


for j in range(numLines): 
  writeInitFile = 0;
  if j < numCentroids and filename != '' :
    writeInitFile = 1
    
  sys.stdout.write(str(j+baseId))
  sys.stdout.write(":")
  if j % 128 == 0 :
    numReviews = rareReviews
  else :
    numReviews = reviewsPerMovie
    
  if writeInitFile :
    f.write(str(j))
    f.write("\t")
    f.write(str(0))
    f.write(" ")
    f.write(str(j+baseId))
    f.write(" ")
    f.write(str(numReviews))
    f.write(" ")
    
  for k in range(numReviews): 
    userId = randint(1,totalReviewers)
    review = randint(1,5)
    sys.stdout.write(str(userId))
    sys.stdout.write("_")
    sys.stdout.write(str(review))
    if k != numReviews - 1 :
      sys.stdout.write(",")
      
    if writeInitFile :
      f.write(str(userId))
      f.write("_")
      f.write(str(review))
      if k != numReviews - 1 :
        f.write(",")
  sys.stdout.write("\n")
  
  if writeInitFile :
    f.write("\n")
