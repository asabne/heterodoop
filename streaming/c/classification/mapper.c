#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAXCLUSTERS 16
#define NUMRATINGS 1024
//#define CENTROID_FILE "/lustre/medusa/asabne/input/classification/initial_centroids"

int main() {
	char *line;
	int read;
	int one;
	int bin;
	int i;
	int j;
	long movie_id[MAXCLUSTERS];
	int total[MAXCLUSTERS];
	float similarity[MAXCLUSTERS];
	int rater_id[MAXCLUSTERS*NUMRATINGS];
	int rating[MAXCLUSTERS*NUMRATINGS];
	int locationHolder[MAXCLUSTERS];
	int centroid_num;
	char temp[30];
	int line1_read;
	int line2_read;
	int numClusters;
	long movieId;
	int clusterId;
	FILE *centroid_file;
	size_t nbytes = 10000;
	line = (char*) malloc(nbytes*sizeof(char));
  char *CENTROID_FILE = 	getenv("CENTROID_FILE");
	centroid_file = fopen( CENTROID_FILE, "r" );
	
	if(!centroid_file) 
		exit(1);
	
	//init
	for(i=0; i< NUMRATINGS*MAXCLUSTERS;i++) {
		rater_id[i] = -1;
		rating[i] = -1;
	}
	
	for(i=0; i< MAXCLUSTERS;i++) {
		locationHolder[i] = i;
	}
	
	centroid_num = 0;
	while( (line1_read = getline(&line, &nbytes, centroid_file)) != -1) {
		int index = -1;
		int reviewNum = 0;
		int number = 0;
		//line2_read = getline(&line3, &nbytes, centroid_file);
		i = 0;
		j=0;
		//printf("line %s", line);
		while( number < 4){
			int token = 0;
			while(line[i] != '\n' && line[i] != ' ' && line[i] != '\t') {
				temp[token++] = line[i];
				i++;
			}
			temp[token] = '\0';
			
			if(number == 0 ) {
				index = atoi(temp);
				//printf("index %d\n", index);
				//i++; i++;
			} else if (number == 1) {
				//printf("temp %s\n", temp);	
				similarity[index] = atof(temp);
			} else if (number == 2) {
				movie_id[index] = atol(temp);
			} else if (number == 3) {
				total[index] = atoi(temp);
			}
			
			while(line[i] == '\n' || line[i] == ' ' || line[i] == '\t') {
				i++;
				//printf("Went up %c\n", line[i]);
			}	
			number++;
		}
		
		//i=0;
		while( i < line1_read) {
			int token = 0;
			while(line[i] != '_') {
				temp[token++] = line[i];
				i++;
			}
			temp[token] = '\0';
			rater_id[NUMRATINGS*index + reviewNum] = atoi(temp);
			token = 0;
			i++;
			while(line[i] != ',' && line[i] !='\n') {
				temp[token++] = line[i];
				i++;
			}
			temp[token] = '\0';
			rating[NUMRATINGS*index + reviewNum] = atoi(temp);
			i++;
			reviewNum++;
		}
	}
	
	/*
	for(j=0; j< MAXCLUSTERS; j++) {
		printf("index %d simi %f movie_id %d total %d\n", j, similarity[j], movie_id[j], total[j]);
		
		for(i=j*NUMRATINGS; ; i++){
			if(rating[i] == -1) {
				break;
			}
			printf("id %d rating %d\n", rater_id[i], rating[i]);
		}
	}
	*/
	
	//sort by movie id
	for(i=0; i<MAXCLUSTERS;i++) {
		int max;
		int replaceTemp;
		max = i;
		for(j=i+1; j < MAXCLUSTERS; j++) {
			if(movie_id[locationHolder[j]] >  movie_id[locationHolder[max]]) {
				max = j;
			}
		}
		replaceTemp = locationHolder[max];
		locationHolder[max] = locationHolder[i];
	    locationHolder[i] = replaceTemp;
	}
	
	//find out number of valid clusters
	numClusters = 0;
	for(i=0; i< MAXCLUSTERS; i++) {
      if(movie_id[i] != -1){
        numClusters++;
      }
    }
	
	/*
	for(i=0; i<MAXCLUSTERS;i++) {
		printf("Movie ID %d\n", movie_id[locationHolder[i]]);
	}
	*/
	
	#pragma mapreduce mapper key(clusterId) value(movieId) keylength(1) vallength(1) sharedRO(movie_id, total, similarity, rating, locationHolder, numClusters) texture(rater_id) kvpairs(16)
	{
		while ( (read = getline(&line, &nbytes, stdin)) != -1) {
			int review;
			int userId;
			int p;
			int q;
			int r;
			int rater;
			int ratingValue;
			int movieIndex;
			float sq_a[16];
			float sq_b[16];
			float numer[16];
			float denom[16];
			float max_similarity = 0.0f;
			float similarity = 0.0f;
			char temp[20];
			int ptr = 0 ;
			//printf("%s", line);
			int count = 0;
			
			for (r = 0; r < 16; r++) {
				numer[r] = 0.0f;
				denom[r] = 0.0f;
				sq_a[r] = 0.0f;
				sq_b[r] = 0.0f;
			}
			
			movieIndex = 0;
				
			while(line[count] != ':' && line[count] != '\n') {
				temp[movieIndex++] = line[count++];
			}
			temp[movieIndex] = '\0';
			
			if(movieIndex) {
				movieId = atol(temp);
				count++;
				clusterId = movieId % numClusters; //randomize the initial cluster num
				while(count < read -1) {
					ptr = 0;
					while(line[count] != '_') {
						temp[ptr++] = line[count++];
					}
					temp[ptr] = '\0';
					userId = atoi(temp);
					count++;
					
					ptr = 0;
					while(line[count] != ',' && line[count] != '\n') {
						temp[ptr++] = line[count++];
					}
					temp[ptr] = '\0';
					review = atoi(temp);
					count++;
					
					for (r = 0; r < numClusters; r++) {
						for (q = 0; q < total[locationHolder[r]]; q++) {
						  rater = rater_id[r*NUMRATINGS + q];
						  ratingValue = rating[r*NUMRATINGS + q];
						  if (userId == rater) {
							numer[r] += (float) (review * ratingValue);
							sq_a[r] += (float) (review * ratingValue);
							sq_b[r] += (float) (ratingValue * ratingValue);
							break; // to avoid multiple ratings by the same reviewer
						  }
						}
					}
				}
				
				for (p = 0; p < numClusters; p++) {
				  denom[p] = (float) ((sqrt((double) sq_a[p])) * (sqrt((double) sq_b[p])));
				  if (denom[p] > 0) {
					similarity = numer[p] / denom[p];
					if (similarity > max_similarity) {
					  max_similarity = similarity;
					  clusterId = p;
					}
				  }
				}
				//output.collect(new IntWritable(clusterId), new Text(movieIdStr));
				printf("%d\t%ld\n", clusterId, movieId);
			}
		}
	}
	free(line);
	
	
	return 0;
	
}
