#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	int prevKey;
	int keyIn;
	long valIn;
	long val;
	int key;
	val = 0;
	prevKey = -1;
	while(scanf("%d", &keyIn) == 1 && scanf("%*[ \n\t]%ld", &valIn) == 1) {
		prevKey = keyIn;
		val = valIn;
		printf("%d\t%ld\n", prevKey, val);
	}
		
		
	return 0;
}
