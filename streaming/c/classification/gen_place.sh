#! /bin/bash

baseId=$1
opFile=$2$4
host=$3
p=$4
if [ "$baseId" -eq 0 ]
then
	python ./data_generator_dyn_sched.py $baseId --initFile=$4 > $opFile	
else
	python ./data_generator_dyn_sched.py $baseId > $opFile 
fi

echo "hadoop fs -put $opFile input/$host-$p"
hadoop fs -put $opFile input/$host-$p

rm -f $opFile
