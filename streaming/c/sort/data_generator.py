import sys
import string
import random
import argparse
import os
numLines = 2700000
#numLines = 4*1024*1024

def random_generator(size=10, chars=string.letters +string.digits +  string.punctuation):
  return ''.join(random.choice(chars) for x in range(size))


parser = argparse.ArgumentParser()
parser.add_argument("filename", help="Provide the filepath for the final file",
                    type=str)
parser.add_argument('--lines', type=int,default=numLines, help='Number of lines to generate')

args = parser.parse_args()

filename = args.filename

#delete if the file exists already
if os.path.isfile(filename):
  os.remove(filename)

f = open(filename, 'a+')

numLines = args.lines

for j in range(numLines):  
  text = random_generator()
  f.write(text)
  for i in range(9):
    f.write(" ")
  payload = random_generator(79, string.ascii_uppercase + string.digits)
  f.write(payload)
  f.write("\n")
  


