#include <stdio.h>
#include <stdlib.h>


int main() {
	
	char ch ;
	char word1[11];
	char word2[91];
	int i;
	
	char *line;
	size_t nbytes = 100000;
	int read;
	int count;
	int ptr;
	line = (char*) malloc(nbytes*sizeof(char));
	
	#pragma mapreduce mapper key(word1) value(word2) keylength(11) vallength(91) kvpairs(1)
	while ( (read = getline(&line, &nbytes, stdin)) != -1) {
		int start1;
		int start2;
		//printf("%s", line);
		count = 0;
		start1 = 0;
		start2 = 0;
		ptr = 0;
		
		while(start1 < 10) {
			word1[start1++] = line[count++];
		}
		word1[start1] = '\0';
		
		
		while(line[count] == ' ' &&  ptr < 8) {
			count++; ptr++;
		}
		while ( start2 < 90 && count < read - 1 )
		{
			word2[start2++] = line[count++];
		}
		word2[start2] = '\0';
		
		if(start1 && start2) {
			printf("%s\t%s\n", word1, word2);
		}
	}
	
  free(line);
	return 0;
}
