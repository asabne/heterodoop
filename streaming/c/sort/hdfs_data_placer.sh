#! /bin/bash

outputDir=$1
host=`hostname`
opFile=$outputDir/$host


python ./data_generator.py $opFile 

d=0
if [ -z "$3" ]
	then
		d=0;
	else	
		d=$3;
fi

for (( c=1; c<=$2; c++ ))
do
	p=`expr $d + $c` 
	hadoop fs -put $opFile input/$host-$p
done

rm -f $opFile
