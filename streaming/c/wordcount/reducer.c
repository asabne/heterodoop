#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	
	FILE *fp, *fop, *find ;
	char ch ;
	int nol = 0, not = 0, nob = 0, noc = 0 ;
	char word[28];
	char* opArr;
	double startTime, endTime;
	int opInd = 0;
	int i;
	int start = 0;
	long crc;
	long *indArr;
	char *indArrChar;
	int indSize;
	int count;
	int val;
	char prevWord[28];
	
	prevWord[0] = '\0';
	count = 0;
	while(scanf("%s", word) == 1) {
		scanf("%*[ \n\t]%d", &val);
		
		if(strcmp(word, prevWord) == 0 ) {
			count += val;
		}
		else {
			if(prevWord[0] != '\0')
				printf("%s\t%d\n", prevWord, count);
			strcpy(prevWord, word);
			count = val;
		}
		
	}
	
	if(prevWord[0] != '\0') 
		printf("%s\t%d\n", prevWord, count);
		
	return 0;
}
