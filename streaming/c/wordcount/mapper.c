#include <stdio.h>
#include <stdlib.h>


int main() {
	
	char ch ;
	char word[28];
	int i;
	int start;
	char *line;
	size_t nbytes = 100000;
	int read;
	int count;
	int one;
	

	line = (char*) malloc(nbytes*sizeof(char));
	
	#pragma mapreduce mapper key(word) value(one) keylength(28) kvpairs(10)
	while ( (read = getline(&line, &nbytes, stdin)) != -1) {
		//printf("%s", line);
		count = 0;
		start = 0;
		one = 1;
		while ( count < read - 1 )
		{
			ch = line[count] ;
			if (ch != ' ' && count != read - 2) {
				word[start] = ch;
				start++;
			} 
			else if (ch == ' ' || count == read - 2) {
				if(count == read - 2) {
					if(ch != ' ' && ch != '\n') {
						word[start] = ch;
						start++;
					}
				}
				if(start) {
					word[start] = '\0';
					printf("%s\t%d\n", word, one);
					//reset the word start location
					start = 0;
				}
			}
			count++;
		}
	}
  
	free(line);
	return 0;
	
}
