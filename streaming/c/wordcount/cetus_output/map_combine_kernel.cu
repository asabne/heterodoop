#include "mrlib.h"
/*******************************************/
/* Added codes for C to CUDA translation */
/*******************************************/
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define DBL_MAX 1.7976931348623158e+308
#define DBL_MIN 2.2250738585072014e-308
#define FLT_MAX 3.402823466e+38
#define FLT_MIN 1.175494351e-38


void bindMapperTextures()
{

}

__global__ void gpu_mapper(char * __ip, int __ipSize, char * devKey, int * devVal, int __storesPerThread, int * __devKvCount, int __limit, int recordsPerThread, int * recordLocator, int __keyLength, int __valLength, int * __devIndexArray, int __totalThreads, int __numReducers)
{
char gpu_ch;
int gpu_read;
int gpu_one;
int gpu_count;
int gpu_start;
size_t gpu_nbytes;
char gpu_word[28];
int __index;
int __tid;
int __start;
__shared__ unsigned int recordIndex;
int threadRecordCounter;
mapSetup(__start, __tid, __index, __limit, __ipSize, __storesPerThread, __ip, __devKvCount, __numReducers, __totalThreads, threadRecordCounter, ( & recordIndex));
gpu_read=0;
#pragma mapreduce  mapper key(word) value(one) keylength(28) kvpairs(10)
while ((gpu_read=getline(__ip, recordsPerThread, ( & recordIndex), __start, recordLocator, threadRecordCounter))!=( - 1))
{
/* printf("%s", line); */
gpu_count=0;
gpu_start=0;
gpu_one=1;
while (gpu_count<(gpu_read-1))
{
gpu_ch=__ip[(gpu_count+__start)];
if (((gpu_ch!=' ')&&(gpu_count!=(gpu_read-2))))
{
gpu_word[gpu_start]=gpu_ch;
gpu_start ++ ;
}
else
{
if (((gpu_ch==' ')||(gpu_count==(gpu_read-2))))
{
if ((gpu_count==(gpu_read-2)))
{
if (((gpu_ch!=' ')&&(gpu_ch!='\n')))
{
gpu_word[gpu_start]=gpu_ch;
gpu_start ++ ;
}
}
if (gpu_start)
{
gpu_word[gpu_start]='\0';
emitKV(gpu_word, ( & gpu_one), devKey, devVal, __index, __devKvCount, __keyLength, __valLength, __numReducers, __totalThreads, __storesPerThread, __devIndexArray);
/* reset the word start location */
gpu_start=0;
}
}
}
gpu_count ++ ;
}
}
mapFinish(__index, __storesPerThread, devKey, __keyLength, __devIndexArray, __totalThreads, __numReducers, __devKvCount);
}

__global__ void gpu_combiner(char * __key, int * __val, char * __opKey, int * __opVal, int * __indexArray, int * __finalCount, int __size, int __combinerThreads, int __mapKeyLength, int __mapValLength, int __combKeyLength, int __combValLength)
{
int __laneID;
int __kvsPerThread;
int __warpID;
int __ptr;
int __high;
int __kvCount;
int __index;
setCombinerVars(__kvsPerThread, __laneID, __warpID, __ptr, __high, __kvCount, __index, __size, __combinerThreads);
if (1)
#pragma mapreduce  key(prevWord) value(count) keylength(28) valuein(val) keyin(word) vallength(1) combiner
{
int read;
int gpu_count;
int gpu_val;
__shared__ char gpu_prevWord[28];
__shared__ char gpu_word[28];
gpu_prevWord[0]='\0';
gpu_count=0;
while (getKeyVal(gpu_word, __key, ( & gpu_val), __val, __ptr, __high, __indexArray, __mapKeyLength, __mapValLength)!=-1)
{
/* printf("KEY %s val %d read %d\n", word, val, read ); */
if ((strcmp(gpu_word, gpu_prevWord, __mapKeyLength)==0))
{
gpu_count+=gpu_val;
}
else
{
if ((gpu_prevWord[0]!='\0'))
{
storeKV(gpu_prevWord, ( & gpu_count), __combKeyLength, __combValLength, __opKey, __opVal, __index, __kvCount);
}
strcpy(gpu_prevWord, gpu_word, __mapKeyLength, 1);
gpu_count=gpu_val;
}
}
if ((gpu_prevWord[0]!='\0'))
{
storeKV(gpu_prevWord, ( & gpu_count), __combKeyLength, __combValLength, __opKey, __opVal, __index, __kvCount);
}
__finalCount[__warpID]=__kvCount;
}
}

