#include "mr.h"
/*
Copyright (C) 1991-2014 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it andor
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http:www.gnu.org/licenses/>. 
*/
/*
This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it. 
*/
/*
glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default. 
*/
/*
wchar_t uses ISOIEC 10646 (2nd ed., published 2011-03-15) /
   Unicode 6.0. 
*/
/* We do not support C11 <threads.h>.  */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
extern __global__ void gpu_combiner(char * __key, int * __val, char * __opKey, int * __opVal, int * __indexArray, int * __finalCount, int __size, int __combinerThreads, int __mapKeyLength, int __mapValLength, int __combKeyLength, int __combValLength);
int combiner(char * __devKey, int * __devVal, int * __devIndexArray, int __mapKVCount, int * * __finalCount, int * * __opVal, char * * __opKey, int __mapKeyLength, int __mapValLength, int __combKeyLength, int __combValLength, int __combineBlocks, int __combineBlockSize)
{
char word[28];
int count;
int val;
char prevWord[28];
int * __devFinalCount;
int * __devOpVal;
char * __devOpKey;
int __combinerThreads;
allocateCombinerData(((void * *)( & __devFinalCount)), ((void * *)( & __devOpKey)), ((void * *)( & __devOpVal)), ((void * *)__finalCount), ((void * *)__opKey), ((void * *)__opVal), __combineBlockSize, __combineBlocks, __combinerThreads, __mapKeyLength, __mapValLength, __combKeyLength, __combValLength, __mapKVCount, _char_, _int_);
gpu_combiner<<<__combineBlocks, __combineBlockSize, 0, 0>>>(__devKey, __devVal, __devOpKey, __devOpVal, __devIndexArray, __devFinalCount, __mapKVCount, __combinerThreads, __mapKeyLength, __mapValLength, __combKeyLength, __combValLength);
cudaDeviceSynchronize();
{

}
cudaMemcpy(( * __opKey), __devOpKey, ((__mapKVCount*__combKeyLength)*sizeof (char)), cudaMemcpyDeviceToHost);
cudaMemcpy(( * __opVal), __devOpVal, ((__mapKVCount*__combValLength)*sizeof (int)), cudaMemcpyDeviceToHost);
cudaMemcpy(( * __finalCount), __devFinalCount, ((__combinerThreads/32)*sizeof (int)), cudaMemcpyDeviceToHost);
releaseCombinerData(__devFinalCount, __devOpKey, __devOpVal);
return 0;
}

