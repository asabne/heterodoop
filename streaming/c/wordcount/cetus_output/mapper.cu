#include "mr.h"
extern void bindMapperTextures();
/*
Copyright (C) 1991-2014 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it andor
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http:www.gnu.org/licenses/>. 
*/
/*
This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it. 
*/
/*
glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default. 
*/
/*
wchar_t uses ISOIEC 10646 (2nd ed., published 2011-03-15) /
   Unicode 6.0. 
*/
/* We do not support C11 <threads.h>.  */
#include <stdio.h>
#include <stdlib.h>
extern __global__ void gpu_mapper(char * __ip, int __ipSize, char * devKey, int * devVal, int __storesPerThread, int * __devKvCount, int __limit, int recordsPerThread, int * recordLocator, int __keyLength, int __valLength, int * __devIndexArray, int __totalThreads, int __numReducers);
extern int combiner(char * __devKey, int * __devVal, int * __devIndexArray, int __mapKVCount, int * * __finalCount, int * * __opVal, char * * __opKey, int __mapKeyLength, int __mapValLength, int __combKeyLength, int __combValLength, int __combineBlocks, int __combineBlockSize);
int mr_main(char * inputPath, int numReducers, char * outputPath, void * fs)
{
char ch;
char word[28];
int i;
int start;
char * line;
size_t nbytes = 100000;
int read;
int count;
int one;
char * __ip;
int __ipSize;
int __mapblocks;
int __mapthreads;
int __combineBlocks;
int __combineBlockSize;
int __totalThreads;
int __limit;
int __storesPerThread;
int __kvStoreSize;
char * devKey;
char * key;
int * devVal;
int * val;
char * __devIp;
int * __devKvCount;
int * __indexArray;
int * __devIndexArray;
int * __newDevIndexArray;
int __combKeyLength;
int __combValLength;
int * __finalCount;
int __numReducers;
int __reducer;
double __timer;
int __storesPerBin;
int __recordsPerThread;
int * __recordLocator;
int __keyLength;
int __valLength;
int * __opVal;
char * __opKey;
int __mapKVcount;
__combKeyLength=28;
__combValLength=1;
__keyLength=28;
__valLength=1;
__numReducers=numReducers;
__ipSize=getFileSizeHDFS(inputPath, ( & __ip), fs);
setThreads(__mapblocks, __mapthreads, __combineBlocks, __combineBlockSize);
setInternalVariables(__mapblocks, __mapthreads, __totalThreads, __ipSize, __limit, __storesPerThread, __kvStoreSize, __numReducers, __ip, 10, __recordsPerThread, __recordLocator);
__timer=timer();
allocateMapData(((void * *)( & devKey)), ((void * *)( & devVal)), ((void * *)( & key)), ((void * *)( & val)), ((void * *)( & __devIp)), ((void * *)( & __devKvCount)), ((void * *)( & __devIndexArray)), ((void * *)( & __newDevIndexArray)), ((void * *)( & __indexArray)), __keyLength, __valLength, sizeof (char), sizeof (int), __kvStoreSize, __ipSize, __totalThreads, __numReducers);
printf("Setup time %lf\n", (timer()-__timer));
line=((char * )malloc((nbytes*sizeof (char))));
{

}
__timer=timer();
cudaMemcpy(__devIp, __ip, __ipSize, cudaMemcpyHostToDevice);
bindMapperTextures();
gpu_mapper<<<__mapblocks, __mapthreads, 0, 0>>>(__devIp, __ipSize, devKey, devVal, __storesPerThread, __devKvCount, __limit, __recordsPerThread, __recordLocator, __keyLength, __valLength, __devIndexArray, __totalThreads, __numReducers);
cudaDeviceSynchronize();
{

}
printf("Map time %lf\n", (timer()-__timer));
__storesPerBin=(__kvStoreSize/__numReducers);
initFile(outputPath);
for (__reducer=0; __reducer<__numReducers; __reducer=(__reducer+1))
{
__mapKVcount=linearize((__devIndexArray+(__storesPerBin*__reducer)), (__newDevIndexArray+(__storesPerBin*__reducer)), (__devKvCount+(__totalThreads*__reducer)), __totalThreads, __storesPerBin, __reducer, __numReducers);
mergeSort((__newDevIndexArray+(__storesPerBin*__reducer)), devKey, __keyLength, __mapKVcount);
__timer=timer();
combiner(devKey, devVal, (__newDevIndexArray+(__storesPerBin*__reducer)), __mapKVcount, ( & __finalCount), ( & __opVal), ( & __opKey), __keyLength, __valLength, __combKeyLength, __combValLength, __combineBlocks, __combineBlockSize);
printf("Combine time %lf\n", (timer()-__timer));
writeOP(__mapKVcount, __finalCount, _char_, _int_, __opKey, __opVal, __combKeyLength, __combValLength);
}
closeFile();
releaseMapData(devKey, devVal, key, val, __devIp, __ip, __devKvCount, __devIndexArray, __newDevIndexArray, __indexArray, __finalCount, __opVal, __opKey, 1);
free(line);
return 0;
}

