#! /bin/bash
source ~/vars.sh

if [ $# -ne 1 ]
then
  echo "Usage: ./parallel_loader.sh [filepath]"
  exit 1
fi

numFiles=`ls -l $1 |wc -l`
numFiles=`expr $numFiles - 1`
#echo $numFiles


numSlaves=`wc -l ${HADOOP_CONF_DIR}/slaves| cut -d' ' -f1`
filesPerSlave=`expr $numFiles \/ $numSlaves`
#echo $numSlaves
perfDiv=`expr $numFiles % $numSlaves`
if [[ "$perfDiv" -ne 0 ]]
then
	filesPerSlave=`expr $filesPerSlave + 1`
fi




slaveNum=0
while read line
do
	name=$line
	start=`expr $filesPerSlave \* $slaveNum`
	end=`expr $start + $filesPerSlave`
	if [[ "$end" -gt $numFiles ]]
	then
		end=$numFiles
	fi
	
	#echo "start $start end $end"
	ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/; nohup ./slave_placer.sh  $1 $start $end > /dev/null 2>&1 &'"
	slaveNum=`expr $slaveNum + 1`

done < ${HADOOP_CONF_DIR}/slaves


#wait until all nodes are done placing files
while read line
do
	name=$line
	running=`ssh -n $name "ps -A | grep slave_place"`
	while [ -n "$running" ]
	do
		running=`ssh -n $name "ps -A | grep slave_place"`
	done
done < ${HADOOP_CONF_DIR}/slaves

exit 1;



node=0
while read line
do
name=$line

case "$1" in

'wordcount')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/wordcount/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/wordcount/ $2 > /dev/null 2>&1 &'"
		;;
'blackscholes')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/blackScholes/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/blackScholes/ $2 > /dev/null 2>&1 &'"
		;;
'histratings')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/histratings/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/histratings/ $2 > /dev/null 2>&1 &'"
		;;
'histmovies')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/histmovies/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/histmovies/ $2 > /dev/null 2>&1 &'"
		;;
'sort')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/sort/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/sort/ $2 > /dev/null 2>&1 &'"
		;;
'grep')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/grep/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/grep/ $2 > /dev/null 2>&1 &'"
		;;
'lr')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/lr/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/lr/ $2 > /dev/null 2>&1 &'"
		;;
'kmeans')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/kmeans/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/kmeans/ $2 $node /lustre/medusa/asabne/input/classification/initial_centroids_script > /dev/null 2>&1 &'"
		;;
'classification')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/classification/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/classification/ $2 $node /lustre/medusa/asabne/input/classification/initial_centroids_script > /dev/null 2>&1 &'"
		;;
*)
	echo "Invalid Application Name specified"
		;;
esac

node=`expr $node + 1` 
done < ${HADOOP_CONF_DIR}/slaves

#done until all nodes are done placing files
while read line
do
	name=$line
	running=`ssh -n $name "ps -A | grep hdfs_data_"`
	while [ -n "$running" ]
	do
		running=`ssh -n $name "ps -A | grep hdfs_data"`
	done
done < ${HADOOP_CONF_DIR}/slaves
