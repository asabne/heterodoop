#! /bin/bash
source ~/vars.sh

if [ $# -ne 2 ]
then
  echo "Usage: ./master_placer.sh [Application Name] [number of Files per slave]"
  exit 1
fi

#hadoop fs -mkdir input

node=0
while read line
do
name=$line

case "$1" in

'wordcount')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/wordcount/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/wordcount/ $2 > /dev/null 2>&1 &'"
		;;
'blackscholes')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/blackScholes/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/blackScholes/ $2 > /dev/null 2>&1 &'"
		;;
'histratings')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/histratings/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/histratings/ $2 > /dev/null 2>&1 &'"
		;;
'histmovies')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/histmovies/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/histmovies/ $2 > /dev/null 2>&1 &'"
		;;
'sort')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/sort/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/sort/ $2 > /dev/null 2>&1 &'"
		;;
'grep')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/grep/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/grep/ $2 > /dev/null 2>&1 &'"
		;;
'lr')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/lr/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/lr/ $2 > /dev/null 2>&1 &'"
		;;
#Use the classification script instead
## 'kmeans')
		#ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/kmeans/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/kmeans/ $2 $node $INPUT_PATH/classification/initial_centroids_script > /dev/null 2>&1 &'"
		#;;
'classification')
		ssh -n $name "sh -c 'cd $HADOOP_HOME/streaming/c/classification/; nohup ./hdfs_data_placer.sh  $INPUT_PATH/classification/ $2 $node $INPUT_PATH/classification/initial_centroids_script > placer_log 2>&1 &'"
		;;
'stop')
		while read line
		do
			name=$line
			echo $line
			ssh -n $name "killall -9 hdfs_data_placer.sh;" < /dev/null
			ssh -n $name "killall -9 python;" < /dev/null
		done < ${HADOOP_CONF_DIR}/slaves
		exit
		;;
*)
	echo "Invalid Application Name specified"
		;;
esac

node=`expr $node + 1` 
done < ${HADOOP_CONF_DIR}/slaves

#wait until all nodes are done placing files
while read line
do
	name=$line
	echo "Waiting on node $name"
	running=`ssh -n $name "ps -A | grep hdfs_data_"`
	while [ -n "$running" ]
	do
		sleep 20
		#echo "Running $running"
		running=`ssh -n $name "ps -A | grep hdfs_data"`
	done
done < ${HADOOP_CONF_DIR}/slaves

