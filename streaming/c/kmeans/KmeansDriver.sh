#!/bin/bash
source ~/vars.sh

THRESHOLD=0.01
if [ $# -ne 6 ]
then
  echo "Usage: ./KmeansDriver.sh [numReds][input directory][output directory][num of iterations][number of GPUs][tail]"
  exit 1
fi
hadoop=$HADOOP_HOME
cd $hadoop
numReds=$1;
input=$2;
output=$3;
iterations=$4;
numGPU=$5;
tail=$6;

numCentroids=16;

similarities[$numCentroids]=0;
moviesIds[$numCentroids]=0;
reviewData[$numCentroids]=0;
movieCounts[$numCentroids]=0;

count=0;
#this is the path for the temporary file that this script uses
base_model="$INPUT_PATH/classification/initial_centroids";

#this is the initial centroid file : Either specified by the user, or is created from a script
centroids_file="$INPUT_PATH/classification/initial_centroids_script";
new_centroids_file=$hadoop/temp_centroids
cp $centroids_file $base_model
rm -r $hadoop/Results/* 2>&1

for (( k = 0; k < $iterations; k++ ))
do
  echo "========================"
  echo " Iteration # `expr $k + 1` "
  echo "========================"
  
  out="tmpout$k" 2>&1
  hadoop dfs -rmr $out 2>&1
  hadoop dfs -rmr $output 2>&1

  hadoop jar build/contrib/streaming/hadoop-streaming-\$\{version\}.jar -Dmapred.map.gpushare=$numGPU -Dmapred.map.tail=$tail -D mapred.reduce.tasks=$numReds -file ~/cuda_rt/requester0 -file ~/cuda_rt/requester1 -file ~/cuda_rt/requester2 -file streaming/c/kmeans/mapper -mapper streaming/c/kmeans/mapper -file streaming/c/kmeans/reducer -reducer streaming/c/kmeans/reducer -partitioner org.apache.hadoop.mapred.lib.IntegerPartitioner -input $input -output $out

  if [ $k -lt $iterations ]
  then
	rm $new_centroids_file
    echo "Getting results from HDFS for iteration $i ..."
    mkdir -p $hadoop/Results/$out 2>&1
    hadoop dfs -get $out/* $hadoop/Results/$out/ 2>&1

    echo "Generating new centroids file ..."
    cd $hadoop/Results/$out 2>&1
    tmpcc=0;
    files=$(ls) 2>&1
    for num in $files
    do
      tmpcc=`expr $tmpcc + 1`
    done
    cc=1;
	centroidNum=0;
    files=$(ls) 2>&1
	for file in $files
	do
        if [ -f $file ]
        then 
          while read -r movie_id  similarity 
          do
               if [ -z $movie_id ]
               then
                   continue
               else
                   #echo "$centroidnum       $similarity $movie_id $movies_total $movies_reviews" >> model_file_tmp 
					echo "$movie_id       $similarity"
					movieIds[$centroidNum]=$movie_id 
					similarities[$centroidNum]=$similarity
				    centroidNum=`expr $centroidNum + 1`
               fi
          done < $file
        fi
        echo "file $cc of $tmpcc scanned ..."
        cc=`expr $cc + 1` 
	done

	cc=1;
	cd $hadoop
	hadoop jar build/contrib/streaming/hadoop-streaming-\$\{version\}.jar -Dmapred.map.gpushare=0 -D mapred.reduce.tasks=0 -file ~/cuda_rt/requester0 -file streaming/c/kmeans/grep/mapper -mapper streaming/c/kmeans/grep/mapper -numReduceTasks 0 -input $input -output grepOut -cmdenv PATTERN0=${movieIds[0]} -cmdenv PATTERN1=${movieIds[1]} -cmdenv PATTERN2=${movieIds[2]} -cmdenv PATTERN3=${movieIds[3]} -cmdenv PATTERN4=${movieIds[4]} -cmdenv PATTERN5=${movieIds[5]} -cmdenv PATTERN6=${movieIds[6]} -cmdenv PATTERN7=${movieIds[7]} -cmdenv PATTERN8=${movieIds[8]} -cmdenv PATTERN9=${movieIds[9]} -cmdenv PATTERN10=${movieIds[10]} -cmdenv PATTERN11=${movieIds[11]} -cmdenv PATTERN12=${movieIds[12]} -cmdenv PATTERN13=${movieIds[13]} -cmdenv PATTERN14=${movieIds[14]} -cmdenv PATTERN15=${movieIds[15]}
	hadoop fs -get grepOut grepOut
	cd grepOut

	for (( i = 0; i < $numCentroids; i++ ))
	do
		numReviews=`grep -e ^${movieIds[$i]}: * | grep  -oh ',' |wc -l`
		numReviews=`expr $numReviews + 1`
		data=`grep -e ^${movieIds[$i]}: * | cut -d: -f3`
		echo "$i	${similarities[$i]} ${movieIds[$i]} $numReviews $data" >> $new_centroids_file
		
	done
	cd $hadoop
	rm -fr grepOut
	hadoop fs -rmr grepOut
  	cp $new_centroids_file $base_model
  fi
done

hadoop dfs -mv $out $output 2>&1
for (( i = 0; i < $iterations - 1; i++ )) 
do
  out="tmpout$i" 2>&1
  rm -r $hadoop/Results/$out 2>&1
  hadoop dfs -rmr $out
done
exit 0


