#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFSIZE 10000
#define numCentroids 16
/* grep main: search for regexp in files */
int main()
{
	int i;
	char *buf; //[BUFSIZ];
	size_t nBytes = BUFSIZE;
	int read = 0;
	char regexp[numCentroids][100];
  char envVar[numCentroids][12] = {"PATTERN0", "PATTERN1", "PATTERN2","PATTERN3","PATTERN4","PATTERN5","PATTERN6","PATTERN7","PATTERN8","PATTERN9","PATTERN10","PATTERN11","PATTERN12","PATTERN13","PATTERN14","PATTERN15"};
  
  for(i=0; i<numCentroids; i++) {
    char *pat = 	getenv(envVar[i]);
    strcpy(regexp[i], pat);
  }
	buf = (char*) malloc(nBytes*sizeof(char));
	
	#pragma mapreduce mapper key(line) value(one) keylength(100) sharedRO(regexp) kvpairs(1)
	while (  (read = getline(&buf, &nBytes, stdin)) != -1) {
		
    for(i=0; i<numCentroids; i++){
      int ptr =0;
      int found = 0;
      if(buf[ptr] == regexp[i][0]) {
        char *cmp = &buf[ptr];
        int count = 0;
        while(regexp[i][count] == cmp[count] && cmp[count] != '\n') {
          count++;
          if(regexp[i][count] == '\0' ) {
            if(cmp[count] ==':'){
              buf[read - 1] = '\0';
              //strcpy(line, &buf[0]);
              printf("%s\n", buf);
              found = 1;
              break;
            }
          }
        }
        if(found) {
          break;
        }
      }
    }
	}

	free(buf);
	return 0;	
}


