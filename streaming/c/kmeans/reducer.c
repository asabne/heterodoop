#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

int main() {
	int prevKey;
	int keyIn;
	int key;
	char similarityBuf[20];
  char movidIdBuf[20];
	prevKey = -1;
  float sumSimilarity = 0.0f;
  long numMovies = 0;
  float avgSimilarity = 0.0f;
  float similarity = 0.0f;
  long s = 0;
  float diff = 0.0f;
  float minDiff = 1.0f;
  long candidate = 0;
  float *allSimilarities;
  long *allMovieIds;
  long movieId;
  
  allSimilarities = (float*) malloc(INT_MAX*sizeof(float));
  if (allSimilarities==NULL) {
    printf("allSimilarities allocation fail\n");
    exit (1);
  }
  allMovieIds = (long*) malloc(INT_MAX*sizeof(long));
  if (allMovieIds==NULL) {
    printf("allMovieIds allocation fail\n");
    exit (1);
  }
  
  //printf("Alloc done\n");
	while(scanf("%d", &keyIn) == 1 && scanf("%*[ \n\t]%s %s", similarityBuf, movidIdBuf) == 2) {
  
		prevKey = keyIn;
    similarity = atof(similarityBuf);
    allSimilarities[numMovies] = similarity;
    //printf("atof done\n");
    //printf("%s\n", movidIdBuf);
    movieId = atol(movidIdBuf);
    //printf("atol done\n");
    allMovieIds[numMovies] = movieId;
    numMovies++;
    sumSimilarity += similarity;
    //printf("In loop %d\t%s %s\n", prevKey, similarityBuf, movidIdBuf);
	}
  
	if (numMovies > 0){
    avgSimilarity = sumSimilarity / (float) numMovies;
  }
  diff = 0.0f;
  minDiff = 1.0f;
  for (s = 0; s < numMovies; s++) {
    diff = fabsf(avgSimilarity - allSimilarities[s]);
    if (diff < minDiff) {
      minDiff = diff;
      candidate = s;
    }
  }

  printf("%ld\t%f\n", allMovieIds[candidate], allSimilarities[candidate]);
  free(allMovieIds);
  free(allSimilarities);	
	return 0;
}
