#! /bin/bash

outputDir=$1
node=$3
host=`hostname`
opFile=$outputDir/$host

#this is the same as in the data generation script (8192*2)
numMoviesPerFile=16384


d=0
if [ -z "$5" ]
	then
		d=0;
	else	
		d=$5;
fi

baseId=`expr $node \* $numMoviesPerFile \* $2`
for (( c=1; c<=$2; c++ ))
do

	if [ "$baseId" -eq 0 ]
	then
		python ./data_generator.py $baseId --initFile=$4 > $opFile	
	else
		python ./data_generator.py $baseId > $opFile 
	fi

	p=`expr $d + $c` 
	hadoop fs -put $opFile input/$host-$p
	baseId=`expr $baseId + $numMoviesPerFile`
done

rm -f $opFile
