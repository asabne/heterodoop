#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	
	char word[92];
	int count;
	int val;
	char prevWord[92];
	

	#pragma mapreduce combiner key(prevWord) value(count) keyin(word) valuein(val) keylength(92) vallength(1)
	{
		prevWord[0] = '\0';
		count = 0;
		int read;
		while( (read = scanf("%[^\t]%d\n", word, &val)) == 2) {
			//printf("KEY %s val %d read %d\n", word, val, read );
			if(strcmp(word, prevWord) == 0 ) {
				count += val;
			}
			else {
				if(prevWord[0] != '\0') {
					printf("%s\t%d\n", prevWord, count);
				}
				strcpy(prevWord, word);
				count = val;
			}
		}
		
		if(prevWord[0] != '\0') {
			printf("%s\t%d\n", prevWord, count);
		}
	}
		
	return 0;
}
