import sys
import string
import random
import argparse
import os
numLines = 128*1024
wordsPerLine=8;
wordLength=9;
minLength=4;
#numLines = 18000
#filename = 'temp'

def random_generator(size=6, chars=string.ascii_uppercase + string.ascii_lowercase +string.digits):
  return ''.join(random.choice(chars) for x in range(size))


parser = argparse.ArgumentParser()
parser.add_argument("filename", help="Provide the filepath for the final file",
                    type=str)
parser.add_argument('--lines', type=int,default=numLines, help='Number of lines to generate')
#parser.add_argument('--wordlength', type=int,default=wordLength, help='Number of lines to generate')

args = parser.parse_args()

filename = args.filename

#delete if the file exists already
if os.path.isfile(filename):
  os.remove(filename)

f = open(filename, 'a+')

numLines = args.lines
#wordLength = args.wordlength

for j in range(numLines):  
  inCurrentLine = 0;
  for k in range(wordsPerLine):  
    size = random.randint(minLength,wordLength)
    text = random_generator(size)
    if (inCurrentLine != 1 and (k == 4) and size == 9):
      text = "grepPattern"
      inCurrentLine= 1;
    f.write(text)
    if k < wordsPerLine - 1:
      f.write(" ")
  f.write("\n")
  
for i in range(4):
  f.seek(0)
  text = f.read()
  f.write(text)
f.close()


