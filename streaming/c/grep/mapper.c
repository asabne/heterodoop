#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFSIZE 100

/* grep main: search for regexp in files */
int main()
{
	int i;
	char *buf; //[BUFSIZ];
	size_t nBytes =BUFSIZE;
	int read = 0;
	char regexp[100];
	char *pat = 	getenv("PATTERN");
	//char *pat = "cvmpe";
	strcpy(regexp, pat);
	buf = (char*) malloc(nBytes*sizeof(char));
	
	//both key, val buffers must be defined outside the while loop
	int one;
	char line[100];
	
	
	#pragma mapreduce mapper key(line) value(one) keylength(92) sharedRO(regexp) kvpairs(1)
	while (  (read = getline(&buf, &nBytes, stdin)) != -1) {
		int ptr =0;
		one = 1;
		while(buf[ptr] != '\n') {
			//printf("here\n");
			int found = 0;
			if(buf[ptr] == regexp[0]) {
				char *cmp = &buf[ptr];
				int count = 1;
				while(regexp[count] == cmp[count] && cmp[count] != '\n') {
					count++;
					if(regexp[count] == '\0' ) {
						
						buf[read - 1] = '\0';
						strcpy(line, &buf[0]);
						printf("%s\t%d\n", line, one);
						found = 1;
						break;
					}
				}
			}
			if(found)
				break;
			ptr++;
		}
		
	}

	free(buf);
	return 0;	
}


