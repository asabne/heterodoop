#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double CND(double d)
{
    double       A1 = 0.31938153;
    double       A2 = -0.356563782;
    double       A3 = 1.781477937;
    double       A4 = -1.821255978;
    double       A5 = 1.330274429;
    double RSQRT2PI = 0.39894228040143267793994605993438;

    double  K = 1.0 / (1.0 + 0.2316419 * fabs(d));

    double    cnd = RSQRT2PI * exp(- 0.5 * d * d) *
          (K * (A1 + K * (A2 + K * (A3 + K * (A4 + K * A5)))));

    if (d > 0)
        cnd = 1.0 - cnd;

    return cnd;
}


int main() {
	char *line;
	
	int read;
	int one;
	float callResult;
	float putResult;
	double      R = 0.02f;
	double    V = 0.30f;
	size_t nbytes = 100000;
	line = (char*) malloc(nbytes*sizeof(char));
	
	
	#pragma mapreduce mapper key(callResult) value(putResult) keylength(1) vallength(1) sharedRO(R, V) kvpairs(1)
	{
		while ( (read = getline(&line, &nbytes, stdin)) != -1) {
			char temp[30];
			float stockPrice;
			float optionStrike;
			float optionYears;
			int ptr = 0;
			double S;
			double X;
			double T;
			double sqrtT;
			double d1;
			double d2;
			double CNDD1;
			double CNDD2;
			double expRT;
			int i;
			
			int count = 0;
			
			while(line[count] != ',' && line[count] != '\n') {
				temp[ptr++] = line[count++];
			}
			temp[ptr] = '\0';
			
			stockPrice = atof(temp);
			//printf("stockprice %f\n", stockPrice);
			count++;
			ptr = 0;
			while(line[count] != ',' && line[count] != '\n') {
				temp[ptr++] = line[count++];
			}
			temp[ptr] = '\0';
			
			optionStrike = atof(temp);
			
			count++;
			ptr = 0;
			while(line[count] != ',' && line[count] != '\n') {
				temp[ptr++] = line[count++];
			}
			temp[ptr] = '\0';
			
			optionYears = atof(temp);
			
			for(i=0; i<128; i++) {
        S = stockPrice;
        X = optionStrike;
        T = optionYears;

        sqrtT = sqrt(T);
        d1 = (log(S / X) + (R + 0.5 * V * V) * T) / (V * sqrtT);
        d2 = d1 - V * sqrtT;
        CNDD1 = CND(d1);
        CNDD2 = CND(d2);
        //Calculate Call and Put simultaneously
        expRT = exp(- R * T);
        callResult   = (float)(S * CNDD1 - X * expRT * CNDD2);
        putResult    = (float)(X * expRT * (1.0 - CNDD2) - S * (1.0 - CNDD1));
      }
              
      //printf("stockprice %f optionStrike %f optionYears %f\n", stockPrice, optionStrike, optionYears);
      printf("%f\t%f\n", callResult, putResult );
		}
	}
  free(line);
	return 0;
	
}
