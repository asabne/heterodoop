#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NUM_LINES (1024*32)

float RandFloat(float low, float high)
{
    float t = (float)rand() / (float)RAND_MAX;
    return (1.0f - t) * low + t * high;
}

int main() {
	int i;
    //Generate options set
    for (i = 0; i < NUM_LINES; i++)
    {
        printf("%.6f,%.6f,%.6f\n", RandFloat(5.0f, 30.0f), RandFloat(1.0f, 100.0f), RandFloat(0.25f, 10.0f));
    }
    
}
