import sys
from random import random
import argparse
numLines = 4*1024*1024

#for maverick
#numLines = 12*1024*1024

parser = argparse.ArgumentParser()
parser.add_argument('--lines', type=int,default=numLines, help='Number of lines to generate')
args = parser.parse_args()

length = 16;


numLines = args.lines
for j in range(numLines):  
  sys.stdout.write(str(format(random(), '.4f')))
  sys.stdout.write(",")
  sys.stdout.write(str(format(random(), '.4f')))
  sys.stdout.write(",")
  sys.stdout.write(str(format(random(), '.4f')))
  sys.stdout.write("\n")
