#! /bin/bash

if [ $# -ne 3 ]
then
  echo "Usage: ./slave_placer.sh [filepath] [startFileCount] [endFileCount]"
  exit 1
fi

locationToPlace="input"
filearray=( "$1"/* )


for (( c=$2; c<$3; c++ ))
do
	#echo "${filearray[$c]}"
	#echo "hadoop fs -put $locationToPlace ${filearray[$c]}"
	filename=`basename ${filearray[$c]}`
	hadoop fs -put ${filearray[$c]} $locationToPlace/$filename
done
