#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	int prevKey;
	int keyIn;
	int valIn;
	int sum;
	int key;
	#pragma mapreduce combiner key(prevKey) value(sum) keyin(keyIn) valuein(valIn) keylength(1) vallength(1)
	{
		sum = 0;
		prevKey = -1;
		while(scanf("%d", &keyIn) == 1 && scanf("%*[ \n\t]%d", &valIn) == 1) {
			if(prevKey == keyIn) {
				sum += valIn;
			} else {
				if(prevKey != -1)
					printf("%d\t%d\n", prevKey, sum);
				prevKey = keyIn;
				sum = valIn;
			}
		}
		if(prevKey != -1) {
			printf("%d\t%d\n", prevKey, sum);
		}
	}
		
	return 0;
}
