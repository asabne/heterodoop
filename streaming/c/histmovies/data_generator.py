import sys
from random import randint
import argparse
numLines = 1024*1024*2 - 36000
#numLines = 18000

parser = argparse.ArgumentParser()
parser.add_argument("rlength", help="enter 1 if the length should be random between 1 and 512",
                    type=int)
parser.add_argument('--lines', type=int,default=numLines, help='Number of lines to generate')
args = parser.parse_args()

length = 64;


numLines = args.lines
for j in range(numLines):  
  if args.rlength == 1:
    length = randint(1,length);
  review = randint(1,4)
  for i in range(length):
    if review == 1:
      revToUse = randint(1,2);
    else:
      revToUse = randint(review, review+1);
    sys.stdout.write(str(revToUse))
    sys.stdout.write(",")
  sys.stdout.write("\n")
