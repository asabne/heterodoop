#include <stdio.h>
#include <stdlib.h>


int main() {
	char *line;
	
	int read;
	int one;
	int bin;
	size_t nbytes = 100000;
	line = (char*) malloc(nbytes*sizeof(char));
	
	#pragma mapreduce mapper key(bin) value(one) keylength(1) vallength(1) kvpairs(8)
	{
    while ( (read = getline(&line, &nbytes, stdin)) != -1) {
      //printf("%s", line);
      int count = 0;
      char rating[5];
      int ratingSum = 0;
      int numKeys = 0;
      float aveRating;
      bin = 1;
      float binMax = 1.5; 
      one = 1;
      while ( count < read - 1 )
      {
        int start = 0;
        while(line[count] != ',' && line[count] != '\0' && line[count] != '\n' ) {
          rating[start] = line[count];
          start++;
          count++;
        }
        rating[start] = '\0';
        ratingSum += atoi(rating);
        numKeys++;
        count++;
      }
      aveRating = ((float)ratingSum)/numKeys;
      while(aveRating > binMax) {
        bin++;
        binMax += 0.5;
      }
      printf("%d\t%d\n", bin, one);
    }
  }
  
  free(line);
	return 0;
	
}
