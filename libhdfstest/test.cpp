#include "jni.h"
#include "hdfs.h"
#include <sys/time.h>

double timer()
{
	struct timeval time;
	double _ret_val_0;
	gettimeofday(( & time), 0);
	_ret_val_0=(time.tv_sec+(time.tv_usec/1000000.0));
	return _ret_val_0;
}


int
main(int argc, char **argv)
{
	double start, start2;
	start = timer();
	FILE *fp;
	int sz;
	fp = fopen ("../d1", "r" ) ;
	if(fp == NULL) {
		printf("File could not be opened. Exiting\n");
		exit(1);
	}
	fseek(fp, 0L, SEEK_END);
	sz = ftell(fp); 
	fseek(fp, 0L, SEEK_SET);
	//cudaMallocHost((void**)ip, sz);
	//cudaHostAlloc(ip, sz, cudaHostAllocDefault);
	char *ip = (char*) malloc(sz);
	fread(ip, 1, sz, fp);
	fclose (fp); 
	printf("Read time %lf\n", timer() - start);
	
	
	start = timer();
	
  hdfsFS fs = hdfsConnect("default", 0);
  if (!fs) {
        fprintf(stderr, "Cannot connect to HDFS.\n");
        exit(-1);
	}
	printf("Connection time %lf\n", timer() - start);
	/*
	char* fileName = "/user/asabne/input/d1";

	int exists = hdfsExists(fs, fileName);

	if (exists > -1) {
			fprintf(stdout, "File %s exists!\n", fileName);
	} else {
		fprintf(stdout, "File %s does not exist!\n", fileName);
	}
	
	
  const char* writePath = "/user/asabne/output/testfile.txt";
  hdfsFile writeFile = hdfsOpenFile(fs, writePath, O_WRONLY|O_CREAT, 0, 0, 0);
  if(!writeFile) {
    fprintf(stderr, "Failed to open %s for writing!\n", writePath);
    exit(-1);
  }
  const char* buffer = "Hello, World!";
  tSize num_written_bytes = hdfsWrite(fs, writeFile, (void*)buffer, strlen(buffer)+1);
  if (hdfsFlush(fs, writeFile)) {
    fprintf(stderr, "Failed to 'flush' %s\n", writePath);
    exit(-1);
  }
  hdfsCloseFile(fs, writeFile);
  */ 
  
  const char* readPath = "/user/asabne/input/d1";
  hdfsFile readFile = hdfsOpenFile(fs, readPath, O_RDONLY, 0, 0, 0);
  if(!readFile) {
    fprintf(stderr, "Failed to open %s for writing!\n", readPath);
    exit(-1);
  }
  
  start2 = timer();
  hdfsSeek(fs, readFile, SEEK_END); 
  sz = hdfsTell(fs, readFile);
  hdfsSeek(fs, readFile, SEEK_SET);
  hdfsRead(fs, readFile, ip, sz);
  printf("File Read time %lf\n", timer() - start2);
  
	hdfsCloseFile(fs, readFile);
  printf("Read time %lf\n", timer() - start);
}
