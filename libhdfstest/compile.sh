export JAVA_HOME="/usr/local/jdk1.7.0_25/"
export HADOOP_SRC_HOME="/home/asabne/idea/projects/hadoop-1.2.1/"
export HADOOP_HOME="/home/asabne/idea/projects/hadoop-1.2.1/" # CDH
export CXXFLAGS="-O2 -g -Wall -I$HADOOP_SRC_HOME/src/c++/libhdfs/ -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/"
export LDFLAGS="-L$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/ -lhdfs -L$JAVA_HOME/jre/lib/amd64/server/ -ljvm"
export LD_LIBRARY_PATH="$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/:$JAVA_HOME/jre/lib/amd64/server/"

g++ test.cpp $CXXFLAGS $LDFLAGS
