/*
** semrm.c -- removes a semaphore
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <signal.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/shm.h>
#define KEY 30
#define SHM_SIZE 16

volatile int driverDone = 0;

void sigusr1_handler(int sig)
{
	driverDone= 1;
}


struct my_msgbuf {
    long mtype;
    pid_t pid;
		char inputPath[500];
		char outputPath[500];
		int numReducers;
};

//format to call: ./requester num_reducers inputPath outputPath
// e./g ./requester 1 /user/asabne/input/d1 ./
int main(int argc, char **argv)
{
	key_t key;
	int semid;
	struct sembuf sb;
	struct my_msgbuf buf;
	int msqid;
	int shmid;
	char *pidBuffer;

	sb.sem_num = 0;
	sb.sem_op = -1;  /* set to allocate resource */
	sb.sem_flg = SEM_UNDO;
	
	printf("Requesting\n");
	if(argc < 4) {
		printf("Incorrect number of arguments specified, exiting\n");
		exit(1);
	}
	
	struct sigaction sa;

	sa.sa_handler = sigusr1_handler;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);

	if (sigaction(SIGUSR1, &sa, NULL) == -1) {
			perror("sigaction");
			exit(1);
	}	

	/*
	if ((key = ftok("driver.c", 'J')) == -1) {
		perror("ftok");
		exit(1);
	}
	*/
	
	key = KEY;
	/* grab the semaphore set created by seminit.c: */
	if ((semid = semget(key, 1, 0)) == -1) {
		perror("semget");
		exit(1);
	}

	

	//printf("Press return to lock: ");
	//getchar();
	printf("Requester PID: %d - Trying to lock\n", getpid());

	if (semop(semid, &sb, 1) == -1) {
		perror("semop");
		exit(1);
	}

	printf("Locked.\n");
	
	//Send mesg
	if ((msqid = msgget(key, 0644)) == -1) { /* connect to the queue */
		perror("msgget");
		exit(1);
	}
	
	buf.mtype = 1; 
	buf.pid = getpid();
	//printf("Requesting pid %d\n", buf.pid);
	sprintf(buf.inputPath, argv[2]);
	sprintf(buf.outputPath, argv[3]);
	buf.numReducers = atoi(argv[1]);
	
	if (msgsnd(msqid, &buf, sizeof(struct my_msgbuf) - sizeof(long), 0) == -1) 
					perror("msgsnd");
	
	//get PID of the driver
	pid_t pid;
	//Create a shared memory segment
	if ((shmid = shmget(key, SHM_SIZE, 0644)) == -1) {
			perror("shmget");
			exit(1);
	}

	/* attach to the segment to get a pointer to it: */
	pidBuffer = shmat(shmid, (void *)0, 0);
	if (pidBuffer == (char *)(-1)) {
			perror("shmat");
			exit(1);
	}

	printf("Data written to the shared segment %s\n", pidBuffer);
	pid = atoi(pidBuffer);
	/* detach from the segment: */
	if (shmdt(pidBuffer) == -1) {
			perror("shmdt");
			exit(1);
	}
	
	//pid_t pid = 28657;
	printf("Driver process pid us %d\n", pid);
	/*
	//Ensure that the driver is asleep
	int status;
	pid_t returnedPid = waitpid(pid, &status, WNOHANG);
	if(returnedPid != pid) {
		printf("Driver rocess is not running anymore. Pid found is %d, actual is %d\n", returnedPid, pid);
		exit(1);
	}
	while(WIFSTOPPED(status)== 0) {
			if( (returnedPid = waitpid(pid, &status, WNOHANG) ) != pid) {
				printf("Driver rocess is not running anymore. Pid found is %d\n", returnedPid);
			}
	}
	*/
	//Wake up the driver
	kill(pid, SIGUSR1);
	//killall("driver", SIGCONT);

	//wait for driver to send a sigusr1
	while(!driverDone) {
		usleep(10000);
	}
	//sleep yourself
	//kill(getpid(), SIGSTOP);
	
	printf("Returning lock\n");
	//getchar();
	
	//Return the lock
	sb.sem_op = 1; /* free resource */
	if (semop(semid, &sb, 1) == -1) {
		perror("semop");
		exit(1);
	}

	printf("Unlocked\n");

	return 0;
}
