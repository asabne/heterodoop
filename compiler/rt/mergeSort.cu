/*
 * Copyright 1993-2013 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */




/*
 * Based on "Designing efficient sorting algorithms for manycore GPUs"
 * by Nadathur Satish, Mark Harris, and Michael Garland
 * http://mgarland.org/files/papers/gpusort-ipdps09.pdf
 *
 * Victor Podlozhnyuk 09/24/2009
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "mr.h"
typedef unsigned int uint;
#define SHARED_SIZE_LIMIT 1024U
#define     SAMPLE_STRIDE (1024)


////////////////////////////////////////////////////////////////////////////////
// Bitonic sort implementation
////////////////////////////////////////////////////////////////////////////////

__global__ void sort_kernel_ballot(int *data, int k, int j, int SIZE, char *key, int keyLength, int keyFactor) {
	extern __shared__ unsigned int warpData[];
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int totalThreads = SORTBLOCKS*SORTBLOCKSIZE;
	int warpDataBlock = SORTBLOCKSIZE*keyFactor;

	unsigned int *greater = warpData;
	unsigned int *smaller  = warpData + warpDataBlock;
	unsigned int *ijGreater = warpData + warpDataBlock*2;

	int warpStartAdd = threadIdx.x & 0xFFFFFFE0;
	int laneId = 31 - threadIdx.x & 0x1f;

	int loopIter = 0;
	while(i<SIZE) {
		int ij=i^j;
		int cond = (ij)>i;
		ijGreater[threadIdx.x] = ij>i;

		int startI = i & 0xFFFFFFE0;
		//for loop : operates over 32 comparisons that need to be made
		for(int m=0; m <32; m++) {
			int index = warpStartAdd + m;
			if(ijGreater[index]) {
				int newI = blockIdx.x*blockDim.x + totalThreads*loopIter + index;
				int newIJ = newI ^ j;
				int count = 0;
				while(count < keyFactor) {
					greater[index + SORTBLOCKSIZE*count ] =  __ballot((unsigned char)key[data[newI]*keyLength + laneId + count*32] > (unsigned char)key[data[newIJ]*keyLength + laneId + count*32] && laneId < keyLength - count*32);
			
					smaller[index + SORTBLOCKSIZE*count ] =  __ballot((unsigned char)key[data[newI]*keyLength + laneId + count*32] < (unsigned char)key[data[newIJ]*keyLength + laneId + count*32] && laneId < keyLength - count*32);
					count++;
				}
			}
		
		}
		if (cond) {
			if ((i&k)==0 ) {
				int count = 0;
				while(count < keyFactor) {
					if(greater[threadIdx.x + count*SORTBLOCKSIZE] > smaller[threadIdx.x + count*SORTBLOCKSIZE]) {
						//printf("Greater %d smaller %d greater ballot %x nequal ballot %x\n", i, ij, greater, nequal);
						//if((unsigned char) key[data[i]*keyLength] == 255)
							//printf("Greater %s smaller %s\n", &key[data[i]*keyLength], &key[data[ij]*keyLength] );
						int tmp = data[i];
						data[i] = data[ij];
						data[ij] = tmp;
						break;
						//printf("Greater %s smaller %s\n", key[data[i]*keyLength], key[data[ij]*keyLength] );
					} else if(greater[threadIdx.x + count*SORTBLOCKSIZE] == smaller[threadIdx.x + count*SORTBLOCKSIZE]) { // This case happens if both greater and smaller are 0
						count++;
						continue;
					} else {
						break;
					}
				}
			}
			else {
				int count = 0;
				while(count < keyFactor) {
					if(greater[threadIdx.x + count*SORTBLOCKSIZE] > smaller[threadIdx.x + count*SORTBLOCKSIZE]) {
						break;
					}else if(greater[threadIdx.x + count*SORTBLOCKSIZE] == smaller[threadIdx.x + count*SORTBLOCKSIZE]) { // This case happens if both greater and smaller are 0
						count++;
						continue;
					} else {
						int tmp = data[i];
						data[i] = data[ij];
						data[ij] = tmp;
						break;
					}
				}
			}
		}
		i+= totalThreads;
		loopIter++;
	}
}


__global__ void sort_kernel_plain(int *data, int k, int j, int SIZE, char *key, int keyLength) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int totalThreads = SORTBLOCKS*SORTBLOCKSIZE;


	while(i<SIZE) {
		int ij=i^j;
		//if ((ij)>i) {
			if ((ij)>i) {
				if ((i&k)==0 ) {
					int count = 0;
					while(count < keyLength) {
						if(( (unsigned char) key[data[i]*keyLength + count] > (unsigned char) key[data[ij]*keyLength + count])) {
							int tmp = data[i];
							data[i] = data[ij];
							data[ij] = tmp;
							break;
						} else if ((key[data[i]*keyLength + count] == key[data[ij]*keyLength + count])) {
							if((unsigned char) key[data[i]*keyLength + count] == 255)
								break;
							count++;
						} else {
							break;
						}
					}
				}
				else {
					int count = 0;
					while(count < keyLength) {
						if(( (unsigned char) key[data[i]*keyLength + count] < (unsigned char) key[data[ij]*keyLength + count])) {
							int tmp = data[i];
							data[i] = data[ij];
							data[ij] = tmp;
							break;
						} else if ((key[data[i]*keyLength + count] == key[data[ij]*keyLength + count])) {
							if((unsigned char) key[data[i]*keyLength + count] == 255)
								break;
							count++;
						} else {
							break;
						}
					}
				}
			}

		//}
		i+= totalThreads;
	}
}



__global__ void sort_kernel_plain(int *data, int k, int j, int SIZE, int *key, int keyLength) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int totalThreads = SORTBLOCKS*SORTBLOCKSIZE;


	while(i<SIZE) {
		int ij=i^j;
		//if ((ij)>i) {
			if ((ij)>i) {
				if ((i&k)==0 ) {
					int count = 0;
					if((key[data[i]] > key[data[ij]])) {
						int tmp = data[i];
						data[i] = data[ij];
						data[ij] = tmp;
					} 
				}
				else {
					int count = 0;
					if((key[data[i]] < key[data[ij]])) {
						int tmp = data[i];
						data[i] = data[ij];
						data[ij] = tmp;
					} 
				}
			}

		//}
		i+= totalThreads;
	}
}


__global__ void sort_kernel_plain(int *data, int k, int j, int SIZE, long *key, int keyLength) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int totalThreads = SORTBLOCKS*SORTBLOCKSIZE;


	while(i<SIZE) {
		int ij=i^j;
		//if ((ij)>i) {
			if ((ij)>i) {
				if ((i&k)==0 ) {
					int count = 0;
					if((key[data[i]] > key[data[ij]])) {
						int tmp = data[i];
						data[i] = data[ij];
						data[ij] = tmp;
					} 
				}
				else {
					int count = 0;
					if((key[data[i]] < key[data[ij]])) {
						int tmp = data[i];
						data[i] = data[ij];
						data[ij] = tmp;
					} 
				}
			}

		//}
		i+= totalThreads;
	}
}

void bitonic_sort(int *index, char *key, int keyLength, int SIZE) {

	double sortBegin = timer();
	int j,k;
	SIZE = (int) pow( (float)2, (int) ceil(log(SIZE)/log(2)));
	printf("Bitonic sort of size %d\n", SIZE);
	for (k=2; k<=SIZE; k=2*k) {
		for (j=k>>1; j>0; j=j>>1) {
		  //sort_kernel<<<SIZE/256 + 1, 256>>>(index, k, j, SIZE, key, keyLength);
			dim3 grid(SORTBLOCKS, 1 , 1);
			dim3 block(SORTBLOCKSIZE, 1, 1);

			int keyFactor = 1;
			while(keyFactor*32 < keyLength)
				keyFactor++;
			/*
			int count = 0;
			double begin = timer();
			for(int i=0 ; i<SIZE; i++) {
				int ij = i^j;
				if(ij > i) {
					count++;
				}
			}
			printf("SIZE %d count %d time %lf\n", SIZE, count, timer() - begin);			
			*/
			int warpDataBlock = SORTBLOCKSIZE*keyFactor;
			sort_kernel_ballot<<<grid, block, warpDataBlock*3*sizeof(int), 0>>>(index, k, j, SIZE, key, keyLength, keyFactor);
			//sort_kernel_plain<<<grid, block, 0, 0>>>(index, k, j, SIZE, key, keyLength);			
			//cudaDeviceSynchronize();
			/*
			cudaError_t err = cudaGetLastError();
			if (err != cudaSuccess)
			printf("Error in Sort at k %d, j %d: %s\n", k , j, cudaGetErrorString(err));*/
			//printf("sort size %d current k %d j %d\n", SIZE, k, j);
		}
	}
	checkCudaErrors(cudaDeviceSynchronize());
	/*
	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess)
	  printf("Error in Sort at k %d, j %d: %s\n", k , j, cudaGetErrorString(err));
	*/
	printf("Sort time %lf\n", timer() - sortBegin);

}


void bitonic_sort(int *index, int *key, int keyLength, int SIZE) {

	int j,k;
	SIZE = (int) pow( (float)2, (int) ceil(log(SIZE)/log(2)));
	printf("Bitonic sort of size %d\n", SIZE);
	for (k=2; k<=SIZE; k=2*k) {
		for (j=k>>1; j>0; j=j>>1) {
		  //sort_kernel<<<SIZE/256 + 1, 256>>>(index, k, j, SIZE, key, keyLength);
			dim3 grid(SORTBLOCKS, 1 , 1);
			dim3 block(SORTBLOCKSIZE, 1, 1);

			int keyFactor = 1;
			while(keyFactor*32 < keyLength)
				keyFactor++;

			int warpDataBlock = SORTBLOCKSIZE*keyFactor;
			//sort_kernel<<<grid, block, warpDataBlock*4*sizeof(int), 0>>>(index, k, j, SIZE, key, keyLength);
			sort_kernel_plain<<<grid, block, 0, 0>>>(index, k, j, SIZE, key, keyLength);
			checkCudaErrors(cudaDeviceSynchronize());
			/*			
			cudaError_t err = cudaGetLastError();
			if (err != cudaSuccess)
			  printf("Error in Sort at k %d, j %d: %s\n", k , j, cudaGetErrorString(err));			
			*/
		}
	}
}


void bitonic_sort(int *index, long *key, int keyLength, int SIZE) {

	int j,k;
	SIZE = (int) pow( (float)2, (int) ceil(log(SIZE)/log(2)));
	printf("Bitonic sort of size %d\n", SIZE);
	for (k=2; k<=SIZE; k=2*k) {
		for (j=k>>1; j>0; j=j>>1) {
		  //sort_kernel<<<SIZE/256 + 1, 256>>>(index, k, j, SIZE, key, keyLength);
			dim3 grid(SORTBLOCKS, 1 , 1);
			dim3 block(SORTBLOCKSIZE, 1, 1);

			int keyFactor = 1;
			while(keyFactor*32 < keyLength)
				keyFactor++;

			int warpDataBlock = SORTBLOCKSIZE*keyFactor;
			//sort_kernel<<<grid, block, warpDataBlock*4*sizeof(int), 0>>>(index, k, j, SIZE, key, keyLength);
			sort_kernel_plain<<<grid, block, 0, 0>>>(index, k, j, SIZE, key, keyLength);
			checkCudaErrors(cudaDeviceSynchronize());
			/*			
			cudaError_t err = cudaGetLastError();
			if (err != cudaSuccess)
			  printf("Error in Sort at k %d, j %d: %s\n", k , j, cudaGetErrorString(err));			
			*/
		}
	}
}


////////////////////////////////////////////////////////////////////////////////
// Validate sorted keys array (check for integrity and proper order)
////////////////////////////////////////////////////////////////////////////////
int validateSortedKeys(
    int *resKey,
    char *srcKey,
    int batchSize,
    int arrayLength,
    int numValues,
    int sortDir,
	int keyLength
)
{
	int j;
	FILE *fp = fopen("op.txt", "w");
	if(!fp) {
		printf("Output file could not be opened\n");
	}
	for(j=0; j < arrayLength-1; j++) {
		char *str1 = &srcKey[keyLength*resKey[j]];
		char *str2 = &srcKey[keyLength*resKey[j+1]];
		int greater = 0;
		for(int i=0; i < keyLength; i++) {
			if((unsigned char)str1[i] < (unsigned char)str2[i]) {
				break;
			} else if ((unsigned char)str1[i] > (unsigned char)str2[i]){
				greater = 1;
				break;
			} else if((unsigned char)str1[i] == (unsigned char)str2[i] == '\0') {
				break;
			}
		}
		fprintf(fp, "%s\n", &srcKey[keyLength*resKey[j]]);
		if(greater) {
			//printf("Error at %s and %s\n", &srcKey[keyLength*resKey[j]], &srcKey[keyLength*resKey[j+1]]);
			printf("Error in validation\n");
			return -1;
		}
	}
	fprintf(fp, "%s\n", &srcKey[keyLength*resKey[j]]);
	fclose(fp);
	printf("Verification Successful\n");
	return 1;
    
}


int validateSortedKeys(
    int *resKey,
    int *srcKey,
    int batchSize,
    int arrayLength,
    int numValues,
    int sortDir
)
{
    int *srcHist;
    int *resHist;
	FILE *fp = fopen("op.txt", "w");
	if(!fp) {
		printf("Output file could not be opened\n");
	}
    if (arrayLength < 2)
    {
        printf("validateSortedKeys(): arrays too short, exiting...\n");
        return 1;
    }
	for(int i=0; i < arrayLength; i++) {
		//fprintf(fp, "%d\n", resHist[i]);
	}
	fclose(fp);
    printf("...inspecting keys array: ");
    srcHist = (int *)malloc(numValues * sizeof(int));
    resHist = (int *)malloc(numValues * sizeof(int));

    int flag = 1;

    for (int j = 0; j < batchSize; j++, srcKey += arrayLength, resKey += arrayLength)
    {
        //Build histograms for keys arrays
        memset(srcHist, 0, numValues * sizeof(int));
        memset(resHist, 0, numValues * sizeof(int));

        for (int i = 0; i < arrayLength; i++)
        {
            if ((srcKey[i] < numValues) && (resKey[i] < numValues))
            {
                srcHist[srcKey[i]]++;
                resHist[resKey[i]]++;
            }
            else
            {
                fprintf(stderr, "***Set %u source/result key arrays are not limited properly***\n", j);
                flag = 0;
                goto brk;
            }
        }

        //Compare the histograms
        for (int i = 0; i < numValues; i++)
            if (srcHist[i] != resHist[i])
            {
                fprintf(stderr, "***Set %u source/result keys histograms do not match***\n", j);
                flag = 0;
                goto brk;
            }

        //Finally check the ordering
        for (int i = 0; i < arrayLength - 1; i++)
            if ((sortDir && (resKey[i] > resKey[i + 1])) || (!sortDir && (resKey[i] < resKey[i + 1])))
            {
                fprintf(stderr, "***Set %u result key array is not ordered properly***\n", j);
                flag = 0;
                goto brk;
            }
    }

brk:
    free(resHist);
    free(srcHist);

    if (flag) printf("OK\n");

    return flag;
}


int validateSortedKeys(
    long *resKey,
    long *srcKey,
    int batchSize,
    int arrayLength,
    int numValues,
    int sortDir
)
{
    int *srcHist;
    int *resHist;
	FILE *fp = fopen("op.txt", "w");
	if(!fp) {
		printf("Output file could not be opened\n");
	}
    if (arrayLength < 2)
    {
        printf("validateSortedKeys(): arrays too short, exiting...\n");
        return 1;
    }
	for(int i=0; i < arrayLength; i++) {
		//fprintf(fp, "%d\n", resHist[i]);
	}
	fclose(fp);
    printf("...inspecting keys array: ");
    srcHist = (int *)malloc(numValues * sizeof(int));
    resHist = (int *)malloc(numValues * sizeof(int));

    int flag = 1;

    for (int j = 0; j < batchSize; j++, srcKey += arrayLength, resKey += arrayLength)
    {
        //Build histograms for keys arrays
        memset(srcHist, 0, numValues * sizeof(int));
        memset(resHist, 0, numValues * sizeof(int));

        for (int i = 0; i < arrayLength; i++)
        {
            if ((srcKey[i] < numValues) && (resKey[i] < numValues))
            {
                srcHist[srcKey[i]]++;
                resHist[resKey[i]]++;
            }
            else
            {
                fprintf(stderr, "***Set %u source/result key arrays are not limited properly***\n", j);
                flag = 0;
                goto brk;
            }
        }

        //Compare the histograms
        for (int i = 0; i < numValues; i++)
            if (srcHist[i] != resHist[i])
            {
                fprintf(stderr, "***Set %u source/result keys histograms do not match***\n", j);
                flag = 0;
                goto brk;
            }

        //Finally check the ordering
        for (int i = 0; i < arrayLength - 1; i++)
            if ((sortDir && (resKey[i] > resKey[i + 1])) || (!sortDir && (resKey[i] < resKey[i + 1])))
            {
                fprintf(stderr, "***Set %u result key array is not ordered properly***\n", j);
                flag = 0;
                goto brk;
            }
    }

brk:
    free(resHist);
    free(srcHist);

    if (flag) printf("OK\n");

    return flag;
}

////////////////////////////////////////////////////////////////////////////////
// Helper functions
////////////////////////////////////////////////////////////////////////////////
static inline __host__ __device__ int iDivUp(int a, int b) {
    return ((a % b) == 0) ? (a / b) : (a / b + 1);
}

static inline __host__ __device__ int getSampleCount(int dividend) {
    return iDivUp(dividend, SAMPLE_STRIDE);
}

#define W (sizeof(int) * 8)
static inline __device__ int nextPowerOfTwo(int x) {
    /*
        --x;
        x |= x >> 1;
        x |= x >> 2;
        x |= x >> 4;
        x |= x >> 8;
        x |= x >> 16;
        return ++x;
    */
    return 1U << (W - __clz(x - 1));
}


static __forceinline__ __device__ int binarySearchInclusiveBallotOld(int sortDir, int valIndex,  char *keySrc, int *data, int L, int stride, int keyLength, unsigned int *warpData, int keyFactor) {
	unsigned int *greaterArray = warpData;
	unsigned int *smallerArray  = warpData + keyFactor*blockDim.x;
	unsigned int *newPosArray  = warpData + 2*keyFactor*blockDim.x;
	unsigned int *valIndexArray  = warpData + 3*keyFactor*blockDim.x;
	/*
	__shared__ int newPosArray[BS_BLOCKSIZE];
	__shared__ int valIndexArray[BS_BLOCKSIZE];
	__shared__ unsigned int greaterArray[BS_BLOCKSIZE];
	__shared__ unsigned int smallerArray[BS_BLOCKSIZE];
	
	int keyFactor = 1;
	*/
    if (L == 0) {
        return 0;
    }

    int pos = 0;
	int warpStartAdd = threadIdx.x & 0xFFFFFFE0;
	int laneId = 31 - threadIdx.x & 0x1f;
    for (; stride > 0; stride >>= 1) {
        int newPos = umin(pos + stride, L);

		newPosArray[threadIdx.x] = data[newPos - 1];
		valIndexArray[threadIdx.x] = valIndex;
		for(int m=0; m <32; m++) {
			int index = warpStartAdd + m;
			int count = 0;
			int newPosFromShared = newPosArray[index];
			int valIndexFromShared = valIndexArray[index];
			while(count < keyFactor) {
				greaterArray[index + blockDim.x*count] =  __ballot((unsigned char)keySrc[newPosFromShared*keyLength + laneId + count*32] > (unsigned char)keySrc[valIndexFromShared*keyLength + laneId + count*32] && laneId < keyLength - count*32);
		
				smallerArray[index + blockDim.x*count] =  __ballot((unsigned char)keySrc[newPosFromShared*keyLength + laneId + count*32] < (unsigned char)keySrc[valIndexFromShared*keyLength + laneId + count*32] && laneId < keyLength - count*32);
				/*
				__syncthreads();
				if(threadIdx.x == 31 && blockIdx.x == 0 ) {
					printf("str1 %s str2 %s SMg %x SMs %x tid %d keylength %d index %d warpstartadd %x newPosFromShared %d valIndexFromShared %d\n", &keySrc[newPosFromShared*keyLength], &keySrc[valIndexFromShared*keyLength], greaterArray[index + BS_BLOCKSIZE*count], smallerArray[index + BS_BLOCKSIZE*count], threadIdx.x, keyLength, index, warpStartAdd, newPosFromShared, valIndexFromShared );
				}
				*/
				count++;
			}
		
		}
		/*
		int greater2 = (greaterArray[threadIdx.x] > smallerArray[threadIdx.x]) ? 1 : 0;
		int equal2 = (greaterArray[threadIdx.x] == smallerArray[threadIdx.x])? 1 : 0;
		*/
		int greater2=0;
		int smaller2=0;
		int equal2=0;
		int count = 0;
		while(count < keyFactor) {
			if(greaterArray[threadIdx.x] > smallerArray[threadIdx.x]) {
				greater2 = 1;
				break;
			} else if (greaterArray[threadIdx.x] == smallerArray[threadIdx.x]) {
				count++;
				continue;
			} else {
				smaller2 = 1;
				break;
			}
		}
		if(!greater2 && !smaller2) 		
			equal2= 1;
		if ( (sortDir && (smaller2 || equal2))  || (!sortDir && (greater2 || equal2)) ) {
            pos = newPos;
        }
    }

    return pos;
}

static __forceinline__ __device__ int binarySearchInclusiveBallot(int sortDir, int valIndex,  char *keySrc, int *data, int L, int stride, int keyLength, unsigned int *warpData, int keyFactor)
{


	//maximum block size is 1024 --> at max 32 warps
	__shared__ int vals[32];
	__shared__ int news[32];


	unsigned int greaterReg[2];
	unsigned int smallerReg[2];
	unsigned int g,s;
    if (L == 0) {
        return 0;
    }

    int pos = 0;
	int realLane = threadIdx.x & 0x1f;
	int laneId = 31 - realLane;
    for (; stride > 0; stride >>= 1) {
        int newPos = umin(pos + stride, L);

		//#pragma unroll 4
		for(int m=0; m <32; m++) {
			int count = 0;
			if(realLane == m) {
				//if(threadIdx.x > 250 )
					//printf("In here for index %d m %d tid %d\n", index, m , threadIdx.x);
				vals[threadIdx.x/32] = valIndex;
				news[threadIdx.x/32] = data[newPos - 1];
			}
			//this is necessary
			//__syncthreads();
			__threadfence_block();

			int newPosFromShared = news[threadIdx.x/32];
			int valIndexFromShared = vals[threadIdx.x/32];

			while(count < keyFactor) {
				g =  __ballot((unsigned char)keySrc[newPosFromShared*keyLength + laneId + count*32] > (unsigned char)keySrc[valIndexFromShared*keyLength + laneId + count*32] && laneId < keyLength - count*32);
		
				s =  __ballot((unsigned char)keySrc[newPosFromShared*keyLength + laneId + count*32] < (unsigned char)keySrc[valIndexFromShared*keyLength + laneId + count*32] && laneId < keyLength - count*32);

				if(realLane == m) {
					greaterReg[count] = g;
					smallerReg[count] = s;
				}
				count++;
			}
		}
		//this is necessary
		//__syncthreads();
		//assert(greaterArray[threadIdx.x] == greaterReg[0]);
		//assert(smallerArray[threadIdx.x] == smallerReg[0]);
		/*
		int greater2 = (greaterArray[threadIdx.x] > smallerArray[threadIdx.x]) ? 1 : 0;
		int equal2 = (greaterArray[threadIdx.x] == smallerArray[threadIdx.x])? 1 : 0;
		*/
		int greater2=0;
		int equal2=0;
		int smaller2=0;
		int count = 0;
		while(count < keyFactor) {
			if(greaterReg[count] > smallerReg[count]) {
				greater2 = 1;
				break;
			} else if (greaterReg[count] == smallerReg[count]) {
				count++;
				continue;
			} else {
				smaller2 = 1;
				break;
			}
		}
		if(!greater2 && !smaller2) {
			equal2 = 1;
		}
		
		if ( (sortDir && (smaller2 || equal2))  || (!sortDir && (greater2 || equal2)) ) {
            pos = newPos;
        }
    }

    return pos;
}

static inline __device__ int binarySearchInclusiveMod(int sortDir, int valIndex,  char *keySrc, int *data, int L, int stride, int keyLength)
{
    if (L == 0)
    {
        return 0;
    }

    int pos = 0;

    for (; stride > 0; stride >>= 1)
    {
        int newPos = umin(pos + stride, L);
		int greater = 0;
		int smaller = 0;
		int equal = 0;
		int stringIndex1 = data[newPos - 1]*keyLength;
		int stringIndex2 = valIndex*keyLength;
		/*
		assert(stringIndex1 + keyLength <= 16777216*sizeof(char)*keyLength);
		assert(stringIndex1 >= 0);
		assert(stringIndex2 + keyLength <= 16777216*sizeof(char)*keyLength);
		assert(stringIndex2 >= 0);
		*/
		for(int i=0; i < keyLength; i++) {
			if((unsigned char)keySrc[stringIndex1 + i] == (unsigned char)keySrc[stringIndex2 + i]) {
				if((unsigned char)keySrc[stringIndex1 + i] == '\0') {
					equal = 1;
					break;
				}
				continue;
			} else if((unsigned char)keySrc[stringIndex1 + i] < (unsigned char)keySrc[stringIndex2 + i]) {
				smaller = 1;
				break;
			} else if((unsigned char)keySrc[stringIndex1 + i] > (unsigned char)keySrc[stringIndex2 + i]) {
				greater = 1;
				break;
			} 
		}
        if ( (sortDir && (smaller || equal))  || (!sortDir && (greater || equal)) ) {
            pos = newPos;
        }
		
    }

    return pos;
}


static __forceinline__ __device__ int binarySearchExclusiveBallot(int sortDir, int valIndex,  char *keySrc, int *data, int L, int stride, int keyLength, unsigned int *warpData, int keyFactor)
{
	//maximum block size is 1024 --> at max 32 warps
	__shared__ int vals[32];
	__shared__ int news[32];

	unsigned int greaterReg[2];
	unsigned int smallerReg[2];
	unsigned int g,s;
    if (L == 0) {
        return 0;
    }

    int pos = 0;
	int realLane = threadIdx.x & 0x1f;
	int laneId = 31 - realLane;
	
    for (; stride > 0; stride >>= 1)
    {
        int newPos = umin(pos + stride, L);

		//newPosArray[threadIdx.x] = data[newPos - 1];
		//valIndexArray[threadIdx.x] = valIndex;
		for(int m=0; m <32; m++) {
			int count = 0;
			if(realLane == m) {
				//if(threadIdx.x > 250 )
					//printf("In here for index %d m %d tid %d\n", index, m , threadIdx.x);
				vals[threadIdx.x/32] = valIndex;
				news[threadIdx.x/32] = data[newPos - 1];
			}
			//this is necessary?
			//__syncthreads();
			 __threadfence_block();

			int newPosFromShared = news[threadIdx.x/32];
			int valIndexFromShared = vals[threadIdx.x/32];

			while(count < keyFactor) {
				g =  __ballot((unsigned char)keySrc[newPosFromShared*keyLength + laneId + count*32] > (unsigned char)keySrc[valIndexFromShared*keyLength + laneId + count*32] && laneId < keyLength - count*32);
				s =  __ballot((unsigned char)keySrc[newPosFromShared*keyLength + laneId + count*32] < (unsigned char)keySrc[valIndexFromShared*keyLength + laneId + count*32] && laneId < keyLength - count*32);

				if(realLane == m) {
					greaterReg[count] = g;
					smallerReg[count] = s;
				}
				count++;
			}
		
		}
		//this is necessary
		//__syncthreads();

		int greater2=0;
		int smaller2=0;
		int count = 0;
		while(count < keyFactor) {
			if(greaterReg[count] > smallerReg[count]) {
				greater2 = 1;
				break;
			} else if (greaterReg[count] == smallerReg[count]) {
				count++;
				continue;
			} else {
				smaller2 = 1;
				break;
			}
		}
		
		if ( (sortDir && smaller2)  || (!sortDir && greater2) )
        {
            pos = newPos;
        }
    }

    return pos;
}

static __forceinline__ __device__ int binarySearchExclusiveBallotOld(int sortDir, int valIndex,  char *keySrc, int *data, int L, int stride, int keyLength, unsigned int *warpData, int keyFactor)
{
	unsigned int *greaterArray = warpData;
	unsigned int *smallerArray  = warpData + keyFactor*blockDim.x;
	unsigned int *newPosArray  = warpData + 2*keyFactor*blockDim.x;
	unsigned int *valIndexArray  = warpData + 3*keyFactor*blockDim.x;

    if (L == 0)
    {
        return 0;
    }

    int pos = 0;
	int warpStartAdd = threadIdx.x & 0xFFFFFFE0;
	int laneId = 31 - threadIdx.x & 0x1f;
    for (; stride > 0; stride >>= 1)
    {
        int newPos = umin(pos + stride, L);
		newPosArray[threadIdx.x] = data[newPos - 1];
		valIndexArray[threadIdx.x] = valIndex;
		for(int m=0; m <32; m++) {
			int index = warpStartAdd + m;
			int count = 0;
			int newPosFromShared = newPosArray[index];
			int valIndexFromShared = valIndexArray[index];
			while(count < keyFactor) {
				greaterArray[index + blockDim.x*count] =  __ballot((unsigned char)keySrc[newPosFromShared*keyLength + laneId + count*32] > (unsigned char)keySrc[valIndexFromShared*keyLength + laneId + count*32] && laneId < keyLength - count*32);
		
				smallerArray[index + blockDim.x*count] =  __ballot((unsigned char)keySrc[newPosFromShared*keyLength + laneId + count*32] < (unsigned char)keySrc[valIndexFromShared*keyLength + laneId + count*32] && laneId < keyLength - count*32);
				/*
				__syncthreads();
				if(threadIdx.x == 31 && blockIdx.x == 0 ) {
					printf("str1 %s str2 %s SMg %x SMs %x tid %d keylength %d index %d warpstartadd %x newPosFromShared %d valIndexFromShared %d\n", &keySrc[newPosFromShared*keyLength], &keySrc[valIndexFromShared*keyLength], greaterArray[index + BS_BLOCKSIZE*count], smallerArray[index + BS_BLOCKSIZE*count], threadIdx.x, keyLength, index, warpStartAdd, newPosFromShared, valIndexFromShared );
				}
				*/
				count++;
			}
		
		}
		/*
		int greater2 = (greaterArray[threadIdx.x] > smallerArray[threadIdx.x]) ? 1 : 0;
		int equal2 = (greaterArray[threadIdx.x] == smallerArray[threadIdx.x])? 1 : 0;
		*/
		int greater2=0;
		int smaller2=0;
		int count = 0;
		while(count < keyFactor) {
			if(greaterArray[threadIdx.x] > smallerArray[threadIdx.x]) {
				greater2 = 1;
				break;
			} else if (greaterArray[threadIdx.x] == smallerArray[threadIdx.x]) {
				count++;
				continue;
			} else {
				smaller2 = 1;
				break;
			}
		}
				
		
		if ( (sortDir && (smaller2))  || (!sortDir && (greater2)) ) {
            pos = newPos;
        }
    }

    return pos;
}

static inline __device__ int binarySearchExclusiveMod(int sortDir, int valIndex, char *keySrc, int *data, int L, int stride, int keyLength)
{
    if (L == 0) {
        return 0;
    }

    int pos = 0;

    for (; stride > 0; stride >>= 1) {
        int newPos = umin(pos + stride, L);

		int greater = 0;
		int smaller = 0;
		int stringIndex1 = data[newPos - 1]*keyLength;
		int stringIndex2 = valIndex*keyLength;
		/*
		assert(stringIndex1 + keyLength <= 16777216*sizeof(char)*keyLength);
		assert(stringIndex1 >= 0);
		assert(stringIndex2 + keyLength <= 16777216*sizeof(char)*keyLength);
		assert(stringIndex2 >= 0);
		*/
		for(int i=0; i < keyLength; i++) {
			if((unsigned char)keySrc[stringIndex1 + i] == (unsigned char)keySrc[stringIndex2 + i]) {
				if((unsigned char)keySrc[stringIndex1 + i] == '\0') {
				break;
				}
				continue;
			} else if((unsigned char)keySrc[stringIndex1 + i] < (unsigned char)keySrc[stringIndex2 + i]) {
				smaller = 1;
				break;
			} else if ((unsigned char)keySrc[stringIndex1 + i] > (unsigned char)keySrc[stringIndex2 + i]){
				greater = 1;
				break;
			} 
		}
        if ((sortDir && smaller) || (!sortDir && greater)) {
            pos = newPos;
        }
    }

    return pos;
}

static inline __device__ int binarySearchInclusive(int sortDir, int val, int *data, int L, int stride)
{
    if (L == 0) {
        return 0;
    }

    int pos = 0;

    for (; stride > 0; stride >>= 1) {
        int newPos = umin(pos + stride, L);

        if ((sortDir && (data[newPos - 1] <= val)) || (!sortDir && (data[newPos - 1] >= val))) {
            pos = newPos;
        }
    }

    return pos;
}

static inline __device__ int binarySearchExclusive(int sortDir, int val, int *data, int L, int stride)
{
    if (L == 0) {
        return 0;
    }

    int pos = 0;

    for (; stride > 0; stride >>= 1) {
        int newPos = umin(pos + stride, L);

        if ((sortDir && (data[newPos - 1] < val)) || (!sortDir && (data[newPos - 1] > val))) {
            pos = newPos;
        }
    }

    return pos;
}


static inline __device__ int binarySearchInclusiveMod(int sortDir, int valIndex,  int *keySrc, int *data, int L, int stride, int keyLength)
{
    if (L == 0)
    {
        return 0;
    }
    int pos = 0;

    for (; stride > 0; stride >>= 1)
    {
        int newPos = umin(pos + stride, L);

        if ((sortDir && (keySrc[data[newPos - 1]] <= keySrc[valIndex])) || (!sortDir && (keySrc[data[newPos - 1]] >= keySrc[valIndex])))
        {
            pos = newPos;
        }
    }

    return pos;
}

static inline __device__ int binarySearchInclusiveMod(int sortDir, int valIndex,  long *keySrc, int *data, int L, int stride, int keyLength)
{
    if (L == 0)
    {
        return 0;
    }
    int pos = 0;

    for (; stride > 0; stride >>= 1)
    {
        int newPos = umin(pos + stride, L);

        if ((sortDir && (keySrc[data[newPos - 1]] <= keySrc[valIndex])) || (!sortDir && (keySrc[data[newPos - 1]] >= keySrc[valIndex])))
        {
            pos = newPos;
        }
    }

    return pos;
}

static inline __device__ int binarySearchExclusiveMod(int sortDir, int valIndex, int *keySrc, int *data, int L, int stride, int keyLength)
{
    if (L == 0)
    {
        return 0;
    }
    int pos = 0;

    for (; stride > 0; stride >>= 1)
    {
        int newPos = umin(pos + stride, L);

        if ((sortDir && (keySrc[data[newPos - 1]] < keySrc[valIndex])) || (!sortDir && (keySrc[data[newPos - 1]] > keySrc[valIndex])))
        {
            pos = newPos;
        }
    }

    return pos;
}


static inline __device__ int binarySearchExclusiveMod(int sortDir, int valIndex, long *keySrc, int *data, int L, int stride, int keyLength)
{
    if (L == 0)
    {
        return 0;
    }
    int pos = 0;

    for (; stride > 0; stride >>= 1)
    {
        int newPos = umin(pos + stride, L);

        if ((sortDir && (keySrc[data[newPos - 1]] < keySrc[valIndex])) || (!sortDir && (keySrc[data[newPos - 1]] > keySrc[valIndex])))
        {
            pos = newPos;
        }
    }

    return pos;
}

////////////////////////////////////////////////////////////////////////////////
// Bottom-level merge sort (binary search-based)
////////////////////////////////////////////////////////////////////////////////
__global__ void mergeSortKernel(
	int sortDir,
    int *d_DstKeyIndex,
    int *indexArray,
    char *d_SrcKey,
    int arrayLength,
	int keyLength
)
{
	/*
	__shared__ unsigned int warpData[BS_BLOCKSIZE*4];
	*/
	int keyFactor = 1;
	while(keyFactor*32 < keyLength)
		keyFactor++;
	
    //d_SrcKey += blockIdx.x * SHARED_SIZE_LIMIT;
	indexArray += blockIdx.x * SHARED_SIZE_LIMIT;
    d_DstKeyIndex += blockIdx.x * SHARED_SIZE_LIMIT;

    for (int stride = 1; stride < arrayLength; stride <<= 1) {
        int     lPos = threadIdx.x & (stride - 1);
        int *baseKeyIndex = indexArray + 2 * (threadIdx.x - lPos);
        //int *baseVal = d_SrcVal + 2 * (threadIdx.x - lPos);

        __syncthreads();
        int keyIndexA = baseKeyIndex[lPos +      0];
        int keyIndexB = baseKeyIndex[lPos + stride];
        int posA = binarySearchExclusiveMod(sortDir, keyIndexA, d_SrcKey, baseKeyIndex + stride, stride, stride, keyLength) + lPos;
		int posB = binarySearchInclusiveMod(sortDir, keyIndexB, d_SrcKey, baseKeyIndex +      0, stride, stride, keyLength) + lPos;
		//int posA = binarySearchExclusiveBallot(sortDir, keyIndexA, d_SrcKey, baseKeyIndex + stride, stride, stride, keyLength, 0, keyFactor) + lPos;
        //int posB = binarySearchInclusiveBallot(sortDir, keyIndexB, d_SrcKey, baseKeyIndex +      0, stride, stride, keyLength, 0, keyFactor) + lPos;
        __syncthreads();
        baseKeyIndex[posA] = keyIndexA;
        baseKeyIndex[posB] = keyIndexB;
    }

    __syncthreads();
    d_DstKeyIndex[threadIdx.x +                      0] = indexArray[threadIdx.x +                       0];
    d_DstKeyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = indexArray[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
}



////////////////////////////////////////////////////////////////////////////////
// Bottom-level merge sort (binary search-based)
////////////////////////////////////////////////////////////////////////////////
__global__ void mergeSortKernelShared(
	int sortDir,
    int *d_DstKeyIndex,
    int *indexArray,
    char *d_SrcKey,
    int arrayLength,
	int keyLength
)
{
	/*
	__shared__ unsigned int warpData[BS_BLOCKSIZE*4];
	*/
	 __shared__ int s_keyIndex[SHARED_SIZE_LIMIT];

	int keyFactor = 1;
	while(keyFactor*32 < keyLength)
		keyFactor++;
	
    //d_SrcKey += blockIdx.x * SHARED_SIZE_LIMIT;
	indexArray += blockIdx.x * SHARED_SIZE_LIMIT ;
    d_DstKeyIndex += blockIdx.x * SHARED_SIZE_LIMIT ;

	s_keyIndex[threadIdx.x +                       0] = indexArray[threadIdx.x +                      0];
    s_keyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = indexArray[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];


    for (int stride = 1; stride < arrayLength; stride <<= 1) {
        int     lPos = threadIdx.x & (stride - 1);
        int *baseKeyIndex = s_keyIndex + 2 * (threadIdx.x - lPos);
        //int *baseVal = d_SrcVal + 2 * (threadIdx.x - lPos);

        __syncthreads();
        int keyIndexA = baseKeyIndex[lPos +      0];
        int keyIndexB = baseKeyIndex[lPos + stride];
        int posA = binarySearchExclusiveMod(sortDir, keyIndexA, d_SrcKey, baseKeyIndex + stride, stride, stride, keyLength) + lPos;
		int posB = binarySearchInclusiveMod(sortDir, keyIndexB, d_SrcKey, baseKeyIndex +      0, stride, stride, keyLength) + lPos;
		//int posA = binarySearchExclusiveBallot(sortDir, keyIndexA, d_SrcKey, baseKeyIndex + stride, stride, stride, keyLength, 0, keyFactor) + lPos;
        //int posB = binarySearchInclusiveBallot(sortDir, keyIndexB, d_SrcKey, baseKeyIndex +      0, stride, stride, keyLength, 0, keyFactor) + lPos;
        __syncthreads();
        baseKeyIndex[posA] = keyIndexA;
        baseKeyIndex[posB] = keyIndexB;
    }

    __syncthreads();
    d_DstKeyIndex[threadIdx.x +                      0] = s_keyIndex[threadIdx.x +                       0];
    d_DstKeyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = s_keyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
}


////////////////////////////////////////////////////////////////////////////////
// Bottom-level merge sort (binary search-based)
////////////////////////////////////////////////////////////////////////////////
__global__ void mergeSortKernel(
	int sortDir,
    int *d_DstKeyIndex,
    int *indexArray,
    int *d_SrcKey,
    int arrayLength,
	int keyLength
)
{
	/*
	__shared__ unsigned int warpData[BS_BLOCKSIZE*4];
	*/
	int keyFactor = 1;
	while(keyFactor*32 < keyLength)
		keyFactor++;
	
    //d_SrcKey += blockIdx.x * SHARED_SIZE_LIMIT;
	indexArray += blockIdx.x * SHARED_SIZE_LIMIT;
    d_DstKeyIndex += blockIdx.x * SHARED_SIZE_LIMIT;

    for (int stride = 1; stride < arrayLength; stride <<= 1) {
        int     lPos = threadIdx.x & (stride - 1);
        int *baseKeyIndex = indexArray + 2 * (threadIdx.x - lPos);
        //int *baseVal = d_SrcVal + 2 * (threadIdx.x - lPos);

        __syncthreads();
        int keyIndexA = baseKeyIndex[lPos +      0];
        int keyIndexB = baseKeyIndex[lPos + stride];
        int posA = binarySearchExclusiveMod(sortDir, keyIndexA, d_SrcKey, baseKeyIndex + stride, stride, stride, keyLength) + lPos;
		int posB = binarySearchInclusiveMod(sortDir, keyIndexB, d_SrcKey, baseKeyIndex +      0, stride, stride, keyLength) + lPos;
		//int posA = binarySearchExclusiveBallot(sortDir, keyIndexA, d_SrcKey, baseKeyIndex + stride, stride, stride, keyLength, 0, keyFactor) + lPos;
        //int posB = binarySearchInclusiveBallot(sortDir, keyIndexB, d_SrcKey, baseKeyIndex +      0, stride, stride, keyLength, 0, keyFactor) + lPos;
        __syncthreads();
        baseKeyIndex[posA] = keyIndexA;
        baseKeyIndex[posB] = keyIndexB;
    }

    __syncthreads();
    d_DstKeyIndex[threadIdx.x +                      0] = indexArray[threadIdx.x +                       0];
    d_DstKeyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = indexArray[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
}

////////////////////////////////////////////////////////////////////////////////
// Bottom-level merge sort (binary search-based)
////////////////////////////////////////////////////////////////////////////////
__global__ void mergeSortKernelShared(
	int sortDir,
    int *d_DstKeyIndex,
    int *indexArray,
    int *d_SrcKey,
    int arrayLength,
	int keyLength
)
{
	/*
	__shared__ unsigned int warpData[BS_BLOCKSIZE*4];
	*/
	 __shared__ int s_keyIndex[SHARED_SIZE_LIMIT];

	int keyFactor = 1;

    //d_SrcKey += blockIdx.x * SHARED_SIZE_LIMIT;
	indexArray += blockIdx.x * SHARED_SIZE_LIMIT ;
    d_DstKeyIndex += blockIdx.x * SHARED_SIZE_LIMIT ;

	s_keyIndex[threadIdx.x +                       0] = indexArray[threadIdx.x +                      0];
    s_keyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = indexArray[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];


    for (int stride = 1; stride < arrayLength; stride <<= 1) {
        int     lPos = threadIdx.x & (stride - 1);
        int *baseKeyIndex = s_keyIndex + 2 * (threadIdx.x - lPos);
        //int *baseVal = d_SrcVal + 2 * (threadIdx.x - lPos);

        __syncthreads();
        int keyIndexA = baseKeyIndex[lPos +      0];
        int keyIndexB = baseKeyIndex[lPos + stride];
        int posA = binarySearchExclusiveMod(sortDir, keyIndexA, d_SrcKey, baseKeyIndex + stride, stride, stride, keyLength) + lPos;
		int posB = binarySearchInclusiveMod(sortDir, keyIndexB, d_SrcKey, baseKeyIndex +      0, stride, stride, keyLength) + lPos;
		//int posA = binarySearchExclusiveBallot(sortDir, keyIndexA, d_SrcKey, baseKeyIndex + stride, stride, stride, keyLength, 0, keyFactor) + lPos;
        //int posB = binarySearchInclusiveBallot(sortDir, keyIndexB, d_SrcKey, baseKeyIndex +      0, stride, stride, keyLength, 0, keyFactor) + lPos;
        __syncthreads();
        baseKeyIndex[posA] = keyIndexA;
        baseKeyIndex[posB] = keyIndexB;
    }

    __syncthreads();
    d_DstKeyIndex[threadIdx.x +                      0] = s_keyIndex[threadIdx.x +                       0];
    d_DstKeyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = s_keyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
}



////////////////////////////////////////////////////////////////////////////////
// Bottom-level merge sort (binary search-based)
////////////////////////////////////////////////////////////////////////////////
__global__ void mergeSortKernelShared(
	int sortDir,
    int *d_DstKeyIndex,
    int *indexArray,
    long *d_SrcKey,
    int arrayLength,
	int keyLength
)
{
	/*
	__shared__ unsigned int warpData[BS_BLOCKSIZE*4];
	*/
	 __shared__ int s_keyIndex[SHARED_SIZE_LIMIT];

	int keyFactor = 1;

    //d_SrcKey += blockIdx.x * SHARED_SIZE_LIMIT;
	indexArray += blockIdx.x * SHARED_SIZE_LIMIT ;
    d_DstKeyIndex += blockIdx.x * SHARED_SIZE_LIMIT ;

	s_keyIndex[threadIdx.x +                       0] = indexArray[threadIdx.x +                      0];
    s_keyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = indexArray[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];


    for (int stride = 1; stride < arrayLength; stride <<= 1) {
        int     lPos = threadIdx.x & (stride - 1);
        int *baseKeyIndex = s_keyIndex + 2 * (threadIdx.x - lPos);
        //int *baseVal = d_SrcVal + 2 * (threadIdx.x - lPos);

        __syncthreads();
        int keyIndexA = baseKeyIndex[lPos +      0];
        int keyIndexB = baseKeyIndex[lPos + stride];
        int posA = binarySearchExclusiveMod(sortDir, keyIndexA, d_SrcKey, baseKeyIndex + stride, stride, stride, keyLength) + lPos;
		int posB = binarySearchInclusiveMod(sortDir, keyIndexB, d_SrcKey, baseKeyIndex +      0, stride, stride, keyLength) + lPos;
		//int posA = binarySearchExclusiveBallot(sortDir, keyIndexA, d_SrcKey, baseKeyIndex + stride, stride, stride, keyLength, 0, keyFactor) + lPos;
        //int posB = binarySearchInclusiveBallot(sortDir, keyIndexB, d_SrcKey, baseKeyIndex +      0, stride, stride, keyLength, 0, keyFactor) + lPos;
        __syncthreads();
        baseKeyIndex[posA] = keyIndexA;
        baseKeyIndex[posB] = keyIndexB;
    }

    __syncthreads();
    d_DstKeyIndex[threadIdx.x +                      0] = s_keyIndex[threadIdx.x +                       0];
    d_DstKeyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = s_keyIndex[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
}


template< typename T >
static void mergeSortShared(
    int *d_DstKeyIndex,
    int *indexArray,
	T *d_SrcKey,
    int batchSize,
    int arrayLength,
    int sortDir,
	int keyLength
)
{
    if (arrayLength < 2) {
        return;
    }

    assert(SHARED_SIZE_LIMIT % arrayLength == 0);
    assert(((batchSize * arrayLength) % SHARED_SIZE_LIMIT) == 0);
    int blockCount = batchSize * arrayLength / SHARED_SIZE_LIMIT;
    int threadCount = SHARED_SIZE_LIMIT / 2;

    if (sortDir) {
    	//mergeSortKernel<<<blockCount, threadCount>>>(1, d_DstKeyIndex, indexArray, d_SrcKey, arrayLength, keyLength);
		//printf("blocks %d threads %d\n", blockCount, threadCount);
		mergeSortKernelShared<<<blockCount, threadCount>>>(1, d_DstKeyIndex, indexArray, d_SrcKey, arrayLength, keyLength);
        getLastCudaError("mergeSortShared<1><<<>>> failed\n");
    } else {
    	//mergeSortKernel<<<blockCount, threadCount>>>(0, d_DstKeyIndex, indexArray, d_SrcKey, arrayLength, keyLength);
		mergeSortKernelShared<<<blockCount, threadCount>>>(0, d_DstKeyIndex, indexArray, d_SrcKey, arrayLength, keyLength);
        getLastCudaError("mergeSortShared<0><<<>>> failed\n");
    }
}



////////////////////////////////////////////////////////////////////////////////
// Merge step 1: generate sample ranks
////////////////////////////////////////////////////////////////////////////////
__global__ void generateSampleRanksKernelNew(
	int sortDir,
	int *indexArray,
    int *d_RanksA,
    int *d_RanksB,
    char *d_SrcKey,
    int stride,
    int N,
    int threadCount,
	int keyLength
)
{
	//__shared__ unsigned int warpData[256*4];
	int keyFactor = 1;
	while(keyFactor*32 < keyLength)
		keyFactor++;
    int pos = blockIdx.x * blockDim.x + threadIdx.x;

    if (pos >= threadCount) {
        return;
    }

    const int           i = pos & ((stride / SAMPLE_STRIDE) - 1);
    const int segmentBase = (pos - i) * (2 * SAMPLE_STRIDE);
    //d_SrcKey += segmentBase;

	indexArray += segmentBase;
    d_RanksA += segmentBase / SAMPLE_STRIDE;
    d_RanksB += segmentBase / SAMPLE_STRIDE;

    const int segmentElementsA = stride;
    const int segmentElementsB = umin(stride, N - segmentBase - stride);
    const int  segmentSamplesA = getSampleCount(segmentElementsA);
    const int  segmentSamplesB = getSampleCount(segmentElementsB);

	//int *src;
	//src = d_SrcKey;
	//src += segmentBase;	

    if (i < segmentSamplesA) {
        d_RanksA[i] = i * SAMPLE_STRIDE;
		d_RanksB[i] =  binarySearchExclusiveMod(sortDir, indexArray[i * SAMPLE_STRIDE], d_SrcKey, indexArray + stride,
                          segmentElementsB, nextPowerOfTwo(segmentElementsB), keyLength);
		/*d_RanksB[i] =  binarySearchExclusiveBallot(sortDir, indexArray[i * SAMPLE_STRIDE], d_SrcKey, indexArray + stride,
                          segmentElementsB, nextPowerOfTwo(segmentElementsB), keyLength, 0, keyFactor);*/
		
    }

    if (i < segmentSamplesB) {
        d_RanksB[(stride / SAMPLE_STRIDE) + i] = i * SAMPLE_STRIDE;
		d_RanksA[(stride / SAMPLE_STRIDE) + i] = binarySearchInclusiveMod(sortDir, indexArray[stride + i * SAMPLE_STRIDE], 
							d_SrcKey, indexArray, segmentElementsA, nextPowerOfTwo(segmentElementsA), keyLength);
		/*d_RanksA[(stride / SAMPLE_STRIDE) + i] = binarySearchInclusiveBallot(sortDir, indexArray[stride + i * SAMPLE_STRIDE], 
							d_SrcKey, indexArray, segmentElementsA, nextPowerOfTwo(segmentElementsA), keyLength, 0, keyFactor);*/
    }
}


__global__ void generateSampleRanksKernelNew(
	int sortDir,
	int *indexArray,
    int *d_RanksA,
    int *d_RanksB,
    int *d_SrcKey,
    int stride,
    int N,
    int threadCount,
	int keyLength //this should be 1
)
{
    int pos = blockIdx.x * blockDim.x + threadIdx.x;

    if (pos >= threadCount)
    {
        return;
    }

    const int           i = pos & ((stride / SAMPLE_STRIDE) - 1);
    const int segmentBase = (pos - i) * (2 * SAMPLE_STRIDE);
    //d_SrcKey += segmentBase;

	indexArray += segmentBase;
    d_RanksA += segmentBase / SAMPLE_STRIDE;
    d_RanksB += segmentBase / SAMPLE_STRIDE;

    const int segmentElementsA = stride;
    const int segmentElementsB = umin(stride, N - segmentBase - stride);
    const int  segmentSamplesA = getSampleCount(segmentElementsA);
    const int  segmentSamplesB = getSampleCount(segmentElementsB);

	int *src;
	src = d_SrcKey;
	src += segmentBase;	

    if (i < segmentSamplesA)
    {
        d_RanksA[i] = i * SAMPLE_STRIDE;
		d_RanksB[i] =  binarySearchExclusiveMod(sortDir, indexArray[i * SAMPLE_STRIDE], d_SrcKey, indexArray + stride,
                          segmentElementsB, nextPowerOfTwo(segmentElementsB), keyLength);		
    }

    if (i < segmentSamplesB)
    {
        d_RanksB[(stride / SAMPLE_STRIDE) + i] = i * SAMPLE_STRIDE;
		d_RanksA[(stride / SAMPLE_STRIDE) + i] = binarySearchInclusiveMod(sortDir, indexArray[stride + i * SAMPLE_STRIDE], 
							d_SrcKey, indexArray, segmentElementsA, nextPowerOfTwo(segmentElementsA), keyLength);
    }
}


__global__ void generateSampleRanksKernelNew(
	int sortDir,
	int *indexArray,
    int *d_RanksA,
    int *d_RanksB,
    long *d_SrcKey,
    int stride,
    int N,
    int threadCount,
	int keyLength //this should be 1
)
{
    int pos = blockIdx.x * blockDim.x + threadIdx.x;

    if (pos >= threadCount)
    {
        return;
    }

    const int           i = pos & ((stride / SAMPLE_STRIDE) - 1);
    const int segmentBase = (pos - i) * (2 * SAMPLE_STRIDE);
    //d_SrcKey += segmentBase;

	indexArray += segmentBase;
    d_RanksA += segmentBase / SAMPLE_STRIDE;
    d_RanksB += segmentBase / SAMPLE_STRIDE;

    const int segmentElementsA = stride;
    const int segmentElementsB = umin(stride, N - segmentBase - stride);
    const int  segmentSamplesA = getSampleCount(segmentElementsA);
    const int  segmentSamplesB = getSampleCount(segmentElementsB);

	long *src;
	src = d_SrcKey;
	src += segmentBase;	

    if (i < segmentSamplesA)
    {
        d_RanksA[i] = i * SAMPLE_STRIDE;
		d_RanksB[i] =  binarySearchExclusiveMod(sortDir, indexArray[i * SAMPLE_STRIDE], d_SrcKey, indexArray + stride,
                          segmentElementsB, nextPowerOfTwo(segmentElementsB), keyLength);		
    }

    if (i < segmentSamplesB)
    {
        d_RanksB[(stride / SAMPLE_STRIDE) + i] = i * SAMPLE_STRIDE;
		d_RanksA[(stride / SAMPLE_STRIDE) + i] = binarySearchInclusiveMod(sortDir, indexArray[stride + i * SAMPLE_STRIDE], 
							d_SrcKey, indexArray, segmentElementsA, nextPowerOfTwo(segmentElementsA), keyLength);
    }
}

template< typename T >
static void generateSampleRanksNew(
	int *indexArray,
    int *d_RanksA,
    int *d_RanksB,
    T *d_SrcKey,
    int stride,
    int N,
    int sortDir,
	int keyLength
)
{
    int lastSegmentElements = N % (2 * stride);
    int         threadCount = (lastSegmentElements > stride) ? (N + 2 * stride - lastSegmentElements) / (2 * SAMPLE_STRIDE) : (N - lastSegmentElements) / (2 * SAMPLE_STRIDE);

    if (sortDir) {
        generateSampleRanksKernelNew<<<iDivUp(threadCount, 256), 256>>>(sortDir, indexArray, d_RanksA, d_RanksB, d_SrcKey, stride, N, threadCount, keyLength);
        getLastCudaError("generateSampleRanksKernelNew<1U><<<>>> failed\n");
    } else {
        generateSampleRanksKernelNew<<<iDivUp(threadCount, 256), 256>>>(sortDir, indexArray, d_RanksA, d_RanksB, d_SrcKey, stride, N, threadCount, keyLength);
        getLastCudaError("generateSampleRanksKernelNew<0U><<<>>> failed\n");
    }
}


////////////////////////////////////////////////////////////////////////////////
// Merge step 2: generate sample ranks and indices
////////////////////////////////////////////////////////////////////////////////
__global__ void mergeRanksAndIndicesKernel(
    int *d_Limits,
    int *d_Ranks,
    int stride,
    int N,
    int threadCount
)
{
    int pos = blockIdx.x * blockDim.x + threadIdx.x;

    if (pos >= threadCount) {
        return;
    }

    const int           i = pos & ((stride / SAMPLE_STRIDE) - 1);
    const int segmentBase = (pos - i) * (2 * SAMPLE_STRIDE);
    d_Ranks  += (pos - i) * 2;
    d_Limits += (pos - i) * 2;

    const int segmentElementsA = stride;
    const int segmentElementsB = umin(stride, N - segmentBase - stride);
    const int  segmentSamplesA = getSampleCount(segmentElementsA);
    const int  segmentSamplesB = getSampleCount(segmentElementsB);

    if (i < segmentSamplesA) {
        int dstPos = binarySearchExclusive(1, d_Ranks[i], d_Ranks + segmentSamplesA, segmentSamplesB, nextPowerOfTwo(segmentSamplesB)) + i;
        d_Limits[dstPos] = d_Ranks[i];
    }

    if (i < segmentSamplesB) {
        int dstPos = binarySearchInclusive(1, d_Ranks[segmentSamplesA + i], d_Ranks, segmentSamplesA, nextPowerOfTwo(segmentSamplesA)) + i;
        d_Limits[dstPos] = d_Ranks[segmentSamplesA + i];
    }
}


static void mergeRanksAndIndices(
    int *d_LimitsA,
    int *d_LimitsB,
    int *d_RanksA,
    int *d_RanksB,
    int stride,
    int N
)
{
    int lastSegmentElements = N % (2 * stride);
    int         threadCount = (lastSegmentElements > stride) ? (N + 2 * stride - lastSegmentElements) / (2 * SAMPLE_STRIDE) : (N - lastSegmentElements) / (2 * SAMPLE_STRIDE);

    mergeRanksAndIndicesKernel<<<iDivUp(threadCount, 256), 256>>>(
        d_LimitsA,
        d_RanksA,
        stride,
        N,
        threadCount
    );
    getLastCudaError("mergeRanksAndIndicesKernel(A)<<<>>> failed\n");

    mergeRanksAndIndicesKernel<<<iDivUp(threadCount, 256), 256>>>(
        d_LimitsB,
        d_RanksB,
        stride,
        N,
        threadCount
    );
    getLastCudaError("mergeRanksAndIndicesKernel(B)<<<>>> failed\n");
}


template< typename T >
__global__ void mergeElementaryIntervalsKernelNew(
	int sortDir,
    int *d_DstKey,
    int *d_SrcKey,
    int *d_LimitsA,
    int *d_LimitsB,
    int stride,
    int N,
	T *realKeys,
	int keyLength //should be 1
)
{
    __shared__ int s_key[2 * SAMPLE_STRIDE];

	/*
	__shared__ unsigned int warpData[4*SAMPLE_STRIDE];
	int keyFactor = 1;
	while(keyFactor*32 < keyLength)
		keyFactor++;
	*/
    const int   intervalI = blockIdx.x & ((2 * stride) / SAMPLE_STRIDE - 1);
    const int segmentBase = (blockIdx.x - intervalI) * SAMPLE_STRIDE;
    d_SrcKey += segmentBase;
    d_DstKey += segmentBase;

    //Set up threadblock-wide parameters
    __shared__ int startSrcA, startSrcB, lenSrcA, lenSrcB, startDstA, startDstB;

    if (threadIdx.x == 0) {
        int segmentElementsA = stride;
        int segmentElementsB = umin(stride, N - segmentBase - stride);
        int  segmentSamplesA = getSampleCount(segmentElementsA);
        int  segmentSamplesB = getSampleCount(segmentElementsB);
        int   segmentSamples = segmentSamplesA + segmentSamplesB;

        startSrcA    = d_LimitsA[blockIdx.x];
        startSrcB    = d_LimitsB[blockIdx.x];
        int endSrcA = (intervalI + 1 < segmentSamples) ? d_LimitsA[blockIdx.x + 1] : segmentElementsA;
        int endSrcB = (intervalI + 1 < segmentSamples) ? d_LimitsB[blockIdx.x + 1] : segmentElementsB;
        lenSrcA      = endSrcA - startSrcA;
        lenSrcB      = endSrcB - startSrcB;
        startDstA    = startSrcA + startSrcB;
        startDstB    = startDstA + lenSrcA;
    }

    //Load main input data

    __syncthreads();

    if (threadIdx.x < lenSrcA) {
        s_key[threadIdx.x +             0] = d_SrcKey[0 + startSrcA + threadIdx.x];
    }

    if (threadIdx.x < lenSrcB) {
        s_key[threadIdx.x + SAMPLE_STRIDE] = d_SrcKey[stride + startSrcB + threadIdx.x];
    }

    //Merge data in shared memory
    __syncthreads();
	/*
    merge<sortDir>(
        s_key,
        s_key + 0,
        s_key + SAMPLE_STRIDE,
        lenSrcA, SAMPLE_STRIDE,
        lenSrcB, SAMPLE_STRIDE
    );
	*/

	
	//INLINED FUNCTION
    int *dstKey = s_key;
    int *srcAKey = s_key;
    int *srcBKey = s_key + SAMPLE_STRIDE;
    int lenA = lenSrcA;
    int nPowTwoLenA = SAMPLE_STRIDE;
    int lenB = lenSrcB;
    int nPowTwoLenB = SAMPLE_STRIDE;

	int keyA, keyB, dstPosA, dstPosB;

    if (threadIdx.x < lenA) {
        keyA = srcAKey[threadIdx.x];
        dstPosA = binarySearchExclusiveMod(sortDir, keyA, realKeys, srcBKey, lenB, nPowTwoLenB, keyLength) + threadIdx.x;
    }

    if (threadIdx.x < lenB) {
        keyB = srcBKey[threadIdx.x];
        dstPosB = binarySearchInclusiveMod(sortDir, keyB, realKeys, srcAKey, lenA, nPowTwoLenA, keyLength) + threadIdx.x;
    }

    __syncthreads();
	
    if (threadIdx.x < lenA) {
        dstKey[dstPosA] = keyA;
    }

    if (threadIdx.x < lenB) {
        dstKey[dstPosB] = keyB;
    }
	//INLINED FUNCTION DONE

    //Store merged data
    __syncthreads();

    if (threadIdx.x < lenSrcA) {
        d_DstKey[startDstA + threadIdx.x] = s_key[threadIdx.x];
    }

    if (threadIdx.x < lenSrcB) {
        d_DstKey[startDstB + threadIdx.x] = s_key[lenSrcA + threadIdx.x];
    }

}

template< typename T >
static void mergeElementaryIntervalsNew(
    int *d_DstKey,
    int *d_SrcKey,
    int *d_LimitsA,
    int *d_LimitsB,
    int stride,
    int N,
    int sortDir,
	T *realKeys,
	int keyLength
)
{
    int lastSegmentElements = N % (2 * stride);
    int          mergePairs = (lastSegmentElements > stride) ? getSampleCount(N) : (N - lastSegmentElements) / SAMPLE_STRIDE;

    if (sortDir) {
		printf("blocks %d threads %d\n", mergePairs, SAMPLE_STRIDE);
        mergeElementaryIntervalsKernelNew<<<mergePairs, SAMPLE_STRIDE>>>(
			sortDir,
            d_DstKey,
            d_SrcKey,
            d_LimitsA,
            d_LimitsB,
            stride,
            N,
			realKeys,
			keyLength
        );
		checkCudaErrors(cudaDeviceSynchronize());
        getLastCudaError("mergeElementaryIntervalsKernelNew<1> failed\n");
		
    } else {
        mergeElementaryIntervalsKernelNew<<<mergePairs, SAMPLE_STRIDE>>>(
			sortDir,
            d_DstKey,
            d_SrcKey,
            d_LimitsA,
            d_LimitsB,
            stride,
            N,
			realKeys,
			keyLength
        );
		checkCudaErrors(cudaDeviceSynchronize());
        getLastCudaError("mergeElementaryIntervalsKernelNew<0> failed\n");
    }
}

static int *d_RanksA, *d_RanksB, *d_LimitsA, *d_LimitsB;
static const int MAX_SAMPLE_COUNT = 32768*4;

void initMergeSort(void) {
    checkCudaErrors(cudaMalloc((void **)&d_RanksA,  MAX_SAMPLE_COUNT * sizeof(int)));
    checkCudaErrors(cudaMalloc((void **)&d_RanksB,  MAX_SAMPLE_COUNT * sizeof(int)));
    checkCudaErrors(cudaMalloc((void **)&d_LimitsA, MAX_SAMPLE_COUNT * sizeof(int)));
    checkCudaErrors(cudaMalloc((void **)&d_LimitsB, MAX_SAMPLE_COUNT * sizeof(int)));
}

void closeMergeSort(void) {
    checkCudaErrors(cudaFree(d_RanksA));
    checkCudaErrors(cudaFree(d_RanksB));
    checkCudaErrors(cudaFree(d_LimitsB));
    checkCudaErrors(cudaFree(d_LimitsA));
}

void mergeSort(
	int *indexArray,
    char *d_SrcKey,
	int keyLength,
    int N
)
{
    int stageCount = 0;
	const int sortDir = 1;
	int  *d_BufKey, *d_DstKey;
	double sortBegin = timer();
	N = (int) pow( (float)2, (int) ceil(log(N)/log(2)));
	printf("MergeSort size %d\n", N);
    checkCudaErrors(cudaMalloc((void **)&d_DstKey, N * sizeof(int)));
    checkCudaErrors(cudaMalloc((void **)&d_BufKey, N * sizeof(int)));
	initMergeSort();
    for (int stride = SHARED_SIZE_LIMIT; stride < N; stride <<= 1, stageCount++);

    int *ikey, *okey;

    if (stageCount & 1) {
        ikey = d_BufKey;
        okey = d_DstKey;
    } else {
        ikey = d_DstKey;
        okey = d_BufKey;
    }

	if(N < 1024) {
		bitonic_sort(indexArray, d_SrcKey, keyLength, N);
		return;
	} else {
	    assert(N <= (SAMPLE_STRIDE * MAX_SAMPLE_COUNT));
		assert(N % SHARED_SIZE_LIMIT == 0);
		mergeSortShared(ikey, indexArray, d_SrcKey, N / SHARED_SIZE_LIMIT, SHARED_SIZE_LIMIT, sortDir, keyLength);
		cudaDeviceSynchronize();
	}


	//int iter =0;
    for (int stride = SHARED_SIZE_LIMIT; stride < N; stride <<= 1) {
        int lastSegmentElements = N % (2 * stride);
		//printf("iter %d\n", iter++);
		
		generateSampleRanksNew(ikey, d_RanksA, d_RanksB, d_SrcKey, stride, N, sortDir, keyLength);
		checkCudaErrors(cudaDeviceSynchronize());
	
        //Merge ranks and indices
        mergeRanksAndIndices(d_LimitsA, d_LimitsB, d_RanksA, d_RanksB, stride, N);
		checkCudaErrors(cudaDeviceSynchronize());

		mergeElementaryIntervalsNew(okey, ikey, d_LimitsA, d_LimitsB, stride, N, sortDir, d_SrcKey, keyLength);
		checkCudaErrors(cudaDeviceSynchronize());

        if (lastSegmentElements <= stride) {
			//printf("In the non-desired condition\n");
            //Last merge segment consists of a single array which just needs to be passed through
            checkCudaErrors(cudaMemcpy(okey + (N - lastSegmentElements), ikey + (N - lastSegmentElements), lastSegmentElements * sizeof(int), cudaMemcpyDeviceToDevice));
            //checkCudaErrors(cudaMemcpy(oval + (N - lastSegmentElements), ival + (N - lastSegmentElements), lastSegmentElements * sizeof(int), cudaMemcpyDeviceToDevice));
        }

        int *t;
        t = ikey;
        ikey = okey;
        okey = t;
    }
	checkCudaErrors(cudaMemcpy(indexArray, d_DstKey, N * sizeof(int), cudaMemcpyDeviceToDevice));
	//indexArray = d_DstKey;
	//int *tmp =
    cudaFree(d_DstKey);
    cudaFree(d_BufKey);
	closeMergeSort();
	printf("Merge Sort time %lf\n", timer() - sortBegin);
}


void mergeSort(
	int *indexArray,
    int *d_SrcKey,
	int keyLength,
    int N
)
{
    int stageCount = 0;
	const int sortDir = 1;
	int  *d_BufKey, *d_DstKey;
	double sortBegin = timer();
	N = (int) pow( (float)2, (int) ceil(log(N)/log(2)));
	printf("MergeSort size %d\n", N);
    checkCudaErrors(cudaMalloc((void **)&d_DstKey, N * sizeof(int)));
    checkCudaErrors(cudaMalloc((void **)&d_BufKey, N * sizeof(int)));
	initMergeSort();
    for (int stride = SHARED_SIZE_LIMIT; stride < N; stride <<= 1, stageCount++);

    int *ikey, *okey;

    if (stageCount & 1) {
        ikey = d_BufKey;
        okey = d_DstKey;
    } else {
        ikey = d_DstKey;
        okey = d_BufKey;
    }

	if(N < 1024) {
		bitonic_sort(indexArray, d_SrcKey, keyLength, N);
		return;
	} else {
	    assert(N <= (SAMPLE_STRIDE * MAX_SAMPLE_COUNT));
		assert(N % SHARED_SIZE_LIMIT == 0);
		mergeSortShared(ikey, indexArray, d_SrcKey, N / SHARED_SIZE_LIMIT, SHARED_SIZE_LIMIT, sortDir, keyLength);
		cudaDeviceSynchronize();
	}



	//int iter =0;
    for (int stride = SHARED_SIZE_LIMIT; stride < N; stride <<= 1) {
        int lastSegmentElements = N % (2 * stride);
		//printf("iter %d\n", iter++);
		
		generateSampleRanksNew(ikey, d_RanksA, d_RanksB, d_SrcKey, stride, N, sortDir, keyLength);
		checkCudaErrors(cudaDeviceSynchronize());
	
        //Merge ranks and indices
        mergeRanksAndIndices(d_LimitsA, d_LimitsB, d_RanksA, d_RanksB, stride, N);
		checkCudaErrors(cudaDeviceSynchronize());

		mergeElementaryIntervalsNew(okey, ikey, d_LimitsA, d_LimitsB, stride, N, sortDir, d_SrcKey, keyLength);
		checkCudaErrors(cudaDeviceSynchronize());

        if (lastSegmentElements <= stride) {
			//printf("In the non-desired condition\n");
            //Last merge segment consists of a single array which just needs to be passed through
            checkCudaErrors(cudaMemcpy(okey + (N - lastSegmentElements), ikey + (N - lastSegmentElements), lastSegmentElements * sizeof(int), cudaMemcpyDeviceToDevice));
            //checkCudaErrors(cudaMemcpy(oval + (N - lastSegmentElements), ival + (N - lastSegmentElements), lastSegmentElements * sizeof(int), cudaMemcpyDeviceToDevice));
        }

        int *t;
        t = ikey;
        ikey = okey;
        okey = t;
    }
	checkCudaErrors(cudaMemcpy(indexArray, d_DstKey, N * sizeof(int), cudaMemcpyDeviceToDevice));
	//indexArray = d_DstKey;
	//int *tmp =
    cudaFree(d_DstKey);
    cudaFree(d_BufKey);
	closeMergeSort();
	printf("Merge Sort time %lf\n", timer() - sortBegin);
}


void mergeSort(
	int *indexArray,
    long *d_SrcKey,
	int keyLength,
    int N
)
{
    int stageCount = 0;
	const int sortDir = 1;
	int  *d_BufKey, *d_DstKey;
	double sortBegin = timer();
	N = (int) pow( (float)2, (int) ceil(log(N)/log(2)));
	printf("MergeSort size %d\n", N);
    checkCudaErrors(cudaMalloc((void **)&d_DstKey, N * sizeof(int)));
    checkCudaErrors(cudaMalloc((void **)&d_BufKey, N * sizeof(int)));
	initMergeSort();
    for (int stride = SHARED_SIZE_LIMIT; stride < N; stride <<= 1, stageCount++);

    int *ikey, *okey;

    if (stageCount & 1) {
        ikey = d_BufKey;
        okey = d_DstKey;
    } else {
        ikey = d_DstKey;
        okey = d_BufKey;
    }

	if(N < 1024) {
		bitonic_sort(indexArray, d_SrcKey, keyLength, N);
		return;
	} else {
	    assert(N <= (SAMPLE_STRIDE * MAX_SAMPLE_COUNT));
		assert(N % SHARED_SIZE_LIMIT == 0);
		mergeSortShared(ikey, indexArray, d_SrcKey, N / SHARED_SIZE_LIMIT, SHARED_SIZE_LIMIT, sortDir, keyLength);
		cudaDeviceSynchronize();
	}



	//int iter =0;
    for (int stride = SHARED_SIZE_LIMIT; stride < N; stride <<= 1) {
        int lastSegmentElements = N % (2 * stride);
		//printf("iter %d\n", iter++);
		
		generateSampleRanksNew(ikey, d_RanksA, d_RanksB, d_SrcKey, stride, N, sortDir, keyLength);
		checkCudaErrors(cudaDeviceSynchronize());
	
        //Merge ranks and indices
        mergeRanksAndIndices(d_LimitsA, d_LimitsB, d_RanksA, d_RanksB, stride, N);
		checkCudaErrors(cudaDeviceSynchronize());

		mergeElementaryIntervalsNew(okey, ikey, d_LimitsA, d_LimitsB, stride, N, sortDir, d_SrcKey, keyLength);
		checkCudaErrors(cudaDeviceSynchronize());

        if (lastSegmentElements <= stride) {
			//printf("In the non-desired condition\n");
            //Last merge segment consists of a single array which just needs to be passed through
            checkCudaErrors(cudaMemcpy(okey + (N - lastSegmentElements), ikey + (N - lastSegmentElements), lastSegmentElements * sizeof(int), cudaMemcpyDeviceToDevice));
            //checkCudaErrors(cudaMemcpy(oval + (N - lastSegmentElements), ival + (N - lastSegmentElements), lastSegmentElements * sizeof(int), cudaMemcpyDeviceToDevice));
        }

        int *t;
        t = ikey;
        ikey = okey;
        okey = t;
    }
	checkCudaErrors(cudaMemcpy(indexArray, d_DstKey, N * sizeof(int), cudaMemcpyDeviceToDevice));
	//indexArray = d_DstKey;
	//int *tmp =
    cudaFree(d_DstKey);
    cudaFree(d_BufKey);
	closeMergeSort();
	printf("Merge Sort time %lf\n", timer() - sortBegin);
}


////////////////////////////////////////////////////////////////////////////////
// Test driver
////////////////////////////////////////////////////////////////////////////////
void check_string_sort()
{
    int  *h_DstKey, *h_indexArray;
    int  *indexArray;
	char *h_SrcKey, *d_SrcKey;
    StopWatchInterface *hTimer = NULL;

    const int   N = 16 * 1048576;
    const int DIR = 1;
    const int numValues = 65536;
	int keyLength = 12;

    printf("Allocating and initializing host arrays...\n\n");
    sdkCreateTimer(&hTimer);
    h_SrcKey = (char *)malloc(N * keyLength * sizeof(char));
    h_DstKey = (int *)malloc(N * sizeof(int));

    h_indexArray = (int *)malloc(N * sizeof(int));

    srand(2009);

	FILE *fp = fopen("d3", "r");
	if(!fp) {
		printf("File not found\n");
		exit(1);
	}
	FILE *fp2 = fopen("ip.txt", "w");
	if(!fp) {
		printf("Input file for copy could not be opened\n");
	}
	char *line;
	size_t nbytes = 1000;
	line = (char*) malloc(nbytes);
	int wordNum = 0;
	int count, start, read;
	char ch;
	while ( (read = getline(&line, &nbytes, fp)) != -1 && wordNum < 16 * 1048576) {
		//printf("%s", line);
		count = 0;
		start = 0;
		while ( count < read - 1 )
		{
			ch = line[count] ;
			if (ch != ' ' && count != read - 2) {
				h_SrcKey[wordNum*keyLength + start] = ch;
				start++;
			} 
			else if (ch == ' ' || count == read - 2) {
				if(count == read - 2) {
					if(ch != ' ' && ch != '\n') {
						h_SrcKey[wordNum*keyLength + start] = ch;
						start++;
					}
				}
				if(start) {
					h_SrcKey[wordNum*keyLength + start++] = '\0';
					while(start < keyLength) {
						h_SrcKey[wordNum*keyLength + start++] = (unsigned char)255;
					}
					fprintf(fp2, "%s\n", &h_SrcKey[wordNum*keyLength]);
					//reset the word start location
					start = 0;
					//increase wordNum
					wordNum++;
				}
			}
			count++;
		}
	}
	fclose(fp2);
	printf("Num Words read %d\n", wordNum);
	for (int i = 0; i < N; i++) {
		int j;
		for(j=0; j < keyLength; j++) {
			if((unsigned char)h_SrcKey[i*keyLength + j] == '\0') {
				break;
			}
		}
		if(j == keyLength) {
			printf("Input data error\n");
			exit(1);
		}
	}


    for (int i = 0; i < N; i++)
    {
        //h_SrcKey[i] = rand() % numValues;
        h_indexArray[i] = i;
		h_DstKey[i] = -1;
    }

    //fillValues(h_SrcVal, N);

    printf("Allocating and initializing CUDA arrays...\n\n");
    checkCudaErrors(cudaMalloc((void **)&d_SrcKey, N * keyLength * sizeof(char)));
    checkCudaErrors(cudaMalloc((void **)&indexArray, N * sizeof(int)));

    checkCudaErrors(cudaMemcpy(d_SrcKey, h_SrcKey, N * keyLength * sizeof(char), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(indexArray, h_indexArray, N * sizeof(int), cudaMemcpyHostToDevice));

    printf("Initializing GPU merge sort...\n");

    printf("Running GPU merge sort...\n");
    checkCudaErrors(cudaDeviceSynchronize());
    sdkResetTimer(&hTimer);
    sdkStartTimer(&hTimer);
    mergeSort(
    	indexArray,
        d_SrcKey,
       	keyLength,
		N
    );
    checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&hTimer);
    printf("Time: %f ms\n", sdkGetTimerValue(&hTimer));

    printf("Reading back GPU merge sort results...\n");
    checkCudaErrors(cudaMemcpy(h_DstKey, indexArray, N * sizeof(int), cudaMemcpyDeviceToHost));
    //checkCudaErrors(cudaMemcpy(h_DstVal, d_DstVal, N * sizeof(int), cudaMemcpyDeviceToHost));

	/*
	//h_DstKey has references; convert them to values
	for (int i = 0; i < N; i++) {
		h_DstKey[i] = h_SrcKey[h_DstKey[i]];
	}
	*/

    printf("Inspecting the results...\n");
    int keysFlag = validateSortedKeys(
                        h_DstKey,
                        h_SrcKey,
                        1,
                        N,
                        numValues,
                        DIR,
						keyLength
                    );

    printf("Shutting down...\n");
    sdkDeleteTimer(&hTimer);
    checkCudaErrors(cudaFree(indexArray));
    checkCudaErrors(cudaFree(d_SrcKey));
    free(h_DstKey);
    free(h_SrcKey);
    cudaDeviceReset();

}


void check_int_sort()
{
    int *h_SrcKey, *h_DstKey, *h_indexArray;
    int *d_SrcKey, *indexArray;
    StopWatchInterface *hTimer = NULL;

    const int   N = 16 * 1048576;
    const int DIR = 1;
    const int numValues = 65536;
	int keyLength = 1;
    printf("Allocating and initializing host arrays...\n\n");
    sdkCreateTimer(&hTimer);
    h_SrcKey = (int *)malloc(N * sizeof(int));
    h_DstKey = (int *)malloc(N * sizeof(int));

    h_indexArray = (int *)malloc(N * sizeof(int));

    srand(2009);
	FILE *fp = fopen("ip.txt", "w");
	if(!fp) {
		printf("Output file could not be opened\n");
	}
    for (int i = 0; i < N; i++)
    {
        h_SrcKey[i] = rand() % numValues;
		//h_SrcKey[i] = 1;
        h_indexArray[i] = i;
		fprintf(fp, "%d\n", h_SrcKey[i]);
    }

	fclose(fp);

    printf("Allocating and initializing CUDA arrays...\n\n");
    checkCudaErrors(cudaMalloc((void **)&d_SrcKey, N * sizeof(int)));
    checkCudaErrors(cudaMalloc((void **)&indexArray, N * sizeof(int)));

    checkCudaErrors(cudaMemcpy(d_SrcKey, h_SrcKey, N * sizeof(int), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(indexArray, h_indexArray, N * sizeof(int), cudaMemcpyHostToDevice));

    printf("Initializing GPU merge sort...\n");

    printf("Running GPU merge sort...\n");
    checkCudaErrors(cudaDeviceSynchronize());
    sdkResetTimer(&hTimer);
    sdkStartTimer(&hTimer);
    mergeSort(
    	indexArray,
        d_SrcKey,
		keyLength,
        N
    );
    checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&hTimer);
    printf("Time: %f ms\n", sdkGetTimerValue(&hTimer));

    printf("Reading back GPU merge sort results...\n");
    checkCudaErrors(cudaMemcpy(h_DstKey, indexArray, N * sizeof(int), cudaMemcpyDeviceToHost));
    //checkCudaErrors(cudaMemcpy(h_DstVal, d_DstVal, N * sizeof(int), cudaMemcpyDeviceToHost));

	//h_DstKey has references; convert them to values
	for (int i = 0; i < N; i++) {
		h_DstKey[i] = h_SrcKey[h_DstKey[i]];
	}
	

    printf("Inspecting the results...\n");
    int keysFlag = validateSortedKeys(
                        h_DstKey,
                        h_SrcKey,
                        1,
                        N,
                        numValues,
                        DIR
                    );

    printf("Shutting down...\n");
    sdkDeleteTimer(&hTimer);
    checkCudaErrors(cudaFree(indexArray));
    checkCudaErrors(cudaFree(d_SrcKey));
    free(h_DstKey);
    free(h_SrcKey);
    cudaDeviceReset();

}




void check_long_sort()
{
    long *h_SrcKey, *h_DstKey;
	int *h_indexArray, *h_DstKeyPtr, *indexArray;
    long *d_SrcKey ;
    StopWatchInterface *hTimer = NULL;

    const int   N = 16 * 1048576;
    const int DIR = 1;
    const int numValues = 65536;
	int keyLength = 1;
    printf("Allocating and initializing host arrays...\n\n");
    sdkCreateTimer(&hTimer);
    h_SrcKey = (long *)malloc(N * sizeof(long));
	h_DstKeyPtr = (int *)malloc(N * sizeof(int));
    h_DstKey = (long *)malloc(N * sizeof(long));

    h_indexArray = (int *)malloc(N * sizeof(int));

    srand(2009);
	
    for (int i = 0; i < N; i++)
    {
        h_SrcKey[i] = rand() % numValues;
		//h_SrcKey[i] = 1;
        h_indexArray[i] = i;
    }


    printf("Allocating and initializing CUDA arrays...\n\n");
    checkCudaErrors(cudaMalloc((void **)&d_SrcKey, N * sizeof(long)));
    checkCudaErrors(cudaMalloc((void **)&indexArray, N * sizeof(int)));

    checkCudaErrors(cudaMemcpy(d_SrcKey, h_SrcKey, N * sizeof(long), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(indexArray, h_indexArray, N * sizeof(int), cudaMemcpyHostToDevice));

    printf("Initializing GPU merge sort...\n");

    printf("Running GPU merge sort...\n");
    checkCudaErrors(cudaDeviceSynchronize());
    sdkResetTimer(&hTimer);
    sdkStartTimer(&hTimer);
    mergeSort(
    	indexArray,
        d_SrcKey,
		keyLength,
        N
    );
    checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&hTimer);
    printf("Time: %f ms\n", sdkGetTimerValue(&hTimer));

    printf("Reading back GPU merge sort results...\n");
    checkCudaErrors(cudaMemcpy(h_DstKeyPtr, indexArray, N * sizeof(int), cudaMemcpyDeviceToHost));
    //checkCudaErrors(cudaMemcpy(h_DstVal, d_DstVal, N * sizeof(int), cudaMemcpyDeviceToHost));

	//h_DstKey has references; convert them to values
	for (int i = 0; i < N; i++) {
		h_DstKey[i] = h_SrcKey[h_DstKeyPtr[i]];
	}
	

    printf("Inspecting the results...\n");
    int keysFlag = validateSortedKeys(
                        h_DstKey,
                        h_SrcKey,
                        1,
                        N,
                        numValues,
                        DIR
                    );

    printf("Shutting down...\n");
    sdkDeleteTimer(&hTimer);
    checkCudaErrors(cudaFree(indexArray));
    checkCudaErrors(cudaFree(d_SrcKey));
    free(h_DstKey);
	free(h_DstKeyPtr);
    free(h_SrcKey);
    cudaDeviceReset();

}


////////////////////////////////////////////////////////////////////////////////
// Test driver
////////////////////////////////////////////////////////////////////////////////
void check_string_sort2()
{
    int  *h_DstKey, *h_indexArray;
    int  *indexArray;
	char *h_SrcKey, *d_SrcKey;
    StopWatchInterface *hTimer = NULL;

	FILE *fpInd, *fpData, *fpCheck;
	fpInd = fopen("/lustre/medusa/asabne/indFile.txt", "r");
	fpData = fopen("/lustre/medusa/asabne/dataFile.txt", "r");
	fpCheck = fopen("/lustre/medusa/asabne/checkFile.txt", "w");
	if(!fpInd || !fpData || !fpCheck) {
		printf("File open error\n");
		exit(1);
	}
	
    const int   N = 16 * 1048576;
    const int DIR = 1;
    const int numValues = 65536;
	const int storeSize = 67108864;
	int keyLength = 30;

    printf("Allocating and initializing host arrays...\n\n");
    sdkCreateTimer(&hTimer);
    h_SrcKey = (char *)malloc(storeSize * keyLength * sizeof(char));
    h_DstKey = (int *)malloc(storeSize * sizeof(int));

    h_indexArray = (int *)malloc(storeSize * sizeof(int));

    srand(2009);
	int temp;
	int ptr=0;
	while (fscanf(fpData, "%d", &temp) != EOF) {
	  char chopped = temp & 0xFF;
	  //fprintf(fpCheck, "%d\n", chopped);
	  h_SrcKey[ptr++] = chopped;	
	}	

	ptr=0;
	while (fscanf(fpInd, "%d", &temp) != EOF) {
	  //fprintf(fpCheck, "%d\n", chopped);
	  h_indexArray[ptr++] = temp;	
	}	

	fclose(fpInd);
	fclose(fpData);
	fclose(fpCheck);
    printf("File read done...\n\n");
    //fillValues(h_SrcVal, N);

    printf("Allocating and initializing CUDA arrays...\n\n");
    checkCudaErrors(cudaMalloc((void **)&d_SrcKey, storeSize * keyLength * sizeof(char)));
    checkCudaErrors(cudaMalloc((void **)&indexArray, storeSize * sizeof(int)));

    checkCudaErrors(cudaMemcpy(d_SrcKey, h_SrcKey, storeSize * keyLength * sizeof(char), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(indexArray, h_indexArray, storeSize * sizeof(int), cudaMemcpyHostToDevice));

    printf("Initializing GPU merge sort...\n");
    initMergeSort();

    printf("Running GPU merge sort...\n");
    checkCudaErrors(cudaDeviceSynchronize());
    sdkResetTimer(&hTimer);
    sdkStartTimer(&hTimer);
    mergeSort(
    	indexArray,
        d_SrcKey,
       	keyLength,
		N
    );
    checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&hTimer);
    printf("Time: %f ms\n", sdkGetTimerValue(&hTimer));

    printf("Reading back GPU merge sort results...\n");
    checkCudaErrors(cudaMemcpy(h_DstKey, indexArray, N * sizeof(int), cudaMemcpyDeviceToHost));
    //checkCudaErrors(cudaMemcpy(h_DstVal, d_DstVal, N * sizeof(int), cudaMemcpyDeviceToHost));

	/*
	//h_DstKey has references; convert them to values
	for (int i = 0; i < N; i++) {
		h_DstKey[i] = h_SrcKey[h_DstKey[i]];
	}
	*/

    printf("Inspecting the results...\n");
    int keysFlag = validateSortedKeys(
                        h_DstKey,
                        h_SrcKey,
                        1,
                        N,
                        numValues,
                        DIR,
						keyLength
                    );

    printf("Shutting down...\n");
    closeMergeSort();
    sdkDeleteTimer(&hTimer);
    checkCudaErrors(cudaFree(indexArray));
    checkCudaErrors(cudaFree(d_SrcKey));
    free(h_DstKey);
    free(h_SrcKey);
    cudaDeviceReset();

}

//randomly generated keys
void check_string_sort3()
{
    int  *h_DstKey, *h_indexArray;
    int  *indexArray;
	char *h_SrcKey, *d_SrcKey;
    StopWatchInterface *hTimer = NULL;

    const int   N = 16 * 1048576;
    const int DIR = 1;
    const int numValues = 65536;
	int keyLength = 60 ;

    printf("Allocating and initializing host arrays...\n\n");
    sdkCreateTimer(&hTimer);
    h_SrcKey = (char *)malloc(N * keyLength * sizeof(char));
    h_DstKey = (int *)malloc(N * sizeof(int));

    h_indexArray = (int *)malloc(N * sizeof(int));

    srand(2009);


	char *line;
	size_t nbytes = 1000;
	line = (char*) malloc(nbytes);
	int wordNum = 0;
	int count, start, read;
	char ch;
	while (wordNum < N) {
		//printf("%s", line);
		int i = 0;
		int thisKeyLength = rand() % (keyLength - 2);
	    for (i = 0; i < thisKeyLength ; i++)
		{
		    h_SrcKey[wordNum*keyLength + i] = rand() % 65 + 65;
		}
		h_SrcKey[wordNum*keyLength + i] = '\0';
		wordNum++;
	}
	printf("Num Words read %d\n", wordNum);
	for (int i = 0; i < N; i++) {
		int j;
		for(j=0; j < keyLength; j++) {
			if((unsigned char)h_SrcKey[i*keyLength + j] == '\0') {
				break;
			}
		}
		if(j == keyLength) {
			printf("Input data error\n");
			exit(1);
		}
	}
	

    for (int i = 0; i < N; i++)
    {
        //h_SrcKey[i] = rand() % numValues;
        h_indexArray[i] = i;
		h_DstKey[i] = -1;
    }

    //fillValues(h_SrcVal, N);

    printf("Allocating and initializing CUDA arrays...\n\n");
    checkCudaErrors(cudaMalloc((void **)&d_SrcKey, N * keyLength * sizeof(char)));
    checkCudaErrors(cudaMalloc((void **)&indexArray, N * sizeof(int)));

    checkCudaErrors(cudaMemcpy(d_SrcKey, h_SrcKey, N * keyLength * sizeof(char), cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(indexArray, h_indexArray, N * sizeof(int), cudaMemcpyHostToDevice));

    printf("Initializing GPU merge sort...\n");
	
    printf("Running GPU merge sort...\n");
    checkCudaErrors(cudaDeviceSynchronize());
    sdkResetTimer(&hTimer);
    sdkStartTimer(&hTimer);
    mergeSort(
    	indexArray,
        d_SrcKey,
       	keyLength,
		N
    );
    checkCudaErrors(cudaDeviceSynchronize());
    sdkStopTimer(&hTimer);
    printf("Time: %f ms\n", sdkGetTimerValue(&hTimer));
	
    printf("Reading back GPU merge sort results...\n");
    checkCudaErrors(cudaMemcpy(h_DstKey, indexArray, N * sizeof(int), cudaMemcpyDeviceToHost));

    printf("Inspecting the results...\n");
    int keysFlag = validateSortedKeys(
                        h_DstKey,
                        h_SrcKey,
                        1,
                        N,
                        numValues,
                        DIR,
						keyLength
                    );
    printf("Shutting down...\n");
    sdkDeleteTimer(&hTimer);
    checkCudaErrors(cudaFree(indexArray));
    checkCudaErrors(cudaFree(d_SrcKey));
    free(h_DstKey);
    free(h_SrcKey);
    cudaDeviceReset();
}

