#include <string>
#include <sys/time.h>
#include <math.h>


#include "mr.h"

#include <errno.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */

using namespace std;

int *locator;


double timer()
{
	struct timeval time;
	double _ret_val_0;
	gettimeofday(( & time), 0);
	_ret_val_0=(time.tv_sec+(time.tv_usec/1000000.0));
	return _ret_val_0;
}


typedef struct stat Stat;

#ifndef lint
/* Prevent over-aggressive optimizers from eliminating ID string */
const char jlss_id_mkpath_c[] = "@(#)$Id: mkpath.c,v 1.13 2012/07/15 00:40:37 jleffler Exp $";
#endif /* lint */

static int do_mkdir(const char *path, mode_t mode)
{
    Stat            st;
    int             status = 0;

    if (stat(path, &st) != 0)
    {
        /* Directory does not exist. EEXIST for race condition */
        if (mkdir(path, mode) != 0 && errno != EEXIST) {
            status = -1;
				}
    }
    else if (!S_ISDIR(st.st_mode))
    {
        errno = ENOTDIR;
        status = -1;
    }

    return(status);
}

/**
** mkpath - ensure all directories in path exist
** Algorithm takes the pessimistic view and works top-down to ensure
** each directory in path exists, rather than optimistically creating
** the last element and working backwards.
*/
int mkpath(char *path, mode_t mode)
{
    char           *pp;
    char           *sp;
    int             status;
    char           *copypath = (path);

    status = 0;
    pp = copypath;

    while (status == 0 && (sp = strchr(pp, '/')) != 0)
    {
        if (sp != pp)
        {
            /* Neither root nor double slash in path */
            *sp = '\0';
            status = do_mkdir(copypath, mode);
            *sp = '/';
        }
        pp = sp + 1;
    }

    if (status == 0)
        status = do_mkdir(path, mode);
    //FREE(copypath);
    return (status);
}


static unsigned int crc_table[] = {
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
	0xe963a535, 0x9e6495a3,	0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
	0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
	0xf3b97148, 0x84be41de,	0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
	0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,	0x14015c4f, 0x63066cd9,
	0xfa0f3d63, 0x8d080df5,	0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
	0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,	0x35b5a8fa, 0x42b2986c,
	0xdbbbc9d6, 0xacbcf940,	0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
	0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
	0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
	0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,	0x76dc4190, 0x01db7106,
	0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
	0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
	0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
	0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
	0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
	0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
	0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
	0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
	0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
	0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
	0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
	0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
	0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
	0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
	0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
	0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
	0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
	0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
	0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
	0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
	0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
	0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
	0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
	0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
	0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
	0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
	0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
	0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
	0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
	0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
	0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};

__device__ __forceinline__ int bit(int val, int pos) {
	return val & (1 << pos);
}


/*
__device__ __forceinline__ int getKeyVal(char *word, char *key, int *value, int *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength) {
	if(ptr >= high) {
		return -1;
	}else {
		int i;
		for(i=0; i<mapKeyLength; i++) {
			word[i] = key[mapKeyLength*indexArray[ptr] + i];
			if(word[i] == '\0')
				break;
		}
		for(i=0; i<mapValLength; i++) {
			value[i] = val[mapValLength*indexArray[ptr] + i];
		}
		ptr++;
	}
	return 0;
}
*/

#if defined(OPT_ON) && defined(COMB_VECT)
//Assumption : blockSize is 32 for the combiner
__device__ __forceinline__ int getKeyVal(char *word, char *key, int *value, int *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength) {
	if(ptr >= high) {
		return -1;
	}else {
		int i = threadIdx.x;
		while(i<mapKeyLength) {
			word[i] = key[mapKeyLength*indexArray[ptr] + i];
			i +=32;
		}

		i = threadIdx.x;
		while(i<mapValLength) {
				value[i] = val[mapValLength*indexArray[ptr] + i];
			i +=32;
		}

		ptr++;
	}
	return 0;
}

#else
//non-vectorized version
__device__ __forceinline__ int getKeyVal(char *word, char *key, int *value, int *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength) {
	if(ptr >= high) {
		return -1;
	}else {
		int i;
		for(i=0; i<mapKeyLength; i++) {
			word[i] = key[mapKeyLength*indexArray[ptr] + i];
		}

		for(i=0; i<mapValLength; i++) {
			value[i] = val[mapValLength*indexArray[ptr] + i];
		}

		ptr++;
	}
	return 0;
}
#endif

__device__ __forceinline__ int getKeyVal(int *word, int *key, int *value, int *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength) {
	if(ptr >= high) {
		return -1;
	}else {
		int i;
		for(i=0; i<mapKeyLength; i++) {
			word[i] = key[mapKeyLength*indexArray[ptr] + i];
		}
		for(i=0; i<mapValLength; i++) {
			value[i] = val[mapValLength*indexArray[ptr] + i];
		}
		ptr++;
	}
	return 0;
}

__device__ __forceinline__ int getKeyVal(int *word, int *key, long *value, long *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength) {
	if(ptr >= high) {
		return -1;
	}else {
		int i;
		for(i=0; i<mapKeyLength; i++) {
			word[i] = key[mapKeyLength*indexArray[ptr] + i];
		}
		for(i=0; i<mapValLength; i++) {
			value[i] = val[mapValLength*indexArray[ptr] + i];
		}
		ptr++;
	}
	return 0;
}

#if defined(OPT_ON) && defined(COMB_VECT)
//Assumption : blockSize is 32 for the combiner
__device__ __forceinline__ int getKeyVal(int *word, int *key, char *value, char *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength) {
	if(ptr >= high) {
		return -1;
	}else {
		int i = threadIdx.x;
		while(i<mapKeyLength) {
			word[i] = key[mapKeyLength*indexArray[ptr] + i];
			i +=32;
		}

		i = threadIdx.x;
		while(i<mapValLength) {
				value[i] = val[mapValLength*indexArray[ptr] + i];
			i +=32;
		}

		ptr++;
	}
	return 0;
}
#else

//non-vectorized version
__device__ __forceinline__ int getKeyVal(int *word, int *key, char *value, char *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength) {
	if(ptr >= high) {
		return -1;
	}else {
		int i;
		for(i=0; i<mapKeyLength; i++) {
			word[i] = key[mapKeyLength*indexArray[ptr] + i];
		}
		for(i=0; i<mapValLength; i++) {
			value[i] = val[mapValLength*indexArray[ptr] + i];
			if(value[i] == '\0')
				break;
		}
		ptr++;
	}
	return 0;
}
#endif

#if defined(OPT_ON) && defined(COMB_VECT)
//Vectorized version. Assumption : blockSize is 32 for the combiner
__device__ __forceinline__ int getKeyVal(char *word, char *key, char *value, char *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength) {
	if(ptr >= high) {
		return -1;
	}else {
		int i = threadIdx.x;
		while(i<mapKeyLength) {
			word[i] = key[mapKeyLength*indexArray[ptr] + i];
			i +=32;
		}

		i = threadIdx.x;
		while(i<mapValLength) {
				value[i] = val[mapValLength*indexArray[ptr] + i];
			i +=32;
		}

		ptr++;
	}
	return 0;
}
#else
//non-vectorized version
__device__ __forceinline__ int getKeyVal(char *word, char *key, char *value, char *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength) {
	if(ptr >= high) {
		return -1;
	}else {
		int i;
		for(i=0; i<mapKeyLength; i++) {
			word[i] = key[mapKeyLength*indexArray[ptr] + i];
			if(word[i] == '\0')
				break;
		}
		for(i=0; i<mapValLength; i++) {
			value[i] = val[mapValLength*indexArray[ptr] + i];
			if(value[i] == '\0')
				break;
		}
		ptr++;
	}
	return 0;
}
#endif

#if defined(OPT_ON) && defined(COMB_VECT)
//vectorized version; assumes that the blocksize is 32
__device__ __forceinline__ void storeKV(char *prevWord, int *count, int combKeyLength, int combValLength, char *opKey, int *opVal, int &index, int &kvCount) {
	int i = threadIdx.x;
	while(i<combKeyLength) {
		opKey[index*combKeyLength + i] = prevWord[i];
		i +=32;
	}
	i = threadIdx.x;
	while(i<combValLength) {
		opVal[index*combValLength + i] = count[i]; 
		i +=32;
	}
	
	//printf("store key %s val %d", opKey[index*combKeyLength], opVal[index*combValLength]);
	index++;
	kvCount++;
		
}
#else

//non-vectorized version
__device__ __forceinline__ void storeKV(char *prevWord, int *count, int combKeyLength, int combValLength, char *opKey, int *opVal, int &index, int &kvCount) {
	int i;
	for(i=0; i<combKeyLength; i++) {
		opKey[index*combKeyLength + i] = prevWord[i]; 
		if(prevWord[i] == '\0')
			break;
	}
	for(i=0; i<combValLength; i++) {
		opVal[index*combValLength + i] = count[i]; 
	}
	//printf("store key %s val %d", opKey[index*combKeyLength], opVal[index*combValLength]);
	index++;
	kvCount++;
		
}
#endif

__device__ __forceinline__ void storeKV(int *prevWord, int *count, int combKeyLength, int combValLength, int *opKey, int *opVal, int &index, int &kvCount) {
	int i;
	for(i=0; i<combKeyLength; i++) {
		opKey[index*combKeyLength + i] = prevWord[i]; 
		//printf("storeKV key %d\n", prevWord[i]);
	}
	
	for(i=0; i<combValLength; i++) {
		opVal[index*combValLength + i] = count[i]; 
	}
	//printf("store key %s val %d", opKey[index*combKeyLength], opVal[index*combValLength]);
	index++;
	kvCount++;
		
}


__device__ __forceinline__ void storeKV(int *prevWord, long *count, int combKeyLength, int combValLength, int *opKey, long *opVal, int &index, int &kvCount) {
	int i;
	for(i=0; i<combKeyLength; i++) {
		opKey[index*combKeyLength + i] = prevWord[i]; 
		//printf("storeKV key %d\n", prevWord[i]);
	}
	
	for(i=0; i<combValLength; i++) {
		opVal[index*combValLength + i] = count[i]; 
		//printf("storeKV val %ld\n", count[i]);
	}
	//printf("store key %s val %d", opKey[index*combKeyLength], opVal[index*combValLength]);
	index++;
	kvCount++;
		
}

#if defined(OPT_ON) && defined(COMB_VECT)
//vectorized version; assumes that the blocksize is 32
__device__ __forceinline__ void storeKV(int *prevWord, char *count, int combKeyLength, int combValLength, int *opKey, char *opVal, int &index, int &kvCount) {
	int i = threadIdx.x;
	while(i<combKeyLength) {
		opKey[index*combKeyLength + i] = prevWord[i];
		i +=32;
	}
	i = threadIdx.x;
	while(i<combValLength) {
		opVal[index*combValLength + i] = count[i]; 
		i +=32;
	}
	
	index++;
	kvCount++;
}
#else
//non-vectorized version
__device__ __forceinline__ void storeKV(int *prevWord, char *count, int combKeyLength, int combValLength, int *opKey, char *opVal, int &index, int &kvCount) {
	int i;
	for(i=0; i<combKeyLength; i++) {
		opKey[index*combKeyLength + i] = prevWord[i]; 
	}
	
	for(i=0; i<combValLength; i++) {
		opVal[index*combValLength + i] = count[i];
		if(count[i] == '\0')
			break; 
	}
	index++;
	kvCount++;
		
}
#endif

#if defined(OPT_ON) && defined(COMB_VECT)
//vectorized version; assumes that the blocksize is 32
__device__ __forceinline__ void storeKV(char *prevWord, char *count, int combKeyLength, int combValLength, char *opKey, char *opVal, int &index, int &kvCount) {
	int i = threadIdx.x;
	while(i<combKeyLength) {
		opKey[index*combKeyLength + i] = prevWord[i];
		i +=32;
	}
	i = threadIdx.x;
	while(i<combValLength) {
		opVal[index*combValLength + i] = count[i]; 
		i +=32;
	}
	//printf("store key %s val %d", opKey[index*combKeyLength], opVal[index*combValLength]);
	index++;
	kvCount++;
}
#else
//non-vectorized version
__device__ __forceinline__ void storeKV(char *prevWord, char *count, int combKeyLength, int combValLength, char *opKey, char *opVal, int &index, int &kvCount) {
	int i;
	for(i=0; i<combKeyLength; i++) {
		opKey[index*combKeyLength + i] = prevWord[i]; 
		if(prevWord[i] == '\0')
			break;
	}
	for(i=0; i<combValLength; i++) {
		opVal[index*combValLength + i] = count[i]; 
		if(count[i] == '\0')
			break;
	}
	//printf("store key %s val %d", opKey[index*combKeyLength], opVal[index*combValLength]);
	index++;
	kvCount++;
}
#endif

/*
//NOT GOING TO WORK SINCE THE STRINGS ARE NOT EMPTY EARLIER ON
__device__ __forceinline__ int strcmp(char *s1, char *s2, int length) {

	int i = 31 - threadIdx.x;
	int count = 0;
	__shared__ unsigned int greater;
	__shared__ unsigned int smaller;
	while(count*32 < length) {
		greater = __ballot((unsigned char)s1[count*32 + i] > (unsigned char)s2[count*32 + i] && count*32 + i < length );
		smaller = __ballot((unsigned char)s1[count*32 + i] < (unsigned char)s2[count*32 + i] && count*32 + i < length );
		if(greater > smaller) {
			if(threadIdx.x + blockIdx.x * blockDim.x == 1) {
				printf("greater %s smaller %s\n", s1, s2);
			}
			return 1;
		} else if(greater == smaller) {
			count++;
			continue;
		} else {
			//smaller
			if(threadIdx.x + blockIdx.x * blockDim.x == 1) {
				printf("smaller %s greater %s smaller %x greater %x\n", s1, s2, smaller, greater);
			}
			return -1;
		}

		//i += 32;
	}
	if(greater == smaller)
		return 0;
}
*/


__device__ __forceinline__ int strcmp(char *s1, char *s2, int length) {
	int i;
	for(i =0 ; i< length; i++) {
		if(s1[i] > s2[i]) {
			return 1;
		}
		else if (s1[i] < s2[i]) {
			return -1;
		}
		else if(s1[i] == '\0') {
			return 0;
		}
	}

	return 0;
}



__device__ __forceinline__ void strcpy(char *s1, char *s2, int length) {
	int i;
	for(i =0 ; i< length; i++) {
		s1[i] = s2[i];
		if(s2[i] == '\0') {
			break;
		}
	}
}


#if defined(OPT_ON) && defined(COMB_VECT)
//this is used in the combiner
//assumptions : This is being called from the combiner
__device__ __forceinline__ void strcpy(char *s1, char *s2, int length, int noUse) {
	int i =threadIdx.x;
	int count = 0;
	while(count*32 < length) {
		if(i < length) {
			s1[i] = s2[i];
		}
		i+=32;
		count++;
	}
}
#else
//this is used in the combiner
__device__ __forceinline__ void strcpy(char *s1, char *s2, int length, int noUse) {
	int i;
	for(i =0 ; i< length; i++) {
		s1[i] = s2[i];
		if(s2[i] == '\0') {
			break;
		}
	}
}
#endif

__device__ __forceinline__ int atoiAccelerator(char *str) {
	 int res = 0; // Initialize result
 	int neg=1;
	int i = 0;
	if(str[i] == '-') {
		neg = -1;
		i++;
	}
	
    // Iterate through all characters of input string and update result
    for (; str[i] != '\0'; ++i)
        res = res*10 + str[i] - '0';
 
    // return result.
    return res*neg;
}


__device__ __forceinline__ long atolAccelerator(char *str) {
	long res = 0; // Initialize result
 	int neg=1;
	int i = 0;
	if(str[i] == '-') {
		neg = -1;
		i++;
	}
    // Iterate through all characters of input string and update result
    for (; str[i] != '\0'; ++i)
        res = res*10 + str[i] - '0';
 
    // return result.
    return res*neg;
}

__device__ __forceinline__ float atofAccelerator(char *s) {
	float rez = 0, sign = 1;
	float factor = 1.0;
	int ptr = 0;
	if (s[ptr] == '-'){
		ptr++;
		sign = -1;
	};
	for (int point_seen = 0; s[ptr] != '\0'; ptr++) {
		if (s[ptr] == '.'){
			point_seen = 1;
			continue;
		};
		int d = s[ptr] - '0';
		if (d >= 0 && d <= 9){
			if (point_seen) {
				factor /= 10.0f;
				rez = rez + factor*d;
			}
			else {
				rez = rez * 10.0f + (float)d;
			}
		};
	};
	return rez * sign;
}

__device__ __forceinline__ void setCombinerVars(int &kvsPerThread, int &laneID, int &warpId, int &ptr, int &high, int &kvCount, int &index, int size, int combinerThreads) {
	kvsPerThread = size/(combinerThreads/32) + 1;
	laneID = threadIdx.x & 0x1f;
	warpId = (blockIdx.x*blockDim.x + threadIdx.x)/32;
	ptr = warpId*kvsPerThread;
	high = (warpId+1)*kvsPerThread;
	kvCount = 0;
	//index = (size/(combinerThreads/32))*warpId;
	index = (kvsPerThread)*warpId;

	if (high > size) {
			high = size;
	}
	//printf("COMBINER : kvsPerThread %d high %d tid %d\n", kvsPerThread, high, blockIdx.x*blockDim.x + threadIdx.x);
}

//this kernel is run with 1 threadblock, 32 threads
__global__ void countMapKVpairs(int *kvCount, int threadCount) {

	__shared__ int count[32];
	int tid = threadIdx.x;

	count[tid] = 0;
	
	int localIndex = blockIdx.x*threadCount + tid;
	int end = threadCount+ blockIdx.x*threadCount;
	while(localIndex < end) {
		count[tid] += kvCount[localIndex];
		localIndex += 32;
	}

	__syncthreads();

	if(tid == 0 ) {
		kvCount[blockIdx.x*threadCount] = 0;
		for(int i=0; i<32; i++) {
			kvCount[blockIdx.x*threadCount] += count[i];
		}
	}
	
}

__device__ __forceinline__ void mapSetup(int &start, int &tid, int &index, int &limit, int ipSize, int storesPerThread, char *ip, int * __devKvCount, int numReducers, int totalThreads, int &threadRecordCounter, unsigned int *recordIndex)
{
	tid = blockIdx.x*blockDim.x + threadIdx.x;
	start = tid*limit;
	index = tid*storesPerThread;

	threadRecordCounter = threadIdx.x;

    if(threadIdx.x == 0 ) {
    	*recordIndex = 0;
    }
    __syncthreads();

	//for the last thread
	if(start + limit >= ipSize) {
		limit = ipSize - start;
	}

	//fetch upto next line
	if(tid != 0 && start < ipSize) {
		if(ip[start-1] != '\n') { 
			while(ip[start] != '\n' && start < ipSize) {
				start++;
				limit--;
			}
		
			start++;
			limit--;
		}
	}

	for(int i =0; i< numReducers; i++)
		__devKvCount[i*totalThreads + tid] = 0;

}

__device__ __forceinline__ void mapFinish(int index, int storesPerThread, char *key, int keyLength, int *indexArray, int totalThreads, int numReducers, int *kvCount) {
	int tid = blockIdx.x*blockDim.x + threadIdx.x;

	int indicesPerReducer = storesPerThread/numReducers;

	
	for(int i=0 ; i< numReducers; i++) {
		int low = kvCount[i*totalThreads + tid];
		int base = i*totalThreads*indicesPerReducer + tid*indicesPerReducer;
		
		for(int j = low; j < indicesPerReducer; j++) {
			//if(tid == 0) 
				//printf("tid %d base %d count %d index %d j %d\n", tid, base, kvCount[i*totalThreads + tid], index, j);			
			key[index*keyLength ]=255;
			key[index*keyLength + 1]='\0';
			indexArray[base + j] = index;
			index++;
		}
	}
	
}

__device__ __forceinline__ void mapFinish(int index, int storesPerThread, int *key, int keyLength, int *indexArray, int totalThreads, int numReducers, int *kvCount) {
	int tid = blockIdx.x*blockDim.x + threadIdx.x;

	int indicesPerReducer = storesPerThread/numReducers;

	
	for(int i=0 ; i< numReducers; i++) {
		int low = kvCount[i*totalThreads + tid];
		int base = i*totalThreads*indicesPerReducer + tid*indicesPerReducer;
		
		for(int j = low; j < indicesPerReducer; j++) {
			//if(tid == 0) 
				//printf("tid %d base %d count %d index %d j %d\n", tid, base, kvCount[i*totalThreads + tid], index, j);			
			//No need to clear the key (i.e. 255) for integers	
			key[index]=INT_MAX;
			//key[index*keyLength + 1]='\0';
			indexArray[base + j] = index;
			index++;
		}
	}

}


__device__ __forceinline__ void mapFinish(int index, int storesPerThread, float *key, int keyLength, int *indexArray, int totalThreads, int numReducers, int *kvCount) {
	int tid = blockIdx.x*blockDim.x + threadIdx.x;

	int indicesPerReducer = storesPerThread/numReducers;


	for(int i=0 ; i< numReducers; i++) {
		int low = kvCount[i*totalThreads + tid];
		int base = i*totalThreads*indicesPerReducer + tid*indicesPerReducer;

		for(int j = low; j < indicesPerReducer; j++) {
			//if(tid == 0)
				//printf("tid %d base %d count %d index %d j %d\n", tid, base, kvCount[i*totalThreads + tid], index, j);
			//No need to clear the key (i.e. 255) for integers
			key[index]=INT_MAX;
			//key[index*keyLength + 1]='\0';
			indexArray[base + j] = index;
			index++;
		}
	}

}

//TODO : Need a better partitioner for char keys
__device__ __forceinline__ int getReducerNum(char *key, int numReducers, int keyLength) {
	if(numReducers == 0) 
		return 1;

	return key[0]%numReducers;
	//return 0;
}

__device__ __forceinline__ int getReducerNum(int *key, int numReducers, int keyLength) {
	if(numReducers == 0) 
		return 1;
	/*
	int singleKey = 0;
	int digit = 1;
	for(int i=keyLength - 1; i >= 0; i--) {
		singleKey += digit*key[i];
		digit *= 10;
	}
		
	return singleKey%numReducers;
	*/

	return key[0]%numReducers;
	//return 0;
}


__device__ __forceinline__ int getReducerNum(float *key, int numReducers, int keyLength) {
	if(numReducers == 0)
		return 1;
	/*
	int singleKey = 0;
	int digit = 1;
	for(int i=keyLength - 1; i >= 0; i--) {
		singleKey += digit*key[i];
		digit *= 10;
	}
	
	return singleKey%numReducers;
	*/
	return ((int)key[0])%numReducers;
	//return 0;
}

#if defined(OPT_ON) && defined(MAP_VECT)
__device__ __forceinline__ void emitKV(char *keyBuffer, int *valBuffer, char *key, int *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray) { //vectorized with char4
	
	int j;
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	char4 *kb = (char4*) keyBuffer;
	char4 *k = (char4*) key;
	for(j=0; j< keyLength/4 + 1; j++) {
		//key[index*keyLength + j] = keyBuffer[j];
		//if(keyBuffer[j] == '\0')
			//break;
		k[index*keyLength/4 + j] = kb[j];
	}
	for(j=0; j< valLength; j++) {
		val[index*valLength + j] = valBuffer[j];
	}
	

	//Assumption : Division is perfect
	int indicesPerReducer = storesPerThread/numReducers;
	int reducer = getReducerNum(keyBuffer, numReducers, keyLength);
	//if(tid == 0)
		//printf("Key %s, reducer %d\n", keyBuffer, reducer);
	int base =  reducer*totalThreads*indicesPerReducer + tid*indicesPerReducer;
	indexArray[base + kvCount[reducer*totalThreads + tid] ] = index;
	index++;
	kvCount[reducer*totalThreads + tid]++;
}
#else
__device__ __forceinline__ void emitKV(char *keyBuffer, int *valBuffer, char *key, int *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray) {
	
	int j;
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	
	for(j=0; j< keyLength; j++) {
		key[index*keyLength + j] = keyBuffer[j];
		//if(keyBuffer[j] == '\0')
			//break;
	}
	for(j=0; j< valLength; j++) {
		val[index*valLength + j] = valBuffer[j];
	}
	

	//Assumption : Division is perfect
	int indicesPerReducer = storesPerThread/numReducers;
	int reducer = getReducerNum(keyBuffer, numReducers, keyLength);
	//if(tid == 0)
		//printf("Key %s, reducer %d\n", keyBuffer, reducer);
	int base =  reducer*totalThreads*indicesPerReducer + tid*indicesPerReducer;
	indexArray[base + kvCount[reducer*totalThreads + tid] ] = index;
	index++;
	kvCount[reducer*totalThreads + tid]++;
}
#endif

#if defined(OPT_ON) && defined(MAP_VECT)
__device__ __forceinline__ void emitKV(int *keyBuffer, char *valBuffer, int *key, char *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray) { //vectorized with char4
	
	int j;
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	
	for(j=0; j< keyLength; j++) {
		key[index*keyLength + j] = keyBuffer[j];
	}

	char4 *vb = (char4*) valBuffer;
	char4 *v = (char4*) val;

	for(j=0; j< valLength/4 + 1; j++) {
		v[index*valLength/4 + j] = vb[j];
	}
	

	//Assumption : Division is perfect
	int indicesPerReducer = storesPerThread/numReducers;
	int reducer = getReducerNum(keyBuffer, numReducers, keyLength);
	//if(tid == 0)
		//printf("Key %s, reducer %d\n", keyBuffer, reducer);
	int base =  reducer*totalThreads*indicesPerReducer + tid*indicesPerReducer;
	indexArray[base + kvCount[reducer*totalThreads + tid] ] = index;
	index++;
	kvCount[reducer*totalThreads + tid]++;
}
#else
__device__ __forceinline__ void emitKV(int *keyBuffer, char *valBuffer, int *key, char *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray) {
	
	int j;
	int tid = blockIdx.x*blockDim.x + threadIdx.x;

	for(j=0; j< keyLength; j++) {
		key[index*keyLength + j] = keyBuffer[j];
	}

	for(j=0; j< valLength; j++) {
		val[index*valLength + j] = valBuffer[j];
		//if(valBuffer[j] == '\0')
			//break;
	}
	
	//Assumption : Division is perfect
	int indicesPerReducer = storesPerThread/numReducers;
	int reducer = getReducerNum(keyBuffer, numReducers, keyLength);
	//if(tid == 0)
		//printf("Key %s, reducer %d\n", keyBuffer, reducer);
	int base =  reducer*totalThreads*indicesPerReducer + tid*indicesPerReducer;
	indexArray[base + kvCount[reducer*totalThreads + tid] ] = index;
	index++;
	kvCount[reducer*totalThreads + tid]++;
}
#endif

__device__ __forceinline__ void emitKV(int *keyBuffer, int *valBuffer, int *key, int *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray) {
	
	int j;
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	for(j=0; j< keyLength; j++) {
		key[index*keyLength + j] = keyBuffer[j];
	}
	for(j=0; j< valLength; j++) {
		val[index*valLength + j] = valBuffer[j];
	}

	//Assumption : Division is perfect
	int indicesPerReducer = storesPerThread/numReducers;
	int reducer = getReducerNum(keyBuffer, numReducers, keyLength);
	//if(tid == 0)
		//printf("Key %s, reducer %d\n", keyBuffer, reducer);
	int base =  reducer*totalThreads*indicesPerReducer + tid*indicesPerReducer;
	indexArray[base + kvCount[reducer*totalThreads + tid] ] = index;
	index++;
	kvCount[reducer*totalThreads + tid]++;
}


__device__ __forceinline__ void emitKV(int *keyBuffer, long *valBuffer, int *key, long *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray) {
	
	int j;
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	for(j=0; j< keyLength; j++) {
		key[index*keyLength + j] = keyBuffer[j];
	}
	for(j=0; j< valLength; j++) {
		val[index*valLength + j] = valBuffer[j];
	}

	//Assumption : Division is perfect
	int indicesPerReducer = storesPerThread/numReducers;
	int reducer = getReducerNum(keyBuffer, numReducers, keyLength);
	//if(tid == 0)
		//printf("Key %s, reducer %d\n", keyBuffer, reducer);
	int base =  reducer*totalThreads*indicesPerReducer + tid*indicesPerReducer;
	indexArray[base + kvCount[reducer*totalThreads + tid] ] = index;
	index++;
	kvCount[reducer*totalThreads + tid]++;
}


__device__ __forceinline__ void emitKV(char *keyBuffer, char *valBuffer, char *key, char *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray) {
	
	int j;
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	for(j=0; j< keyLength; j++) {
		key[index*keyLength + j] = keyBuffer[j];
	}
	for(j=0; j< valLength; j++) {
		val[index*valLength + j] = valBuffer[j];
	}

	//Assumption : Division is perfect
	int indicesPerReducer = storesPerThread/numReducers;
	int reducer = getReducerNum(keyBuffer, numReducers, keyLength);
	//if(tid == 0)
		//printf("Key %s, reducer %d\n", keyBuffer, reducer);
	int base =  reducer*totalThreads*indicesPerReducer + tid*indicesPerReducer;
	indexArray[base + kvCount[reducer*totalThreads + tid] ] = index;
	index++;
	kvCount[reducer*totalThreads + tid]++;
}


__device__ __forceinline__ void emitKV(float *keyBuffer, float *valBuffer, float *key, float *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray) {

	int j;
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	for(j=0; j< keyLength; j++) {
		key[index*keyLength + j] = keyBuffer[j];
	}
	for(j=0; j< valLength; j++) {
		val[index*valLength + j] = valBuffer[j];
	}

	//Assumption : Division is perfect
	int indicesPerReducer = storesPerThread/numReducers;
	int reducer = getReducerNum(keyBuffer, numReducers, keyLength);
	//if(tid == 0)
		//printf("Key %s, reducer %d\n", keyBuffer, reducer);
	int base =  reducer*totalThreads*indicesPerReducer + tid*indicesPerReducer;
	indexArray[base + kvCount[reducer*totalThreads + tid] ] = index;
	index++;
	kvCount[reducer*totalThreads + tid]++;
}


//the 'int &counter' variable is necessary for static partitioning; for dynamic partitioning, it can be skipped
__device__ __forceinline__ int getline(char *ip, int recordsPerThread, unsigned int *lock, int &st, int *recordLocator, int &counter) {
	int mapThreads = blockDim.x;
	int mpaBlocks = gridDim.x;
	int base = recordsPerThread*mapThreads* blockIdx.x;
	int maxRecordsPerBlock = recordsPerThread*mapThreads;
	
	#ifdef OPT_ON
	//DYNAMIC scheduling
	unsigned int index = atomicInc(lock, maxRecordsPerBlock);
	if (index >= maxRecordsPerBlock - 1) {
		return -1;
	} else {
		if(recordLocator[base + index] != 0xFFFFFFFF) {
			st = recordLocator[base + index];
			int read = 0;
			while(ip[st + read] != '\n') {
				read++;
			}
			return read+1;
		}
		else {
			return -1;
		}
	}
	#else
	//STATIC scheduling
	if(counter < maxRecordsPerBlock) {
		if(recordLocator[base + counter] != 0xFFFFFFFF) {
			st = recordLocator[base + counter];
			int read = 0;
			while(ip[st + read] != '\n') {
				read++;
			}
			counter += mapThreads;
			//getline function in C returns the value of chars read + the newline char
			return read+1;
		} else {
			return -1;
		}
	} else {
		return -1;
	}
	#endif
}

__device__ __forceinline__ int getline(char *ip, int ipSize, int &st, int &limit, int read) {
	
	st += read;
	limit -= read;
	if(st >= ipSize || limit <= 0)
		return -1;

	int count = 1;
	int start = st;
	while(ip[start] != '\n' && start < ipSize) {
		start++;
		count++;
	}

	return count;
}

int getFileSize(char *name, char **ip) {
	FILE *fp;
	int sz;
	fp = fopen (name, "r" ) ;
	if(fp == NULL) {
		printf("File could not be opened. Exiting\n");
		exit(1);
	}
	fseek(fp, 0L, SEEK_END);
	sz = ftell(fp); 
	fseek(fp, 0L, SEEK_SET);
	//cudaMallocHost((void**)ip, sz);
	if(cudaHostAlloc(ip, sz, cudaHostAllocDefault) != cudaSuccess) {
		printf("Host Alloc failure in getFileSize for mem size %ld MB\n", ((long)sz)/(1024*1024) );
	}
	fread(*ip, 1, sz, fp);
	fclose (fp); 
	return sz;
}


//Depracated: use getFileSizeHDFS instead
//This function uses hdfsClient to get the input file
int getFileSizeHDFS_lock_based(char *name, char **ip, hdfsFS fs) {
	//printf("Reading from HDFS path %s\n", name);
  int count = 0;	
  while(count < 3) {
	  if(*name == '/') {
		  count++;
	  }
	  name++;
  }	
  name--;  

	int sz = -1;
	int attempt = 0;
	double begin = timer();
	while(attempt < HDFS_ATTEMPT_LIMIT && sz == -1){
		attempt++;
		//getLock	
		printf("Trying to get HDFS lock %s\n", str);	

		struct flock fl = {F_WRLCK, SEEK_SET,   0,      0,     0 };
		int fd;

		fl.l_pid = getpid();

		if ((fd = open(str, O_RDWR)) == -1) {
			  perror("HDFS lock");
			  exit(1);
		}


		if (fcntl(fd, F_SETLKW, &fl) == -1) {
			  perror("fcntl");
			  exit(1);
		}

		printf("Got HDFS lock\n");

		sprintf(driverNum, "%d", getDriverNum());
		//write on the shared segments for HDFS client
		sprintf(filepath, name);
		sprintf(requesterPID, "%d", getpid());

		pid_t hdfspid = atoi(clientPID);

		
		printf("Client pid %d\n", hdfspid);
		//wake up the hdfsClient
		kill(hdfspid, SIGUSR1);

	

		//int i = 0;
		//wait for hdfsClient to send a sigusr2
		while(!hdfsDone) {
			/*		
			i++;
			if(i== 1000000) {
				printf("Waiting on hdfs\n");
				i=0;
			}
			*/
			usleep(10000);
		}
		hdfsDone = 0;

		fl.l_type = F_UNLCK;  /* set to unlock same region */

		if (fcntl(fd, F_SETLK, &fl) == -1) {
		  perror("fcntl");
		  exit(1);
		}

		printf("Returned HDFS lock\n");


		sz = atoi(bytesRead);
		printf("HDFS Bytes read = %d\n", sz);
	}

	if(cudaHostAlloc(ip, FILESIZE_MAX, cudaHostAllocDefault) != cudaSuccess) {
		printf("Host Alloc failure in getFileSizeHDFS for mem size %ld MB\n", ((long)FILESIZE_MAX)/(1024*1024));
	}

	memcpy(*ip, file0, sz);
	printf("HDFS file read time %lf\n", timer() - begin);

	/*
		//Return the lock
	sb.sem_op = 1; 
	if (semop(semidSM, &sb, 1) == -1) {
		perror("semop");
		exit(1);
	}
	*/
	


	/*
	double begin = timer();
  hdfsFile readFile = hdfsOpenFile(fs, name, O_RDONLY, 0, 0, 0);
  if(!readFile) {
    fprintf(stderr, "Failed to open %s for writing!\n", name);
    exit(-1);
  }
  
  if(cudaHostAlloc(ip, FILESIZE_MAX, cudaHostAllocDefault) != cudaSuccess) {
		printf("Host Alloc failure in getFileSize for mem size %ld MB\n", ((long)FILESIZE_MAX)/(1024*1024));
	}
  int sz = hdfsRead(fs, readFile, *ip, FILESIZE_MAX);
	printf("HDFS file read time %lf\n", timer() - begin);  

	hdfsCloseFile(fs, readFile);
	*/

	return sz;
}


//This function directly reads file from the HDFS, without relying on the hdfsClient
int getFileSizeHDFS(char *name, char **ip, hdfsFS fs) {
	//printf("Reading from HDFS path %s\n", name);
  int count = 0;	
  while(count < 3) {
	  if(*name == '/') {
		  count++;
	  }
	  name++;
  }	
  name--;  

	int sz = -1;
	double begin = timer();
	
	if(cudaHostAlloc(ip, FILESIZE_MAX, cudaHostAllocDefault) != cudaSuccess) {
		printf("Host Alloc failure in getFileSizeHDFS for mem size %ld MB\n", ((long)FILESIZE_MAX)/(1024*1024));
	}
	
	hdfsFile readFile = hdfsOpenFile(fs, name, O_RDONLY, 0, 0, 0);
	if(!readFile) {
		fprintf(stderr, "Failed to open %s for reading!\n", filepath);
		exit(1);
	}

	sz = hdfsRead(fs, readFile, *ip, FILESIZE_MAX);
	hdfsCloseFile(fs, readFile);

	printf("HDFS file read time %lf\n", timer() - begin);

	return sz;
}


void allocateMapData(void **devKey, void **devVal, void **key, void **val, void **devIp, void **devKvCount, void **devIndexArray, void ** newDevIndexArray, void **indexArray, int keyLength, int valLength, int sizeofKeyType, int sizeofValType, int kvStoreSize, int sz, int totalThreads, int numReducers) {
	printf("KVstoresize %d\n", kvStoreSize);
	if(cudaHostAlloc(key, kvStoreSize*keyLength*sizeofKeyType, cudaHostAllocDefault) != cudaSuccess) {
		printf("storesize %d keylength %d sizeofkey %d\n", kvStoreSize, keyLength, sizeofKeyType);
		printf("Host Alloc failure in allocateMapData for size %dMB\n", kvStoreSize*keyLength*sizeofKeyType/1024/1024);
	}
	if(cudaHostAlloc(val, kvStoreSize*valLength*sizeofValType, cudaHostAllocDefault)!= cudaSuccess) {
		printf("Host Alloc failure in allocateMapData\n");
	}
	checkCudaErrors(cudaMalloc((void**)devKey, kvStoreSize*keyLength*sizeofKeyType));
	//getLastCudaError("devKey allocation on GPU failed\n");
	checkCudaErrors(cudaMemset(*devKey, 0x0, kvStoreSize*keyLength*sizeofKeyType));
	checkCudaErrors(cudaMalloc((void**)devVal, kvStoreSize*valLength*sizeofValType));
	//getLastCudaError("devVal allocation on GPU failed\n");
	checkCudaErrors(cudaMalloc(((void**)(devIp)), sz));
	//getLastCudaError("devIp allocation on GPU failed\n");
	checkCudaErrors(cudaMalloc(((void**)(devKvCount)), numReducers*totalThreads*sizeof(int)));
	//getLastCudaError("devKvCount allocation on GPU failed\n");
	if(cudaHostAlloc((void**)indexArray, kvStoreSize*sizeof(int), cudaHostAllocDefault)!= cudaSuccess) {
		printf("Host Alloc failure in allocateMapData\n");
	}
	checkCudaErrors(cudaMalloc((void**)devIndexArray, kvStoreSize*sizeof(int)));
	//getLastCudaError("devIndexArray allocation on GPU failed\n");
	checkCudaErrors(cudaMalloc((void**)newDevIndexArray, kvStoreSize*sizeof(int)));
	//getLastCudaError("newDevIndexArray allocation on GPU failed\n");
}

void releaseMapData(void *devKey, void *devVal, void *key, void *val, void *devIp, void *ip, void *devKvCount, void *devIndexArray, void *newDevIndexArray, void *indexArray, void *finalCount, void *opVal, void *opKey, int hasCombiner){
	checkCudaErrors(cudaFree(devKey));
	checkCudaErrors(cudaFree(devVal));
	checkCudaErrors(cudaFreeHost(key));
	checkCudaErrors(cudaFreeHost(val));
	checkCudaErrors(cudaFree(devIp));
	checkCudaErrors(cudaFreeHost(ip));
	checkCudaErrors(cudaFree(devKvCount));
	checkCudaErrors(cudaFree(devIndexArray));
	checkCudaErrors(cudaFree(newDevIndexArray));
	checkCudaErrors(cudaFreeHost(indexArray));
	if(hasCombiner) {
		checkCudaErrors(cudaFreeHost(finalCount));
		checkCudaErrors(cudaFreeHost(opVal));
		checkCudaErrors(cudaFreeHost(opKey));
	}
	//This array was available globally
	checkCudaErrors(cudaFree(locator));
}

//This kernel rearranges the locator array into a new array (reArrangedLocator), so that with the prefixSum data, individual record indices are
//provided to a particular threadblock
__global__ void reArranger(uint *prefixSum, int *locator, int *reArrangedLocator, int recordsPerThread, int *recordCount, char *ip) {
	int tid = threadIdx.x + blockIdx.x*blockDim.x;
	int base = recordsPerThread*blockDim.x* blockIdx.x ;
	int i;

	int offset = prefixSum[blockIdx.x*blockDim.x];
	for(i=0; i <recordCount[tid]; i++) {
		reArrangedLocator[base + prefixSum[tid] - offset + i] = locator[tid*recordsPerThread + i];
	}
	/*
	//special case for last thread
	if(threadIdx.x  == mapBlockSize -1) {
		reArrangedLocator[base + prefixSum[tid] - offset + i] = 0xFFFFFFFF;
	}
	*/
}

//counts the records in the input
//it also keeps a tag of the number of records read by each thread
__global__ void recordCounter(char *ip, int totalThreads, int recordsPerThread, int *recordCount, int sz, int *locator) {
	int tid = threadIdx.x + blockIdx.x*blockDim.x;
	int count = 0;
	int start;

	int index = recordsPerThread*tid;

	if(tid < totalThreads) {
		//special case for tid 0 : The first line has to be counted by this guy.
		if(tid == 0) {
			count++;
			locator[index++] = 0;
		}

		start = tid;
		while(start < sz) {
			if(ip[start] == '\n') {
				if(start+1 < sz) {
					count++;
					locator[index++] = start+1;
				}
			}
			start += totalThreads;
		}
		recordCount[tid] = count;
	}
}



void setInternalVariables(int mapBlocks, int mapThreads, int &totalThreads, int sz, int &limit, int &storesPerThread, int &kvStoreSize, int numReducers, char *ip, int pairs, int &recordsPerThread, int * &recordLocator) {
	totalThreads = mapBlocks*mapThreads;     
	limit = sz/(totalThreads) + 1;

	///record counter
	double counterBegin = timer();
	int *recordCount;

	//int *recordCountCPU;
	//cudaHostAlloc((void**) &recordCountCPU, blocks*threads*sizeof(int), cudaHostAllocDefault);
	cudaMalloc((void **) &recordCount, totalThreads*sizeof(int));
	recordsPerThread = sz / AVE_CHARS_PER_LINE / totalThreads;
	if(recordsPerThread == 0) {
		printf("recordsPerThread is 0 : setting it to 1\n");
	}
	int *interimLocator;
	if (cudaMalloc((void**) &interimLocator, recordsPerThread*totalThreads*sizeof(int)) != cudaSuccess) {
		printf("cudaMalloc failed on locator allocation of size %d MB", recordsPerThread*totalThreads*sizeof(int)/1024/1024);
	}
	//printf("record count start\n");
	recordCounter<<<mapBlocks, mapThreads, 0, 0>>>(ip, mapBlocks*mapThreads, recordsPerThread, recordCount, sz, interimLocator);
	cudaDeviceSynchronize();
	//printf("counter kernel done\n");
	//printf("recordCounter time %lf\n", timer() - counterBegin);
	uint *d_Output;
	int records, t2;
	cudaMalloc((void **)&d_Output, mapBlocks*mapThreads * sizeof(uint));
	prefixSum(d_Output, (uint*) recordCount, 1, totalThreads);
	cudaDeviceSynchronize();

	cudaMemcpy(( & records), (recordCount+(totalThreads) - 1 ), sizeof (int), cudaMemcpyDeviceToHost);
	cudaMemcpy(( & t2), d_Output + (totalThreads - 1 ), sizeof (int), cudaMemcpyDeviceToHost);
	records += t2;

	int *reArrangedLocator;
	if (cudaMalloc((void**) &reArrangedLocator, recordsPerThread*totalThreads*sizeof(int)) != cudaSuccess) {
		printf("cudaMalloc failed on reArrangedLocator allocation of size %d MB", recordsPerThread*totalThreads*sizeof(int)/1024/1024);
	}
	//printf("memset starting\n");
	cudaMemset(reArrangedLocator, 0xFF, recordsPerThread*totalThreads*sizeof(int));
	//printf("rearrange starting\n");
	reArranger<<<mapBlocks, mapThreads, 0, 0>>>(d_Output, interimLocator, reArrangedLocator, recordsPerThread, recordCount, ip);
	cudaDeviceSynchronize();
	//printf("rearrange done\n");

	/*
	//For verification
	int *hostReArrangedLocator = (int*) malloc(recordsPerThread*totalThreads*sizeof(int));
	checkCudaErrors(cudaMemcpy(hostReArrangedLocator, reArrangedLocator, recordsPerThread*totalThreads*sizeof(int), cudaMemcpyDeviceToHost));
	int sumRecord = 0;
	for(int i=0; i < recordsPerThread*totalThreads; i++) {
		if(hostReArrangedLocator[i] != 0xFFFFFFFF) {
			sumRecord++;
		}
	}
	printf("Total rearranged records %d\n", sumRecord);
	free(hostReArrangedLocator);
	*/

	cudaFree(recordCount);
	cudaFree(interimLocator);

	locator = reArrangedLocator;
	recordLocator = locator;
	//cudaFreeHost(recordCountCPU);
	cudaFree(d_Output);
	printf("Input records %d time %lf\n", records, timer() - counterBegin);
	fflush(stdout);
	//pairs is greater than 0 if kvpairs clause is specified on the input code
	if(pairs > 0) {
		///
		// The 2 below is a blow-up factor to deal with uneven sized KV buckets
		kvStoreSize = records*pairs * 2;
		
		storesPerThread = kvStoreSize/(totalThreads*numReducers);
		if(storesPerThread == 0) {
			storesPerThread = 1;
		}
		kvStoreSize = storesPerThread * totalThreads;

		kvStoreSize = (int) pow((float) 2, (int) ( (int) (log(kvStoreSize)/log(2)) + 1 ) ) * numReducers;

		//Small kvStoreSize would make the linearization fail
		while(kvStoreSize < 16384) {
			kvStoreSize *= 2;
			//printf("While loop kvStoreSize %d\n", kvStoreSize);
		}
		//printf("While loop OUT recordCounter start\n");
		//printf("kvStoreSize %d, storesPerThread %d\n", kvStoreSize, storesPerThread );
		//ASSUMPTION : totalThreads is always a power of 2
		storesPerThread = kvStoreSize/totalThreads;

	} else {
		storesPerThread = (sz/COMPRESS_FACTOR)/(totalThreads*numReducers);
		if(storesPerThread == 0) {
			storesPerThread = 1;
		}
		//storesPerThread = 4*(sz/COMPRESS_FACTOR)/(totalThreads*numReducers);
		kvStoreSize = storesPerThread * totalThreads;

		kvStoreSize = (int) pow((float) 2, (int) ( (int) (log(kvStoreSize)/log(2)) + 1 ) ) * numReducers ;
		//printf("kvStoreSize %d, storesPerThread %d\n", kvStoreSize, storesPerThread );
		//ASSUMPTION : totalThreads is always a power of 2
		storesPerThread = kvStoreSize/totalThreads;
	}
}

//type 0 : CHAR
//type 1 : INT
//type 2 : LONG
//type 3 : FLOAT
void allocateCombinerData(void **devFinalCount, void **devOpKey, void **devOpVal, void **finalCount, void **opKey, void **opVal,  int combineBlockSize, int combineBlocks, int &combinerThreads, int mapKeyLength, int mapValLength, int combKeyLength, int combValLength, int mapKVCount, int keyType, int valType) {

	int keyTypeSize=1;
	int valTypeSize=1;

	if(keyType == 0) {
		keyTypeSize = sizeof(char);
	} else if(keyType == 1) {
		keyTypeSize = sizeof(int);
	} else if(keyType == 2) {
		keyTypeSize = sizeof(long);
	} else if(keyType == 3) {
		keyTypeSize = sizeof(float);
	} else {
		printf("allocateCombinerData : Invalid key type %d\n", keyTypeSize);
	}

	if(valType == 0) {
		valTypeSize = sizeof(char);
	} else if(valType == 1) {
		valTypeSize = sizeof(int);
	} else if(valType == 2) {
		valTypeSize = sizeof(long);
	} else if(valType == 3) {
		valTypeSize = sizeof(float);
	} else {
		printf("allocateCombinerData : Invalid key type %d\n", valTypeSize);
	}
	

	combinerThreads = combineBlockSize * combineBlocks;

	int numWarps = combinerThreads/32;
	int maxKVsGeneratedByCombiner = ( (mapKVCount/numWarps) + 1 )* numWarps ;

	if(cudaMalloc((void**)devFinalCount, (combinerThreads/32) *sizeof(int)) != cudaSuccess) {
		printf("Cuda Malloc Failed in allocateCombinerData for data size %dMB\n", (combinerThreads/32) *sizeof(int)/1024/1024);
	}
	if(cudaMalloc((void**)devOpKey, maxKVsGeneratedByCombiner*mapKeyLength*keyTypeSize) != cudaSuccess)  {
		printf("Cuda Malloc Failed in allocateCombinerData for data size %dMB\n", (combinerThreads/32) *sizeof(int)/1024/1024);
	}
	if(cudaMalloc((void**)devOpVal, maxKVsGeneratedByCombiner*mapValLength*valTypeSize) != cudaSuccess) {
		printf("Cuda Malloc Failed in allocateCombinerData for data size %dMB\n", (combinerThreads/32) *sizeof(int)/1024/1024);
	}
	if(cudaHostAlloc(finalCount, (combinerThreads/32)*sizeof(int), cudaHostAllocDefault)!= cudaSuccess) {
		printf("Host Alloc failure in allocateCombinerData for data size %dMB\n", (combinerThreads/32) *sizeof(int)/1024/1024);
	}
	if(cudaHostAlloc(opKey, maxKVsGeneratedByCombiner*combKeyLength*keyTypeSize, cudaHostAllocDefault)!= cudaSuccess) {
		printf("Host Alloc failure in allocateCombinerData for data size %dMB\n", (combinerThreads/32) *sizeof(int)/1024/1024);
	}
	if(cudaHostAlloc(opVal, maxKVsGeneratedByCombiner*combValLength*valTypeSize, cudaHostAllocDefault)!= cudaSuccess) {
		printf("Host Alloc failure in allocateCombinerData for data size %dMB\n", (combinerThreads/32) *sizeof(int)/1024/1024);
	}
}

void releaseCombinerData(void *devFinalCount, void *devOpKey, void *devOpVal) {
	printf("In release combiner data\n");
	if (cudaFree(devFinalCount) != cudaSuccess) {
		printf("cudaFree failure in releaseCombinerData\n");
	}
	if (cudaFree(devOpKey)!= cudaSuccess) {
		printf("cudaFree failure in releaseCombinerData\n");
	}
	if (cudaFree(devOpVal)!= cudaSuccess) {
		printf("cudaFree failure in releaseCombinerData\n");
	}	
	printf("Combiner data release done\n");
}


void setThreads( int &mapblocks, int &mapthreads, int &combineBlocks, int &combineBlockSize) {
	mapblocks = MAPBLOCKS;
	mapthreads = MAPBLOCKSIZE;
	combineBlocks = COMBINEBLOCKS;
	combineBlockSize = COMBINEBLOCKSIZE;
}


void reverse(char *in, char *out, int count) {
	
	int tmp;
	for(int i =0; i<count; i++) {
		out[i] = in[count - 1 - i];
	} 
	
}

inline unsigned int CRC32_function(char *buf, unsigned long len, unsigned long &crc, int getFinal)
{
	//static unsigned long crc = 0xFFFFFFFFUL;
	
	if (getFinal) {
		crc = crc ^ 0xFFFFFFFFUL; 
		unsigned int temp;
		reverse((char*)&crc, (char*)&temp, 4);
		//reset CRC
		crc = 0xFFFFFFFFUL;
		return temp;
	}
	
	while (len--)
		crc = crc_table[(crc ^ *buf++) & 0xFF] ^ (crc >> 8);

	return crc;
}


inline void writeFile_old(FILE *file, char *keyIn, char *valIn, int writeCRC ) {
	
	static unsigned long crc = 0xFFFFFFFFUL; 
	
	if(writeCRC) {
		char ender = EOF;
		CRC32_function(&ender, 1, crc, 0);
		CRC32_function(&ender, 1, crc, 0);
		fwrite((char*)&ender, sizeof(char), 1, file);
		fwrite((char*)&ender, sizeof(char), 1, file);
		int crcFinal = CRC32_function(0, 0, crc, 1);
		fwrite((char*)&crcFinal, sizeof(char), 4, file);
		//reset the CRC
		crc = 0xFFFFFFFFUL;
		return;
	}
	printf("In writeFile with key %s val %s\n", keyIn, valIn);
	char keyLength = strlen(keyIn);
	char valLength = strlen(valIn);
	char keyPack = keyLength + 1;
	char valPack = valLength + 1;
	
	fwrite((char*)&keyPack, sizeof(char), 1, file);
	fwrite((char*)&valPack, sizeof(char), 1, file);
	fwrite((char*)&keyLength, sizeof(char), 1, file);
	fwrite(keyIn, sizeof(char), keyLength, file);
	fwrite((char*)&valLength, sizeof(char), 1, file);
	fwrite(valIn, sizeof(char), valLength, file);

	printf("fwrites done\n");
	
	CRC32_function(&keyPack, 1, crc, 0);
	CRC32_function(&valPack, 1, crc, 0);
	CRC32_function(&keyLength, 1, crc, 0);
	CRC32_function(keyIn, keyLength, crc, 0);
	CRC32_function(&valLength, 1, crc, 0);
	CRC32_function(valIn, valLength, crc, 0);
	printf("writeFile done\n");
	//file.write(reinterpret_cast<const char *>(&num), sizeof(num));
	
}

int getLength(char *buf, int data) {
	int ptr=1;
	if(data & 0xFF000000) {
		buf[ptr++] = (data & 0xFF000000)>>24;
	}
	if(data & 0xFF0000) {
		buf[ptr++] = (data & 0xFF0000)>>16;
	}
	if(data & 0xFF00) {
		buf[ptr++] = (data & 0xFF00)>>8;
	}
	if(data & 0xFF) {
		buf[ptr++] = (data & 0xFF);
	}
	
	//write sentinel byte
	if(ptr == 2) {
		buf[0] = 143;
	} else {
		buf[0] = 142;
	}
	return ptr;
}


//This is inline with the Hadoop binary file format writeFile_old is an incomplete predecessor.
inline void writeFile(FILE *file, char *keyIn, char *valIn, int writeCRC ) {
	
	static unsigned long crc = 0xFFFFFFFFUL; 
	
	if(writeCRC) {
		char ender = EOF;
		CRC32_function(&ender, 1, crc, 0);
		CRC32_function(&ender, 1, crc, 0);
		fwrite((char*)&ender, sizeof(char), 1, file);
		fwrite((char*)&ender, sizeof(char), 1, file);
		int crcFinal = CRC32_function(0, 0, crc, 1);
		fwrite((char*)&crcFinal, sizeof(char), 4, file);
		//reset the CRC
		crc = 0xFFFFFFFFUL;
		return;
	}
	//printf("In writeFile with key %s val %s\n", keyIn, valIn);
	int keyLength = strlen(keyIn);
	int valLength = strlen(valIn);
	int keyPack = keyLength + 1;
	int valPack = valLength + 1;	

	int keyLengthSize=0;
	int valLengthSize=0;
	char keyLengthSizePrinter[10];
	char valLengthSizePrinter[10];
	int keyPackSize=0;
	int valPackSize=0;
	char keyPackSizePrinter[10];
	char valPackSizePrinter[10];
	if(keyLength > 127) {
		keyLengthSize = getLength(keyLengthSizePrinter, keyLength);
		keyPackSize = getLength(keyPackSizePrinter , keyLength + keyLengthSize);
	}

	if(valLength > 127) {
		valLengthSize = getLength(valLengthSizePrinter, valLength);
		valPackSize = getLength(valPackSizePrinter, valLength + valLengthSize);
	}

	if(keyLength > 127) {
		fwrite(keyPackSizePrinter, sizeof(char), keyPackSize, file);
		CRC32_function(keyPackSizePrinter, keyPackSize, crc, 0);
	} else {
		char keyP = keyPack;
		fwrite(&keyP, sizeof(char), 1, file);
		CRC32_function(&keyP, 1, crc, 0);
	}

	if(valLength > 127) {
		fwrite(valPackSizePrinter, sizeof(char), valPackSize, file);
		CRC32_function(valPackSizePrinter, valPackSize, crc, 0);
	} else {
		char valP = valPack;
		fwrite(&valP, sizeof(char), 1, file);
		CRC32_function(&valP, 1, crc, 0);
	}

	if(keyLength > 127) {
		fwrite(keyLengthSizePrinter, sizeof(char), keyLengthSize, file);
		CRC32_function(keyLengthSizePrinter, keyLengthSize, crc, 0);
	} else {
		char keyL = keyLength;
		fwrite(&keyL, sizeof(char), 1, file);
		CRC32_function(&keyL, 1, crc, 0);
	}
	
	fwrite(keyIn, sizeof(char), keyLength, file);
	CRC32_function(keyIn, keyLength, crc, 0);

	if(valLength > 127) {
		fwrite(valLengthSizePrinter, sizeof(char), valLengthSize, file);
		CRC32_function(valLengthSizePrinter, valLengthSize, crc, 0);
	} else {
		char valL = valLength;
		fwrite(&valL, sizeof(char), 1, file);
		CRC32_function(&valL, 1, crc, 0);
	}

	fwrite(valIn, sizeof(char), valLength, file);

	
	CRC32_function(valIn, valLength, crc, 0);
	//printf("writeFile done\n");
	//file.write(reinterpret_cast<const char *>(&num), sizeof(num));
	
}


void writeIndexFile(FILE *indexFile, long count, long partitionSize, int writeCRC) {
	
	static unsigned long crc = 0xFFFFFFFFUL; 
	
	if(writeCRC) {
		int crcFinal = CRC32_function(0, 0, crc, 1);
		int zero = 0;
		fwrite((char*)&zero, sizeof(char), 4, indexFile);
		CRC32_function((char*)&zero, 4, crc, 0);
		fwrite((char*)&crcFinal, sizeof(char), 4, indexFile);
		//reset the CRC
		crc = 0xFFFFFFFFUL;
		return;
	}
	
	long tmp;
	reverse((char*)&count, (char*)&tmp, 8);
	fwrite((char*)&tmp, sizeof(char), 8, indexFile);
	CRC32_function((char*)&tmp, 8, crc, 0);
	
	partitionSize -= 4;
	reverse((char*)&partitionSize, (char*)&tmp, 8);
	fwrite((char*)&tmp, sizeof(char), 8, indexFile);
	CRC32_function((char*)&tmp, 8, crc, 0);
	
	partitionSize += 4;
	reverse((char*)&partitionSize, (char*)&tmp, 8);
	fwrite((char*)&tmp, sizeof(char), 8, indexFile);
	CRC32_function((char*)&tmp, 8, crc, 0);
	
}


FILE *opFile, *opIndexFile;

void initFile(char *path) {
	char command[500];
  char temp[500];
	char opPath[500];
  char opPathInd[500];

  strcpy(temp, path);

	strcpy(command, "mkdir -p ");
  strcat(command, temp);
  //system(command);	
	if(mkpath(temp, 0775) != 0 ) {
		printf("Failed to create output directory %s\n", temp);
	}
	strcpy(opPath, path);
  strcat(opPath, "/file.out");
  strcpy(opPathInd, path);

	//printf("Output is being written at %s\n", opPath);
  //strcat(opPathInd, argv[2]);
  strcat(opPathInd, "/file.out.index");
  opFile = fopen(opPath, "wb");
  opIndexFile = fopen(opPathInd, "wb");

	if (opFile == NULL) {
		printf("Failed to create output file %s\n", opPath);
	}
	
	if (opIndexFile == NULL) {
		printf("Failed to create output index file %s\n", opPathInd);
	}

}

void closeFile() {
	writeIndexFile(opIndexFile, 0, 0 ,1);
	fclose(opFile);
	fclose(opIndexFile);
}

//this function converts float keys and values into strings that are stored in the buffer
__device__ int kvprinter(float key, float val, char *buffer) {
	int ptr =0;
	//if(blockIdx.x == 0) 
		//printf("Input key %f\n", key);
	//handle negative numbers
	if(key < 0) {
		key = -key;
		buffer[ptr++] = 45;		
	}
	int residue = 1;
	while( residue*10  < key) {
		residue = residue*10;
	}
	while( residue >= 1) {
		int digit = key / residue;
		key = key - digit*residue;  
		buffer[ptr++] = 48 + digit;	
		residue = residue/10;
	}
	//print decimal point
	buffer[ptr++] = 46;

	//if(blockIdx.x ==0) 
		//printf("Before decimal %f\n", key);
	//residue is 0.1 here. We are now processing digits after decimal point
	for(int p=0; p <6; p++) {
		int digit = key*10;
		//assert(0 <= digit && digit <=9);
		
		buffer[ptr++] = 48 + digit;	
		/*
		if(blockIdx.x ==0) {
			printf("key*10 %f 10*digit %f key %f newKey %f  digit %d \n", (float) key*10, (float)10*digit, key, (float) key*10 - digit, digit);
		}
		*/
		key = key*10 - digit;
		
	}
	//print tab
	buffer[ptr++] = 9;

	//print value
	if(val < 0) {
		val = -val;
		buffer[ptr++] = 45;		
	}
	residue = 1;
	while( residue*10  < val) {
		residue = residue*10;
	}
	while( residue >= 1) {
		int digit = val / residue;
		val = val - digit*residue;  
		buffer[ptr++] = 48 + digit;	
		residue = residue/10;
	}
	//print decimal point
	buffer[ptr++] = 46;
	//residue is 0.1 here. We are now processing digits after decimal point
	for(int p=0; p <6; p++) {
		int digit = val*10;
		buffer[ptr++] = 48 + digit;	
		val = val*10 - digit;
	}
	//print newline
	buffer[ptr++] = 10;

	return ptr;
}

//only one thread per warp does the work
__global__ void sprintfDev(float *keyArray, float *valArray, int *indexArray, int keyLength, int valLength, int *kvCount, char *opBuffer, int opBufferLen, int *bufferWrittenLengths, int indicesPerThread, int totalThreads) {
	//int totalThreads = 128*128;
	int threadsForMe = totalThreads/gridDim.x;
	int laneId = threadIdx.x & 0x1F;
	int bytesWritten = 0;
	opBuffer += (opBufferLen/gridDim.x) * blockIdx.x;
	//int indicesPerThread = kvStoreSize/totalThreads;
	if(laneId == 0) {
		for(int i = blockIdx.x*threadsForMe ; i< blockIdx.x*threadsForMe + threadsForMe; i++) {
			int indexBase = indicesPerThread*i;
			for(int k=0; k < kvCount[i]; k++) {
				int index = indexArray[indexBase+k];
				float currentKey = ((float*)keyArray)[index*keyLength];
				float currentVal = ((float*)valArray)[index*valLength];
				bytesWritten += kvprinter(currentKey, currentVal, opBuffer + bytesWritten);

			}
		}		
		bufferWrittenLengths[blockIdx.x] = bytesWritten;
	}	

}


//This function is depracated : use writeOPonHDFSwithDevHelp instead. This function used the hdfsClient for file writing; the newer one does not.
//type 0 : CHAR
//type 1 : INT
//type 2 : LONG
//type 3 : FLOAT
//If you get into this function, the assumption is that the number of reducers = 0!
void writeOPonHDFS(void *devKey, void *devVal, void *keyArray, void *valArray, int *devKvCount, int *devIndexArray, int *indexArray, int keyLength, int valLength, int keyType, int valType, int kvStoreSize, int totalThreads, char *outputPath, hdfsFS fs) {
  int count = 0;	
  double begin = timer();
  while(count < 3) {
	  if(*outputPath == '/') {
		  count++;
	  }
	  outputPath++;
  }	
  outputPath--; 
  int keyTypeSize = 1;
  int valTypeSize = 1;

  	if(keyType == 0) {
		keyTypeSize = sizeof(char);
	} else if(keyType == 1) {
		keyTypeSize = sizeof(int);
	} else if(keyType == 2) {
		keyTypeSize = sizeof(long);
	} else if(keyType == 3) {
		keyTypeSize = sizeof(float);
	} else {
		printf("writeOPonHDFS : Invalid key type %d\n", keyType);
	}

	if(valType == 0) {
		valTypeSize = sizeof(char);
	} else if(valType == 1) {
		valTypeSize = sizeof(int);
	} else if(valType == 2) {
		valTypeSize = sizeof(long);
	} else if(valType == 3) {
		valTypeSize = sizeof(float);
	} else {
		printf("writeOPonHDFS : Invalid key type %d\n", valType);
	}

	cudaMemcpy(keyArray, devKey, kvStoreSize*keyLength*keyTypeSize, cudaMemcpyDeviceToHost);
	cudaMemcpy(valArray, devVal, kvStoreSize*valLength*valTypeSize, cudaMemcpyDeviceToHost);

	int *kvCount;
	double time = timer();
	cudaHostAlloc( (void**) & kvCount, totalThreads*sizeof(int) ,cudaHostAllocDefault);

	cudaMemcpy(kvCount, devKvCount, totalThreads*sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(indexArray, devIndexArray, kvStoreSize*sizeof(int), cudaMemcpyDeviceToHost);


	//Assumption : Division is perfect
	int indicesPerThread = kvStoreSize/totalThreads;

	//printf("Key %s Val %d\n", &  ( ((char*)key) [index*keyLength*sizeof(char)] ), ((int*)val)[index]);

	
	printf("Trying to get HDFS lock for output file writing %s\n", str);	

	struct flock fl = {F_WRLCK, SEEK_SET,   0,      0,     0 };
	int fd;

	fl.l_pid = getpid();

	if ((fd = open(str, O_RDWR)) == -1) {
			perror("HDFS lock");
			exit(1);
	}


	if (fcntl(fd, F_SETLKW, &fl) == -1) {
			perror("fcntl");
			exit(1);
	}

	printf("Got HDFS lock\n");
	printf("WriteOPonHDFS: Lock acquisition time %lf\n", timer() - time);

	if( sprintf(driverNum, "%d", getDriverNum()) < 0) {
		printf("sprinf error in writing drivernum\n");
	}
	/*	
	if (sprintf(filepath, "%s", "output/dummy")<0) {
		printf("sprintf error in writing filepat\n") ;
	}
	*/

	sprintf(filepath, outputPath);
	sprintf(requesterPID, "%d", getpid());
	//set write flag
	sprintf(writePerm, "%d", 1);
	pid_t hdfspid = atoi(clientPID);

	printf("About to start dumping the output\n");
	char *tempBuffer1 = (char*) malloc(200);
	char *tempBuffer2 = (char*) malloc(200);
	char *valBuf;
	char *keyBuf;
	
	int head = 0;
	
	time = timer();
	for(int i=0; i<totalThreads; i++) {
		//printf("Going for thread %d with KVCount %d\n", i, kvCount[i]);
		int indexBase = indicesPerThread*i;
		for(int j=0; j < kvCount[i]; j++) {
			int index = indexArray[indexBase+j];

			if (keyType == 0) {
				int bytes = sprintf(file0 + head,  "%s", (char*)((char*)keyArray + index*keyLength));
				if(bytes < 0 ) {
					printf("sprintf error in dumping the output\n");
				} else {
					head += bytes;
				}
			} else if (keyType == 1) {
				sprintf(tempBuffer2, "%d", ((int*)keyArray)[index*keyLength]);
				int bytes = sprintf(file0 + head,  "%d", ((int*)keyArray)[index*keyLength]);
				if(bytes < 0 ) {
					printf("sprintf error in dumping the output\n");
				} else {
					head += bytes;
				}
				//printf("KeyBuf %s\n", tempBuffer2);
			} else if (keyType == 3) {
				int bytes = sprintf(file0 + head,  "%f", ((float*)keyArray)[index*keyLength]);
				if(bytes < 0 ) {
					printf("sprintf error in dumping the output\n");
				} else {
					head += bytes;
				}
			}
			
			if (valType == 0) {
				//printf("writing to HDFS\n");
				int bytes = sprintf(file0 + head, "\t%s", (char*)((char*)valArray + index*valLength));
				if(bytes < 0 ) {
					printf("sprintf error in dumping the output\n");
				} else {
					head += bytes;
				}
				//printf("ValBuf %s\n", valBuf);
			} else if (valType == 1) {
				int bytes = sprintf(file0 + head, "\t%d\n", ((int*)valArray)[index*valLength]);
				if(bytes < 0 ) {
					printf("sprintf error in dumping the output\n");
				} else {
					head += bytes;
				}
				//printf("ValBuf %s\n", tempBuffer1);
			} else if (valType == 2) {
				int bytes = sprintf(file0 + head, "\t%ld\n", ((long*)valArray)[index*valLength]);
				if(bytes < 0 ) {
					printf("sprintf error in dumping the output\n");
				} else {
					head += bytes;
				}
				//printf("ValBuf %s data %ld\n", tempBuffer1, ((long*)valArray)[j*valLength]);
			} else if (valType == 3) {
				//printf("writing to HDFS\n");
				int bytes = sprintf(file0 + head, "\t%f\n", ((float*)valArray)[index*valLength]);
				if(bytes < 0 ) {
					printf("sprintf error in dumping the output\n");
				} else {
					head += bytes;
				}
				//printf("ValBuf %s data %ld\n", tempBuffer1, ((long*)valArray)[j*valLength]);
			}


		}
	}
	free(tempBuffer1);
	free(tempBuffer2);
	printf("WriteOPonHDFS: Output buffer creation time %lf\n", timer() - time);
	
	/*	
	printf("Total bytes written %d\n", head);
	for(int i=0; i< head; i++) {
  	putc( file0[i], stdout );
	}
	*/

	sprintf(bytesRead, "%d", head);
	printf("Client pid %d\n", hdfspid);
	//wake up the hdfsClient
	kill(hdfspid, SIGUSR1);
	while(!hdfsDone) {
		/*		
		i++;
		if(i== 1000000) {
			printf("Waiting on hdfs\n");
			i=0;
		}
		*/
		//usleep(10000);
	}
	hdfsDone = 0;
	//remove write flag
	sprintf(writePerm, "%d", 0);

	fl.l_type = F_UNLCK;  /* set to unlock same region */

	if (fcntl(fd, F_SETLK, &fl) == -1) {
		  perror("fcntl");
		  exit(1);
	}

	printf("Returned HDFS lock\n");
	printf("HDFS file write time %lf\n", timer() - begin);

			//printf("Val %d\n", ((int*)val)[index]);
		

	
	cudaFreeHost(kvCount);
	
}


// This function assumes that NO hdfsClient is running
//type 0 : CHAR
//type 1 : INT
//type 2 : LONG
//type 3 : FLOAT
//If you get into this function, the assumption is that the number of reducers = 0!
//For each KV pair, currently a buffer size of 40 is used. This has to change later on when differet kinds of datatypes are supported
void writeOPonHDFSwithDevHelp(void *devKey, void *devVal, void *keyArray, void *valArray, int *devKvCount, int *devIndexArray, int *indexArray, int keyLength, int valLength, int keyType, int valType, int kvStoreSize, int totalThreads, char *outputPath, hdfsFS fs) {
  int count = 0;	
  double begin = timer();
  char *fileBuffer;
  while(count < 3) {
	  if(*outputPath == '/') {
		  count++;
	  }
	  outputPath++;
  }	
  outputPath--; 
  int keyTypeSize = 1;
  int valTypeSize = 1;
	
  	if(keyType == 0) {
		keyTypeSize = sizeof(char);
	} else if(keyType == 1) {
		keyTypeSize = sizeof(int);
	} else if(keyType == 2) {
		keyTypeSize = sizeof(long);
	} else if(keyType == 3) {
		keyTypeSize = sizeof(float);
	} else {
		printf("writeOPonHDFSwithDevHelp : Invalid key type %d\n", keyType);
	}

	if(valType == 0) {
		valTypeSize = sizeof(char);
	} else if(valType == 1) {
		valTypeSize = sizeof(int);
	} else if(valType == 2) {
		valTypeSize = sizeof(long);
	} else if(valType == 3) {
		valTypeSize = sizeof(float);
	} else {
		printf("writeOPonHDFSwithDevHelp : Invalid key type %d\n", valType);
	}

	//Assumption : Division is perfect
	int indicesPerThread = kvStoreSize/totalThreads;

	//printf("Key %s Val %d\n", &  ( ((char*)key) [index*keyLength*sizeof(char)] ), ((int*)val)[index]);
	char *buffer, *bufferCPU;
	int *bufferLengths, *bufferLengthsCPU;
	int blocks= totalThreads/128;
	int threads=32;
	double time = timer();	
	int bufferSize = kvStoreSize * 40;
	checkCudaErrors(cudaMalloc(&buffer, sizeof(char) * bufferSize));	
	checkCudaErrors(cudaMalloc(&bufferLengths, sizeof(int) * blocks));	

	checkCudaErrors(cudaHostAlloc( (void**) & bufferLengthsCPU, sizeof(int) * blocks ,cudaHostAllocDefault));
	checkCudaErrors(cudaHostAlloc( (void**) & fileBuffer, sizeof(char) * FILESIZE_MAX, cudaHostAllocDefault));

	sprintfDev<<<blocks, threads>>>((float *)devKey, (float *)devVal, devIndexArray, keyLength, valLength, devKvCount, buffer, bufferSize, bufferLengths, indicesPerThread, totalThreads);
	checkCudaErrors(cudaDeviceSynchronize());

	printf("sprintDev kernel time %lf\n", timer() - time);

	//get the data back
	checkCudaErrors(cudaMemcpy(bufferLengthsCPU, bufferLengths, blocks*sizeof(int), cudaMemcpyDeviceToHost));
	//FILE *fpTemp = fopen("test_op.txt", "w");
	int head = 0;
	for(int i=0; i<blocks; i++) {
		checkCudaErrors(cudaMemcpy(fileBuffer + head, buffer+i*(bufferSize/blocks), bufferLengthsCPU[i]*sizeof(char), cudaMemcpyDeviceToHost));
		//fwrite(bufferCPU , sizeof(char), bufferLengthsCPU[i], fpTemp);
		head += bufferLengthsCPU[i];
	}

	hdfsFile writeFile = hdfsOpenFile(fs, outputPath, O_WRONLY|O_CREAT, 0, 0, 0);
	if(!writeFile) {
		fprintf(stderr, "Failed to open %s for writing!\n", outputPath);
		exit(1);
	}
	/*
	for(int i=0; i< atoi(bytesRead); i++) {
		putc( outPath[i], stdout);
	}
	*/

	tSize num_written_bytes = hdfsWrite(fs, writeFile, (void*) fileBuffer, head);
	//printf("Bytes written %d\n", num_written_bytes);
	if (hdfsFlush(fs, writeFile)) {
		 fprintf(stderr, "Failed to 'flush' %s\n", writeFile); 
		exit(-1);
	}
	hdfsCloseFile(fs, writeFile);

	checkCudaErrors(cudaFree(buffer));
	checkCudaErrors(cudaFree(bufferLengths));
	cudaFreeHost(bufferLengthsCPU);
	cudaFreeHost(fileBuffer);
	printf("writeOPonHDFSwithDevHelp %lf\n", timer() - begin);
}

//type 0 : CHAR
//type 1 : INT
//type 2 : LONG
//type 3 : FLOAT
void writeOP(int mapKVcount, int *finalCount, int keyType, int valType, void *keyArray, void *valArray, int keyLength, int valLength) {
	//long before = file.tellp();
		double begin = timer();
		long before= ftell(opFile); 
		char *tempBuffer1 = (char*) malloc(1024);
		char *tempBuffer2 = (char*) malloc(1024);
		char *valBuf; 
		char *keyBuf;
		int combinerThreads = COMBINEBLOCKS*COMBINEBLOCKSIZE;
		for(int i=0; i<(combinerThreads/32); i++) {
			int kvsPerThread = (mapKVcount/(combinerThreads/32)) + 1;
			int start =  kvsPerThread *i;
			/*
			printf("Finalcount %d is %d\n", i, finalCount[i]);
			for(int j= start; j< start + finalCount[i]; j++)
				printf("Key %s Value %d\n", &(__opKey[j*STRING_LENGTH]), __opVal[j]);
			*/
			/*
			for(int j= start; j< start + finalCount[i]; j++) {
				printf("Key %d in writeOP %d\n", j, ((int*)keyArray)[j]);
			}			
			*/

			for(int j= start; j< start + finalCount[i]; j++) {
				if (valType == 0) {
					valBuf = (char*)((char*)valArray + j*valLength);
					//printf("ValBuf %s\n", valBuf);
				} else if (valType == 1) {
					sprintf(tempBuffer1, "%d", ((int*)valArray)[j*valLength]);
					valBuf = tempBuffer1;
					//printf("ValBuf %s\n", tempBuffer1);
				} else if (valType == 2) {
					sprintf(tempBuffer1, "%ld", ((long*)valArray)[j*valLength]);
					valBuf = tempBuffer1;
					//printf("ValBuf %s data %ld\n", tempBuffer1, ((long*)valArray)[j*valLength]);
				} else if (valType == 3) {
					sprintf(tempBuffer1, "%f", ((float*)valArray)[j*valLength]);
					valBuf = tempBuffer1;
					//printf("ValBuf %s data %ld\n", tempBuffer1, ((long*)valArray)[j*valLength]);
				}

				if (keyType == 0) {
					keyBuf = (char*)((char*)keyArray + j*keyLength);
				} else if (keyType == 1) {
					sprintf(tempBuffer2, "%d", ((int*)keyArray)[j*keyLength]);
					keyBuf = tempBuffer2;
					//printf("KeyBuf %s\n", tempBuffer2);
				} else if (keyType == 3) {
					sprintf(tempBuffer2, "%f", ((float*)keyArray)[j*keyLength]);
					keyBuf = tempBuffer2;
					//printf("KeyBuf %s\n", tempBuffer2);
				}
				//sprintf(valBuf, "%d", valArray[j*valLength]);
				writeFile(opFile, keyBuf, valBuf, 0); 
				//printf("Iter in writeFile %d\n", j - start);
			}
			
		}
		writeFile(opFile, 0, 0, 1);
		long bytes = ftell(opFile) - before;
		writeIndexFile(opIndexFile, before, bytes, 0);
		free(tempBuffer1);
		free(tempBuffer2);
		printf("WriteOP time %lf\n", timer() - begin);
}

//type 0 : CHAR
//type 1 : INT
//type 2 : LONG
//This function is to be used when no combiner is used
void writeOPNoCombiner( int *newDevIndexArray, int * indexArray, void* keyArray, void *valArray, void *devKey, void *devVal,  int keyLength, int valLength, int keyType, int valType, int mapKVcount, int kvStoreSize) {
	double begin = timer();
	long before= ftell(opFile); 
	char *tempBuffer1 = (char*) malloc(200);
	char *tempBuffer2 = (char*) malloc(200);
	char *valBuf; 
	char *keyBuf;
	int keyTypeSize, valTypeSize;

	if(keyType == 0) {
		keyTypeSize = sizeof(char);
	} else if (keyType == 1) {
		keyTypeSize = sizeof(int);
	} else if (keyType == 2) {
		keyTypeSize = sizeof(long);
	} else if (keyType == 3) {
		keyTypeSize = sizeof(float);
	} else {
		printf("writeOPNoCombiner : Invalid Key Type %d\n", keyType);
	}

	if(valType == 0) {
		valTypeSize = sizeof(char);
	} else if (valType == 1) {
		valTypeSize = sizeof(int);
	} else if (valType == 2) {
		valTypeSize = sizeof(long);
	} else if (valType == 3) {
		valTypeSize = sizeof(float);
	} else {
		printf("writeOPNoCombiner : Invalid Value Type %d\n", valTypeSize);
	}

	checkCudaErrors(cudaMemcpy(indexArray, newDevIndexArray, mapKVcount*sizeof(int), cudaMemcpyDeviceToHost));
	//getLastCudaError("writeOPNoCombiner : cudaMemcpy for indexArray failed\n");
	checkCudaErrors(cudaMemcpy(keyArray, devKey, kvStoreSize*keyLength*keyTypeSize, cudaMemcpyDeviceToHost));
	//getLastCudaError("writeOPNoCombiner : cudaMemcpy for key failed\n");
	checkCudaErrors(cudaMemcpy(valArray, devVal, kvStoreSize*valLength*valTypeSize, cudaMemcpyDeviceToHost));
	//getLastCudaError("writeOPNoCombiner : cudaMemcpy for val failed\n");

	for(int j= 0; j< mapKVcount; j++) {
		if (valType == 0) {
			valBuf = (char*)((char*)valArray + indexArray[j]*valLength);
			//printf("ValBuf %s\n", valBuf);
		} else if (valType == 1) {
			sprintf(tempBuffer1, "%d", ((int*)valArray)[indexArray[j]*valLength]);
			valBuf = tempBuffer1;
			//printf("ValBuf %s\n", tempBuffer1);
		} else if (valType == 2) {
			sprintf(tempBuffer1, "%ld", ((long*)valArray)[indexArray[j]*valLength]);
			valBuf = tempBuffer1;
			//printf("ValBuf %s data %ld\n", tempBuffer1, ((long*)valArray)[j*valLength]);
		} else if (valType == 3) {
			sprintf(tempBuffer1, "%f", ((float*)valArray)[indexArray[j]*valLength]);
			valBuf = tempBuffer1;
			//printf("ValBuf %s data %ld\n", tempBuffer1, ((long*)valArray)[j*valLength]);
		}

		if (keyType == 0) {
			keyBuf = (char*)((char*)keyArray + indexArray[j]*keyLength);
		} else if (keyType == 1) {
			sprintf(tempBuffer2, "%d", ((int*)keyArray)[indexArray[j]*keyLength]);
			keyBuf = tempBuffer2;
			//printf("KeyBuf %s\n", tempBuffer2);
		} else if (keyType == 3) {
			sprintf(tempBuffer2, "%f", ((float*)keyArray)[indexArray[j]*keyLength]);
			keyBuf = tempBuffer2;
			//printf("KeyBuf %s\n", tempBuffer2);
		}
		//sprintf(valBuf, "%d", valArray[j*valLength]);
		//printf("PAIR %s\n", keyBuf);
		writeFile(opFile, keyBuf, valBuf, 0); 
		
	}
		
	writeFile(opFile, 0, 0, 1);
	long bytes = ftell(opFile) - before;
	writeIndexFile(opIndexFile, before, bytes, 0);
	free(tempBuffer1);
	free(tempBuffer2);
	printf("WriteOPNoCombiner time %lf\n", timer() - begin);
}

//######### Scan code ####################

//All three kernels run 512 threads per workgroup
//Must be a power of two
#define SCAN_BLOCK_SIZE 256

/*
void getLastCudaError(char *s) {
		cudaError_t err = cudaGetLastError();

		if (err != cudaSuccess) 
  		printf("Error: %s with message %s\n", cudaGetErrorString(err), s);

}
*/
////////////////////////////////////////////////////////////////////////////////
// Basic scan codelets
////////////////////////////////////////////////////////////////////////////////
//Naive inclusive scan: O(N * log2(N)) operations
//Allocate 2 * 'size' local memory, initialize the first half
//with 'size' zeros avoiding if(pos >= offset) condition evaluation
//and saving instructions
inline __device__ uint scan1Inclusive(uint idata, volatile uint *s_Data, uint size)
{
    uint pos = 2 * threadIdx.x - (threadIdx.x & (size - 1));
    s_Data[pos] = 0;
    pos += size;
    s_Data[pos] = idata;

    for (uint offset = 1; offset < size; offset <<= 1)
    {
        __syncthreads();
        uint t = s_Data[pos] + s_Data[pos - offset];
        __syncthreads();
        s_Data[pos] = t;
    }

    return s_Data[pos];
}

inline __device__ uint scan1Exclusive(uint idata, volatile uint *s_Data, uint size)
{
    return scan1Inclusive(idata, s_Data, size) - idata;
}


inline __device__ uint4 scan4Inclusive(uint4 idata4, volatile uint *s_Data, uint size)
{
    //Level-0 inclusive scan
    idata4.y += idata4.x;
    idata4.z += idata4.y;
    idata4.w += idata4.z;

    //Level-1 exclusive scan
    uint oval = scan1Exclusive(idata4.w, s_Data, size / 4);

    idata4.x += oval;
    idata4.y += oval;
    idata4.z += oval;
    idata4.w += oval;

    return idata4;
}

//Exclusive vector scan: the array to be scanned is stored
//in local thread memory scope as uint4
inline __device__ uint4 scan4Exclusive(uint4 idata4, volatile uint *s_Data, uint size)
{
    uint4 odata4 = scan4Inclusive(idata4, s_Data, size);
    odata4.x -= idata4.x;
    odata4.y -= idata4.y;
    odata4.z -= idata4.z;
    odata4.w -= idata4.w;
    return odata4;
}

////////////////////////////////////////////////////////////////////////////////
// Scan kernels
////////////////////////////////////////////////////////////////////////////////
__global__ void scanExclusiveShared(
    uint4 *d_Dst,
    uint4 *d_Src,
    uint size
)
{
    __shared__ uint s_Data[2 * SCAN_BLOCK_SIZE];

    uint pos = blockIdx.x * blockDim.x + threadIdx.x;

    //Load data
    uint4 idata4 = d_Src[pos];

    //Calculate exclusive scan
    uint4 odata4 = scan4Exclusive(idata4, s_Data, size);

    //Write back
    d_Dst[pos] = odata4;
}

//Exclusive scan of top elements of bottom-level scans (4 * SCAN_BLOCK_SIZE)
__global__ void scanExclusiveShared2(
    uint *d_Buf,
    uint *d_Dst,
    uint *d_Src,
    uint N,
    uint arrayLength
)
{
    __shared__ uint s_Data[2 * SCAN_BLOCK_SIZE];

    //Skip loads and stores for inactive threads of last threadblock (pos >= N)
    uint pos = blockIdx.x * blockDim.x + threadIdx.x;

    //Load top elements
    //Convert results of bottom-level scan back to inclusive
    uint idata = 0;

    if (pos < N)
        idata =
            d_Dst[(4 * SCAN_BLOCK_SIZE) - 1 + (4 * SCAN_BLOCK_SIZE) * pos] +
            d_Src[(4 * SCAN_BLOCK_SIZE) - 1 + (4 * SCAN_BLOCK_SIZE) * pos];

    //Compute
    uint odata = scan1Exclusive(idata, s_Data, arrayLength);

    //Avoid out-of-bound access
    if (pos < N)
    {
        d_Buf[pos] = odata;
    }
}

//Final step of large-array scan: combine basic inclusive scan with exclusive scan of top elements of input arrays
__global__ void uniformUpdate(
    uint4 *d_Data,
    uint *d_Buffer
)
{
    __shared__ uint buf;
    uint pos = blockIdx.x * blockDim.x + threadIdx.x;

    if (threadIdx.x == 0)
    {
        buf = d_Buffer[blockIdx.x];
    }

    __syncthreads();

    uint4 data4 = d_Data[pos];
    data4.x += buf;
    data4.y += buf;
    data4.z += buf;
    data4.w += buf;
    d_Data[pos] = data4;
}

////////////////////////////////////////////////////////////////////////////////
// Interface function
////////////////////////////////////////////////////////////////////////////////
//Derived as 32768 (max power-of-two gridDim.x) * 4 * SCAN_BLOCK_SIZE
//Due to scanExclusiveShared<<<>>>() 1D block addressing
extern "C" const uint MAX_BATCH_ELEMENTS = 64 * 1048576;
extern "C" const uint MIN_SHORT_ARRAY_SIZE = 4;
extern "C" const uint MAX_SHORT_ARRAY_SIZE = 4 * SCAN_BLOCK_SIZE;
extern "C" const uint MIN_LARGE_ARRAY_SIZE = 8 * SCAN_BLOCK_SIZE;
extern "C" const uint MAX_LARGE_ARRAY_SIZE = 4 * SCAN_BLOCK_SIZE * SCAN_BLOCK_SIZE;

//Internal exclusive scan buffer
static uint *d_Buf;


static uint factorRadix2(uint &log2L, uint L)
{
    if (!L)
    {
        log2L = 0;
        return 0;
    }
    else
    {
        for (log2L = 0; (L & 1) == 0; L >>= 1, log2L++);

        return L;
    }
}

static uint iDivUp(uint dividend, uint divisor)
{
    return ((dividend % divisor) == 0) ? (dividend / divisor) : (dividend / divisor + 1);
}

size_t prefixSum(
    uint *d_Dst,
    uint *d_Src,
    uint batchSize,
    uint arrayLength
)
{
    //Check power-of-two factorization
    uint log2L;
    uint factorizationRemainder = factorRadix2(log2L, arrayLength);
    assert(factorizationRemainder == 1);

    //Check supported size range
    assert((arrayLength >= MIN_LARGE_ARRAY_SIZE) && (arrayLength <= MAX_LARGE_ARRAY_SIZE));

    //Check total batch size limit
    assert((batchSize * arrayLength) <= MAX_BATCH_ELEMENTS);

		(cudaMalloc((void **)&d_Buf, (MAX_BATCH_ELEMENTS / (4 * SCAN_BLOCK_SIZE)) * sizeof(uint)));


    scanExclusiveShared<<<(batchSize * arrayLength) / (4 * SCAN_BLOCK_SIZE), SCAN_BLOCK_SIZE>>>(
        (uint4 *)d_Dst,
        (uint4 *)d_Src,
        4 * SCAN_BLOCK_SIZE
    );
    //getLastCudaError("scanExclusiveShared() execution FAILED\n");
		cudaDeviceSynchronize();

    //Not all threadblocks need to be packed with input data:
    //inactive threads of highest threadblock just don't do global reads and writes
    const uint blockCount2 = iDivUp((batchSize * arrayLength) / (4 * SCAN_BLOCK_SIZE), SCAN_BLOCK_SIZE);
    scanExclusiveShared2<<< blockCount2, SCAN_BLOCK_SIZE>>>(
        (uint *)d_Buf,
        (uint *)d_Dst,
        (uint *)d_Src,
        (batchSize *arrayLength) / (4 * SCAN_BLOCK_SIZE),
        arrayLength / (4 * SCAN_BLOCK_SIZE)
    );
    //getLastCudaError("scanExclusiveShared2() execution FAILED\n");
		cudaDeviceSynchronize();

    uniformUpdate<<<(batchSize *arrayLength) / (4 * SCAN_BLOCK_SIZE), SCAN_BLOCK_SIZE>>>(
        (uint4 *)d_Dst,
        (uint *)d_Buf
    );
    //getLastCudaError("uniformUpdate() execution FAILED\n");
		cudaDeviceSynchronize();

		(cudaFree(d_Buf));
    return SCAN_BLOCK_SIZE;
}



//################# Linearization Pass ######################

__global__ void linearize_kernel(int *devIndexArray, int *newIndexArray, int *kvCount, int *prefixSummed, int totalKVPairs, int totalThreads, int extendedSizeofIndexArray, int storesPerBin, int reducer, int numReducers) {
	int tid = blockIdx.x*blockDim.x + threadIdx.x;
	int kvStoreSize = storesPerBin * numReducers;
	int storesPerThread = kvStoreSize/totalThreads;
	int indicesPerReducer = storesPerThread/numReducers;
	//int newBase = reducer*totalThreads*indicesPerReducer;

	int index = tid*indicesPerReducer;

	int i;

	if(tid < totalThreads) {
		int base = prefixSummed[tid];
		for(i=base; i< base +  kvCount[tid]; i++) {
			assert(i < totalKVPairs);
			newIndexArray[i] = devIndexArray[index + i - base];
		}

		i = totalKVPairs + tid; 
		
		while(i < extendedSizeofIndexArray) {
			//fill up the newIndexArray with last element of devIndexArray, which is expected to be pointing to an empty element
			newIndexArray[i] = devIndexArray[storesPerBin - 1];
			i += totalThreads;
		}
	}

}


int linearize(int *devIndexArray, int *newIndexArray, int *kvCount, int totalThreads, int storesPerBin, int reducer, int numReducers) {
	uint *d_Output;
	int mapKVcount, temp;
	double begin = timer();
	checkCudaErrors(cudaMalloc((void **)&d_Output, totalThreads * sizeof(uint)));
	prefixSum(d_Output, (uint*) kvCount, numReducers, totalThreads);
	checkCudaErrors(cudaDeviceSynchronize());

	checkCudaErrors(cudaMemcpy(( & mapKVcount), (kvCount+(totalThreads) - 1 ), sizeof (int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(( & temp), d_Output + (totalThreads - 1 ), sizeof (int), cudaMemcpyDeviceToHost));
	mapKVcount += temp;
	printf("KV pairs from linearize %d\n", mapKVcount);

	int extendedSizeofIndexArray =  (int) pow( (float)2, (int) ceil((double) log(mapKVcount)/log(2)));

	linearize_kernel<<<MAPBLOCKS, MAPBLOCKSIZE, 0, 0>>>(devIndexArray, newIndexArray, kvCount, (int*) d_Output, mapKVcount, totalThreads, extendedSizeofIndexArray, storesPerBin, reducer, numReducers);
	checkCudaErrors(cudaDeviceSynchronize());
	
	cudaFree(d_Output);
	printf("Linearize time %lf\n", timer() - begin);
	return mapKVcount;

}



