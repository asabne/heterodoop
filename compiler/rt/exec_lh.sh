export JAVA_HOME="/share/apps/teragrid/jdk64/jdk1.7.0_25"
export HADOOP_SRC_HOME="/home/01691/asabne/hadoop-1.2.1/"
export HADOOP_HOME="/home/01691/asabne/hadoop-1.2.1/" # CDH
export CXXFLAGS="-O2 -g -I$HADOOP_SRC_HOME/src/c++/libhdfs/ -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/"
export LDFLAGS="-L$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/ -lhdfs -L$JAVA_HOME/jre/lib/amd64/server/ -ljvm"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/:$JAVA_HOME/jre/lib/amd64/server/"

export CLASSPATH=`$HADOOP_HOME/bin/hadoop classpath` 
export JAVA_OPTS=-Xmx2048M
export LD_LIBRARY_PATH=/share/apps/cuda/4.1/cuda/lib64:/opt/apps/intel11_1/mvapich2/1.4/lib:/opt/apps/intel11_1/mvapich2/1.4/lib/shared:/opt/apps/intel/11.1/lib/intel64:/share/apps/cuda/4.1/cuda/computeprof/bin:$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/:$JAVA_HOME/jre/lib/amd64/server/
#./a.out
unbuffer ~/cuda_rt/driver0 &> ~/cuda_rt/log_${HOSTNAME}_0&
unbuffer ~/cuda_rt/driver1 &> ~/cuda_rt/log_${HOSTNAME}_1&
