/*
** semdemo.c -- demonstrates semaphore use as a file locking mechanism
*/

#include "jni.h"
#include "hdfs.h"
#include <sys/time.h>

#include <stdio.h>
#include <stdlib.h>


#include "mr.h"

#define MAX_RETRIES 10
#define KEY 10
#define SHM_SIZE 16


struct my_msgbuf {
    long mtype;
    pid_t pid;
		char inputPath[500];
		char outputPath[500];
		int numReducers;
};

union semun {
	int val;
	struct semid_ds *buf;
	ushort *array;
};

int driverNumGlobal;

int getDriverNum() {
	return driverNumGlobal;
}

void setDriverNum(int num) {
	driverNumGlobal = num;
}

/*
** initsem() -- more-than-inspired by W. Richard Stevens' UNIX Network
** Programming 2nd edition, volume 2, lockvsem.c, page 295.
*/
int initsem(key_t key, int nsems)  /* key from ftok() */
{
	int i;
	union semun arg;
	struct semid_ds buf;
	struct sembuf sb;
	int semid;

	semid = semget(key, nsems, IPC_CREAT | IPC_EXCL | 0666);

	if (semid >= 0) { /* we got it first */
		sb.sem_op = 1; sb.sem_flg = 0;
		arg.val = 1;

		//printf("press return\n"); getchar();
		printf("Creating a lock\n");
		for(sb.sem_num = 0; sb.sem_num < nsems; sb.sem_num++) { 
			/* do a semop() to "free" the semaphores. */
			/* this sets the sem_otime field, as needed below. */
			if (semop(semid, &sb, 1) == -1) {
				int e = errno;
				semctl(semid, 0, IPC_RMID); /* clean up */
				errno = e;
				return -1; /* error, check errno */
			}
		}

	} else if (errno == EEXIST) { /* someone else got it first */
		int ready = 0;

		semid = semget(key, nsems, 0); /* get the id */
		if (semid < 0) return semid; /* error, check errno */

		/* wait for other process to initialize the semaphore: */
		arg.buf = &buf;
		for(i = 0; i < MAX_RETRIES && !ready; i++) {
			semctl(semid, nsems-1, IPC_STAT, arg);
			if (arg.buf->sem_otime != 0) {
				ready = 1;
			} else {
				sleep(1);
			}
		}
		if (!ready) {
			errno = ETIME;
			return -1;
		}
	} else {
		return semid; /* error, check errno */
	}

	return semid;
}

volatile int msqid;
volatile int semid;
volatile int shmid;

volatile int request = 0;
void sigusr1_handler(int sig)
{
	request = 1;
}

volatile int done = 0;
void sigterm_handler(int sig) { 
	done = 1;
}

void cleaner()
{
	if (semctl(semid, 0, IPC_RMID) == -1) {
		perror("semctl");
		//exit(1);
	}

	//printf("About to destroy the message queue\n");
	if (msgctl(msqid, IPC_RMID, NULL) != 0) {
		perror("msgctl");
		//exit(1);
	}

	if (shmctl(shmid, IPC_RMID, NULL) != 0) {
		perror("shmctl");
		//exit(1);
	}
	//exit(1);
}

extern int mr_main(char *inputPath, int numReducers, char *outPath, hdfsFS fs);


char * file0, * file1, * file2, *filepath, *driverNum, *requesterPID, *bytesRead, *clientPID, *writePerm;
volatile int semidSM;
volatile int shmid0;
volatile int shmid1;
volatile int shmid2;
volatile int shmid3;
volatile int shmid4;
volatile int shmid5;
volatile int shmid6;
volatile int shmid7;
volatile int shmid8;
volatile int hdfsDone = 0;

void sigusr2_handler(int sig)
{
	//printf("SIGALRM received\n");
	hdfsDone= 1;
}

struct sembuf sb;

void init_hdfs() {

	sb.sem_num = 0;
	sb.sem_op = -1;  /* set to allocate resource */
	sb.sem_flg = SEM_UNDO;

	struct sigaction sa;

	sa.sa_handler = sigusr2_handler;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);

	if (sigaction(SIGUSR2, &sa, NULL) == -1) {
			perror("sigaction");
			exit(1);
	}	
	
	if(getDriverNum() == 0) {
		//Create shared memory segments for the files
		if ((shmid0 = shmget(KEY_F0, FILESIZE_MAX, 0644 | IPC_CREAT)) == -1) {
				perror("shmget F0");
				exit(1);
		}
		/* attach to the segment to get a pointer to it: */
		file0 = (char*) shmat(shmid0, (void *)0, 0);
		if (file0 == (char *)(-1)) {
				perror("shmat");
				exit(1);
		}

	} else if(getDriverNum() == 1) {
		//Create shared memory segments for the files
		if ((shmid1 = shmget(KEY_F1, FILESIZE_MAX, 0644 | IPC_CREAT)) == -1) {
				perror("shmget F1");
				exit(1);
		}
		/* attach to the segment to get a pointer to it: */
		file0 = (char*) shmat(shmid1, (void *)0, 0);
		if (file0 == (char *)(-1)) {
				perror("shmat");
				exit(1);
		}
	} else if(getDriverNum() == 2) {
		//Create shared memory segments for the files
		if ((shmid2 = shmget(KEY_F2, FILESIZE_MAX, 0644 | IPC_CREAT)) == -1) {
				perror("shmget F2");
				exit(1);
		}
		/* attach to the segment to get a pointer to it: */
		file0 = (char*) shmat(shmid2, (void *)0, 0);
		if (file0 == (char *)(-1)) {
				perror("shmat");
				exit(1);
		}
	}

	//Create shared memory segments for IPC
	if ((shmid3 = shmget(KEY_F3, FILEPATH_SIZE, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget F3");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	filepath = (char*) shmat(shmid3, (void *)0, 0);
	if (filepath == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}

	if ((shmid4 = shmget(KEY_F4, 16, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget F4");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	driverNum = (char*) shmat(shmid4, (void *)0, 0);
	if (driverNum == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}
	
	if ((shmid5 = shmget(KEY_F5, 16, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget F5");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	requesterPID = (char*) shmat(shmid5, (void *)0, 0);
	if (requesterPID == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}

	if ((shmid6 = shmget(KEY_F6, 16, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget F6");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	bytesRead = (char*) shmat(shmid6, (void *)0, 0);
	if (bytesRead == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}

	if ((shmid7 = shmget(KEY_F7, 16, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget F7");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	clientPID = (char*) shmat(shmid7, (void *)0, 0);
	if (clientPID == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}

	if ((shmid8 = shmget(KEY_F8, 16, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget F8");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	writePerm = (char*) shmat(shmid8, (void *)0, 0);
	if (writePerm == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}

		/* grab the semaphore set created by seminit.c: */
	if ((semidSM = semget(KEY_SEM, 1, 0)) == -1) {
		perror("semget SEM");
		exit(1);
	}

}

char str[200];

int main(int argc, char **argv)
{
	key_t key;
	char *pidBuffer;
	struct my_msgbuf buf;
	
	struct sigaction sa;

	sa.sa_handler = sigterm_handler;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);

	if (sigaction(SIGTERM, &sa, NULL) == -1) {
			perror("sigaction");
			exit(1);
	}
	

	struct sigaction sa2;

	sa2.sa_handler = sigusr1_handler;
	sa2.sa_flags = 0;
	sigemptyset(&sa2.sa_mask);

	if (sigaction(SIGUSR1, &sa2, NULL) == -1) {
			perror("sigaction");
			exit(1);
	}

	
	strcpy (str,argv[1]);
	printf("Before String Cat host %s\n", str);
	strcat(str, argv[2]);

	setDriverNum(atoi(argv[3]));

	init_hdfs();
	atexit(cleaner);
	
	/*
	hdfsFS fs = hdfsConnect("default", 0);
  if (!fs) {
        fprintf(stderr, "Cannot connect to HDFS.\n");
        exit(-1);
	}
	*/

	/*
	struct sembuf sb;
	
	sb.sem_num = 0;
	sb.sem_op = -1;  
	sb.sem_flg = SEM_UNDO;
	*/
	/*
	if ((key = ftok("driver.c", 'J')) == -1) {
		perror("ftok");
		exit(1);
	}
	*/
	key = 10 * (getDriverNum() + 1 );
	/* grab the semaphore set created by seminit.c: */
	if ((semid = initsem(key, 1)) == -1) {
		perror("initsem");
		exit(1);
	}

	printf("Semaphore initialized\n");

	if ((msqid = msgget(key, 0644 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
	}
	
	printf("Message queue initialized\n");

	//Create a shared memory segment
	if ((shmid = shmget(key, SHM_SIZE, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget");
		  exit(1);
	}

	/* attach to the segment to get a pointer to it: */
	pidBuffer = (char*) shmat(shmid, (void *)0, 0);
	if (pidBuffer == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}

	/* read or modify the segment, based on the command line: */
	char tempBuffer[SHM_SIZE];
	sprintf(tempBuffer, "%d", getpid());
	strncpy(pidBuffer, tempBuffer, SHM_SIZE);

	printf("Data written to the shared segment %s\n", pidBuffer);

	/* detach from the segment: */
	if (shmdt(pidBuffer) == -1) {
		  perror("shmdt");
		  exit(1);
	}

	//Initialize CUDA
	cudaSetDevice(getDriverNum());

	while(1) {
		//kill(getpid(), SIGSTOP);
		//int i;
		while(!request) {
			/*
			i++;
			if(i== 1000000) {
				printf("Waiting on requester\n");
				i=0;
			}*/
			usleep(10000);
			if(done) {
				break;
			}
		}
		if(done) {
				break;
		}
		
		request = 0;		


		//wait for an HDFS lock
		/*
		printf("File seeker PID: %d - Trying to lock\n", getpid());

		if (semop(semidSM, &sb, 1) == -1) {
			perror("semop");
			exit(1);
		}
		
		printf("Got HDFS Lock.\n");
		*/
		//write the driver number on HDFS shared segment
		//sprintf(driverNum, "%d", 0);


		if (msgrcv(msqid, &buf, sizeof(struct my_msgbuf) - sizeof(long), 0, 0) == -1) {
			perror("msgrcv");
			exit(1);
		}
		
		//make the requester go to sleep
		//kill(buf.pid, SIGSTOP);
		
		printf("Message details pid %d inputPath %s outputPath %s numReducers %d\n", 
			buf.pid, buf.inputPath, buf.outputPath, buf.numReducers);
		
		char command[500];
		//Create the output directory if it does not exist
		strcpy(command, "mkdir -p ");
    strcat(command, buf.outputPath);
    system(command);
	
		//mr_main(buf.inputPath, buf.numReducers, buf.outputPath, fs);
		mr_main(buf.inputPath, buf.numReducers, buf.outputPath, 0);
		
		/*
		//Ensure that the requester is asleep
		int status;
		waitpid(buf.pid, &status, WNOHANG);

		while(WIFSTOPPED(status)== 0) {
				waitpid(buf.pid, &status, WNOHANG);
		}	
		*/

		//wake up the requesting process
		//kill(buf.pid, SIGCONT);
			kill(buf.pid, SIGUSR1);
		printf("Done signal sent to the requester with PID %d\n", buf.pid);
	}
	
	/* remove the semaphore and the message queue: */
	/*
	if (semctl(semid, 0, IPC_RMID) == -1) {
		perror("semctl");
		exit(1);
	}
	
	if (msgctl(msqid, IPC_RMID, NULL) == -1) {
		perror("msgctl");
		exit(1);
	}
	*/
	return 0;
}
