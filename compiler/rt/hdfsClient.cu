

#include "jni.h"
#include "hdfs.h"
#include <sys/time.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <signal.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/shm.h>

#include "mr.h"

#define MAX_RETRIES 10

/*
#define KEY 99
#define KEY_F0 199
#define KEY_F1 299
#define KEY_F2 399
#define KEY_F3 499
#define KEY_F4 599
#define KEY_F5 699
#define KEY_F6 799
#define KEY_F7 899
#define FILEPATH_SIZE 256
*/

union semun {
	int val;
	struct semid_ds *buf;
	ushort *array;
};


int initsem(key_t key, int nsems)  /* key from ftok() */
{
	int i;
	union semun arg;
	struct semid_ds buf;
	struct sembuf sb;
	int semid;

	semid = semget(key, nsems, IPC_CREAT | IPC_EXCL | 0666);

	if (semid >= 0) { /* we got it first */
		sb.sem_op = 1; sb.sem_flg = 0;
		arg.val = 1;

		//printf("press return\n"); getchar();
		printf("Creating a lock for HDFS\n");
		for(sb.sem_num = 0; sb.sem_num < nsems; sb.sem_num++) { 
			/* do a semop() to "free" the semaphores. */
			/* this sets the sem_otime field, as needed below. */
			if (semop(semid, &sb, 1) == -1) {
				int e = errno;
				semctl(semid, 0, IPC_RMID); /* clean up */
				errno = e;
				return -1; /* error, check errno */
			}
		}

	} else if (errno == EEXIST) { /* someone else got it first */
		int ready = 0;

		semid = semget(key, nsems, 0); /* get the id */
		if (semid < 0) return semid; /* error, check errno */

		/* wait for other process to initialize the semaphore: */
		arg.buf = &buf;
		for(i = 0; i < MAX_RETRIES && !ready; i++) {
			semctl(semid, nsems-1, IPC_STAT, arg);
			if (arg.buf->sem_otime != 0) {
				ready = 1;
			} else {
				sleep(1);
			}
		}
		if (!ready) {
			errno = ETIME;
			return -1;
		}
	} else {
		return semid; /* error, check errno */
	}

	return semid;
}

volatile int semid;
volatile int shmid0;
volatile int shmid1;
volatile int shmid2;
volatile int shmid3;
volatile int shmid4;
volatile int shmid5;
volatile int shmid6;
volatile int shmid7;
volatile int shmid8;

volatile int request = 0;
volatile int done = 0;
void sigusr1_handler(int sig)
{	
	//printf("SIGUSR2 received\n");
	request = 1;
}

void sigterm_handler(int sig)
{	
	//printf("SIGUSR2 received\n");
	done = 1;
}


void cleaner()
{
	if (semctl(semid, 0, IPC_RMID) == -1) {
		perror("semctl destroy");
		//exit(1);
	}

	//printf("About to destroy the Shared memory segments\n");
	if (shmctl(shmid0, IPC_RMID, NULL) != 0) {
		perror("shmctl");
		//exit(1);
	}

	if (shmctl(shmid1, IPC_RMID, NULL) != 0) {
		perror("shmctl");
		//exit(1);
	}

	if (shmctl(shmid2, IPC_RMID, NULL) != 0) {
		perror("shmctl");
		//exit(1);
	}

	if (shmctl(shmid3, IPC_RMID, NULL) != 0) {
		perror("shmctl");
		//exit(1);
	}

	if (shmctl(shmid4, IPC_RMID, NULL) != 0) {
		perror("shmctl");
		//exit(1);
	}

	if (shmctl(shmid5, IPC_RMID, NULL) != 0) {
		perror("shmctl");
		//exit(1);
	}

	if (shmctl(shmid6, IPC_RMID, NULL) != 0) {
		perror("shmctl");
		//exit(1);
	}

	if (shmctl(shmid7, IPC_RMID, NULL) != 0) {
		perror("shmctl");
		//exit(1);
	}

	if (shmctl(shmid8, IPC_RMID, NULL) != 0) {
		perror("shmctl");
		//exit(1);
	}
	printf("Done destroying the Shared memory segments\n");
	//exit(1);
}



int main(int argc, char **argv) {
	
	char * file0, * file1, * file2, *filepath, *driverNum, *pid, *bytesRead, *clientPID, *writePerm;

	key_t key;

	key = KEY_SEM;
	
	struct sigaction sa;
	hdfsFile readFile;

	//attach the signal handler for sigusr1
	sa.sa_handler = sigusr1_handler;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);

	if (sigaction(SIGUSR1, &sa, NULL) == -1) {
			perror("sigaction");
			exit(1);
	}

	
	struct sigaction sa2;

	sa2.sa_handler = sigterm_handler;
	sa2.sa_flags = 0;
	sigemptyset(&sa2.sa_mask);

	if (sigaction(SIGTERM, &sa2, NULL) == -1) {
			perror("sigaction");
			exit(1);
	}	
	

	atexit(cleaner);

	//attach the signal handler for sigusr2
/*
	sa.sa_handler = sigusr2_handler;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);

	if (sigaction(SIGUSR2, &sa, NULL) == -1) {
			perror("sigaction");
			exit(1);
	}
	*/

	/* grab the semaphore set created by seminit.c: */
	if ((semid = initsem(key, 1)) == -1) {
		perror("initsem");
		exit(1);
	}


	//Create shared memory segments for 3 files

	if ((shmid0 = shmget(KEY_F0, FILESIZE_MAX, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget : F0");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	file0 = (char*) shmat(shmid0, (void *)0, 0);
	if (file0 == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}

	if ((shmid1 = shmget(KEY_F1, FILESIZE_MAX, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget : F1");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	file1 = (char*) shmat(shmid1, (void *)0, 0);
	if (file1 == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}


	if ((shmid2 = shmget(KEY_F2, FILESIZE_MAX, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget : F2");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	file2 = (char*) shmat(shmid2, (void *)0, 0);
	if (file2 == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}


	//Create shared memory segments for IPC
	if ((shmid3 = shmget(KEY_F3, FILEPATH_SIZE, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget : F3");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	filepath = (char*) shmat(shmid3, (void *)0, 0);
	if (filepath == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}

	if ((shmid4 = shmget(KEY_F4, 16, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget : F4");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	driverNum = (char*) shmat(shmid4, (void *)0, 0);
	if (driverNum == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}
	
	if ((shmid5 = shmget(KEY_F5, 16, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget : F5");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	pid = (char*) shmat(shmid5, (void *)0, 0);
	if (pid == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}

	if ((shmid6 = shmget(KEY_F6, 16, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget : F6");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	bytesRead = (char*) shmat(shmid6, (void *)0, 0);
	if (bytesRead == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}
	
	if ((shmid7 = shmget(KEY_F7, 16, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget : F7");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	clientPID = (char*) shmat(shmid7, (void *)0, 0);
	if (clientPID == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}

	if ((shmid8 = shmget(KEY_F8, 16, 0644 | IPC_CREAT)) == -1) {
		  perror("shmget : F8");
		  exit(1);
	}
	/* attach to the segment to get a pointer to it: */
	writePerm = (char*) shmat(shmid8, (void *)0, 0);
	if (writePerm == (char *)(-1)) {
		  perror("shmat");
		  exit(1);
	}


	//write the clientPID
	sprintf(clientPID, "%d", getpid());


	//clear the write flag by default
	sprintf(writePerm, "%d", 0);

	//connect to the hdfs
	hdfsFS fs = hdfsConnect("default", 0);
  if (!fs) {
      fprintf(stderr, "Cannot connect to HDFS.\n");
      kill(getpid(), SIGUSR1);
	}

	//create lockFile for HDFS
	//char*	host = getenv("HOSTNAME");
	char str[200];
	strcpy (str,argv[1]);
	//printf("Before String Cat host %s\n", str);
	strcat(str, argv[2]);
	
	FILE *lock = fopen(str, "w");
	if(!lock) {
		perror("Lock file creation error");
	}	
	fclose(lock);

	int count = 0;
	while(1) {
		//go to sleep 
		//kill(getpid(), SIGSTOP);
		while(!request) {
			usleep(10000);
			if(done) {
				printf("Ending\n");
				break;
			}
		}
		if(done) {
			break;
		}	
		
		request = 0;
	
		char *outPath;
		int driver = atoi(driverNum);
		if(driver == 0) {
			outPath = file0;
		} else if(driver == 1) {
			outPath = file1;
		} else if(driver == 2) {
			outPath = file2;
		} else {
			printf("Erroreous driver number\n");
			break;
		}

		if(atoi(writePerm) == 0) {
			printf("Request to read file %s from driver %d, count %d\n", filepath, driver, count);
			readFile = hdfsOpenFile(fs, filepath, O_RDONLY, 0, 0, 0);
			if(!readFile) {
				fprintf(stderr, "Failed to open %s for reading!\n", filepath);
				break;
			}

			int sz;
			sz = hdfsRead(fs, readFile, outPath, FILESIZE_MAX);
			hdfsCloseFile(fs, readFile);
			//write the number of bytes read
			sprintf(bytesRead, "%d", sz);		
		} else {
			printf("Request to write file %s from driver %d, count %d for %d bytes\n", filepath, driver, count, atoi(bytesRead));
			hdfsFile writeFile = hdfsOpenFile(fs, filepath, O_WRONLY|O_CREAT, 0, 0, 0);
			if(!writeFile) {
				fprintf(stderr, "Failed to open %s for writing!\n", filepath);
				break;
			}
			/*
			for(int i=0; i< atoi(bytesRead); i++) {
				putc( outPath[i], stdout);
			}
			*/

		  tSize num_written_bytes = hdfsWrite(fs, writeFile, (void*) outPath, atoi(bytesRead));
			//printf("Bytes written %d\n", num_written_bytes);
		  if (hdfsFlush(fs, writeFile)) {
		         fprintf(stderr, "Failed to 'flush' %s\n", writeFile); 
		        exit(-1);
		  }
			hdfsCloseFile(fs, writeFile);

		}
		//send signal to the seeker
		printf("Sending SIGUSR2 to driver pid %d\n", atoi(pid));
		kill((pid_t) atoi(pid), SIGUSR2);
		
		count ++;

	}

	//kill self, just for checking
	//kill(getpid(), SIGUSR1);


}

