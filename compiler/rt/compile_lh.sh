export JAVA_HOME="/share/apps/teragrid/jdk64/jdk1.7.0_25"
export HADOOP_SRC_HOME="/home/01691/asabne/hadoop-1.2.1/"
export HADOOP_HOME="/home/01691/asabne/hadoop-1.2.1/" # CDH
export CXXFLAGS="-O2 -g -I$HADOOP_SRC_HOME/src/c++/libhdfs/ -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/"
export LDFLAGS="-L$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/ -lhdfs -L$JAVA_HOME/jre/lib/amd64/server/ -ljvm"
export LD_LIBRARY_PATH="$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/:$JAVA_HOME/jre/lib/amd64/server/"

#g++ driver.cpp $CXXFLAGS $LDFLAGS -o driver
nvcc -arch=sm_13 $CXXFLAGS driver0.cu mapper.cu combiner.cu map_combine_kernel.cu $LDFLAGS -o driver0
nvcc -arch=sm_13 $CXXFLAGS driver1.cu mapper.cu combiner.cu map_combine_kernel.cu $LDFLAGS -o driver1
