export JAVA_HOME="/nics/c/home/asabne/jdk1.7.0_25/"
export HADOOP_SRC_HOME="/nics/c/home/asabne/hadoop-1.2.1/"
export HADOOP_HOME="/nics/c/home/asabne/hadoop-1.2.1/" # CDH
export CXXFLAGS="-g -I$HADOOP_SRC_HOME/src/c++/libhdfs/ -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/"
export LDFLAGS="-L$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/ -lhdfs -L$JAVA_HOME/jre/lib/amd64/server/ -ljvm"
export LD_LIBRARY_PATH="$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/:$JAVA_HOME/jre/lib/amd64/server/"

nvcc -O0 $CXXFLAGS hdfsClient.cu $LDFLAGS -o hdfsClient
#nvcc -O0 $CXXFLAGS test.cu $LDFLAGS -o test

#g++ driver.cpp $CXXFLAGS $LDFLAGS -o driver
nvcc -arch=sm_20 --ptxas-options=-v $CXXFLAGS driver0.cu mapper.cu combiner.cu map_combine_kernel.cu $LDFLAGS -o driver0 -O3
nvcc -arch=sm_20 --ptxas-options=-v $CXXFLAGS driver1.cu mapper.cu combiner.cu map_combine_kernel.cu $LDFLAGS -o driver1 -O3
nvcc -arch=sm_20 --ptxas-options=-v $CXXFLAGS driver2.cu mapper.cu combiner.cu map_combine_kernel.cu $LDFLAGS -o driver2 -O3
