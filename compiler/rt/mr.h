#include <string>
#include <sys/time.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <math.h>
#include "jni.h"
#include "hdfs.h"
#include <assert.h>

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <signal.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <limits.h>
#include <cuda_runtime.h>
#include <helper_functions.h>
#include <helper_cuda.h>

#define FILESIZE_MAX (268435456)
//#define FILESIZE_MAX (536870912)

#define HDFS_ATTEMPT_LIMIT 5

#define DRIVER_RESTART_PERIOD 4
#define DRIVER_LOCK_PATH "/tmp/driver"

#ifndef MR

#define MR
using namespace std;

#define COMPRESS_FACTOR 32
#define STRING_LENGTH 10

#define MAPBLOCKS 64
#define MAPBLOCKSIZE 128

#define COMBINEBLOCKS 128
#define COMBINEBLOCKSIZE 32


//The Hadoop input should have AVE_CHARS_PER_LINE characters per line (better if : atleast, OK if : on average)
#define AVE_CHARS_PER_LINE (16)

#define OPT_ON
#define MAP_VECT
#define COMB_VECT

//TRY NOT TO CHANGE THIS : TUNED FOR CC2.0
#define SORTBLOCKS 128
#define SORTBLOCKSIZE 128

#define _char_ 0
#define _int_ 1
#define _long_ 2
#define _float_ 3

#define KEY_SEM 90
#define KEY_F0 190
#define KEY_F1 290
#define KEY_F2 390
#define KEY_F3 490
#define KEY_F4 590
#define KEY_F5 690
#define KEY_F6 790
#define KEY_F7 890
#define KEY_F8 990
#define FILEPATH_SIZE 256


extern volatile int semidSM;
extern volatile int shmid0;
extern volatile int shmid1;
extern volatile int shmid2;
extern volatile int shmid3;
extern volatile int shmid4;
extern volatile int shmid5;
extern volatile int shmid6;
extern volatile int shmid7;
extern volatile int shmid8;
extern volatile int hdfsDone;
extern char * file0; 
extern char * file1;
extern char * file2;
extern char * filepath;
extern char * driverNum;
extern char * requesterPID;
extern char * bytesRead;
extern char * clientPID;
extern char * writePerm;
extern struct sembuf sb;
extern char str[200];


extern double timer();


extern __global__ void sort_kernel(int *data, int k, int j, int SIZE, char *key, int keyLength);

extern __device__ int getKeyVal(char *word, char *key, int *value, int *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength);

extern __device__  int getKeyVal(int *word, int *key, int *value, int *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength);

extern __device__  int getKeyVal(int *word, int *key, long *value, long *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength);

extern __device__  int getKeyVal(int *word, int *key, char *value, char *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength);

extern __device__ int getKeyVal(char *word, char *key, char *value, char *val, int &ptr, int high, int *indexArray,  int mapKeyLength, int mapValLength);

extern __device__ int strcmp(char *s1, char *s2, int length);

extern __device__ void strcpy(char *s1, char *s2, int length);

//This is used for the combiner
extern __device__ void strcpy(char *s1, char *s2, int length, int noUse);

extern __device__ int atoiAccelerator(char *str);

extern __device__ long atolAccelerator(char *str);

extern __device__  float atofAccelerator(char *s);

extern __device__ void setCombinerVars(int &kvsPerThread, int &laneID, int &warpId, int &ptr, int &high, int &kvCount, int &index, int size, int combinerThreads) ;
//this kernel is run with 1 threadblock, 32 threads
extern __global__ void countMapKVpairs(int *kvCount, int threadCount) ;

extern __device__ void emitKV(char *keyBuffer, int *valBuffer, char *key, int *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray);

extern __device__ void emitKV(int *keyBuffer, char *valBuffer, int *key, char *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray);

extern __device__ void emitKV(int *keyBuffer, int *valBuffer, int *key, int *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray);

extern __device__ void emitKV(float *keyBuffer, float *valBuffer, float *key, float *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray);

extern __device__ void emitKV(int *keyBuffer, long *valBuffer, int *key, long *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray);

extern __device__ void emitKV(char *keyBuffer, char *valBuffer, char *key, char *val, int &index, int *kvCount, int keyLength, int valLength, int numReducers, int totalThreads, int storesPerThread, int *indexArray);

extern __device__ void mapSetup(int &start, int &tid, int &index, int &limit, int ipSize, int storesPerThread, char *ip, int * __devKvCount, int numReducers, int totalThreads, int &threadRecordCounter, unsigned int *recordIndex);

extern __device__ void mapFinish(int index, int storesPerThread, char *key, int keyLength, int *indexArray, int totalThreads, int numReducers, int *kvCount);

extern __device__ void mapFinish(int index, int storesPerThread, int *key, int keyLength, int *indexArray, int totalThreads, int numReducers, int *kvCount);

extern __device__ void mapFinish(int index, int storesPerThread, float *key, int keyLength, int *indexArray, int totalThreads, int numReducers, int *kvCount);

extern __device__ int getline(char *ip, int ipSize, int &st, int &limit, int read);

extern __device__ int getline(char *ip, int recordsPerThread, unsigned int *lock, int &st, int *recordLocator, int &counter);

extern int getFileSize(char *name, char **ip) ;

extern int getFileSizeHDFS(char *name, char **ip, hdfsFS fs) ;

extern void allocateMapData(void **devKey, void **devVal, void **key, void **val, void **devIp, void **devKvCount, void **devIndexArray, void ** newDevIndexArray, void **indexArray, int keyLength, int valLength, int sizeofKeyType, int sizeofValType, int kvStoreSize, int sz, int totalThreads, int numReducers);

extern void releaseMapData(void *devKey, void *devVal, void *key, void *val, void *devIp, void *ip, void *devKvCount, void *devIndexArray, void *newDevIndexArray, void *indexArray, void *finalCount, void *opVal, void *opKey, int hasCombiner);

extern void setInternalVariables(int mapBlocks, int mapThreads, int &totalThreads, int sz, int &limit, int &storesPerThread, int &kvStoreSize, int numReducers, char *ip, int pairs, int &recordsPerThread, int *&recordLocator);

extern void allocateCombinerData(void **devFinalCount, void **devOpKey, void **devOpVal, void **finalCount, void **opKey, void **opVal,  int combineBlockSize, int combineBlocks, int &combinerThreads, int mapKeyLength, int mapValLength, int combKeyLength, int combValLength, int mapKVCount, int keyType, int valType) ;

extern void releaseCombinerData(void *devFinalCount, void *devOpKey, void *devOpVal);

extern void bitonic_sort(int *index, char *key, int keyLength, int SIZE) ;

extern void bitonic_sort(int *index, int *key, int keyLength, int SIZE) ;

extern void setThreads( int &mapblocks, int &mapthreads, int &combineBlocks, int &combineBlockSize) ;

extern __device__ void storeKV(char *prevWord, int *count, int combKeyLength, int combValLength, char *opKey, int *opVal, int &index, int &kvCount);

extern __device__ void storeKV(int *prevWord, int *count, int combKeyLength, int combValLength, int *opKey, int *opVal, int &index, int &kvCount);

extern __device__ void storeKV(int *prevWord, long *count, int combKeyLength, int combValLength, int *opKey, long *opVal, int &index, int &kvCount);

extern __device__ void storeKV(int *prevWord, char *count, int combKeyLength, int combValLength, int *opKey, char *opVal, int &index, int &kvCount);

extern __device__ void storeKV(char *prevWord, char *count, int combKeyLength, int combValLength, char *opKey, char *opVal, int &index, int &kvCount);

extern void initFile(char *path);

extern void closeFile();

extern void writeOP(int mapKVcount, int *finalCount, int keyType, int valType, void *keyArray, void *valArray, int keyLength, int valLength);

extern void writeOPNoCombiner( int *newDevIndexArray, int * indexArray, void* keyArray, void *valArray, void *devKey, void *devVal,  int keyLength, int valLength, int keyType, int valType, int mapKVcount, int kvStoreSize);

//extern void getLastCudaError(char *s);

extern int linearize(int *devIndexArray, int *newIndexArray, int *kvCount, int totalThreads, int storesPerBin, int reducer, int numReducers);

extern int getDriverNum();

extern void writeOPonHDFS(void *devKey, void *devVal, void *key, void *val, int *devKvCount, int *devIndexArray, int *indexArray, int keyLength, int valLength, int keyType, int valType, int kvStoreSize, int totalThreads, char *outputPath, hdfsFS fs);

extern void writeOPonHDFSwithDevHelp(void *devKey, void *devVal, void *key, void *val, int *devKvCount, int *devIndexArray, int *indexArray, int keyLength, int valLength, int keyType, int valType, int kvStoreSize, int totalThreads, char *outputPath, hdfsFS fs);

extern size_t prefixSum(uint *d_Dst, uint *d_Src, uint batchSize, uint arrayLength);

extern void mergeSort(int *indexArray, char *d_SrcKey, int keyLength, int N);
extern void mergeSort(int *indexArray, int *d_SrcKey, int keyLength, int N);

extern void check_long_sort();
//extern int tempCount;

#endif
