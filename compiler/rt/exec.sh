export JAVA_HOME="/usr/local/jdk1.7.0_25/"
export HADOOP_SRC_HOME="/home/asabne/hadoop-1.2.1/"
export HADOOP_HOME="/home/asabne/hadoop-1.2.1/" # CDH
export CXXFLAGS="-O2 -g -Wall -I$HADOOP_SRC_HOME/src/c++/libhdfs/ -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/"
export LDFLAGS="-L$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/ -lhdfs -L$JAVA_HOME/jre/lib/amd64/server/ -ljvm"
export LD_LIBRARY_PATH="$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/:$JAVA_HOME/jre/lib/amd64/server/"

CLASSPATH=./
for f in $HADOOP_HOME/*.jar; do
CLASSPATH=${CLASSPATH}:$f;
done
for f in $HADOOP_HOME/lib/*.jar; do
CLASSPATH=${CLASSPATH}:$f;
done
export CLASSPATH=$CLASSPATH

export CLASSPATH=`hadoop classpath` 




#export LOCKPATH="/nics/c/home/asabne/cuda_rt/"
export LOCKPATH="/home/asabne/cuda_rt/"
#~/cuda_rt/hdfsClient &
#sleep 5
#killall -SIGUSR1 hdfsClient; killall -SIGCONT hdfsClient;

unbuffer ~/cuda_rt/hdfsClient ${LOCKPATH} ${HOSTNAME} &> ~/cuda_rt/log_${HOSTNAME}_HDFS&
sleep 2
#~/cuda_rt/test
#exit

#./a.out
unbuffer ~/cuda_rt/driver0 ${LOCKPATH} ${HOSTNAME} > ~/cuda_rt/log_${HOSTNAME}_0 2>&1 &
