export JAVA_HOME="/usr/local/jdk1.7.0_25/"
export HADOOP_SRC_HOME="/home/asabne/hadoop-1.2.1/"
export HADOOP_HOME="/home/asabne/hadoop-1.2.1/" # CDH
export CXXFLAGS="-O3 -g -I$HADOOP_SRC_HOME/src/c++/libhdfs/ -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux/"
export LDFLAGS="-L$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/ -lhdfs -L$JAVA_HOME/jre/lib/amd64/server/ -ljvm"
export LD_LIBRARY_PATH="$HADOOP_SRC_HOME/build/c++/Linux-amd64-64/lib/:$JAVA_HOME/jre/lib/amd64/server/"

nvcc $CXXFLAGS hdfsClient.cu $LDFLAGS -o hdfsClient
#g++ driver.cpp $CXXFLAGS $LDFLAGS -o driver
nvcc -arch=sm_35 driver0.cu mapper.cu map_combine_kernel.cu $CXXFLAGS $LDFLAGS -o driver0
