This directory contains the HeteroDoop compiler source code and the necessary libraries.

Following is the directory map:

lib: contains cetus.jar and antlr.jar. These jars are necessary to build the heteroDoop compiler

rt: contains the runtime system source files.

src: contains source for the base cetus package and the heteroDoop.

