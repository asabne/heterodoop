package cudamr.exec;


/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/9/13
 * Time: 11:36 AM
 * To change this template use File | Settings | File Templates.
 * @author Amit Sabne <asabne@purdue.edu>
 *         Purdue University
 */


import java.io.*;
import java.util.*;

import cetus.analysis.*;
import cetus.hir.*;
import cetus.transforms.*;
import cetus.codegen.*;
import cetus.exec.*;

import cudamr.codegen.*;

public class cudamrDriver extends Driver
{

    protected cudamrDriver() {
            super();
      addOptions();
    }

  public static void addOptions() {
    options.add(options.TRANSFORM,
            "noReducer",
            "Do not use any reducer for the translation");
    options.add(options.TRANSFORM,
            "noCombiner",
            "Do not use any combiner for the translation");
  }
  public void run(String[] args)
    {
        parseCommandLine(args);
        String value = getOptionValue("addIncludePath");
        if( value != null )
        {
            String newCPPCMD = getOptionValue("preprocessor") + " -I" + value;
            setOptionValue("preprocessor", newCPPCMD);
        }

        parseFiles();

        if (getOptionValue("debug_parser_output") != null)
        {
            System.err.println("print parsed output and exit without doing any analysis/transformation!");
            System.exit(0);
        }

        value = Driver.getOptionValue("noReducer");
        if( value != null ) {
          c2cu.noReducer = true;
        } else {
          c2cu.noReducer = false;
        }

        value = Driver.getOptionValue("noCombiner");
        if( value != null ) {
          c2cu.noCombiner = true;
        } else {
          c2cu.noCombiner = false;
        }
        //setPasses();

        runPasses();

        CodeGenPass.run(new c2cu(program));

        PrintTools.printlnStatus("Printing...", 1);

        try {
            program.print();
        } catch (IOException e) {
            System.err.println("could not write output files: " + e);
            System.exit(1);
        }
    }
    /**
     * Entry point for Cetus; creates a new Driver object,
     * and calls run on it with args.
     *
     * @param args Command line options.
     */
    public static void main(String[] args)
    {
        cudamrDriver cudamr = new cudamrDriver();
        cudamr.run(args);
    }

}
