package cudamr.codegen;

import cetus.codegen.CodeGenPass;
import cetus.hir.*;

import cetus.transforms.SingleDeclarator;
import cetus.transforms.TransformPass;
import cudamr.transform.*;
import cudamr.analysis.mapreduceParser;

import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/9/13
 * Time: 11:36 AM
 * To change this template use File | Settings | File Templates.
 * @author Amit Sabne <asabne@purdue.edu>
 *         Purdue University
 */


public class c2cu extends CodeGenPass {
    public static Specifier keySpec;
    public static Specifier valSpec;
    public static Expression mapBufKey;
    public static Expression mapBufVal;
    public static Expression mapKVCount;
    public static Procedure mainProc;
    public static Expression combBufKey;
    public static Expression combBufVal;
    public static Specifier combKeySpec;
    public static Specifier combValSpec;
    public static Expression combBufInKey;
    public static Expression combBufInVal;
    public static boolean noReducer;
    public static boolean noCombiner;

    public static HashSet<Symbol> mapperSharedVars;
    public static HashSet<Symbol> mapperTextureVars;

    public static TranslationUnit kernelsTranslationUnit = new TranslationUnit("map_combine_kernel.cu");
    public c2cu(Program program) {
        super(program);
    }

    public void start(){
        mapperSharedVars = new HashSet<Symbol>();
        mapperTextureVars = new HashSet<Symbol>();
        //Add kernel translation unit to the program
        program.addTranslationUnit(kernelsTranslationUnit);

        //TransformPass.run(new DeclarationInitSeparator(program));

        TransformPass.run(new mapreduceAnnotationParser(program));
        TransformPass.run(new privatizer(program));
        TransformPass.run(new mrCUDASemanticProcessor(program));
        TransformPass.run(new kernelExtractor(program));

        PrintTools.println("[c2cuTranslator] begin", 0);
        c2cuTranslator c2cu = new c2cuTranslator(program);
        c2cu.start();
        PrintTools.println("[c2cuTranslator] end", 0);

        //TransformTools.removeUnusedSymbols(program);
    }

    public String getPassName()
    {
        return new String("[c2cu]");
    }
}
