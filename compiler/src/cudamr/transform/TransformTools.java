package cudamr.transform;

import cetus.hir.*;
import cudamr.analysis.AnalysisTools;
import cudamr.hir.CUDASpecifier;
import cudamr.hir.KernelFunctionCall;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/14/13
 * Time: 4:54 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class TransformTools {
    private TransformTools()
    {
    }

    public static Specifier getType(CompoundStatement cstmt, String varName) {
        Specifier retVal = Specifier.ABSTRACT;
        VariableDeclaration keyD = (VariableDeclaration)cstmt.findSymbol(new NameID(varName));
        if(keyD.getSpecifiers().contains(Specifier.FLOAT)){
            retVal = Specifier.FLOAT;
        } else if(keyD.getSpecifiers().contains(Specifier.INT)) {
            retVal = Specifier.INT;
        } else if(keyD.getSpecifiers().contains(Specifier.CHAR)) {
            retVal = Specifier.CHAR;
        }  else if(keyD.getSpecifiers().contains(Specifier.LONG)) {
          retVal = Specifier.LONG;
        }

        return  retVal;
    }

    public static void addStatementBefore(CompoundStatement parent, Statement ref_stmt, Statement new_stmt) {
        List<Traversable> children = parent.getChildren();
        int index = Tools.indexByReference(children, ref_stmt);
        if (index == -1)
            throw new IllegalArgumentException();
        if (new_stmt.getParent() != null)
            throw new NotAnOrphanException();
        children.add(index, new_stmt);
        new_stmt.setParent(parent);
        if( new_stmt instanceof DeclarationStatement ) {
            Declaration decl = ((DeclarationStatement)new_stmt).getDeclaration();
            SymbolTools.addSymbols(parent, decl);
        }
    }


    static public void removeUnusedSymbols(Program prog) {
        Set<TranslationUnit> skipTrList = new HashSet<TranslationUnit>();
        List<Procedure> procList = IRTools.getProcedureList(prog);
        for( Procedure proc : procList ) {
            List returnTypes = proc.getTypeSpecifiers();
            if( returnTypes.contains(CUDASpecifier.CUDA_GLOBAL)
                    || returnTypes.contains(CUDASpecifier.CUDA_DEVICE) ) {
                skipTrList.add(IRTools.getParentTranslationUnit(proc));
            }
            CompoundStatement pBody = proc.getBody();
            List<OmpAnnotation> ompAnnots = AnalysisTools.collectPragmas(pBody, OmpAnnotation.class);
            boolean checkOmpAnnots = false;
            if( (ompAnnots != null) && (!ompAnnots.isEmpty()) ) {
                checkOmpAnnots = true;
            }
            Set<Symbol> accessedSymbols = AnalysisTools.getAccessedVariables(pBody, true);
            //Set<Symbol> declaredSymbols = SymbolTools.getVariableSymbols(pBody);
            Set<Symbol> declaredSymbols = SymbolTools.getLocalSymbols(pBody);
            List<SomeExpression> someExps = IRTools.getExpressionsOfType(pBody, SomeExpression.class);
            HashSet<Symbol> unusedSymbolSet = new HashSet<Symbol>();
            //Symbols used in kernel function configuration are not visible to IR tree.
            //Therefore, they should be checked manually.
            List<FunctionCall> funcCalls = IRTools.getFunctionCalls(pBody);
            if( funcCalls != null ) {
                for( FunctionCall fCall : funcCalls ) {
                    if( fCall instanceof KernelFunctionCall) {
                        List<Traversable> kConfList = (List<Traversable>)((KernelFunctionCall)fCall).getConfArguments();
                        for( Traversable tConf : kConfList ) {
                            Set<Symbol> tSyms = AnalysisTools.getAccessedVariables(tConf, true);
                            if( tSyms != null ) {
                                accessedSymbols.addAll(tSyms);
                            }
                        }
                    }
                }
            }

            for( Symbol sym : declaredSymbols ) {
                String sname = sym.getSymbolName();
                CompoundStatement cParent = null;
                if( sname.startsWith("dimBlock") || sname.startsWith("dimGrid") ||
                        sname.startsWith("_gtid") || sname.startsWith("_bid") ||
                        sname.startsWith("row_temp_") || sname.startsWith("lred__") ||
                        sname.endsWith("__extended") || //sname.startsWith("gpu__") ||
                        sname.startsWith("red__") || sname.startsWith("param__") ||
                        sname.startsWith("pitch__") || sname.startsWith("sh__") ) {
                    continue;
                }
                if( !accessedSymbols.contains(sym) ) {
                    String symName = sym.getSymbolName();
                    boolean notUsed = true;
                    if( someExps != null ) {
                        for( SomeExpression sExp: someExps ) {
                            if( sExp.toString().contains(symName) ) {
                                notUsed = false;
                                break;
                            }
                        }
                    }
                    if( notUsed ) {
                        if( sym instanceof VariableDeclarator ) {
                            VariableDeclarator declr = (VariableDeclarator)sym;
                            if( !AnalysisTools.isClassMember(declr) ) {
                                Declaration decl = declr.getDeclaration();
                                cParent = (CompoundStatement)decl.getParent().getParent();
                                unusedSymbolSet.add(sym);
                                //pBody.removeChild(decl.getParent());
                                cParent.removeChild(decl.getParent());
                            }
                        }
                        if( checkOmpAnnots ) {
                            //Remove unused symbols from OpenMP clauses.
                            for( OmpAnnotation oAnnot : ompAnnots ) {
                                for( String key: oAnnot.keySet() ) {
                                    Object obj = oAnnot.get(key);
                                    if( (obj instanceof String) && ((String)obj).equals(symName) ) {
                                        oAnnot.remove(key);
                                    } else if( obj instanceof Set ) {
                                        Set<String> sSet = (Set<String>)obj;
                                        sSet.remove(symName);
                                        if( sSet.isEmpty() ) {
                                            oAnnot.remove(key);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if( !unusedSymbolSet.isEmpty() ) {
                PrintTools.println(" Declarations removed from a procedure, " +
                        proc.getSymbolName() + ": " + AnalysisTools.symbolsToString(unusedSymbolSet, ", "), 2);
            }
        }

        //DEBUG: removing unused dummy variables are temporarily disabled, since it may cause
        //problems when one file is manually included in another file.
/*		List<String> dummyVarList = Arrays.asList(dummyVars);
		for ( Traversable tt : prog.getChildren() )
		{
			HashSet<Symbol> unusedSymbolSet = new HashSet<Symbol>();
			TranslationUnit tu = (TranslationUnit)tt;
			String iFileName = tu.getInputFilename();
			int dot = iFileName.lastIndexOf(".h");
			if( dot >= 0 ) {
				continue;
			} else if( skipTrList.contains(tu)) {
				continue;
			}
			Set<Symbol> accessedSymbols = SymbolTools.getAccessedSymbols(tu);
			if( accessedSymbols == null ) {
				continue;
			}
			for(String var : dummyVarList) {
				Declaration symDecl = SymbolTools.findSymbol(tu, var);
				if( ( symDecl != null ) && ( symDecl instanceof VariableDeclaration ) ) {
					VariableDeclarator sym = (VariableDeclarator)((VariableDeclaration)symDecl).getDeclarator(0);
					if( !accessedSymbols.contains(sym)) {
						unusedSymbolSet.add(sym);
						tu.removeChild(symDecl);
					}
				}
			}
			if( !unusedSymbolSet.isEmpty() ) {
				PrintTools.println(" Declarations removed from a translation unit, " +
						iFileName + ": " + AnalysisTools.symbolsToString(unusedSymbolSet, ", "), 2);
			}
		}*/
    }


}
