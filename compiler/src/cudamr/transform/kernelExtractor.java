package cudamr.transform;

import cetus.hir.*;
import cetus.transforms.TransformPass;
import cudamr.analysis.AnalysisTools;
import cudamr.codegen.c2cu;
import cudamr.hir.CUDASpecifier;
import cudamr.hir.KernelFunctionCall;
import cudamr.hir.TextureSpecifier;
import cudamr.hir.mapreduceAnnotation;


import java.util.*;
/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/16/13
 * Time: 7:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class kernelExtractor extends TransformPass {
    private static String pass_name = "[kernelExtractor]";

    public kernelExtractor(Program program) {
        super(program);
    }

    public String getPassName() {
        return pass_name;
    }

    public void start() {
        List<Procedure> procedureList = IRTools.getProcedureList(program);
        for( Procedure cProc : procedureList ) {
            List<mapreduceAnnotation> regionAnnots = AnalysisTools.collectPragmas(cProc, mapreduceAnnotation.class);
            for( mapreduceAnnotation pAnnot : regionAnnots ) {
                //Process the mapper part
                if(pAnnot.get("mapper")!= null) {
                    List<Specifier> new_proc_ret_type = new LinkedList<Specifier>();
                    //new_proc_ret_type.add(CUDASpecifier.EXTERN);
                    //new_proc_ret_type.add(CUDASpecifier.EXTERN_C);
                    new_proc_ret_type.add(CUDASpecifier.CUDA_GLOBAL);
                    new_proc_ret_type.add(Specifier.VOID);
                    String new_func_name = "gpu_mapper";
                    Procedure new_proc = new Procedure(new_proc_ret_type,
                            new ProcedureDeclarator(new NameID(new_func_name),
                                    new LinkedList()), new CompoundStatement());
                    List<Expression> kernelConf = new ArrayList<Expression>();
                    KernelFunctionCall call_to_new_proc = new KernelFunctionCall(new NameID(
                            new_func_name), new LinkedList(), kernelConf);
                    call_to_new_proc.setLinkedProcedure(new_proc);

                    List<Specifier> specs = null;

                    Declaration ip_decl = null;
                    VariableDeclarator ip_declarator = new VariableDeclarator(new NameID("__ip"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.CHAR);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    ip_decl = new VariableDeclaration(specs, ip_declarator);
                    new_proc.addDeclaration(ip_decl);

                    Declaration ipSize_decl = null;
                    VariableDeclarator ipSize_declarator = new VariableDeclarator(new NameID("__ipSize"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    ipSize_decl = new VariableDeclaration(specs, ipSize_declarator);
                    new_proc.addDeclaration(ipSize_decl);


                    //Define arrays for keys and values
                    Declaration devKey = null;
                    VariableDeclarator devKeyDecl = new VariableDeclarator(new NameID("devKey"));
                    specs = new LinkedList<Specifier>();
                    specs.add(c2cu.keySpec);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    devKey = new VariableDeclaration(specs, devKeyDecl);
                    new_proc.addDeclaration(devKey);


                    Declaration devVal = null;
                    VariableDeclarator devValDecl = new VariableDeclarator(new NameID("devVal"));
                    specs = new LinkedList<Specifier>();
                    specs.add(c2cu.valSpec);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    devVal = new VariableDeclaration(specs, devValDecl);
                    new_proc.addDeclaration(devVal);

                    Declaration storesPerThread_decl = null;
                    VariableDeclarator storesPerThread_declarator = new VariableDeclarator(new NameID("__storesPerThread"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    storesPerThread_decl = new VariableDeclaration(specs, storesPerThread_declarator);
                    new_proc.addDeclaration(storesPerThread_decl);

                    Declaration devKvCount_decl = null;
                    VariableDeclarator devKvCount_declarator = new VariableDeclarator(new NameID("__devKvCount"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    devKvCount_decl = new VariableDeclaration(specs, devKvCount_declarator);
                    new_proc.addDeclaration(devKvCount_decl);

                    Declaration limit_decl = null;
                    VariableDeclarator limit_declarator = new VariableDeclarator(new NameID("__limit"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    limit_decl = new VariableDeclaration(specs, limit_declarator);
                    new_proc.addDeclaration(limit_decl);

                    Declaration recordsPerThread_decl = null;
                    VariableDeclarator recordsPerThread_declarator = new VariableDeclarator(new NameID("recordsPerThread"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    recordsPerThread_decl = new VariableDeclaration(specs, recordsPerThread_declarator);
                    new_proc.addDeclaration(recordsPerThread_decl);

                    Declaration recordLocator_decl = null;
                    VariableDeclarator recordLocator_declarator = new VariableDeclarator(new NameID("recordLocator"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    recordLocator_decl = new VariableDeclaration(specs, recordLocator_declarator);
                    new_proc.addDeclaration(recordLocator_decl);

                    //Create variables for key and value lengths
                    Declaration keyLength_decl = null;
                    VariableDeclarator keyLength_declarator = new VariableDeclarator(new NameID("__keyLength"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    keyLength_decl = new VariableDeclaration(specs, keyLength_declarator);
                    new_proc.addDeclaration(keyLength_decl);

                    Declaration valLength_decl = null;
                    VariableDeclarator valLength_declarator = new VariableDeclarator(new NameID("__valLength"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    valLength_decl = new VariableDeclaration(specs, valLength_declarator);
                    new_proc.addDeclaration(valLength_decl);

                    Declaration devIndexArray_decl = null;
                    VariableDeclarator devIndexArray_declarator = new VariableDeclarator(new NameID("__devIndexArray"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    devIndexArray_decl = new VariableDeclaration(specs, devIndexArray_declarator);
                    new_proc.addDeclaration(devIndexArray_decl);

                    Declaration totalthreads_decl = null;
                    VariableDeclarator totalthreads_declarator = new VariableDeclarator(new NameID("__totalThreads"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    totalthreads_decl = new VariableDeclaration(specs, totalthreads_declarator);
                    new_proc.addDeclaration(totalthreads_decl);

                    Declaration numReducers_decl = null;
                    VariableDeclarator numReducers_declarator = new VariableDeclarator(new NameID("__numReducers"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    numReducers_decl = new VariableDeclaration(specs, numReducers_declarator);
                    new_proc.addDeclaration(numReducers_decl);


                    Declaration opVal_decl = null;
                    VariableDeclarator opVal_declarator = new VariableDeclarator(new NameID("__opVal"));
                    specs = new LinkedList<Specifier>();
                    specs.add(c2cu.combValSpec);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    opVal_decl = new VariableDeclaration(specs, opVal_declarator);
                    cProc.getBody().addDeclaration(opVal_decl);

                    Declaration opKey_decl = null;
                    VariableDeclarator opKey_declarator = new VariableDeclarator(new NameID("__opKey"));
                    specs = new LinkedList<Specifier>();
                    specs.add(c2cu.combKeySpec);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    opKey_decl = new VariableDeclaration(specs, opKey_declarator);
                    cProc.getBody().addDeclaration(opKey_decl);


                    Expression keyLength = (Expression)keyLength_decl.getDeclaredIDs().get(0);
                    Expression valLength = (Expression)valLength_decl.getDeclaredIDs().get(0);
                    Expression ip = (Expression)ip_decl.getDeclaredIDs().get(0);
                    Expression ipSize = (Expression)ipSize_decl.getDeclaredIDs().get(0);
                    Expression limit = (Expression)limit_decl.getDeclaredIDs().get(0);
                    Expression storesPerThread = (Expression)storesPerThread_decl.getDeclaredIDs().get(0);
                    Expression devKeys = (Expression)devKey.getDeclaredIDs().get(0);
                    Expression devVals = (Expression)devVal.getDeclaredIDs().get(0);
                    Expression devKvCount = (Expression)devKvCount_decl.getDeclaredIDs().get(0);
                    Expression devIndexArray = (Expression)devIndexArray_decl.getDeclaredIDs().get(0);
                    Expression totalThreads = (Expression)totalthreads_decl.getDeclaredIDs().get(0);
                    Expression numReducers = (Expression)numReducers_decl.getDeclaredIDs().get(0);
                    Expression recordLocator = (Expression)recordLocator_decl.getDeclaredIDs().get(0);
                    Expression recordsPerThread = (Expression)recordsPerThread_decl.getDeclaredIDs().get(0);

                    //char * ip, char *key, int* val, int limit, int ipSize, int storesPerThread, int *kvCount
                    VariableDeclaration devIp_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__devIp"));
                    VariableDeclaration ipSize_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__ipSize"));
                    VariableDeclaration devKey_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("devKey"));
                    VariableDeclaration devVal_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("devVal"));
                    VariableDeclaration limit_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__limit"));
                    VariableDeclaration storesPerThread_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__storesPerThread"));
                    VariableDeclaration devKvCount_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__devKvCount"));
                    VariableDeclaration keyLength_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__keyLength"));
                    VariableDeclaration valLength_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__valLength"));
                    VariableDeclaration devIndexArray_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__devIndexArray"));
                    VariableDeclaration newDevIndexArray_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__newDevIndexArray"));
                    VariableDeclaration totalThreads_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__totalThreads"));
                    VariableDeclaration finalCount_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__finalCount"));
                    VariableDeclaration opKey_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__opKey"));
                    VariableDeclaration opVal_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__opVal"));
                    VariableDeclaration mapKeyLength_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__keyLength"));
                    VariableDeclaration mapValLength_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__valLength"));
                    VariableDeclaration combKeyLength_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__combKeyLength"));
                    VariableDeclaration combValLength_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__combValLength"));
                    VariableDeclaration kvStoreSize_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__kvStoreSize"));
                    VariableDeclaration mapblocks_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__mapblocks"));
                    VariableDeclaration mapthreads_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__mapthreads"));
                    VariableDeclaration numReducers_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__numReducers"));
                    VariableDeclaration reducer_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__reducer"));
                    VariableDeclaration storesPerBin_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__storesPerBin"));
                    VariableDeclaration outputPath_cpu = (VariableDeclaration)cProc.findSymbol(new NameID("outputPath"));
                    VariableDeclaration key_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("key"));
                    VariableDeclaration val_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("val"));
                    VariableDeclaration ip_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__ip"));
                    VariableDeclaration indexArray_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__indexArray"));
                    VariableDeclaration recordLocator_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__recordLocator"));
                    VariableDeclaration recordsPerThread_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__recordsPerThread"));
                    VariableDeclaration fs_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("fs"));
                    VariableDeclaration timer_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__timer"));

                    Expression keyLengthCPU = (Expression)keyLength_cpu.getDeclaredIDs().get(0);
                    Expression valLengthCPU = (Expression)valLength_cpu.getDeclaredIDs().get(0);
                    Expression devIpCPU = (Expression)devIp_cpu.getDeclaredIDs().get(0);
                    Expression ipSizeCPU = (Expression)ipSize_cpu.getDeclaredIDs().get(0);
                    Expression limitCPU = (Expression)limit_cpu.getDeclaredIDs().get(0);
                    Expression storesPerThreadCPU = (Expression)storesPerThread_cpu.getDeclaredIDs().get(0);
                    Expression devKeysCPU = (Expression)devKey.getDeclaredIDs().get(0);
                    Expression devValsCPU = (Expression)devVal.getDeclaredIDs().get(0);
                    Expression devKvCountCPU = (Expression)devKvCount_cpu.getDeclaredIDs().get(0);
                    Expression devIndexArrayCPU = (Expression)devIndexArray_cpu.getDeclaredIDs().get(0);
                    Expression newDevIndexArrayCPU = (Expression)newDevIndexArray_cpu.getDeclaredIDs().get(0);
                    Expression totalThreadsCPU = (Expression)totalThreads_cpu.getDeclaredIDs().get(0);
                    Expression finalCountCPU = (Expression)finalCount_cpu.getDeclaredIDs().get(0);
                    Expression opKeyCPU = (Expression)opKey_cpu.getDeclaredIDs().get(0);
                    Expression opValCPU = (Expression)opVal_cpu.getDeclaredIDs().get(0);
                    Expression mapKeyLengthCPU = (Expression)mapKeyLength_cpu.getDeclaredIDs().get(0);
                    Expression mapValLengthCPU = (Expression)mapValLength_cpu.getDeclaredIDs().get(0);
                    Expression combKeyLengthCPU = (Expression)combKeyLength_cpu.getDeclaredIDs().get(0);
                    Expression combValLengthCPU = (Expression)combValLength_cpu.getDeclaredIDs().get(0);
                    Expression kvStoreSizeCPU = (Expression)kvStoreSize_cpu.getDeclaredIDs().get(0);
                    Expression mapBlocksCPU = (Expression)mapblocks_cpu.getDeclaredIDs().get(0);
                    Expression mapThreadsCPU = (Expression)mapthreads_cpu.getDeclaredIDs().get(0);
                    Expression numReducersCPU = (Expression)numReducers_cpu.getDeclaredIDs().get(0);
                    Expression reducerCPU = (Expression)reducer_cpu.getDeclaredIDs().get(0);
                    Expression storesPerBinCPU = (Expression)storesPerBin_cpu.getDeclaredIDs().get(0);
                    Expression outputPathCPU = (Expression)outputPath_cpu.getDeclaredIDs().get(0);
                    Expression keyCPU = (Expression)key_cpu.getDeclaredIDs().get(0);
                    Expression valCPU = (Expression)val_cpu.getDeclaredIDs().get(0);
                    Expression ipCPU = (Expression)ip_cpu.getDeclaredIDs().get(0);
                    Expression indexArrayCPU = (Expression)indexArray_cpu.getDeclaredIDs().get(0);
                    Expression recordLocatorCPU = (Expression)recordLocator_cpu.getDeclaredIDs().get(0);
                    Expression recordsPerThreadCPU = (Expression)recordsPerThread_cpu.getDeclaredIDs().get(0);
                    Expression fsCPU = (Expression)fs_cpu.getDeclaredIDs().get(0);
                    Expression timerCPU = (Expression)timer_cpu.getDeclaredIDs().get(0);

                    call_to_new_proc.addArgument(devIpCPU.clone());
                    call_to_new_proc.addArgument(ipSizeCPU.clone());
                    call_to_new_proc.addArgument(devKeysCPU.clone());
                    call_to_new_proc.addArgument(devValsCPU.clone());
                    call_to_new_proc.addArgument(storesPerThreadCPU.clone());
                    call_to_new_proc.addArgument(devKvCountCPU.clone());
                    call_to_new_proc.addArgument(limitCPU.clone());
                    call_to_new_proc.addArgument(recordsPerThreadCPU.clone());
                    call_to_new_proc.addArgument(recordLocatorCPU.clone());
                    call_to_new_proc.addArgument(keyLengthCPU.clone());
                    call_to_new_proc.addArgument(valLengthCPU.clone());
                    call_to_new_proc.addArgument(devIndexArrayCPU.clone());
                    call_to_new_proc.addArgument(totalThreadsCPU.clone());
                    call_to_new_proc.addArgument(numReducersCPU.clone());





                    Statement region = (Statement) pAnnot.getAnnotatable().getParent();
                    Statement reference = region;
                    CompoundStatement parent = (CompoundStatement)region.getParent();

                    handleTextureVars(call_to_new_proc, new_proc, region);
                    handleSharedVars(call_to_new_proc, new_proc, region);

                    /*
                    VariableDeclarator dimGrid_declarator = new VariableDeclarator(new NameID("dimGrid_"+new_func_name), new ArraySpecifier(new IntegerLiteral(3)));
                    Identifier dimGrid = new Identifier(dimGrid_declarator);
                    Declaration dimGrid_decl = new VariableDeclaration(CUDASpecifier.INT, dimGrid_declarator);
                    DeclarationStatement dimGrid_stmt = new DeclarationStatement(dimGrid_decl);
                    TransformTools.addStatementBefore((CompoundStatement)region.getParent(), reference, dimGrid_stmt);
                    */

                    kernelConf.add(mapBlocksCPU.clone());
                    kernelConf.add(mapThreadsCPU.clone());
                    kernelConf.add(new IntegerLiteral(0));
                    kernelConf.add(new IntegerLiteral(0));
                    call_to_new_proc.setConfArguments(kernelConf);

                    Statement kernelCall_stmt = new ExpressionStatement(call_to_new_proc);

                    parent.addStatementBefore(region, kernelCall_stmt);

                    Statement kernelRegion = new_proc.getBody();
                    region.swapWith(kernelRegion);

                    //Traversable cst = (Traversable)region.getParent();
                    //cst.removeStatement(region);
                    //((CompoundStatement) region.getParent()).removeStatement(region);
                    //parent.removeChild(parent);

                    //Add the new mapper kernel to the mapper_combiner_kernels.cu
                    c2cu.kernelsTranslationUnit.addDeclaration(new_proc);

                    //Edit the kernel code
                    Declaration index_decl = null;
                    VariableDeclarator index_declarator = new VariableDeclarator(new NameID("__index"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    index_decl = new VariableDeclaration(specs, index_declarator);
                    new_proc.getBody().addDeclaration(index_decl);

                    Declaration tid_decl = null;
                    VariableDeclarator tid_declarator = new VariableDeclarator(new NameID("__tid"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    tid_decl = new VariableDeclaration(specs, tid_declarator);
                    new_proc.getBody().addDeclaration(tid_decl);

                    Declaration start_decl = null;
                    VariableDeclarator start_declarator = new VariableDeclarator(new NameID("__start"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    start_decl = new VariableDeclaration(specs, start_declarator);
                    new_proc.getBody().addDeclaration(start_decl);

                    Declaration recordIndex_decl = null;
                    VariableDeclarator recordIndex_declarator = new VariableDeclarator(new NameID("recordIndex"));
                    specs = new LinkedList<Specifier>();
                    specs.add(CUDASpecifier.CUDA_SHARED);
                    specs.add(Specifier.UNSIGNED);
                    specs.add(Specifier.INT);
                    recordIndex_decl = new VariableDeclaration(specs, recordIndex_declarator);
                    new_proc.getBody().addDeclaration(recordIndex_decl);

                    Declaration threadRecordCounter_decl = null;
                    VariableDeclarator threadRecordCounter_declarator = new VariableDeclarator(new NameID("threadRecordCounter"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    threadRecordCounter_decl = new VariableDeclaration(specs, threadRecordCounter_declarator);
                    new_proc.getBody().addDeclaration(threadRecordCounter_decl);


                    Expression index = (Expression)index_decl.getDeclaredIDs().get(0);
                    Expression tid = (Expression)tid_decl.getDeclaredIDs().get(0);
                    Expression start = (Expression)start_decl.getDeclaredIDs().get(0);
                    Expression recordIndex = (Expression)recordIndex_decl.getDeclaredIDs().get(0);
                    Expression threadRecordCounter = (Expression)threadRecordCounter_decl.getDeclaredIDs().get(0);

                    //mapSetup(__start, __tid, __index, __limit, __ipSize, __storesPerThread, __ip, __devKvCount);
                    FunctionCall mapSetup = new FunctionCall(new NameID("mapSetup"));
                    mapSetup.addArgument(start.clone());
                    mapSetup.addArgument(tid.clone());
                    mapSetup.addArgument(index.clone());
                    mapSetup.addArgument(limit.clone());
                    mapSetup.addArgument(ipSize.clone());
                    mapSetup.addArgument(storesPerThread.clone());
                    mapSetup.addArgument(ip.clone());
                    mapSetup.addArgument(devKvCount.clone());
                    mapSetup.addArgument(numReducers.clone());
                    mapSetup.addArgument(totalThreads.clone());
                    mapSetup.addArgument(threadRecordCounter.clone());
                    mapSetup.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF, recordIndex.clone()));

                    ExpressionStatement mapSetupCall = new ExpressionStatement(mapSetup);
                    new_proc.getBody().addStatementBefore(IRTools.getFirstNonDeclarationStatement(new_proc.getBody()),
                            mapSetupCall);

                    //Add extern declaration for the mapper function
                    TranslationUnit tuMain = (TranslationUnit) c2cu.mainProc.getParent();
                    specs.clear();
                    specs.add(Specifier.EXTERN);
                    specs.addAll(new_proc.getReturnType());
                    VariableDeclaration mapDecl = new VariableDeclaration(specs, new_proc.getDeclarator());
                    tuMain.addDeclarationBefore(tuMain.getFirstDeclaration(), mapDecl);

                    List<FunctionCall> funcList = IRTools.getFunctionCalls(new_proc);
                    for (FunctionCall f : funcList) {
                        if(f.getName().toString().equals("getline")) {
                            Expression line = f.getArgument(0);

                            if(line instanceof UnaryExpression) {
                                UnaryExpression l = (UnaryExpression) line;
                                line = l.getExpression();
                            }

                            //Replace the 'line' with new variable in ArrayAccesses
                            List<Expression> accesses = IRTools.findExpressions(new_proc, line);
                            for(Expression ex : accesses) {
                                if(ex.getParent() instanceof ArrayAccess) {
                                    ArrayAccess acc = (ArrayAccess) ex.getParent();
                                    BinaryExpression exp =  new BinaryExpression(acc.getIndex(0).clone(), BinaryOperator.ADD,
                                            (Expression)start_decl.getDeclaredIDs().get(0).clone());
                                    ArrayAccess newAcc = new ArrayAccess(ip.clone(), exp);
                                    IRTools.replaceAll(acc.getParent(), acc, newAcc);
                                }
                            }

                            //Replace other occurances of the 'line' variable with the new variable
                            IRTools.replaceAll(new_proc.getBody(), line, ip);

                            //make special case for __ip, remove the definition in the body
                            List<Traversable> children = new_proc.getBody().getChildren();
                            Iterator<Traversable> iter = children.iterator();
                            while(iter.hasNext()) {
                              Traversable setElement = iter.next();
                              if(setElement instanceof  DeclarationStatement) {
                                DeclarationStatement ipDecInProcBody = (DeclarationStatement) setElement;
                                VariableDeclaration decl = (VariableDeclaration) ipDecInProcBody.getDeclaration();
                                if(decl.getDeclaredIDs().get(0).toString().equals("__ip")) {
                                  iter.remove();
                                }
                              }
                            }


                            AssignmentExpression callExp = (AssignmentExpression) f.getParent();
                            //Insert new getline call (GPU function)
                            /*  Older getline GPU variant
                            f.setArgument(0, ip.clone());
                            f.setArgument(1, ipSize.clone());
                            f.setArgument(2, start.clone());
                            f.addArgument(limit.clone());
                            //f.setArgument(3, limit.clone());
                            f.addArgument(callExp.getLHS().clone());
                            */

                            //new GPU getline function API
                            f.setArgument(0, ip.clone());
                            f.setArgument(1, recordsPerThread.clone());
                            f.setArgument(2, new UnaryExpression(UnaryOperator.ADDRESS_OF, recordIndex.clone()));
                            f.addArgument(start.clone());
                            f.addArgument(recordLocator.clone());
                            f.addArgument(threadRecordCounter.clone());

                            // Set initial value to 0 for read variable
                            AssignmentExpression setZero = new AssignmentExpression(callExp.getLHS().clone(),
                                AssignmentOperator.NORMAL, new IntegerLiteral(0));
                            ExpressionStatement setZeroSt = new ExpressionStatement(setZero);
                            CompoundStatement hostingPt = (CompoundStatement)f.getParent().getParent().getParent().getParent();
                            hostingPt.addStatementBefore((Statement)f.getParent().getParent().getParent(), setZeroSt);

                        }
                        if(f.getName().toString().equals("printf")) {
                            FunctionCall kvEmit = new FunctionCall(new NameID(
                                    "emitKV"));
                            if(SymbolTools.isScalar(SymbolTools.getSymbolOfName(c2cu.mapBufKey.toString(), new_proc.getBody()))) {
                              kvEmit.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF,c2cu.mapBufKey.clone()));
                            } else {
                              kvEmit.addArgument(c2cu.mapBufKey.clone());
                            }
                            if(SymbolTools.isScalar(SymbolTools.getSymbolOfName(c2cu.mapBufVal.toString(), new_proc.getBody()))) {
                                kvEmit.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF,c2cu.mapBufVal.clone()));
                            } else {
                                kvEmit.addArgument(c2cu.mapBufVal.clone());
                            }
                            kvEmit.addArgument(devKeys.clone());
                            kvEmit.addArgument(devVals.clone());
                            kvEmit.addArgument(index.clone());
                            kvEmit.addArgument(devKvCount.clone());
                            kvEmit.addArgument(keyLength.clone());
                            kvEmit.addArgument(valLength.clone());
                            kvEmit.addArgument(numReducers.clone());
                            kvEmit.addArgument(totalThreads.clone());
                            kvEmit.addArgument(storesPerThread.clone());
                            kvEmit.addArgument(devIndexArray.clone());

                            ExpressionStatement kvEmitCall = new ExpressionStatement(kvEmit);
                            Statement printCall = (Statement)f.getParent();
                            printCall.swapWith(kvEmitCall);
                        }


                        if(f.getName().toString().equals("strcmp") || f.getName().toString().equals("strcpy")) {
                          List<Expression> args = f.getArguments();
                          for (Expression arg : args) {
                            if(arg.toString().equals((c2cu.mapBufKey.toString())) && args.size() == 2) {
                              f.addArgument(keyLength.clone());
                            }
                          }
                        }

                        if(f.getName().toString().equals("atoi")) {
                          FunctionCall atoiAcc = new FunctionCall(new NameID("atoiAccelerator"));
                          for(int i=0; i< f.getNumArguments(); i++) {
                            atoiAcc.addArgument(f.getArgument(i).clone());
                          }
                          //atoiAcc.setArguments(f.getArguments());
                          IRTools.replaceAll(f.getParent(), f , atoiAcc);
                        }

                        if(f.getName().toString().equals("atol")) {
                          FunctionCall atolAcc = new FunctionCall(new NameID("atolAccelerator"));
                          for(int i=0; i< f.getNumArguments(); i++) {
                            atolAcc.addArgument(f.getArgument(i).clone());
                          }
                          //atolAcc.setArguments(f.getArguments());
                          IRTools.replaceAll(f.getParent(), f , atolAcc);
                        }

                        if(f.getName().toString().equals("atof")) {
                          FunctionCall atolAcc = new FunctionCall(new NameID("atofAccelerator"));
                          for(int i=0; i< f.getNumArguments(); i++) {
                            atolAcc.addArgument(f.getArgument(i).clone());
                          }
                          //atolAcc.setArguments(f.getArguments());
                          IRTools.replaceAll(f.getParent(), f , atolAcc);
                        }

                    }

                    //Add a call to mapFinish
                    CompoundStatement cst = (CompoundStatement) pAnnot.getAnnotatable().getParent();
                    FunctionCall mapFinish = new FunctionCall(new NameID("mapFinish"));
                    //int index, int storesPerThread, double *key, int keyLength
                    mapFinish.addArgument(index.clone());
                    mapFinish.addArgument(storesPerThread.clone());
                    mapFinish.addArgument(devKeys.clone());
                    mapFinish.addArgument(keyLength.clone());
                    mapFinish.addArgument(devIndexArray.clone());
                    mapFinish.addArgument(totalThreads.clone());
                    mapFinish.addArgument(numReducers.clone());
                    mapFinish.addArgument(devKvCount.clone());

                    ExpressionStatement mapFinishCall = new ExpressionStatement(mapFinish);
                    cst.addStatementAfter((Statement)pAnnot.getAnnotatable(), mapFinishCall);

                    FunctionCall closeFile;
                    ExpressionStatement closeFileCall = null;
                    //Below has to be done only if the reducer is present.
                    if(c2cu.noReducer == false) {
                      //Perform sort
                      /*
                      List<Expression> kernelConf2 = new ArrayList<Expression>();
                      KernelFunctionCall countMapKVpairs = new KernelFunctionCall(new NameID(
                              "countMapKVpairs"), new LinkedList(), kernelConf2);

                      countMapKVpairs.addArgument(devKvCountCPU.clone());
                      countMapKVpairs.addArgument(totalThreadsCPU.clone());
                      kernelConf2.add(numReducersCPU.clone());
                      kernelConf2.add(new IntegerLiteral(32));
                      kernelConf2.add(new IntegerLiteral(0));
                      kernelConf2.add(new IntegerLiteral(0));
                      countMapKVpairs.setConfArguments(kernelConf2);

                      ExpressionStatement countMapKVpairsCall = new ExpressionStatement(countMapKVpairs);
                      parent.addStatementAfter(kernelRegion, countMapKVpairsCall);
                      */

                      //print map timer
                      BinaryExpression newTime = new BinaryExpression(new FunctionCall(new NameID("timer")),
                              BinaryOperator.SUBTRACT, timerCPU.clone());
                      FunctionCall mapTimePrint = new FunctionCall(new NameID("printf"));
                      mapTimePrint.addArgument(new NameID("\"Map time %lf\\n\""));
                      mapTimePrint.addArgument(newTime.clone());
                      ExpressionStatement mapTimePrinter = new ExpressionStatement(mapTimePrint);
                      parent.addStatementAfter(kernelRegion, mapTimePrinter);

                      //Set stores per bin
                      AssignmentExpression setStoresPerBin = new AssignmentExpression(storesPerBinCPU.clone(),
                              AssignmentOperator.NORMAL, new BinaryExpression(kvStoreSizeCPU.clone(), BinaryOperator.DIVIDE,
                              numReducersCPU.clone()));
                      ExpressionStatement setStoresPerBinSt = new ExpressionStatement(setStoresPerBin);

                      parent.addStatementAfter(mapTimePrinter, setStoresPerBinSt);

                      FunctionCall initFile = new FunctionCall(new NameID("initFile"));
                      initFile.addArgument(outputPathCPU.clone());
                      ExpressionStatement initFileCall = new ExpressionStatement(initFile);
                      parent.addStatementAfter(setStoresPerBinSt, initFileCall);
                      //Insert reducer for loop
                      ExpressionStatement forLoopInit = new ExpressionStatement(new AssignmentExpression(reducerCPU.clone(),
                              AssignmentOperator.NORMAL, new IntegerLiteral(0)));
                      BinaryExpression forLoopCond = new BinaryExpression(reducerCPU.clone(),
                              BinaryOperator.COMPARE_LT, numReducersCPU.clone());
                      BinaryExpression forLoopIter  = new AssignmentExpression(reducerCPU.clone(),
                              AssignmentOperator.NORMAL, new BinaryExpression(reducerCPU.clone(), BinaryOperator.ADD,
                              new IntegerLiteral(1)));

                      CompoundStatement forLoopBody = new CompoundStatement();
                      ForLoop reducerLoop = new ForLoop(forLoopInit, forLoopCond, forLoopIter, forLoopBody );

                      parent.addStatementAfter(initFileCall, reducerLoop);

                      Declaration mapKVcount_decl = null;
                      VariableDeclarator mapKVcount_declarator = new VariableDeclarator(new NameID("__mapKVcount"));
                      specs = new LinkedList<Specifier>();
                      specs.add(Specifier.INT);
                      mapKVcount_decl = new VariableDeclaration(specs, mapKVcount_declarator);
                      cProc.getBody().addDeclaration(mapKVcount_decl);

                      c2cu.mapKVCount = (Expression)mapKVcount_decl.getDeclaredIDs().get(0);

                      //Perform memcpy for mapKVCount
                      /*
                      FunctionCall memCpy = new FunctionCall(new NameID("cudaMemcpy"));
                      memCpy.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF, c2cu.mapKVCount.clone()));
                      memCpy.addArgument(new BinaryExpression(devKvCountCPU.clone(), BinaryOperator.ADD,
                              new BinaryExpression(totalThreadsCPU.clone(), BinaryOperator.MULTIPLY, reducerCPU.clone())));
                      specs = new LinkedList<Specifier>();
                      specs.add(Specifier.INT);
                      memCpy.addArgument(new SizeofExpression(specs));
                      memCpy.addArgument(new NameID("cudaMemcpyDeviceToHost"));

                      ExpressionStatement memCpyCall = new ExpressionStatement(memCpy);
                      //parent.addStatementAfter(countMapKVpairsCall, memCpyCall);
                      forLoopBody.addStatement(memCpyCall);
                      */

                      //call linearize function call
                      /* __mapKVcount= linearize(__devIndexArray+(__storesPerBin*__reducer), newIndexArray+(__storesPerBin*__reducer),
                      __devKvCount + __totalThreads*__reducer, __totalThreads,  __storesPerBin,  __reducer,  __numReducers); */
                      FunctionCall linearize = new FunctionCall(new NameID("linearize"));
                      BinaryExpression e1 = new BinaryExpression(devIndexArrayCPU.clone(), BinaryOperator.ADD,
                              new BinaryExpression(storesPerBinCPU.clone(), BinaryOperator.MULTIPLY, reducerCPU.clone()));
                      BinaryExpression e2 = new BinaryExpression(newDevIndexArrayCPU.clone(), BinaryOperator.ADD,
                            new BinaryExpression(storesPerBinCPU.clone(), BinaryOperator.MULTIPLY, reducerCPU.clone()));
                      BinaryExpression e3 =  new BinaryExpression(devKvCountCPU.clone(), BinaryOperator.ADD,
                              new BinaryExpression(totalThreadsCPU.clone(), BinaryOperator.MULTIPLY, reducerCPU.clone()));
                      linearize.addArgument(e1.clone());
                      linearize.addArgument(e2.clone());
                      linearize.addArgument(e3.clone());
                      linearize.addArgument(totalThreadsCPU.clone());
                      linearize.addArgument(storesPerBinCPU.clone());
                      linearize.addArgument(reducerCPU.clone());
                      linearize.addArgument(numReducersCPU.clone());
                      AssignmentExpression mapKVCountSet = new AssignmentExpression(c2cu.mapKVCount.clone(), AssignmentOperator.NORMAL, linearize);
                      ExpressionStatement mapKVCountSetter = new ExpressionStatement(mapKVCountSet);
                      forLoopBody.addStatement(mapKVCountSetter);

                      //bitonic_sort(devIndexArray, devKey, kvStoreSize);
                      FunctionCall bitSort = new FunctionCall(new NameID("mergeSort"));
                      bitSort.addArgument(e2.clone());
                      bitSort.addArgument(devKeysCPU.clone());
                      bitSort.addArgument(keyLengthCPU.clone());
                      bitSort.addArgument(c2cu.mapKVCount.clone());
                      ExpressionStatement bitSortCall = new ExpressionStatement(bitSort);
                      //parent.addStatementAfter(memCpyCall, bitSortCall);
                      forLoopBody.addStatementAfter(mapKVCountSetter, bitSortCall);

                      if(c2cu.noCombiner == false) {
                        AssignmentExpression timerStart = new AssignmentExpression(timerCPU.clone(),
                                AssignmentOperator.NORMAL, new FunctionCall(new NameID("timer")));
                        ExpressionStatement combineTimerStartCall = new ExpressionStatement(timerStart);
                        forLoopBody.addStatementAfter(bitSortCall, combineTimerStartCall);
                        //combiner(devKey, devVal, devIndexArray, mapKVCount, &finalCount, &opVal, &opKey,
                        // STRING_LENGTH, 1, STRING_LENGTH, 1);
                        VariableDeclaration combineBlocks_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__combineBlocks"));
                        VariableDeclaration combineBlockSize_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__combineBlockSize"));

                        Expression combineBlockCPU = (Expression)combineBlocks_cpu.getDeclaredIDs().get(0);
                        Expression combineBlockSizeCPU = (Expression)combineBlockSize_cpu.getDeclaredIDs().get(0);

                        FunctionCall combiner = new FunctionCall(new NameID("combiner"));
                        combiner.addArgument(devKeysCPU.clone());
                        combiner.addArgument(devValsCPU.clone());
                        combiner.addArgument(e2.clone());
                        combiner.addArgument(c2cu.mapKVCount.clone());
                        combiner.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF, finalCountCPU.clone() ));
                        combiner.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF, opValCPU.clone()));
                        combiner.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF, opKeyCPU.clone()));
                        combiner.addArgument(mapKeyLengthCPU.clone());
                        combiner.addArgument(mapValLengthCPU.clone());
                        combiner.addArgument(combKeyLengthCPU.clone());
                        combiner.addArgument(combValLengthCPU.clone());
                        combiner.addArgument(combineBlockCPU.clone());
                        combiner.addArgument(combineBlockSizeCPU.clone());
                        ExpressionStatement combinerCall = new ExpressionStatement(combiner);
                        //parent.addStatementAfter(bitSortCall, combinerCall);
                        forLoopBody.addStatementAfter(combineTimerStartCall, combinerCall);

                        //print map timer
                        BinaryExpression combEndTime = new BinaryExpression(new FunctionCall(new NameID("timer")),
                                BinaryOperator.SUBTRACT, timerCPU.clone());
                        FunctionCall combTimePrint = new FunctionCall(new NameID("printf"));
                        combTimePrint.addArgument(new NameID("\"Combine time %lf\\n\""));
                        combTimePrint.addArgument(combEndTime.clone());
                        ExpressionStatement combTimePrinter = new ExpressionStatement(combTimePrint);
                        forLoopBody.addStatementAfter(combinerCall, combTimePrinter);

                        FunctionCall writeOP = new FunctionCall(new NameID("writeOP"));
                        writeOP.addArgument(c2cu.mapKVCount.clone());
                        writeOP.addArgument(finalCountCPU.clone());
                        writeOP.addArgument(new NameID("_" + c2cu.combKeySpec.toString() + "_"));
                        writeOP.addArgument(new NameID("_" + c2cu.combValSpec.toString() + "_"));
                        writeOP.addArgument(opKeyCPU.clone());
                        writeOP.addArgument(opValCPU.clone());
                        writeOP.addArgument(combKeyLengthCPU.clone());
                        writeOP.addArgument(combValLengthCPU.clone());
                        ExpressionStatement writeOPCall = new ExpressionStatement(writeOP);

                        forLoopBody.addStatementAfter(combTimePrinter, writeOPCall);

                      } else {
                        //writeOPNoCombiner((__newDevIndexArray+(__storesPerBin*__reducer)), __indexArray+(__storesPerBin*__reducer),
                        // key, val, devKey, devVal, __keyLength, __valLength, _char_, _char_,  __mapKVcount, __kvStoreSize);
                        BinaryExpression e4 = new BinaryExpression(indexArrayCPU.clone(), BinaryOperator.ADD,
                                new BinaryExpression(storesPerBinCPU.clone(), BinaryOperator.MULTIPLY, reducerCPU.clone()));
                        FunctionCall writeOPNoCombiner = new FunctionCall(new NameID("writeOPNoCombiner"));
                        writeOPNoCombiner.addArgument(e2.clone());
                        writeOPNoCombiner.addArgument(e4);
                        writeOPNoCombiner.addArgument(keyCPU.clone());
                        writeOPNoCombiner.addArgument(valCPU.clone());
                        writeOPNoCombiner.addArgument(devKeysCPU.clone());
                        writeOPNoCombiner.addArgument(devValsCPU.clone());
                        writeOPNoCombiner.addArgument(keyLengthCPU.clone());
                        writeOPNoCombiner.addArgument(valLengthCPU.clone());
                        writeOPNoCombiner.addArgument(new NameID("_" + c2cu.keySpec.toString() + "_"));
                        writeOPNoCombiner.addArgument(new NameID("_" + c2cu.valSpec.toString() + "_"));
                        writeOPNoCombiner.addArgument(c2cu.mapKVCount.clone());
                        writeOPNoCombiner.addArgument(kvStoreSizeCPU.clone());

                        ExpressionStatement writeOPNoCombinerCall = new ExpressionStatement(writeOPNoCombiner);
                        forLoopBody.addStatementAfter(bitSortCall,writeOPNoCombinerCall);
                      }

                      closeFile = new FunctionCall(new NameID("closeFile"));
                      closeFileCall = new ExpressionStatement(closeFile);
                      parent.addStatementAfter(reducerLoop, closeFileCall);

                      //releaseMapData(devKey, devVal, key, val, __devIp, __ip, __devKvCount,
                      // __devIndexArray, __indexArray, __finalCount, __opVal, __opKey);
                    }

                    FunctionCall releaseMap = new FunctionCall(new NameID("releaseMapData"));
                    releaseMap.addArgument(devKeysCPU.clone());
                    releaseMap.addArgument(devValsCPU.clone());
                    releaseMap.addArgument(keyCPU.clone());
                    releaseMap.addArgument(valCPU.clone());
                    releaseMap.addArgument(devIpCPU.clone());
                    releaseMap.addArgument(ipCPU.clone());
                    releaseMap.addArgument(devKvCountCPU.clone());
                    releaseMap.addArgument(devIndexArrayCPU.clone());
                    releaseMap.addArgument(newDevIndexArrayCPU.clone());
                    releaseMap.addArgument(indexArrayCPU.clone());
                    releaseMap.addArgument(finalCountCPU.clone());
                    releaseMap.addArgument(opValCPU.clone());
                    releaseMap.addArgument(opKeyCPU.clone());
                    if(c2cu.noCombiner) {
                      releaseMap.addArgument(new IntegerLiteral(0));
                    } else {
                      releaseMap.addArgument(new IntegerLiteral(1));
                    }
                    ExpressionStatement releaseMapCall = new ExpressionStatement(releaseMap);

                    if(c2cu.noReducer) {
                      //writeOPonHDFS( (void *) devKey, (void *) devVal, (void *) key, (void *) val, __devKvCount, __devIndexArray,  __indexArray, __keyLength, __valLength, _char_, _int_, __kvStoreSize, __totalThreads, outputPath);
                      FunctionCall writeOPonHDFS = new FunctionCall(new NameID("writeOPonHDFSwithDevHelp"));
                      specs = new LinkedList<Specifier>();
                      specs.add(Specifier.VOID);
                      specs.add(PointerSpecifier.UNQUALIFIED);
                      writeOPonHDFS.addArgument(new Typecast(specs, devKeysCPU.clone()));
                      writeOPonHDFS.addArgument(new Typecast(specs, devValsCPU.clone()));
                      writeOPonHDFS.addArgument(new Typecast(specs, keyCPU.clone()));
                      writeOPonHDFS.addArgument(new Typecast(specs, valCPU.clone()));
                      writeOPonHDFS.addArgument(devKvCountCPU.clone());
                      writeOPonHDFS.addArgument(devIndexArrayCPU.clone());
                      writeOPonHDFS.addArgument(indexArrayCPU.clone());
                      writeOPonHDFS.addArgument(keyLengthCPU.clone());
                      writeOPonHDFS.addArgument(valLengthCPU.clone());
                      writeOPonHDFS.addArgument(new NameID("_" + c2cu.keySpec + "_"));
                      writeOPonHDFS.addArgument(new NameID("_" + c2cu.valSpec + "_"));
                      writeOPonHDFS.addArgument(kvStoreSizeCPU.clone());
                      writeOPonHDFS.addArgument(totalThreadsCPU.clone());
                      writeOPonHDFS.addArgument(outputPathCPU.clone());
                      writeOPonHDFS.addArgument(fsCPU.clone());

                      ExpressionStatement writeOPonHDFSCall = new ExpressionStatement(writeOPonHDFS);
                      parent.addStatementAfter(kernelRegion, writeOPonHDFSCall);
                      parent.addStatementAfter(writeOPonHDFSCall, releaseMapCall);
                    } else {
                      parent.addStatementAfter(closeFileCall, releaseMapCall);
                    }

                  handleFunctionCalls(new_proc, cProc);

                }
                if(pAnnot.get("combiner")!= null)  {
                    List<Specifier> new_proc_ret_type = new LinkedList<Specifier>();
                    //new_proc_ret_type.add(CUDASpecifier.EXTERN);
                    //new_proc_ret_type.add(CUDASpecifier.EXTERN_C);
                    new_proc_ret_type.add(CUDASpecifier.CUDA_GLOBAL);
                    new_proc_ret_type.add(Specifier.VOID);
                    String new_func_name = "gpu_combiner";
                    Procedure new_proc = new Procedure(new_proc_ret_type,
                            new ProcedureDeclarator(new NameID(new_func_name),
                                    new LinkedList()), new CompoundStatement());
                    List<Expression> kernelConf = new ArrayList<Expression>();
                    KernelFunctionCall call_to_new_proc = new KernelFunctionCall(new NameID(
                            new_func_name), new LinkedList(), kernelConf);
                    call_to_new_proc.setLinkedProcedure(new_proc);

                    List<Specifier> specs = null;

                    //Insert function arguments for the combiner
                    Declaration key_decl = null;
                    VariableDeclarator key_declarator = new VariableDeclarator(new NameID("__key"));
                    specs = new LinkedList<Specifier>();
                    specs.add(c2cu.keySpec);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    key_decl = new VariableDeclaration(specs, key_declarator);
                    new_proc.addDeclaration(key_decl);

                    Declaration val_decl = null;
                    VariableDeclarator val_declarator = new VariableDeclarator(new NameID("__val"));
                    specs = new LinkedList<Specifier>();
                    specs.add(c2cu.valSpec);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    val_decl = new VariableDeclaration(specs, val_declarator);
                    new_proc.addDeclaration(val_decl);

                    Declaration opKey_decl = null;
                    VariableDeclarator opKey_declarator = new VariableDeclarator(new NameID("__opKey"));
                    specs = new LinkedList<Specifier>();
                    specs.add(c2cu.combKeySpec);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    opKey_decl = new VariableDeclaration(specs, opKey_declarator);
                    new_proc.addDeclaration(opKey_decl);

                    Declaration opVal_decl = null;
                    VariableDeclarator opVal_declarator = new VariableDeclarator(new NameID("__opVal"));
                    specs = new LinkedList<Specifier>();
                    specs.add(c2cu.combValSpec);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    opVal_decl = new VariableDeclaration(specs, opVal_declarator);
                    new_proc.addDeclaration(opVal_decl);

                    Declaration indexArray_decl = null;
                    VariableDeclarator indexArray_declarator = new VariableDeclarator(new NameID("__indexArray"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    indexArray_decl = new VariableDeclaration(specs, indexArray_declarator);
                    new_proc.addDeclaration(indexArray_decl);

                    Declaration finalCount_decl = null;
                    VariableDeclarator finalCount_declarator = new VariableDeclarator(new NameID("__finalCount"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    specs.add(PointerSpecifier.UNQUALIFIED);
                    finalCount_decl = new VariableDeclaration(specs, finalCount_declarator);
                    new_proc.addDeclaration(finalCount_decl);

                    Declaration size_decl = null;
                    VariableDeclarator size_declarator = new VariableDeclarator(new NameID("__size"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    size_decl = new VariableDeclaration(specs, size_declarator);
                    new_proc.addDeclaration(size_decl);

                    Declaration combinerThreads_decl = null;
                    VariableDeclarator combinerThreads_declarator = new VariableDeclarator(new NameID("__combinerThreads"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    combinerThreads_decl = new VariableDeclaration(specs, combinerThreads_declarator);
                    new_proc.addDeclaration(combinerThreads_decl);
                    
                    Declaration mapKeyLength_decl = null;
                    VariableDeclarator mapKeyLength_declarator = new VariableDeclarator(new NameID("__mapKeyLength"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    mapKeyLength_decl = new VariableDeclaration(specs, mapKeyLength_declarator);
                    new_proc.addDeclaration(mapKeyLength_decl);

                    Declaration mapValLength_decl = null;
                    VariableDeclarator mapValLength_declarator = new VariableDeclarator(new NameID("__mapValLength"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    mapValLength_decl = new VariableDeclaration(specs, mapValLength_declarator);
                    new_proc.addDeclaration(mapValLength_decl);

                    Declaration combKeyLength_decl = null;
                    VariableDeclarator combKeyLength_declarator = new VariableDeclarator(new NameID("__combKeyLength"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    combKeyLength_decl = new VariableDeclaration(specs, combKeyLength_declarator);
                    new_proc.addDeclaration(combKeyLength_decl);

                    Declaration combValLength_decl = null;
                    VariableDeclarator combValLength_declarator = new VariableDeclarator(new NameID("__combValLength"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    combValLength_decl = new VariableDeclaration(specs, combValLength_declarator);
                    new_proc.addDeclaration(combValLength_decl);

                    VariableDeclaration devKey_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__devKey"));
                    VariableDeclaration devVal_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__devVal"));
                    VariableDeclaration devOpKey_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__devOpKey"));
                    VariableDeclaration devOpVal_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__devOpVal"));
                    VariableDeclaration devIndexArray_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__devIndexArray"));
                    VariableDeclaration devFinalCount_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__devFinalCount"));
                    VariableDeclaration mapKVCount_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__mapKVCount"));
                    VariableDeclaration combinerThreads_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__combinerThreads"));
                    VariableDeclaration mapKeyLength_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__mapKeyLength"));
                    VariableDeclaration mapValLength_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__mapValLength"));
                    VariableDeclaration combKeyLength_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__combKeyLength"));
                    VariableDeclaration combValLength_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__combValLength"));
                    VariableDeclaration combineBlocks_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__combineBlocks"));
                    VariableDeclaration combineBlockSize_cpu = (VariableDeclaration)cProc.getBody().findSymbol(new NameID("__combineBlockSize"));

                    Expression devKeyCPU = (Expression)devKey_cpu.getDeclaredIDs().get(0);
                    Expression devValCPU = (Expression)devVal_cpu.getDeclaredIDs().get(0);
                    Expression devOpKeyCPU = (Expression)devOpKey_cpu.getDeclaredIDs().get(0);
                    Expression devOpValCPU = (Expression)devOpVal_cpu.getDeclaredIDs().get(0);
                    Expression devIndexArrayCPU = (Expression)devIndexArray_cpu.getDeclaredIDs().get(0);
                    Expression devFinalCountCPU = (Expression)devFinalCount_cpu.getDeclaredIDs().get(0);
                    Expression mapKVCountCPU = (Expression)mapKVCount_cpu.getDeclaredIDs().get(0);
                    Expression combinerThreadsCPU = (Expression)combinerThreads_cpu.getDeclaredIDs().get(0);
                    Expression mapKeyLengthCPU = (Expression)mapKeyLength_cpu.getDeclaredIDs().get(0);
                    Expression mapValLengthCPU = (Expression)mapValLength_cpu.getDeclaredIDs().get(0);
                    Expression combKeyLengthCPU = (Expression)combKeyLength_cpu.getDeclaredIDs().get(0);
                    Expression combValLengthCPU = (Expression)combValLength_cpu.getDeclaredIDs().get(0);
                    Expression combineBlockCPU = (Expression)combineBlocks_cpu.getDeclaredIDs().get(0);
                    Expression combineBlockSizeCPU = (Expression)combineBlockSize_cpu.getDeclaredIDs().get(0);

                    Expression key = (Expression)key_decl.getDeclaredIDs().get(0);
                    Expression val = (Expression)val_decl.getDeclaredIDs().get(0);
                    Expression opKey = (Expression)opKey_decl.getDeclaredIDs().get(0);
                    Expression opVal = (Expression)opVal_decl.getDeclaredIDs().get(0);
                    Expression indexArray = (Expression)indexArray_decl.getDeclaredIDs().get(0);
                    Expression finalCount = (Expression)finalCount_decl.getDeclaredIDs().get(0);
                    Expression size = (Expression)size_decl.getDeclaredIDs().get(0);
                    Expression combinerThreads = (Expression)combinerThreads_decl.getDeclaredIDs().get(0);
                    Expression mapKeyLength = (Expression)mapKeyLength_decl.getDeclaredIDs().get(0);
                    Expression mapValLength = (Expression)mapValLength_decl.getDeclaredIDs().get(0);
                    Expression combKeyLength = (Expression)combKeyLength_decl.getDeclaredIDs().get(0);
                    Expression combValLength = (Expression)combValLength_decl.getDeclaredIDs().get(0);

                     /*combiner_kernel(char* key, int *val, char *opKey, int *opVal, int *indexArray, int *finalCount,
                    int size, int combinerThreads, int mapKeyLength, int mapValLength, int combKeyLength, int combValLength)
                     */

                    /*
                    (devKey, devVal, devOpKey, devOpVal, devIndexArray, devFinalCount,mapKVCount,
                    COMBINEBLOCKS*COMBINEBLOCKSIZE, mapKeyLength, mapValLength, combKeyLength, combValLength);
                     */

                    //Add arguments to the kernel call
                    call_to_new_proc.addArgument(devKeyCPU.clone());
                    call_to_new_proc.addArgument(devValCPU.clone());
                    call_to_new_proc.addArgument(devOpKeyCPU.clone());
                    call_to_new_proc.addArgument(devOpValCPU.clone());
                    call_to_new_proc.addArgument(devIndexArrayCPU.clone());
                    call_to_new_proc.addArgument(devFinalCountCPU.clone());
                    call_to_new_proc.addArgument(mapKVCountCPU.clone());
                    call_to_new_proc.addArgument(combinerThreadsCPU.clone());
                    call_to_new_proc.addArgument(mapKeyLengthCPU.clone());
                    call_to_new_proc.addArgument(mapValLengthCPU.clone());
                    call_to_new_proc.addArgument(combKeyLengthCPU.clone());
                    call_to_new_proc.addArgument(combValLengthCPU.clone());

                    Statement region = (Statement) pAnnot.getAnnotatable();
                    Statement reference = region;
                    CompoundStatement parent = (CompoundStatement)region.getParent();

                    /*
                    VariableDeclarator dimGrid_declarator = new VariableDeclarator(new NameID("dimGrid_"+new_func_name), new ArraySpecifier(new IntegerLiteral(3)));
                    Identifier dimGrid = new Identifier(dimGrid_declarator);
                    Declaration dimGrid_decl = new VariableDeclaration(CUDASpecifier.INT, dimGrid_declarator);
                    DeclarationStatement dimGrid_stmt = new DeclarationStatement(dimGrid_decl);
                    TransformTools.addStatementBefore((CompoundStatement)region.getParent(), reference, dimGrid_stmt);
                    */

                    kernelConf.add(combineBlockCPU.clone());
                    kernelConf.add(combineBlockSizeCPU.clone());
                    kernelConf.add(new IntegerLiteral(0));
                    kernelConf.add(new IntegerLiteral(0));
                    call_to_new_proc.setConfArguments(kernelConf);

                    Statement kernelCall_stmt = new ExpressionStatement(call_to_new_proc);

                    parent.addStatementBefore(region, kernelCall_stmt);

                    Statement kernelRegion = new_proc.getBody();
                    region.swapWith(kernelRegion);

                    c2cu.kernelsTranslationUnit.addDeclaration(new_proc);

                    //Edit the kernel code
                    CompoundStatement encapsulate = new CompoundStatement();
                    CompoundStatement body = new_proc.getBody();
                    body.swapWith(encapsulate);


                    Declaration laneID_decl = null;
                    VariableDeclarator lanID_declarator = new VariableDeclarator(new NameID("__laneID"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    laneID_decl = new VariableDeclaration(specs, lanID_declarator);
                    new_proc.getBody().addDeclaration(laneID_decl);

                    Declaration kvsPerThread_decl = null;
                    VariableDeclarator kvsPerThread_declarator = new VariableDeclarator(new NameID("__kvsPerThread"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    kvsPerThread_decl = new VariableDeclaration(specs, kvsPerThread_declarator);
                    new_proc.getBody().addDeclaration(kvsPerThread_decl);

                    Declaration warpID_decl = null;
                    VariableDeclarator warpID_declarator = new VariableDeclarator(new NameID("__warpID"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    warpID_decl = new VariableDeclaration(specs, warpID_declarator);
                    new_proc.getBody().addDeclaration(warpID_decl);

                    Declaration ptr_decl = null;
                    VariableDeclarator ptr_declarator = new VariableDeclarator(new NameID("__ptr"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    ptr_decl = new VariableDeclaration(specs, ptr_declarator);
                    new_proc.getBody().addDeclaration(ptr_decl);

                    Declaration high_decl = null;
                    VariableDeclarator high_declarator = new VariableDeclarator(new NameID("__high"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    high_decl = new VariableDeclaration(specs, high_declarator);
                    new_proc.getBody().addDeclaration(high_decl);

                    Declaration kvCount_decl = null;
                    VariableDeclarator kvCount_declarator = new VariableDeclarator(new NameID("__kvCount"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    kvCount_decl = new VariableDeclaration(specs, kvCount_declarator);
                    new_proc.getBody().addDeclaration(kvCount_decl);

                    Declaration index_decl = null;
                    VariableDeclarator index_declarator = new VariableDeclarator(new NameID("__index"));
                    specs = new LinkedList<Specifier>();
                    specs.add(Specifier.INT);
                    index_decl = new VariableDeclaration(specs, index_declarator);
                    new_proc.getBody().addDeclaration(index_decl);

                    //int kvsPerThread, laneID, warpId, ptr, high, kvCount, index;

                    Expression kvsPerThread = (Expression)kvsPerThread_decl.getDeclaredIDs().get(0);
                    Expression laneID = (Expression)laneID_decl.getDeclaredIDs().get(0);
                    Expression warpID = (Expression)warpID_decl.getDeclaredIDs().get(0);
                    Expression ptr = (Expression)ptr_decl.getDeclaredIDs().get(0);
                    Expression high = (Expression)high_decl.getDeclaredIDs().get(0);
                    Expression kvCount = (Expression)kvCount_decl.getDeclaredIDs().get(0);
                    Expression index = (Expression)index_decl.getDeclaredIDs().get(0);

                    //new_proc.getBody().addStatement(body.clone());
                    //BinaryExpression cond = new BinaryExpression(laneID.clone(), BinaryOperator.COMPARE_EQ, new IntegerLiteral(0));
                    Expression cond = (Expression) new IntegerLiteral(1);
                    IfStatement ifStmt = new IfStatement(cond, body);
                    new_proc.getBody().addStatement(ifStmt);

                    AssignmentExpression finalCountSet = new AssignmentExpression(new ArrayAccess(finalCount.clone(),
                            warpID.clone()), AssignmentOperator.NORMAL, kvCount.clone());
                    Statement finalCountSetSt = new ExpressionStatement(finalCountSet);

                    body.addStatement(finalCountSetSt);

                    //setCombinerVars(kvsPerThread, laneID, warpId, ptr, high,kvCount, index, size, combinerThreads);
                    FunctionCall setVars = new FunctionCall(new NameID("setCombinerVars"));
                    setVars.addArgument(kvsPerThread.clone());
                    setVars.addArgument(laneID.clone());
                    setVars.addArgument(warpID.clone());
                    setVars.addArgument(ptr.clone());
                    setVars.addArgument(high.clone());
                    setVars.addArgument(kvCount.clone());
                    setVars.addArgument(index.clone());
                    setVars.addArgument(size.clone());
                    setVars.addArgument(combinerThreads.clone());

                    ExpressionStatement setVarsCall = new ExpressionStatement(setVars);
                    new_proc.getBody().addStatementBefore(ifStmt,
                            setVarsCall);

                    //Add extern decl for the combiner kernel
                    TranslationUnit tuMain = (TranslationUnit) parent.getProcedure().getParent();
                    specs.clear();
                    specs.add(Specifier.EXTERN);
                    specs.addAll(new_proc.getReturnType());
                    VariableDeclaration combDecl = new VariableDeclaration(specs, new_proc.getDeclarator());
                    tuMain.addDeclarationBefore(tuMain.getFirstDeclaration(), combDecl);

                    //Replace function calls with GPU versions
                    List<WhileLoop> loops = IRTools.getDescendentsOfType(body, WhileLoop.class);
                    for( WhileLoop w : loops) {
                        List<FunctionCall> funcList = IRTools.getFunctionCalls(w);
                        funcList.addAll(IRTools.getFunctionCalls(w.getBody()));
                        DFIterator<Annotatable> iter = new DFIterator<Annotatable>(w, FunctionCall.class);
                        for(FunctionCall f : funcList) {
                            if(f.getName().toString().equals("scanf")) {
                                List<Expression> args = f.getArguments();
                                for (Expression arg : args) {
                                    if (arg.toString().contains((c2cu.combBufInKey.toString()))) {
                                        FunctionCall getKV = new FunctionCall(new NameID("getKeyVal"));
                                        if(SymbolTools.isScalar(SymbolTools.getSymbolOfName(c2cu.combBufInKey.toString(), body))) {
                                          getKV.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF,
                                                  c2cu.combBufInKey.clone()));
                                        } else {
                                          getKV.addArgument(c2cu.combBufInKey.clone());
                                        }
                                        getKV.addArgument(key.clone());
                                        if(SymbolTools.isScalar(SymbolTools.getSymbolOfName(c2cu.combBufInVal.toString(), body))) {
                                            getKV.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF,
                                                    c2cu.combBufInVal.clone()));
                                        } else {
                                            getKV.addArgument(c2cu.combBufInVal.clone());
                                        }
                                        getKV.addArgument(val.clone());
                                        getKV.addArgument(ptr.clone());
                                        getKV.addArgument(high.clone());
                                        getKV.addArgument(indexArray.clone());
                                        getKV.addArgument(mapKeyLength.clone());
                                        getKV.addArgument(mapValLength.clone());

                                        BinaryExpression newCond = new BinaryExpression(getKV, BinaryOperator.COMPARE_NE,
                                                new IntegerLiteral(-1));
                                        w.setCondition(newCond);
                                    }
                                }
                            }
                        }
                    }

                    List<FunctionCall> funcList = IRTools.getFunctionCalls(new_proc.getBody());

                    for(FunctionCall f : funcList) {
                        if(f.getName().toString().equals("strcmp") || f.getName().toString().equals("strcpy")) {
                            List<Expression> args = f.getArguments();
                            for (Expression arg : args) {
                                if(arg.toString().equals((c2cu.combBufInKey.toString())) && args.size() == 2) {
                                    f.addArgument(mapKeyLength.clone());
                                }
                            }
                            if(f.getName().toString().equals("strcpy")){
                              f.addArgument(new IntegerLiteral(1));
                            }
                        }
                        if(f.getName().toString().equals("atoi")) {
                          FunctionCall atoiAcc = new FunctionCall(new NameID("atoiAccelerator"));
                          for(int i=0; i< f.getNumArguments(); i++) {
                            atoiAcc.addArgument(f.getArgument(i).clone());
                          }
                          //atoiAcc.setArguments(f.getArguments());
                          IRTools.replaceAll(f.getParent(), f , atoiAcc);
                        }
                        //Currently, blindly replace the print with storeKV
                        if(f.getName().toString().equals("printf")) {
                            //storeKV(prevWord, &count, combKeyLength, combValLength, opKey, opVal, index, kvCount);
                            FunctionCall storeKV = new FunctionCall(new NameID("storeKV"));
                            if(SymbolTools.isScalar(SymbolTools.getSymbolOfName(c2cu.combBufKey.toString(), body))) {
                              storeKV.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF, c2cu.combBufKey.clone()));
                            } else {
                              storeKV.addArgument(c2cu.combBufKey.clone());
                            }
                            if(SymbolTools.isScalar(SymbolTools.getSymbolOfName(c2cu.combBufVal.toString(), body))) {
                                storeKV.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF, c2cu.combBufVal.clone()));
                            } else {
                                storeKV.addArgument(c2cu.combBufVal.clone());
                            }
                            storeKV.addArgument(combKeyLength.clone());
                            storeKV.addArgument(combValLength.clone());
                            storeKV.addArgument(opKey.clone());
                            storeKV.addArgument(opVal.clone());
                            storeKV.addArgument(index.clone());
                            storeKV.addArgument(kvCount.clone());
                            ExpressionStatement storeKVCall = new ExpressionStatement(storeKV);
                            if(f.getParent() instanceof Statement) {
                                ((Statement)f.getParent()).swapWith(storeKVCall);
                            }

                        }
                    }


                }
            }
        }

    }

    public void handleFunctionCalls(Procedure proc, Procedure oldProc) {
      List<FunctionCall> funcList  = IRTools.getFunctionCalls(proc);
      HashSet<Expression> functionNamesSet = new HashSet<Expression>();
      for(FunctionCall f : funcList) {
        functionNamesSet.add(f.getName());
      }
      List<Expression> functionNames = new ArrayList<Expression>(functionNamesSet);
      List<Procedure> deleteSet = new ArrayList<Procedure>();
      for(int i=0 ; i<functionNames.size(); i++) {
        Expression f = functionNames.get(i);
        for ( Traversable tt : oldProc.getParent().getChildren() )
        {
            if(tt instanceof Procedure) {
              Procedure cproc = (Procedure)tt;
              Expression name = cproc.getName();
            /* f2c code uses MAIN__ */
              if (name.equals(f)) {
                //Procedure copy = cproc.clone();
                List<Specifier> new_proc_ret_type = new LinkedList<Specifier>();
                new_proc_ret_type.add(CUDASpecifier.CUDA_DEVICE);
                new_proc_ret_type.addAll(cproc.getTypeSpecifiers());

                Procedure copy = new Procedure(new_proc_ret_type, cproc.getDeclarator().clone(), cproc.getBody().clone());
                deleteSet.add(cproc);
                //Procedure copyDecl = new Procedure(new_proc_ret_type, cproc.getDeclarator().clone(), new CompoundStatement());

                //ProcedureDeclarator copyProcDecl = new ProcedureDeclarator(new_proc_ret_type, cproc.getName().clone(), new ArrayList<Expression>());

                //c2cu.kernelsTranslationUnit.addDeclarationFirst(copyDecl);
                c2cu.kernelsTranslationUnit.addDeclaration(copy);

                //TranslationUnit tuMain = (TranslationUnit) c2cu.mainProc.getParent();
                List<Specifier> specs = new ArrayList<Specifier>();
                specs.clear();
                specs.addAll(copy.getReturnType());
                VariableDeclaration funcDecl = new VariableDeclaration(specs, copy.getDeclarator());
                c2cu.kernelsTranslationUnit.addDeclarationFirst(funcDecl);
                //c2cu.kernelsTranslationUnit.addDeclarationFirst((VariableDeclaration)copyProcDecl);
                //c2cu.kernelsTranslationUnit.a

                for(FunctionCall fnew : IRTools.getFunctionCalls(cproc)){
                   if(!functionNames.contains(fnew.getName())) {
                     functionNames.add(fnew.getName());
                   }
                }
              }
            }
        }

      }

      for(Procedure p : deleteSet){
        p.detach();
      }
    }

    //This procedure is meant  to handle the read-only shared variables
    public void handleSharedVars( KernelFunctionCall call_to_new_proc, Procedure new_proc, Statement region) {
      for(Symbol sym : c2cu.mapperSharedVars) {
        genCommForSharedVars(call_to_new_proc, new_proc, region, sym);
      }
    }

    public void genCommForSharedVars(KernelFunctionCall call_to_new_proc, Procedure new_proc, Statement region, Symbol sym) {
      boolean found = false;
      CompoundStatement cstmt = null;

      CompoundStatement addPoint;
      Traversable stmt = region.getParent();
      while(!(stmt instanceof CompoundStatement)) {
        stmt = stmt.getParent();
      }
      addPoint = (CompoundStatement) stmt;

      stmt = region.getParent();

      while(stmt != null) {
        if(stmt instanceof  CompoundStatement) {
          cstmt = (CompoundStatement) stmt;
          if(cstmt.containsSymbol(sym)) {
            found = true;
          }
        }

        if(found) {
          break;
        }
        if(stmt instanceof Procedure) {
          break;
        }
        stmt = stmt.getParent();
      }

      if(found) {
        int a;
        boolean isArray = SymbolTools.isArray(sym);

        VariableDeclaration decl = (VariableDeclaration)cstmt.findSymbol(new NameID(sym.getSymbolName()));
        Expression dimension = new IntegerLiteral(1);

        //decl.clone();
        List<Specifier> specs;

        Declaration sharedVar_decl = null;
        VariableDeclarator sharedVar_declarator = new VariableDeclarator(new NameID(sym.getSymbolName()));
        specs = new LinkedList<Specifier>();
        specs.addAll(decl.getSpecifiers());
        if(isArray) {
          dimension = ((ArraySpecifier)sym.getArraySpecifiers().get(0)).getDimension(0);
          specs.add(PointerSpecifier.UNQUALIFIED) ;
        }
        sharedVar_decl = new VariableDeclaration(specs, sharedVar_declarator);
        new_proc.addDeclaration(sharedVar_decl);
        Expression sharedVar =sharedVar_declarator.getID();
        if(isArray) {

          Declaration sharedVarCPU_decl = null;
          VariableDeclarator sharedVarCPU_declarator = new VariableDeclarator(new NameID("gpu_" + sym.getSymbolName()));
          sharedVarCPU_decl = new VariableDeclaration(specs, sharedVarCPU_declarator);
          addPoint.addDeclaration(sharedVarCPU_decl);

          Expression sharedVarCPU = sharedVarCPU_declarator.getID();

          call_to_new_proc.addArgument(sharedVarCPU.clone());

          //Perform malloc
          FunctionCall cuMalloc = new FunctionCall(new NameID("cudaMalloc"));
          cuMalloc.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF, sharedVarCPU.clone()));
          List<Specifier> specs2= new ArrayList<Specifier>() ;
          specs2.add((Specifier)sharedVarCPU_declarator.getTypeSpecifiers().get(0));
          BinaryExpression size = new BinaryExpression(dimension, BinaryOperator.MULTIPLY, new SizeofExpression(specs2));
          cuMalloc.addArgument(size);

          ExpressionStatement cuMallocCall = new ExpressionStatement(cuMalloc);
          addPoint.addStatementBefore(region, cuMallocCall);
            /*
            specs = new LinkedList<Specifier>();
            specs.add(Specifier.INT);                */
          FunctionCall memCpy = new FunctionCall(new NameID("cudaMemcpy"));
          memCpy.addArgument(sharedVarCPU.clone());
          memCpy.addArgument(sharedVar.clone());
          memCpy.addArgument(size.clone());
          memCpy.addArgument(new NameID("cudaMemcpyHostToDevice"));

          ExpressionStatement memCpyCall = new ExpressionStatement(memCpy);
          addPoint.addStatementBefore(region, memCpyCall);

          FunctionCall cufree = new FunctionCall(new NameID("cudaFree"));
          cufree.addArgument(sharedVarCPU.clone());

          ExpressionStatement cufreeCall = new ExpressionStatement(cufree);
          addPoint.addStatementAfter(region, cufreeCall);

        } else { //scalar - pass by value - place on constant memory
          call_to_new_proc.addArgument(sharedVar.clone());
        }


      }
    }

    public void handleTextureVars( KernelFunctionCall call_to_new_proc, Procedure new_proc, Statement region) {
        if(c2cu.mapperTextureVars == null) {
          return;
        }

        //create texture binding function
        Procedure texBind = new Procedure(Specifier.VOID, new ProcedureDeclarator(new NameID("bindMapperTextures"),
                new LinkedList()), new CompoundStatement());
        //Create fuction call for this procedure
        FunctionCall bindTexCall = new FunctionCall(new NameID("bindMapperTextures"));

        for(Symbol sym : c2cu.mapperTextureVars) {
          if(SymbolTools.isScalar(sym)) {
            System.out.println("handleTextureVars : Scalar passed for texture caching");
            System.exit(0);
          }

          genCommForSharedVars(call_to_new_proc, new_proc, region, sym);

          List<Specifier> specs = new LinkedList<Specifier>();
          specs.addAll(((VariableDeclaration) sym.getDeclaration()).getSpecifiers());

          Declaration texture_decl = null;
          VariableDeclarator texture_declarator = new VariableDeclarator(new NameID(sym.getSymbolName() + "_texture"));
          TextureSpecifier textureSpec = new TextureSpecifier(specs);
          texture_decl = new VariableDeclaration(textureSpec, texture_declarator);
          c2cu.kernelsTranslationUnit.addDeclarationFirst(texture_decl);

          //Add declaration for the variables in the binding process
          Declaration array_decl = null;
          VariableDeclarator array_declarator = new VariableDeclarator(new NameID(sym.getSymbolName() + "_ptr"));
          specs.add(PointerSpecifier.UNQUALIFIED);
          texture_decl = new VariableDeclaration(specs, array_declarator);
          texBind.addDeclaration(texture_decl);

          specs = new LinkedList<Specifier>();
          specs.add(Specifier.INT);
          Declaration size_decl = null;
          VariableDeclarator size_declarator = new VariableDeclarator(new NameID(sym.getSymbolName() + "_size"));
          size_decl = new VariableDeclaration(specs, size_declarator);
          texBind.addDeclaration(size_decl);

          Expression array = array_declarator.getID();
          Expression size = size_declarator.getID();
          Expression tex = texture_declarator.getID();
          //(cudaBindTexture(0, rater_id_tex, a, size1)
          FunctionCall cudaBind = new FunctionCall(new NameID("cudaBindTexture"));
          cudaBind.addArgument(new IntegerLiteral(0));
          cudaBind.addArgument(tex.clone());
          cudaBind.addArgument(array.clone());
          cudaBind.addArgument(size.clone());

          ExpressionStatement cudaBindCall = new ExpressionStatement(cudaBind);
          texBind.getBody().addStatement(cudaBindCall);

          Expression arrayDim = ((ArraySpecifier)sym.getArraySpecifiers().get(0)).getDimension(0);
          specs.clear();
          specs.addAll(sym.getTypeSpecifiers());
          BinaryExpression arraySize = new BinaryExpression(arrayDim.clone(), BinaryOperator.MULTIPLY, new SizeofExpression(specs));

          CompoundStatement cstmt = null;
          Traversable tr = region;
          while(! (tr instanceof CompoundStatement)) {
             tr = tr.getParent();
          }
          cstmt = (CompoundStatement)tr;

          VariableDeclaration gpuVarDecl = (VariableDeclaration) cstmt.findSymbol(new NameID("gpu_" + sym.getSymbolName()));
          bindTexCall.addArgument(gpuVarDecl.getDeclaredIDs().get(0).clone());
          bindTexCall.addArgument(arraySize.clone());

          //Replace the array access with tex1Dfetch
          new_proc.findSymbol(new NameID(sym.getSymbolName()));
          List<Expression> accesses = IRTools.findExpressions(region, new Identifier(sym));
          for(Expression ex : accesses) {
            if(ex.getParent() instanceof ArrayAccess) {
              ArrayAccess acc = (ArrayAccess) ex.getParent();
              FunctionCall texFetch = new FunctionCall(new NameID("tex1Dfetch"));
              texFetch.addArgument(tex.clone());
              texFetch.addArgument(acc.getIndex(0).clone());
              IRTools.replaceAll(acc.getParent(), acc, texFetch);
            }
          }

          //Replace other occurances of the 'line' variable with the new variable
          //IRTools.replaceAll(new_proc.getBody(), line, ip);

        }
        ExpressionStatement bindTexCallMapper = new ExpressionStatement(bindTexCall);
        ((CompoundStatement)region.getParent()).addStatementBefore(region, bindTexCallMapper);
        c2cu.kernelsTranslationUnit.addDeclaration(texBind);

        //Add extern declaration for the texture bind function
        List<Specifier> specs = new LinkedList<Specifier>();
        TranslationUnit tuMain = (TranslationUnit) c2cu.mainProc.getParent();
        specs.clear();
        specs.add(Specifier.EXTERN);
        specs.addAll(texBind.getReturnType());
        VariableDeclaration texBindMapperDecl = new VariableDeclaration(specs, texBind.getDeclarator());
        tuMain.addDeclarationFirst(texBindMapperDecl);

    }
}
