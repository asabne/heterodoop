package cudamr.transform;

import cetus.hir.*;
import cetus.transforms.TransformPass;
import cudamr.analysis.AnalysisTools;
import cudamr.codegen.c2cu;
import cudamr.hir.CUDASpecifier;
import cudamr.hir.mapreduceAnnotation;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/15/13
 * Time: 11:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class privatizer extends TransformPass {
    private static String pass_name = "[privatizer]";

    protected TranslationUnit mrHeader = new TranslationUnit("mr.h");

    public privatizer(Program program) {
        super(program);
    }

    public String getPassName() {
        return pass_name;
    }

    public void start() {
        List<Procedure> procedureList = IRTools.getProcedureList(program);
        for( Procedure cProc : procedureList ) {
            List<mapreduceAnnotation> regionAnnots = /*AnalysisTools.collectPragmas(cProc, OmpAnnotation.class,
                    OmpAnnotation.keywords, false);*/
                    AnalysisTools.collectPragmas(cProc, mapreduceAnnotation.class);
            //Encapsulate the mapper region inside a compound statement
            for( mapreduceAnnotation pAnnot : regionAnnots ) {
                //Process the mapper part
                if(pAnnot.get("mapper") != null) {
                    CompoundStatement c1 = (CompoundStatement) new CompoundStatement();
                    CompoundStatement c2 = new CompoundStatement();
                    c1.addStatement(c2);
                    cProc.getBody().addStatementAfter((Statement) pAnnot.getAnnotatable(), c1);
                    Statement region = (Statement) pAnnot.getAnnotatable();
                    region.swapWith(c2);
                }
            }

        }

        for( Procedure cProc : procedureList ) {
            List<mapreduceAnnotation> regionAnnots = /*AnalysisTools.collectPragmas(cProc, OmpAnnotation.class,
                    OmpAnnotation.keywords, false);*/
                    AnalysisTools.collectPragmas(cProc, mapreduceAnnotation.class);

            for( mapreduceAnnotation pAnnot : regionAnnots ) {
                //Process the mapper part
                if(pAnnot.get("mapper")!= null) {
                    privatize((Statement)pAnnot.getAnnotatable().getParent(), cProc, pAnnot, 1);
                } else if (pAnnot.get("combiner") != null) {
                    privatize((Statement)pAnnot.getAnnotatable(), cProc, pAnnot, 0);
                }
            }

        }
    }

    public void privatize(Statement region, Procedure proc, Annotation pAnnot, int mapper) {
        /*List<Symbol> allSyms = new LinkedList<Symbol>();
        Traversable tr = region;
        while(tr.getParent() != null) {
            tr = (Traversable) tr.getParent();
            if(tr instanceof CompoundStatement) {
                allSyms.addAll(((CompoundStatement) tr).getSymbols());
            }
        }*/

        Set<Symbol> allSyms = SymbolTools.getAccessedSymbols(region);
        List<CompoundStatement> clist = IRTools.getDescendentsOfType(region, CompoundStatement.class);
        List<CompoundStatement> clistParent = new LinkedList<CompoundStatement>();
        Traversable tr = region;

        while(tr.getParent() != null) {
            tr = tr.getParent();
            if(tr instanceof CompoundStatement) {
                clistParent.add((CompoundStatement)tr);
            }
        }

        HashSet<NameID> sharedROSyms = pAnnot.get("sharedRO");
        HashSet<NameID> textureSyms = pAnnot.get("texture");
        Iterator<Symbol> iterator = allSyms.iterator();
        while(iterator.hasNext()) {
          Symbol setElement = iterator.next();
          NameID sym = new NameID(setElement.getSymbolName().toString());
          if(sharedROSyms != null) {
            if(sharedROSyms.contains(sym)) {
              c2cu.mapperSharedVars.add(setElement);
              iterator.remove();
              continue;
            }
          }
          if(textureSyms != null) {
            if(textureSyms.contains(sym)) {
              c2cu.mapperTextureVars.add(setElement);
              iterator.remove();
            }
          }
        }


        /*
        //find out the def set
        Set<Symbol> defSet = DataFlowTools.getDefSymbol(tr);

        //find symbols used in functions
        Set<Symbol> funcArgsSet = new HashSet<Symbol>();

        List<FunctionCall> functionCalls = IRTools.getFunctionCalls(tr);

        for(FunctionCall f: functionCalls) {
          funcArgsSet.addAll(DataFlowTools.getUseSymbol(f));
        }

        //remove the symbols which are read-only, except for the 'key'. Don't remove those symbols that were used in function calls
        //Add the read-only symbols to mapperSharedVars set
        Iterator<Symbol> iterator = allSyms.iterator();
        while(iterator.hasNext()) {
          Symbol setElement = iterator.next();
          if( !defSet.contains(setElement) && !funcArgsSet.contains(setElement) && !pAnnot.get("key").toString().equals(setElement.getSymbolName()) && !pAnnot.get("value").toString().equals(setElement.getSymbolName()) )  {
            c2cu.mapperSharedVars.add(setElement);
            iterator.remove();

          }
        }
        */


      for (Symbol sym : allSyms) {
            boolean found = false;
            for (CompoundStatement cstmt : clist) {
                if(cstmt.containsSymbol(sym)) {
                    found = true;
                    break;
                }
            }
            if(!found) {
                CompoundStatement defContainer = null;
                for (CompoundStatement cstmt : clistParent) {
                    if(cstmt.containsSymbol(sym)) {
                        defContainer = cstmt;
                        break;
                    }
                }
                if(defContainer != null) {
                    VariableDeclaration decl = (VariableDeclaration)defContainer.findSymbol(new NameID(sym.getSymbolName()));
                    Expression oldVar = new Identifier((Symbol)decl.getDeclarator(0));
                    List<Specifier> specs = new LinkedList<Specifier>();
                    if(mapper == 0) {
                      if(pAnnot.get("keyin").toString().equals(sym.getSymbolName()) || pAnnot.get("valuein").toString().equals(sym.getSymbolName()) || pAnnot.get("key").toString().equals(sym.getSymbolName()) || pAnnot.get("value").toString().equals(sym.getSymbolName()) ) {
                        if(SymbolTools.isArray(sym)) {
                          specs.add(CUDASpecifier.CUDA_SHARED);
                        }
                      }
                    }

                    Expression newVar = declareClonedVariable(region, sym, "gpu_" + sym.getSymbolName(), null, specs, false );
                    if(pAnnot.get("key").toString().equals(sym.getSymbolName())) {
                        if(mapper == 1) {
                            c2cu.mapBufKey = newVar;
                        } else {
                            c2cu.combBufKey = newVar;
                        }
                    }
                    if(pAnnot.get("value").toString().equals(sym.getSymbolName())) {
                        if(mapper == 1) {
                            c2cu.mapBufVal = newVar;
                        } else {
                            c2cu.combBufVal = newVar;
                        }
                    }
                    if(mapper == 0) {
                        if(pAnnot.get("keyin").toString().equals(sym.getSymbolName())) {
                            c2cu.combBufInKey = newVar;
                        }
                        if(pAnnot.get("valuein").toString().equals(sym.getSymbolName())) {
                            c2cu.combBufInVal = newVar;
                        }
                    }
                    IRTools.replaceAll(region, oldVar, newVar);


                }
            }
        }

    }

    public Identifier declareClonedVariable(Traversable where, Symbol inSym, String name, List<Specifier> removeSpecs,
                                                   List<Specifier> addSpecs, boolean removeLeftMostDim) {
        NameID id = new NameID(name);
        Identifier newID = null;
        List specs = null;
        List orgArraySpecs = null;
        List arraySpecs = new LinkedList();
        SymbolTable st;
        if (where instanceof SymbolTable) {
            st = (SymbolTable)where;
        } else {
            st = IRTools.getAncestorOfType(where, SymbolTable.class);
        }
        if (st instanceof Loop) {
            st = IRTools.getAncestorOfType(st, SymbolTable.class);
        }
        Set<String> symNames = AnalysisTools.symbolsToStringSet(st.getSymbols());
        if( symNames.contains(name) ) {
            VariableDeclaration decl = (VariableDeclaration)st.findSymbol(new NameID(name));
            newID = new Identifier((Symbol)decl.getDeclarator(0));
        } else {
            if( inSym instanceof VariableDeclarator ) {
                VariableDeclarator varSym = (VariableDeclarator)inSym;
                specs = varSym.getTypeSpecifiers();
                if( removeSpecs != null ) {
                    specs.removeAll(removeSpecs);
                }
                if( addSpecs != null ) {
                    for(Specifier spec : addSpecs) {
                      specs.add(0, spec);
                    }
                }
                // Separate declarator/declaration specifiers.
                List declaration_specs = new ArrayList(specs.size());
                List declarator_specs = new ArrayList(specs.size());
                for (int i = 0; i < specs.size(); i++) {
                    Object spec = specs.get(i);
                    if (spec instanceof PointerSpecifier) {
                        declarator_specs.add(spec);
                    } else {
                        declaration_specs.add(spec);
                    }
                }
                orgArraySpecs = varSym.getArraySpecifiers();
                if(  (orgArraySpecs != null) && (!orgArraySpecs.isEmpty()) ) {
                    for( Object obj : orgArraySpecs ) {
                        if( obj instanceof ArraySpecifier ) {
                            ArraySpecifier oldAS = (ArraySpecifier)obj;
                            int numDims = oldAS.getNumDimensions();
                            List<Expression> dimensions = new LinkedList<Expression>();
                            if( removeLeftMostDim ) {
                                dimensions.add(null);
                            } else {
                                dimensions.add(oldAS.getDimension(0));
                            }
                            for( int i=1; i<numDims; i++ ) {
                                dimensions.add(oldAS.getDimension(i));
                            }
                            ArraySpecifier newAS = new ArraySpecifier(dimensions);
                            arraySpecs.add(newAS);
                        } else {
                            arraySpecs.add(obj);
                        }
                    }
                }
                VariableDeclarator Declr = null;
                if( declarator_specs.isEmpty() ) {
                    if( arraySpecs == null || arraySpecs.isEmpty() ) {
                        Declr = new VariableDeclarator(id);
                    } else {
                        Declr = new VariableDeclarator(id, arraySpecs);
                    }
                } else if ( arraySpecs == null || arraySpecs.isEmpty() ) {
                    Declr = new VariableDeclarator(declarator_specs, id);
                } else {
                    Declr = new VariableDeclarator(declarator_specs, id, arraySpecs);
                }

                Declaration decls = new VariableDeclaration(declaration_specs, Declr);
                st.addDeclaration(decls);
                newID = new Identifier(Declr);
            } else if( inSym instanceof NestedDeclarator ) {
                NestedDeclarator nestedSym = (NestedDeclarator)inSym;
                Declarator childDeclarator = nestedSym.getDeclarator();
                List childDeclr_specs = null;
                List childArray_specs = null;
                if( childDeclarator instanceof VariableDeclarator ) {
                    VariableDeclarator varSym = (VariableDeclarator)childDeclarator;
                    childDeclr_specs = varSym.getSpecifiers();
                    childArray_specs = varSym.getArraySpecifiers();
                } else {
                    Tools.exit("[ERROR in TransformTools.declareClonedVariable()] nested declarator whose child declarator is also " +
                            "nested declarator is not supported yet; exit!\n" +
                            "Symbol: " + inSym + "\n");
                }
                VariableDeclarator childDeclr = null;
                if (childDeclr_specs.isEmpty()) {
                    if (childArray_specs == null || childArray_specs.isEmpty()) {
                        childDeclr = new VariableDeclarator(id);
                    } else {
                        childDeclr = new VariableDeclarator(id, childArray_specs);
                    }
                } else if (childArray_specs == null || childArray_specs.isEmpty()) {
                    childDeclr = new VariableDeclarator(childDeclr_specs, id);
                } else {
                    childDeclr = new VariableDeclarator(childDeclr_specs, id, childArray_specs);
                }
                specs = nestedSym.getTypeSpecifiers();
                if( removeSpecs != null ) {
                    specs.removeAll(removeSpecs);
                }
                if( addSpecs != null ) {
                    specs.addAll(addSpecs);
                }
                // Separate declarator/declaration specifiers.
                List declaration_specs = new ArrayList(specs.size());
                List declarator_specs = new ArrayList(specs.size());
                for (int i = 0; i < specs.size(); i++) {
                    Object spec = specs.get(i);
                    if (spec instanceof PointerSpecifier) {
                        declarator_specs.add(spec);
                    } else {
                        declaration_specs.add(spec);
                    }
                }
                arraySpecs = nestedSym.getArraySpecifiers();
                NestedDeclarator nestedDeclr = null;
                if( declarator_specs.isEmpty() ) {
                    if( arraySpecs == null || arraySpecs.isEmpty() ) {
                        nestedDeclr = new NestedDeclarator(childDeclr);
                    } else {
                        nestedDeclr = new NestedDeclarator(declarator_specs, childDeclr, null, arraySpecs);
                    }
                } else if ( arraySpecs == null || arraySpecs.isEmpty() ) {
                    nestedDeclr = new NestedDeclarator(declarator_specs, childDeclr, null);
                } else {
                    nestedDeclr = new NestedDeclarator(declarator_specs, childDeclr, null, arraySpecs);
                }

                Declaration decls = new VariableDeclaration(declaration_specs, nestedDeclr);
                st.addDeclaration(decls);
                newID = new Identifier(nestedDeclr);
            }
        }
        return newID;
    }
}
