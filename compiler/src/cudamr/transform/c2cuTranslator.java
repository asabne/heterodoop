package cudamr.transform;

import cetus.exec.Driver;
import cetus.hir.*;
import cudamr.analysis.AnalysisTools;
import cudamr.codegen.c2cu;
import cudamr.hir.CUDASpecifier;
import cudamr.hir.KernelFunctionCall;
import cudamr.hir.mapreduceAnnotation;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/10/13
 * Time: 10:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class c2cuTranslator extends c2gpuTranslator {
    protected String pass_name = "[c2cuTranslator]";


    ////////////////////////////////////////////////////////
    // Default values are for CUDA Compute Capability 1.x //
    ////////////////////////////////////////////////////////
    protected int maxBlockDimXY = 512;
    protected int maxBlockDimZ = 64;
    protected int maxBlockSize = 512;
    protected int maxGridDimensionality = 2;
    protected int maxGridDimSize = 65535; //Max. dim. size of Grid = 65535
    protected int maxSMemSize = 16384;
    protected int maxCMemSize = 65536;
    protected long max1DTextureLMBound = 134217728; //Max 1D linear memory texture bound size in bytes
    protected int warpSize = 32; //default warp size. [CAUTION] if future CUDA has different warp size, this should be set by command-line input.


    protected boolean opt_shrdSclrCachingOnSM = false;

    public c2cuTranslator(Program prog) {
        super(prog);
        pass_name = "[c2cuTranslator]";

        boolean found_main = false;
        for ( Traversable tt : program.getChildren() )
        {
            TranslationUnit tu = (TranslationUnit)tt;
            String iFileName = tu.getInputFilename();
            int dot = iFileName.lastIndexOf(".h");
            if( dot >= 0 ) {
                continue;
            }
            DFIterator<Procedure> iter =
                    new DFIterator<Procedure>(tu, Procedure.class);
            iter.pruneOn(Procedure.class);
            iter.pruneOn(Statement.class);
            iter.pruneOn(Declaration.class);
            while( iter.hasNext() ) {
                Procedure proc = iter.next();
                String name = proc.getName().toString();
					/* f2c code uses MAIN__ */
                if ( (name.equals("main") || name.equals("MAIN__"))) {
                    main = proc;
                    main_TrUnt = tu;
                    found_main = true;
                    main_global_table = (SymbolTable) main_TrUnt;
                    break;
                }
            }
            if(found_main) {
                break;
            }
        }
        CUDAInitializer();
    }


    protected void CUDAInitializer() {
        Statement acc_init_stmt, acc_shutdown_stmt;
        Statement optPrintStmt;
        Statement confPrintStmt;

        String value ;

        value = Driver.getOptionValue("cudaMaxGridDimSize");
        if( value != null ) {
            maxGridDimSize = Integer.valueOf(value).intValue();
        }

        value = Driver.getOptionValue("cudaSharedMemSize");
        if( value != null ) {
            maxSMemSize = Integer.valueOf(value).intValue();
        }

        value = Driver.getOptionValue("shrdSclrCachingOnSM");
        if( value != null ) {
            opt_shrdSclrCachingOnSM = true;
            FunctionCall shrdSclrCachingOnSMPrintCall = new FunctionCall(new NameID("printf"));
            shrdSclrCachingOnSMPrintCall.addArgument(
                    new StringLiteral("====> Cache shared scalar variables onto GPU shared memory.\\n"));
            optPrintStmts.add( new ExpressionStatement(shrdSclrCachingOnSMPrintCall) );
        }

        value = Driver.getOptionValue("shrdSclrCachingOnReg");
        if( value != null ) {
            FunctionCall shrdSclrCachingOnRegPrintCall = new FunctionCall(new NameID("printf"));
            if( opt_shrdSclrCachingOnSM ) {
                shrdSclrCachingOnRegPrintCall.addArgument(
                        new StringLiteral("====> Cache shared scalar variables onto GPU registers.\\n"
                                + "      (Because shrdSclrCachingOnSM is on, R/O shared scalar variables\\n"
                                + "       are cached on shared memory, instead of registers.)\\n"));
            } else {
                shrdSclrCachingOnRegPrintCall.addArgument(
                        new StringLiteral("====> Cache shared scalar variables onto GPU registers.\\n"));
            }
            optPrintStmts.add( new ExpressionStatement(shrdSclrCachingOnRegPrintCall) );
        }

        value = Driver.getOptionValue("shrdArryElmtCachingOnReg");
        if( value != null ) {
            FunctionCall shrdArryElmtCachingOnRegPrintCall = new FunctionCall(new NameID("printf"));
            shrdArryElmtCachingOnRegPrintCall.addArgument(
                    new StringLiteral("====> Cache shared array elements onto GPU registers.\\n"));
            optPrintStmts.add( new ExpressionStatement(shrdArryElmtCachingOnRegPrintCall) );
        }

        value = Driver.getOptionValue("prvtArryCachingOnSM");
        if( value != null ) {
            FunctionCall prvtArryCachingOnSMPrintCall = new FunctionCall(new NameID("printf"));
            prvtArryCachingOnSMPrintCall.addArgument(
                    new StringLiteral("====> Cache private array variables onto GPU shared memory.\\n"));
            optPrintStmts.add( new ExpressionStatement(prvtArryCachingOnSMPrintCall) );
        }

        value = Driver.getOptionValue("shrdArryCachingOnTM");
        if( value != null ) {
            FunctionCall shrdArryCachingOnTMPrintCall = new FunctionCall(new NameID("printf"));
            shrdArryCachingOnTMPrintCall.addArgument(
                    new StringLiteral("====> Cache 1-dimensional, R/O shared array variables onto GPU texture memory.\\n"));
            optPrintStmts.add( new ExpressionStatement(shrdArryCachingOnTMPrintCall) );
        }

        value = Driver.getOptionValue("shrdSclrCachingOnConst");
        if( value != null ) {
            FunctionCall shrdSclrCachingOnConstPrintCall = new FunctionCall(new NameID("printf"));
            shrdSclrCachingOnConstPrintCall.addArgument(
                    new StringLiteral("====> Cache R/O shared scalar variables onto GPU constant memory.\\n"));
            optPrintStmts.add( new ExpressionStatement(shrdSclrCachingOnConstPrintCall) );
        }
        value = Driver.getOptionValue("shrdArryCachingOnConst");
        if( value != null ) {
            FunctionCall shrdArryCachingOnConstPrintCall = new FunctionCall(new NameID("printf"));
            shrdArryCachingOnConstPrintCall.addArgument(
                    new StringLiteral("====> Cache R/O shared array variables onto GPU constant memory.\\n"));
            optPrintStmts.add( new ExpressionStatement(shrdArryCachingOnConstPrintCall) );
        }
        value = Driver.getOptionValue("noReducer");
        if( value != null ) {
          c2cu.noReducer = true;
          FunctionCall noReducerSpecify = new FunctionCall(new NameID("printf"));
          noReducerSpecify.addArgument(
                  new StringLiteral("====> No reducers used\\n"));
            optPrintStmts.add( new ExpressionStatement(noReducerSpecify) );
        } else {
          c2cu.noReducer = false;
        }


        // Insert macro for kernel file
            /* Insert CUDA-related header files and macros */
        StringBuilder kernelStr = new StringBuilder(2048);
        kernelStr.append("/*******************************************/\n");
        kernelStr.append("/* Added codes for C to CUDA translation */\n");
        kernelStr.append("/*******************************************/\n");
        kernelStr.append("#define MAX(a,b) (((a) > (b)) ? (a) : (b))\n");
        kernelStr.append("#define MIN(a,b) (((a) < (b)) ? (a) : (b))\n");
        kernelStr.append("#define DBL_MAX 1.7976931348623158e+308\n");
        kernelStr.append("#define DBL_MIN 2.2250738585072014e-308\n");
        kernelStr.append("#define FLT_MAX 3.402823466e+38\n");
        kernelStr.append("#define FLT_MIN 1.175494351e-38\n");
        kernelStr.append("\n");
        CodeAnnotation accHeaderAnnot = new CodeAnnotation(kernelStr.toString());
        AnnotationDeclaration accHeaderDecl = new AnnotationDeclaration(accHeaderAnnot);

        c2cu.kernelsTranslationUnit.addDeclarationFirst(accHeaderDecl);


        for ( Traversable tt : program.getChildren() )
        {
            TranslationUnit tu = (TranslationUnit)tt;

            //If the translation unit does not contain any declaration, skip
            if(tu.getDeclarations().size() == 0)
                continue;

            String iFileName = tu.getInputFilename();
            int dot = iFileName.lastIndexOf(".h");
            if( dot >= 0 ) {
                continue;
            }
            dot = iFileName.lastIndexOf(".cu");
            if( dot >= 0 ) {
                continue;
            }
            PrintTools.println(pass_name + "Input file name = " + iFileName, 5);

            CodeAnnotation headAnnot = new CodeAnnotation("#include \"mr.h\"");
            AnnotationDeclaration mrh = new AnnotationDeclaration(headAnnot);
            tu.addDeclarationFirst(mrh);

            String[] name = iFileName.split("\\.");
            String oFileName = name[0] + ".cu";
            tu.setOutputFilename(oFileName);


        }

        CodeAnnotation headAnnot = new CodeAnnotation("#include \"mrlib.h\"");
        AnnotationDeclaration mrLib = new AnnotationDeclaration(headAnnot);
        c2cu.kernelsTranslationUnit.addDeclarationFirst(mrLib);
    }

    protected void extractComputeRegion(Procedure cProc, mapreduceAnnotation cAnnot, String new_func_name, boolean IRSymbolOnly) {
        PrintTools.println("[extractComputeRegion() begins] current Procedure: " + cProc.getSymbolName()
                + "\nMapReduce annotation: " + cAnnot +"\n", 1);
        Statement region = (Statement)cAnnot.getAnnotatable();
        SymbolTable global_table = (SymbolTable) cProc.getParent();
        TranslationUnit parentTrUnt = (TranslationUnit)cProc.getParent();

        //////////////////////////////////////////////////////////////////
        // Extract internal directives attached to this compute region. //
        //////////////////////////////////////////////////////////////////
        HashSet<Symbol> accsharedSet = new HashSet<Symbol>();
        HashSet<Symbol> accreadonlySet = new HashSet<Symbol>();
        //HashSet<Symbol> accprivateSet = new HashSet<Symbol>();
        //HashSet<Symbol> rcreateSet = new HashSet<Symbol>();
        HashSet<Symbol> accreductionSet = new HashSet<Symbol>();

        //////////////////////////////////////////////////////////////
        // Extract CUDA directives attached to this compute region. //
        //////////////////////////////////////////////////////////////
        Set<Symbol> cudaRegisterROSet = new HashSet<Symbol>();
        Set<Symbol> cudaRegisterSet = new HashSet<Symbol>();
        Set<Symbol> cudaNoRegisterSet = new HashSet<Symbol>();
        Set<Symbol> cudaSharedROSet = new HashSet<Symbol>();
        Set<Symbol> cudaSharedSet = new HashSet<Symbol>();
        Set<Symbol> cudaNoSharedSet = new HashSet<Symbol>();
        Set<Symbol> cudaTextureSet = new HashSet<Symbol>();
        Set<Symbol> cudaNoTextureSet = new HashSet<Symbol>();
        Set<Symbol> cudaConstantSet = new HashSet<Symbol>();
        Set<Symbol> cudaNoConstantSet = new HashSet<Symbol>();
        //Set<Symbol> cudaNoRedUnrollSet = new HashSet<Symbol>();

        /*TODO : Set up above sets */
        /*
        Statement confRefStmt = region;
        CompoundStatement confRefParent = (CompoundStatement)confRefStmt.getParent();
        CompoundStatement prefixStmts = new CompoundStatement();
        CompoundStatement postscriptStmts = new CompoundStatement();
        */

        /*
        ////////////////////////////////////////////////////////
        // Auxiliary variables used for GPU kernel conversion //
        ////////////////////////////////////////////////////////
        VariableDeclaration bytes_decl = (VariableDeclaration)SymbolTools.findSymbol(global_table, "gpuBytes");
        Identifier cloned_bytes = new Identifier((VariableDeclarator)bytes_decl.getDeclarator(0));
        VariableDeclaration gmem_decl = null;
        Identifier gmemsize = null;
        VariableDeclaration smem_decl = null;
        Identifier smemsize = null;
        ExpressionStatement gMemAdd_stmt = null;
        ExpressionStatement gMemSub_stmt =  null;

        gmem_decl = (VariableDeclaration)SymbolTools.findSymbol(global_table, "gpuGmemSize");
        gmemsize = new Identifier((VariableDeclarator)gmem_decl.getDeclarator(0));
        smem_decl = (VariableDeclaration)SymbolTools.findSymbol(global_table, "gpuSmemSize");
        smemsize = new Identifier((VariableDeclarator)smem_decl.getDeclarator(0));
        gMemAdd_stmt = new ExpressionStatement( new AssignmentExpression(gmemsize,
                AssignmentOperator.ADD, (Identifier)cloned_bytes.clone()) );
        gMemSub_stmt = new ExpressionStatement( new AssignmentExpression((Identifier)gmemsize.clone(),
                AssignmentOperator.SUBTRACT, (Identifier)cloned_bytes.clone()) );

        VariableDeclaration numBlocks_decl = (VariableDeclaration)SymbolTools.findSymbol(global_table, "gpuNumBlocks");
        Identifier numBlocks = new Identifier((VariableDeclarator)numBlocks_decl.getDeclarator(0));
        VariableDeclaration numThreads_decl = (VariableDeclaration)SymbolTools.findSymbol(global_table, "gpuNumThreads");
        Identifier numThreads = new Identifier((VariableDeclarator)numThreads_decl.getDeclarator(0));
        VariableDeclaration totalNumThreads_decl = (VariableDeclaration)SymbolTools.findSymbol(global_table, "totalGpuNumThreads");
        Identifier totalNumThreads = new Identifier((VariableDeclarator)totalNumThreads_decl.getDeclarator(0));
        ExpressionStatement gpuBytes_stmt = null;
        VariableDeclarator rowidSymbol = null;

        // The following variables will be added to each GPU kernel.
        // int _bid;
        // int _bsize;
        // int _tid;
        // int _gtid;
        Identifier bid = null;
        Identifier bsize = null;
        Identifier tid = null;
        Identifier gtid = null;

        ///////////////////////////////////////////////////////////////////////////////////////
        // Create a kernel procedure, to which the current compute region is converted into, //
        // and a function call to the kernel procedure.                                      //
        ///////////////////////////////////////////////////////////////////////////////////////
        List<Specifier> new_proc_ret_type = new LinkedList<Specifier>();
        new_proc_ret_type.add(CUDASpecifier.EXTERN);
        new_proc_ret_type.add(CUDASpecifier.EXTERN_C);
        new_proc_ret_type.add(CUDASpecifier.CUDA_GLOBAL);
        new_proc_ret_type.add(Specifier.VOID);

        Procedure new_proc = new Procedure(new_proc_ret_type,
                new ProcedureDeclarator(new NameID(new_func_name),
                        new LinkedList()), new CompoundStatement());
        List<Expression> kernelConf = new ArrayList<Expression>();
        KernelFunctionCall call_to_new_proc = new KernelFunctionCall(new NameID(
                new_func_name), new LinkedList(), kernelConf);
        call_to_new_proc.setLinkedProcedure(new_proc);
        Statement kernelCall_stmt = new ExpressionStatement(call_to_new_proc);
        */
    }

}
