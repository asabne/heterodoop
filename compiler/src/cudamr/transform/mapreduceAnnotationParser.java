package cudamr.transform;

import cetus.exec.Driver;
import cetus.hir.*;
import cetus.transforms.TransformPass;

import cudamr.hir.mapreduceAnnotation;
import cudamr.analysis.mapreduceParser;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/11/13
 * Time: 2:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class mapreduceAnnotationParser extends TransformPass {
    
    public mapreduceAnnotationParser(Program program)
    {
        super(program);
    }

    public String getPassName()
    {
        return new String("[mapreduceAnnotParser]");
    }

    public void start()
    {
        Annotation new_annot = null;
        boolean attach_to_next_annotatable = false;
        HashMap<String, Object> new_map = null;
        HashMap<String, String> macroMap = null;
        LinkedList<Annotation> annots_to_be_attached = new LinkedList<Annotation>();
        int ASPENModelGen = 0;

		
		/* Iterate over the program in Depth First order and search for Annotations */
		/* Change this such that each TranslationUnit is searched one-by-one. */
        for( Traversable trUnt : program.getChildren() ) {
            macroMap = new HashMap<String, String>();
            DFIterator<Traversable> iter = new DFIterator<Traversable>(trUnt);

            while(iter.hasNext())
            {
                Object obj = iter.next();

                ////////////////////////////////////////////////////////////////////////
                // AnnotationParser store Openmapreduce annotations as PragmaAnnotations in //
                // either AnnotationStatements or AnnotationDeclaration, depending on //
                // their locations.                                                   //
                ////////////////////////////////////////////////////////////////////////
                if ((obj instanceof AnnotationStatement) || (obj instanceof AnnotationDeclaration) )
                {
                    Annotatable annot_container = (Annotatable)obj;
                    List<PragmaAnnotation> annot_list =
                            annot_container.getAnnotations(PragmaAnnotation.class);
                    if( (annot_list == null) || (annot_list.size() == 0) ) {
                        continue;
                    }
                    ////////////////////////////////////////////////////////////////////////////
                    // AnnotationParser creates one AnnotationStatement/AnnotationDeclaration //
                    // for each PragmaAnnotation.                                             //
                    ////////////////////////////////////////////////////////////////////////////
                    PragmaAnnotation pAnnot = annot_list.get(0);
                    //////////////////////////////////////////////////////////////////////////
                    // The above pAnnot may be a standalone CetusAnnotation, OmpAnnotation, //
                    // InlineAnnotation, PragmaAnnotation.Event, or PragmaAnnotation.Range. //
                    // If so, skip it.                                                      //
                    // (The below annotations are child classes of PragmaAnnotation.)       //
                    // DEBUG: If new child class of the PragmaAnnotation is added, below    //
                    // should be updated too.                                               //
                    // (ex: If CudaAnnotation is added, it should be checked here.)         //
                    //////////////////////////////////////////////////////////////////////////
                    if( pAnnot instanceof CetusAnnotation || pAnnot instanceof OmpAnnotation ||
                            pAnnot instanceof InlineAnnotation || pAnnot instanceof PragmaAnnotation.Event ||
                            pAnnot instanceof PragmaAnnotation.Range) {
                        continue;
                    }
                    String old_annot = pAnnot.getName();
                    if( old_annot == null ) {
                        PrintTools.println("\n[WARNING in mapreduceAnnotationParser] Pragma annotation, "
                                + pAnnot +", does not have name.\n", 0);
                        continue;
                    }
                    old_annot = modifyAnnotationString(old_annot);

					/* -------------------------------------------------------------------------
					 * STEP 1:
					 * Find the annotation type by parsing the text in the input annotation and
					 * create a new Annotation of the corresponding type
					 * -------------------------------------------------------------------------
					 */
                    String[] token_array = old_annot.split("\\s+");
                    // If old_annot string has a leading space, the 2nd token should be checked.
                    //String old_annot_key = token_array[1];
                    String old_annot_key = token_array[0];
					/* Check for mapreduce annotations */
                    if (old_annot_key.compareTo("mapreduce")==0) {
                        String old_annot_key2 = token_array[1];
                        if( old_annot_key2.equals("#") ) { //Preprocess macros on Openmapreduce directives.
                            mapreduceParser.preprocess_mapreduce_pragma(token_array, macroMap);
                            continue;
                        } else {
							/* ---------------------------------------------------------------------
							 * Parse the contents:
							 * mapreduceParser puts the Openmapreduce directive parsing results into new_map
							 * ---------------------------------------------------------------------
							 */
                            new_map = new HashMap<String, Object>();
                            attach_to_next_annotatable = mapreduceParser.parse_mapreduce_pragma(new_map, token_array, macroMap);
							/* Create an mapreduceAnnotation and copy the parsed contents from new_map
							 * into a new mapreduceAnnotation */
                            new_annot = new mapreduceAnnotation();
                            for (String key : new_map.keySet())
                                new_annot.put(key, new_map.get(key));
                        }
                    }
                    else {
                        //Check whether current annotation mapreduceidentally missed mapreduce prefix; if so print error.
                        if( mapreduceAnnotation.mapreduceDirectiveSet.contains(old_annot_key) ) {
                            Tools.exit("[mapreduceAnnotationParsing Error] the following annotation seems to be an Openmapreduce directive, but" +
                                    " \"mapreduce\" prefix seems to be omitted. If so, please add it.\nAnnotation: " + pAnnot + "\n");
                        } else {
                            continue;
                        }
                    }

					/* ----------------------------------------------------------------------------------
					 * STEP 2:
					 * Based on whether the newly created annotation needs to be attached to an Annotatable
					 * object or needs to be inserted as a standalone Annotation contained within
					 * AnnotationStatement or AnnotationDeclaration, perform the following IR
					 * insertion and deletion operations
					 * ----------------------------------------------------------------------------------
					 */
					/* If the annotation doesn't need to be attached to an existing Annotatable object,
					 * remove old PragmaAnnotation and insert the new mapreduceAnnotation into the existing
					 * container.
					 */
                    if (!attach_to_next_annotatable)
                    {
                        annot_container.removeAnnotations(PragmaAnnotation.class);
                        annot_container.annotate(new_annot);

						/* In order to allow non-attached annotations mixed with attached annotations,
						 * check if the to_be_attached list is not empty. If it isn't, some annotations still
						 * exist that need to attached to the very next Annotatable. Hence, ... */
                        if ( !annots_to_be_attached.isEmpty() )
                            attach_to_next_annotatable = true;

                    }
                    else
                    {
						/* Add the newly created Annotation to a list of Annotations that will be attached
						 * to the required Annotatable object in the IR
						 */
                        annots_to_be_attached.add(new_annot);
						/* Remove the old annotation container from the IR */
                        Traversable container_parent = (Traversable)annot_container.getParent();
                        container_parent.removeChild(annot_container);
                    }
                }
				/* -----------------------------------------------------------------------------------
				 * STEP 3:
				 * A list of newly created Annotations to be attached has been created. Attach it to
				 * the instance of Annotatable object that does not already contain an input Annotation, 
				 * this is encountered next
				 * -----------------------------------------------------------------------------------
				 */
                else if ((obj instanceof DeclarationStatement) &&
                        (IRTools.containsClass((Traversable)obj, PreAnnotation.class))) {
                    continue;
                }
                else if ((attach_to_next_annotatable) && (obj instanceof Annotatable))
                {
                    Annotatable container = (Annotatable)obj;
                    if (!annots_to_be_attached.isEmpty() && container != null)
                    {
						/* Attach all the new annotations to this container */
                        for (Annotation annot_to_be_attached : annots_to_be_attached)
                            container.annotate(annot_to_be_attached);
                    }
                    else
                    {
                        System.out.println("Error");
                        System.exit(0);
                    }
					/* reset the flag to false, we've attached all annotations */
                    attach_to_next_annotatable = false;
					/* Clear the list of annotations to be attached, we're done with them */
                    annots_to_be_attached.clear();
                }
            }
        }
    }

    static public String modifyAnnotationString(String old_annotation_str)
    {
        String str = null;
        // The delimiter for split operation is white space(\s).
        // Parenthesis, comma, bracket, and colon are delimiters, too. However, we want to leave 
        // them in the pragma token array. Thus, we append a space before and after the
        // parenthesis and colons so that the split operation can recognize them as
        // independent tokens.
        // Sharp (#) is used as a special delimiter for directive preprocessing.
        // Binary operators (+, -, *, /, %) are also used as a delimiter for macro preprocessing.
        // FIXME: below split will not work on class/struct member mapreduceess expression.
        old_annotation_str = old_annotation_str.replace("(", " ( ");
        old_annotation_str = old_annotation_str.replace(")", " ) ");
        old_annotation_str = old_annotation_str.replace("[", " [ ");
        old_annotation_str = old_annotation_str.replace("]", " ] ");
        old_annotation_str = old_annotation_str.replace(":", " : ");
        old_annotation_str = old_annotation_str.replace(",", " , ");
        old_annotation_str = old_annotation_str.replace("#", " # ");
        old_annotation_str = old_annotation_str.replace("+", " + ");
        old_annotation_str = old_annotation_str.replace("-", " - ");
        old_annotation_str = old_annotation_str.replace("*", " * ");
        old_annotation_str = old_annotation_str.replace("/", " / ");
        old_annotation_str = old_annotation_str.replace("%", " % ");

        str = old_annotation_str;
        return str;
    }
}
