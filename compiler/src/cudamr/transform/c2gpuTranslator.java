package cudamr.transform;

import cetus.hir.*;

import cudamr.hir.*;
import cudamr.analysis.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/11/13
 * Time: 12:18 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class c2gpuTranslator {
    protected Program program;
    protected String pass_name = "[c2gpuTranslator]";
    protected List<Statement> optPrintStmts = new LinkedList<Statement>();
    protected TranslationUnit main_TrUnt;
    protected Procedure main;
    protected SymbolTable main_global_table;
    protected boolean IRSymbolOnly = true;



    protected c2gpuTranslator(Program prog) {
        program = prog;
        //GPUInitializer();
    }

    protected abstract void extractComputeRegion(Procedure cProc, mapreduceAnnotation cAnnot,  String new_func_name,
                                                 boolean IRSymbolOnly);
    public void start() {
        List<Procedure> procedureList = IRTools.getProcedureList(program);
        for( Procedure cProc : procedureList ) {
            List<mapreduceAnnotation> regionAnnots = /*AnalysisTools.collectPragmas(cProc, OmpAnnotation.class,
                    OmpAnnotation.keywords, false);*/
                    AnalysisTools.collectPragmas(cProc,mapreduceAnnotation.class);
            TranslationUnit trUnt = (TranslationUnit)cProc.getParent();
            if( AnalysisTools.isInHeaderFile(cProc, trUnt) && !regionAnnots.isEmpty() ) {
                Tools.exit("[ERROR in c2gpuTranslator] " +
                        "Input file: " + trUnt.getInputFilename() +  "Proc Name"  + cProc.getName() +"\n");
            }
            
            convMapRegionsToGPUKernels(cProc, regionAnnots);
        }
    }

    protected void convMapRegionsToGPUKernels(Procedure cProc, List<mapreduceAnnotation> regionAnnots){
        int kernelCnt = 0;
        for( mapreduceAnnotation pAnnot : regionAnnots ) {
            String GPUKernelName = cProc.getName().toString() + "_kernel" + kernelCnt++;
            Annotatable at = pAnnot.getAnnotatable();
            extractComputeRegion(cProc, pAnnot, GPUKernelName, IRSymbolOnly);
        }
    }


}
