package cudamr.transform;

import cetus.hir.*;
import cetus.transforms.TransformPass;
import cudamr.analysis.AnalysisTools;
import cudamr.codegen.c2cu;
import cudamr.hir.mapreduceAnnotation;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/13/13
 * Time: 7:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class mrCUDASemanticProcessor extends TransformPass {
    private static String pass_name = "[mrCUDASemanticProcessor]";

    public mrCUDASemanticProcessor(Program program) {
        super(program);
    }

    public String getPassName() {
        return pass_name;
    }

    public void start() {
        List<Procedure> procedureList = IRTools.getProcedureList(program);
        for( Procedure cProc : procedureList ) {
            List<mapreduceAnnotation> regionAnnots = /*AnalysisTools.collectPragmas(cProc, OmpAnnotation.class,
                    OmpAnnotation.keywords, false);*/
                    AnalysisTools.collectPragmas(cProc, mapreduceAnnotation.class);
            TranslationUnit trUnt = (TranslationUnit)cProc.getParent();
            if( AnalysisTools.isInHeaderFile(cProc, trUnt) && !regionAnnots.isEmpty() ) {
                Tools.exit("[ERROR in mrCUDASemanticProcessor] "  +
                        "Input file: " + trUnt.getInputFilename() + "Proc Name" + cProc.getName() + "\n");
            }

            handleGPUSemantics(cProc, regionAnnots);
        }

    }

    public void handleGPUSemantics(Procedure proc, List<mapreduceAnnotation> regionAnnots) {
        //Initialize the combner key value specs, both to characters
        c2cu.combKeySpec = Specifier.CHAR;
        c2cu.combValSpec = Specifier.CHAR;
        for( mapreduceAnnotation pAnnot : regionAnnots ) {
            //Process the mapper part
            if(pAnnot.get("mapper")!= null) {
                List<Specifier> specs = null;
                /*
                //Add argc, argv to this process
                Declaration argc_decl = null;
                VariableDeclarator argc_declarator = new VariableDeclarator(new NameID("argc"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                argc_decl = new VariableDeclaration(specs, argc_declarator);
                proc.addDeclaration(argc_decl);

                Declaration argv_decl = null;
                VariableDeclarator argv_declarator = new VariableDeclarator(new NameID("argv"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.CHAR);
                specs.add(PointerSpecifier.UNQUALIFIED);
                specs.add(PointerSpecifier.UNQUALIFIED);
                argv_decl = new VariableDeclaration(specs, argv_declarator);
                proc.addDeclaration(argv_decl);
                */
                Declaration inputPath_decl = null;
                VariableDeclarator inputPath_declarator = new VariableDeclarator(new NameID("inputPath"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.CHAR);
                specs.add(PointerSpecifier.UNQUALIFIED);
                inputPath_decl = new VariableDeclaration(specs, inputPath_declarator);
                proc.addDeclaration(inputPath_decl);

                Declaration numReducers_decl1 = null;
                VariableDeclarator numReducers_declarator1 = new VariableDeclarator(new NameID("numReducers"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                numReducers_decl1 = new VariableDeclaration(specs, numReducers_declarator1);
                proc.addDeclaration(numReducers_decl1);

                Declaration outputPath_decl = null;
                VariableDeclarator outputPath_declarator = new VariableDeclarator(new NameID("outputPath"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.CHAR);
                specs.add(PointerSpecifier.UNQUALIFIED);
                outputPath_decl = new VariableDeclaration(specs, outputPath_declarator);
                proc.addDeclaration(outputPath_decl);

                Declaration fs_decl = null;
                VariableDeclarator fs_declarator = new VariableDeclarator(new NameID("fs"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.VOID);
                specs.add(PointerSpecifier.UNQUALIFIED);
                fs_decl = new VariableDeclaration(specs, fs_declarator);
                proc.addDeclaration(fs_decl);

                //Change the name of the mapper process
                proc.setName(new NameID("mr_main"));

                Annotatable mapperLoop = pAnnot.getAnnotatable();
                if(/*mapperLoop instanceof WhileLoop*/ true) {
                    //WhileLoop loop = (WhileLoop) mapperLoop;
                    c2cu.mainProc = proc;
                    List<FunctionCall> funcList = IRTools.getFunctionCalls(mapperLoop);
                    for (FunctionCall f : funcList) {
                        if(f.getName().toString().equals("getline")) {
                            Declaration ip_decl = null;
                            VariableDeclarator ip_declarator = new VariableDeclarator(new NameID("__ip"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.CHAR);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            ip_decl = new VariableDeclaration(specs, ip_declarator);
                            proc.getBody().addDeclaration(ip_decl);

                            Declaration ipSize_decl = null;
                            VariableDeclarator ipSize_declarator = new VariableDeclarator(new NameID("__ipSize"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            ipSize_decl = new VariableDeclaration(specs, ipSize_declarator);
                            proc.getBody().addDeclaration(ipSize_decl);

                            Declaration mapblocks_decl = null;
                            VariableDeclarator mapblocks_declarator = new VariableDeclarator(new NameID("__mapblocks"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            mapblocks_decl = new VariableDeclaration(specs, mapblocks_declarator);
                            proc.getBody().addDeclaration(mapblocks_decl);

                            Declaration mapthreads_decl = null;
                            VariableDeclarator mapthreads_declarator = new VariableDeclarator(new NameID("__mapthreads"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            mapthreads_decl = new VariableDeclaration(specs, mapthreads_declarator);
                            proc.getBody().addDeclaration(mapthreads_decl);

                            Declaration combineblocks_decl = null;
                            VariableDeclarator combineblocks_declarator = new VariableDeclarator(new NameID("__combineBlocks"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            combineblocks_decl = new VariableDeclaration(specs, combineblocks_declarator);
                            proc.getBody().addDeclaration(combineblocks_decl);

                            Declaration combineblocksize_decl = null;
                            VariableDeclarator combineblocksize_declarator = new VariableDeclarator(new NameID("__combineBlockSize"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            combineblocksize_decl = new VariableDeclaration(specs, combineblocksize_declarator);
                            proc.getBody().addDeclaration(combineblocksize_decl);

                            Declaration totalthreads_decl = null;
                            VariableDeclarator totalthreads_declarator = new VariableDeclarator(new NameID("__totalThreads"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            totalthreads_decl = new VariableDeclaration(specs, totalthreads_declarator);
                            proc.getBody().addDeclaration(totalthreads_decl);

                            Declaration limit_decl = null;
                            VariableDeclarator limit_declarator = new VariableDeclarator(new NameID("__limit"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            limit_decl = new VariableDeclaration(specs, limit_declarator);
                            proc.getBody().addDeclaration(limit_decl);

                            Declaration storesPerThread_decl = null;
                            VariableDeclarator storesPerThread_declarator = new VariableDeclarator(new NameID("__storesPerThread"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            storesPerThread_decl = new VariableDeclaration(specs, storesPerThread_declarator);
                            proc.getBody().addDeclaration(storesPerThread_decl);

                            Declaration kvStoreSize_decl = null;
                            VariableDeclarator kvStoreSize_declarator = new VariableDeclarator(new NameID("__kvStoreSize"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            kvStoreSize_decl = new VariableDeclaration(specs, kvStoreSize_declarator);
                            proc.getBody().addDeclaration(kvStoreSize_decl);

                            //Define arrays for keys and values
                            Declaration devKey = null;
                            VariableDeclarator devKeyDecl = new VariableDeclarator(new NameID("devKey"));
                            specs = new LinkedList<Specifier>();
                            c2cu.keySpec = TransformTools.getType(proc.getBody(), pAnnot.get("key").toString());
                            specs.add(c2cu.keySpec);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            devKey = new VariableDeclaration(specs, devKeyDecl);
                            proc.getBody().addDeclaration(devKey);

                            Declaration key = null;
                            VariableDeclarator keyDecl = new VariableDeclarator(new NameID("key"));
                            key = new VariableDeclaration(specs, keyDecl);
                            proc.getBody().addDeclaration(key);

                            Declaration devVal = null;
                            VariableDeclarator devValDecl = new VariableDeclarator(new NameID("devVal"));
                            specs = new LinkedList<Specifier>();
                            c2cu.valSpec = TransformTools.getType(proc.getBody(), pAnnot.get("value").toString());
                            specs.add(c2cu.valSpec);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            devVal = new VariableDeclaration(specs, devValDecl);
                            proc.getBody().addDeclaration(devVal);

                            Declaration val = null;
                            VariableDeclarator valDecl = new VariableDeclarator(new NameID("val"));
                            val = new VariableDeclaration(specs, valDecl);
                            proc.getBody().addDeclaration(val);

                            Declaration devIp_decl = null;
                            VariableDeclarator devIp_declarator = new VariableDeclarator(new NameID("__devIp"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.CHAR);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            devIp_decl = new VariableDeclaration(specs, devIp_declarator);
                            proc.getBody().addDeclaration(devIp_decl);

                            Declaration devKvCount_decl = null;
                            VariableDeclarator devKvCount_declarator = new VariableDeclarator(new NameID("__devKvCount"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            devKvCount_decl = new VariableDeclaration(specs, devKvCount_declarator);
                            proc.getBody().addDeclaration(devKvCount_decl);

                            Declaration indexArray_decl = null;
                            VariableDeclarator indexArray_declarator = new VariableDeclarator(new NameID("__indexArray"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            indexArray_decl = new VariableDeclaration(specs, indexArray_declarator);
                            proc.getBody().addDeclaration(indexArray_decl);

                            Declaration devIndexArray_decl = null;
                            VariableDeclarator devIndexArray_declarator = new VariableDeclarator(new NameID("__devIndexArray"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            devIndexArray_decl = new VariableDeclaration(specs, devIndexArray_declarator);
                            proc.getBody().addDeclaration(devIndexArray_decl);

                            Declaration newDevIndexArray_decl = null;
                            VariableDeclarator newDevIndexArray_declarator = new VariableDeclarator(new NameID("__newDevIndexArray"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            newDevIndexArray_decl = new VariableDeclaration(specs, newDevIndexArray_declarator);
                            proc.getBody().addDeclaration(newDevIndexArray_decl);

                            Declaration combKeyLength_decl = null;
                            VariableDeclarator combKeyLength_declarator = new VariableDeclarator(new NameID("__combKeyLength"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            combKeyLength_decl = new VariableDeclaration(specs, combKeyLength_declarator);
                            proc.getBody().addDeclaration(combKeyLength_decl);

                            Declaration combValLength_decl = null;
                            VariableDeclarator combValLength_declarator = new VariableDeclarator(new NameID("__combValLength"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            combValLength_decl = new VariableDeclaration(specs, combValLength_declarator);
                            proc.getBody().addDeclaration(combValLength_decl);

                            Declaration finalCount_decl = null;
                            VariableDeclarator finalCount_declarator = new VariableDeclarator(new NameID("__finalCount"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            finalCount_decl = new VariableDeclaration(specs, finalCount_declarator);
                            proc.getBody().addDeclaration(finalCount_decl);

                            Declaration numReducers_decl = null;
                            VariableDeclarator numReducers_declarator = new VariableDeclarator(new NameID("__numReducers"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            numReducers_decl = new VariableDeclaration(specs, numReducers_declarator);
                            proc.getBody().addDeclaration(numReducers_decl);

                            Declaration reducer_decl = null;
                            VariableDeclarator reducer_declarator = new VariableDeclarator(new NameID("__reducer"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            reducer_decl = new VariableDeclaration(specs, reducer_declarator);
                            proc.getBody().addDeclaration(reducer_decl);

                            Declaration timer_decl = null;
                            VariableDeclarator timer_declarator = new VariableDeclarator(new NameID("__timer"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.DOUBLE);
                            timer_decl = new VariableDeclaration(specs, timer_declarator);
                            proc.getBody().addDeclaration(timer_decl);

                            Declaration storesPerBin_decl = null;
                            VariableDeclarator storesPerBin_declarator = new VariableDeclarator(new NameID("__storesPerBin"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            storesPerBin_decl = new VariableDeclaration(specs, storesPerBin_declarator);
                            proc.getBody().addDeclaration(storesPerBin_decl);

                            Declaration recordsPerThread_decl = null;
                            VariableDeclarator recordsPerThread_declarator = new VariableDeclarator(new NameID("__recordsPerThread"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            recordsPerThread_decl = new VariableDeclaration(specs, recordsPerThread_declarator);
                            proc.getBody().addDeclaration(recordsPerThread_decl);

                            Declaration recordLocator_decl = null;
                            VariableDeclarator recordLocator_declarator = new VariableDeclarator(new NameID("__recordLocator"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            recordLocator_decl = new VariableDeclaration(specs, recordLocator_declarator);
                            proc.getBody().addDeclaration(recordLocator_decl);

                            Expression ip = (Expression)ip_decl.getDeclaredIDs().get(0);
                            Expression ipSize = (Expression)ipSize_decl.getDeclaredIDs().get(0);
                            //Expression start = (Expression)start_decl.getDeclaredIDs().get(0);
                            Expression mapBlocks = (Expression)mapblocks_decl.getDeclaredIDs().get(0);
                            Expression mapThreads = (Expression)mapthreads_decl.getDeclaredIDs().get(0);
                            Expression totalThreads = (Expression)totalthreads_decl.getDeclaredIDs().get(0);
                            Expression limit = (Expression)limit_decl.getDeclaredIDs().get(0);
                            Expression storesPerThread = (Expression)storesPerThread_decl.getDeclaredIDs().get(0);
                            Expression kvStoreSize = (Expression)kvStoreSize_decl.getDeclaredIDs().get(0);
                            Expression keys = (Expression)key.getDeclaredIDs().get(0);
                            Expression devKeys = (Expression)devKey.getDeclaredIDs().get(0);
                            Expression vals = (Expression)val.getDeclaredIDs().get(0);
                            Expression devVals = (Expression)devVal.getDeclaredIDs().get(0);
                            Expression devIp = (Expression)devIp_decl.getDeclaredIDs().get(0);
                            Expression devKvCount = (Expression)devKvCount_decl.getDeclaredIDs().get(0);
                            Expression indexArray = (Expression)indexArray_decl.getDeclaredIDs().get(0);
                            Expression devIndexArray = (Expression)devIndexArray_decl.getDeclaredIDs().get(0);
                            Expression newDevIndexArray = (Expression)newDevIndexArray_decl.getDeclaredIDs().get(0);
                            Expression combineBlocks = (Expression)combineblocks_decl.getDeclaredIDs().get(0);
                            Expression combineBlockSize = (Expression)combineblocksize_decl.getDeclaredIDs().get(0);
                            Expression numReducers = (Expression)numReducers_decl.getDeclaredIDs().get(0);
                            Expression numReducers1 = (Expression)numReducers_decl1.getDeclaredIDs().get(0);
                            Expression inputPath = (Expression)inputPath_decl.getDeclaredIDs().get(0);
                            Expression outputPath = (Expression)outputPath_decl.getDeclaredIDs().get(0);
                            Expression fs = (Expression)fs_decl.getDeclaredIDs().get(0);
                            Expression recordsPerThread = (Expression)recordsPerThread_decl.getDeclaredIDs().get(0);
                            Expression recordLocator = (Expression)recordLocator_decl.getDeclaredIDs().get(0);
                            Expression timer = (Expression)timer_decl.getDeclaredIDs().get(0);

                            //Create variables for key and value lengths
                            Declaration keyLength_decl = null;
                            VariableDeclarator keyLength_declarator = new VariableDeclarator(new NameID("__keyLength"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            keyLength_decl = new VariableDeclaration(specs, keyLength_declarator);
                            proc.getBody().addDeclaration(keyLength_decl);

                            Declaration valLength_decl = null;
                            VariableDeclarator valLength_declarator = new VariableDeclarator(new NameID("__valLength"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.INT);
                            valLength_decl = new VariableDeclaration(specs, valLength_declarator);
                            proc.getBody().addDeclaration(valLength_decl);

                            Expression keyLength = (Expression)keyLength_decl.getDeclaredIDs().get(0);
                            Expression valLength = (Expression)valLength_decl.getDeclaredIDs().get(0);

                            //Find out the length of the key, set the variable value
                            String defString;
                            if(pAnnot.get("keylength") != null) {
                                defString = pAnnot.get("keylength").toString();
                            } else {
                                defString = "1";
                            }
                            IntegerLiteral keyL = new IntegerLiteral(defString);

                            AssignmentExpression assignKeyLength = new AssignmentExpression(keyLength.clone(),
                                    AssignmentOperator.NORMAL, keyL);

                            ExpressionStatement stmt1 = new ExpressionStatement(assignKeyLength);
                            proc.getBody().addStatementBefore(IRTools.getFirstNonDeclarationStatement(proc.getBody()),
                                    stmt1);

                            //Find out the length of the value, set the variable value
                            String defString2;
                            if(pAnnot.get("vallength") != null) {
                                defString2 = pAnnot.get("vallength").toString();
                            } else {
                                defString2 = "1";
                            }

                            IntegerLiteral valL = new IntegerLiteral(defString2);

                            AssignmentExpression assignValLength = new AssignmentExpression(valLength.clone(),
                                    AssignmentOperator.NORMAL, valL);

                            ExpressionStatement stmt2 = new ExpressionStatement(assignValLength);
                            proc.getBody().addStatementAfter(stmt1,
                                    stmt2);

                            //Handle file copy
                            FunctionCall getFileSize = new FunctionCall(new NameID("getFileSizeHDFS"));
                            getFileSize.addArgument(inputPath.clone());
                            getFileSize.addArgument(new UnaryExpression(UnaryOperator.ADDRESS_OF, ip.clone()));
                            getFileSize.addArgument(fs.clone());
                            AssignmentExpression call = new AssignmentExpression(ipSize.clone(),
                                    AssignmentOperator.NORMAL, getFileSize);
                            ExpressionStatement getFileSizeCall = new ExpressionStatement(call);

                            proc.getBody().addStatementAfter(stmt2, getFileSizeCall);

                            //set number of reducers call
                            AssignmentExpression reducerSetExp = new AssignmentExpression(numReducers.clone(),
                                    AssignmentOperator.NORMAL, numReducers1.clone());
                            ExpressionStatement reducerSetCall = new ExpressionStatement(reducerSetExp);

                            proc.getBody().addStatementBefore(getFileSizeCall, reducerSetCall);


                            //Function call to set number of threads used in mapper and combiner
                            //etThreads(__mapblocks, __mapthreads, __combineBlocks, __combineBlockSize);
                            FunctionCall setThreads = new FunctionCall(new NameID("setThreads"));
                            setThreads.addArgument(mapBlocks.clone());
                            setThreads.addArgument(mapThreads.clone());
                            setThreads.addArgument(combineBlocks.clone());
                            setThreads.addArgument(combineBlockSize.clone());
                            ExpressionStatement setThreadsCall = new ExpressionStatement(setThreads);
                            proc.getBody().addStatementAfter(getFileSizeCall, setThreadsCall);

                            Statement last = setThreadsCall;
                            if(pAnnot.get("threads") != null) {
                              AssignmentExpression threadAssign = new AssignmentExpression(mapThreads.clone(), AssignmentOperator.NORMAL, (IntegerLiteral)pAnnot.get("threads"));
                              ExpressionStatement threadAssignment = new ExpressionStatement(threadAssign);
                              proc.getBody().addStatementAfter(last, threadAssignment);
                              last = threadAssignment;
                            }
                            if(pAnnot.get("blocks") != null) {
                              AssignmentExpression blockAssign = new AssignmentExpression(mapBlocks.clone(), AssignmentOperator.NORMAL, (IntegerLiteral)pAnnot.get("blocks"));
                              ExpressionStatement blockAssignment = new ExpressionStatement(blockAssign);
                              proc.getBody().addStatementAfter(last, blockAssignment);
                              last = blockAssignment;
                            }

                          //Function call to initialize variables
                            FunctionCall setInternalVariables = new FunctionCall(new NameID("setInternalVariables"));
                            setInternalVariables.addArgument(mapBlocks.clone());
                            setInternalVariables.addArgument(mapThreads.clone());
                            setInternalVariables.addArgument(totalThreads.clone());
                            setInternalVariables.addArgument(ipSize.clone());
                            setInternalVariables.addArgument(limit.clone());
                            setInternalVariables.addArgument(storesPerThread.clone());
                            setInternalVariables.addArgument(kvStoreSize.clone());
                            setInternalVariables.addArgument(numReducers.clone());
                            setInternalVariables.addArgument(ip.clone());

                            if(pAnnot.get("kvpairs") != null) {
                              setInternalVariables.addArgument(((IntegerLiteral)pAnnot.get("kvpairs")).clone());
                            } else {
                              setInternalVariables.addArgument(new IntegerLiteral(-1));
                            }
                            setInternalVariables.addArgument(recordsPerThread.clone());
                            setInternalVariables.addArgument(recordLocator.clone());

                            //AssignmentExpression call2 = new AssignmentExpression(ipSize.clone(),
                                    //AssignmentOperator.NORMAL, setInternalVariables);
                            ExpressionStatement setInternalVariablesCall = new ExpressionStatement(setInternalVariables);

                            proc.getBody().addStatementAfter(last, setInternalVariablesCall);
                            //start timer
                            AssignmentExpression timerStart = new AssignmentExpression(timer.clone(), AssignmentOperator.NORMAL, new FunctionCall(new NameID("timer")));
                            ExpressionStatement setupTimerStartCall = new ExpressionStatement(timerStart);
                            proc.getBody().addStatementAfter(setInternalVariablesCall, setupTimerStartCall);

                            //Allocate memory for arrays for keys and values (runtime function call)
                            FunctionCall allocateMapData = new FunctionCall(new NameID("allocateMapData"));
                            specs = new LinkedList<Specifier>();
                            specs.add(Specifier.VOID);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            specs.add(PointerSpecifier.UNQUALIFIED);
                            allocateMapData.addArgument(new Typecast(specs,new UnaryExpression(UnaryOperator.ADDRESS_OF, devKeys.clone())));
                            allocateMapData.addArgument(new Typecast(specs,new UnaryExpression(UnaryOperator.ADDRESS_OF, devVals.clone())));
                            allocateMapData.addArgument(new Typecast(specs,new UnaryExpression(UnaryOperator.ADDRESS_OF, keys.clone())));
                            allocateMapData.addArgument(new Typecast(specs,new UnaryExpression(UnaryOperator.ADDRESS_OF, vals.clone())));
                            allocateMapData.addArgument(new Typecast(specs,new UnaryExpression(UnaryOperator.ADDRESS_OF, devIp.clone())));
                            allocateMapData.addArgument(new Typecast(specs,new UnaryExpression(UnaryOperator.ADDRESS_OF, devKvCount.clone())));
                            allocateMapData.addArgument(new Typecast(specs,new UnaryExpression(UnaryOperator.ADDRESS_OF, devIndexArray.clone())));
                            allocateMapData.addArgument(new Typecast(specs,new UnaryExpression(UnaryOperator.ADDRESS_OF, newDevIndexArray.clone())));
                            allocateMapData.addArgument(new Typecast(specs,new UnaryExpression(UnaryOperator.ADDRESS_OF, indexArray.clone())));
                            allocateMapData.addArgument(keyLength.clone());
                            allocateMapData.addArgument(valLength.clone());

                            specs = new LinkedList<Specifier>();
                            specs.add(c2cu.keySpec);
                            allocateMapData.addArgument(new SizeofExpression(specs));
                            specs = new LinkedList<Specifier>();
                            specs.add(c2cu.valSpec);
                            allocateMapData.addArgument(new SizeofExpression(specs));
                            allocateMapData.addArgument(kvStoreSize.clone());
                            allocateMapData.addArgument(ipSize.clone());
                            allocateMapData.addArgument(totalThreads.clone());
                            allocateMapData.addArgument(numReducers.clone());

                            ExpressionStatement allocateMapDataCall = new ExpressionStatement(allocateMapData);
                            proc.getBody().addStatementAfter(setupTimerStartCall,allocateMapDataCall);

                            //print setup time
                            BinaryExpression newTime = new BinaryExpression(new FunctionCall(new NameID("timer")),
                                     BinaryOperator.SUBTRACT, timer.clone());
                            FunctionCall setupTimePrint = new FunctionCall(new NameID("printf"));
                            setupTimePrint.addArgument(new NameID("\"Setup time %lf\\n\""));
                            setupTimePrint.addArgument(newTime.clone());
                            ExpressionStatement setupTimePrinter = new ExpressionStatement(setupTimePrint);
                            proc.getBody().addStatementAfter(allocateMapDataCall, setupTimePrinter);

                            //move the while loop into a new CompoundStatement
                            /*CompoundStatement c1 = (CompoundStatement) new CompoundStatement();
                            CompoundStatement c2 = new CompoundStatement();
                            c1.addStatement(c2);
                            proc.getBody().addStatementAfter(allocateMapDataCall, c1);
                            Statement region = (Statement) pAnnot.getAnnotatable();
                            region.swapWith(c2);*/
                            //region.annotate(pAnnot);


                            //Perform Memory Copy for input data
                            FunctionCall memCpy = new FunctionCall(new NameID("cudaMemcpy"));
                            memCpy.addArgument(devIp.clone());
                            memCpy.addArgument(ip.clone());
                            memCpy.addArgument(ipSize.clone());
                            memCpy.addArgument(new NameID("cudaMemcpyHostToDevice"));

                            ExpressionStatement memCpyCall = new ExpressionStatement(memCpy);
                            proc.getBody().addStatementBefore((Statement) mapperLoop.getParent(), memCpyCall);
                            //restart timer for mapper
                            proc.getBody().addStatementBefore(memCpyCall, setupTimerStartCall.clone());

                        }
                    }
                }

            }
            //Process the combiner part
            if(pAnnot.get("combiner") != null) {
                //Outline the combiner process
                TranslationUnit tu = (TranslationUnit)proc.getParent();
                Procedure new_proc = new Procedure(proc.getReturnType(),
                        new ProcedureDeclarator(new NameID("combiner"),
                                new LinkedList()), new CompoundStatement());

                proc.getBody().swapWith(new_proc.getBody());
                tu.addDeclaration(new_proc);

                proc.detach();

                Procedure combinerProc = new_proc;

                CompoundStatement cst = (CompoundStatement)pAnnot.getAnnotatable();
                String defString;
                if(pAnnot.get("keylength") != null) {
                    defString = pAnnot.get("keylength").toString();
                } else {
                    defString = "1";
                }
                IntegerLiteral keyL = new IntegerLiteral(defString);

                String defString2;
                if(pAnnot.get("vallength") != null) {
                    defString2 = pAnnot.get("vallength").toString();
                } else {
                    defString2 = "1";
                }
                IntegerLiteral valL = new IntegerLiteral(defString2);
                List<Specifier> specs = new LinkedList<Specifier>();

                VariableDeclaration combKeyLengthDecl = (VariableDeclaration)c2cu.mainProc.getBody().findSymbol(new NameID("__combKeyLength"));
                VariableDeclaration combValLengthDecl = (VariableDeclaration)c2cu.mainProc.getBody().findSymbol(new NameID("__combValLength"));

                Expression combKeyLength = (Expression)combKeyLengthDecl.getDeclaredIDs().get(0);
                Expression combValLength = (Expression)combValLengthDecl.getDeclaredIDs().get(0);

                AssignmentExpression assignKeyLength = new AssignmentExpression(combKeyLength.clone(),
                        AssignmentOperator.NORMAL, keyL);

                ExpressionStatement stmt1 = new ExpressionStatement(assignKeyLength);
                c2cu.mainProc.getBody().addStatementBefore(IRTools.getFirstNonDeclarationStatement(c2cu.mainProc.getBody()),
                        stmt1);

                //Find out the length of the value, set the variable value
                AssignmentExpression assignValLength = new AssignmentExpression(combValLength.clone(),
                        AssignmentOperator.NORMAL, valL);

                ExpressionStatement stmt2 = new ExpressionStatement(assignValLength);
                c2cu.mainProc.getBody().addStatementAfter(stmt1, stmt2);

                //Find out the key-value specs for the combiner output
                c2cu.combKeySpec = TransformTools.getType(combinerProc.getBody(), pAnnot.get("key").toString());
                c2cu.combValSpec = TransformTools.getType(combinerProc.getBody(), pAnnot.get("value").toString());

                //Start creating new variables in the combiner file
                Declaration devFinalCount_decl = null;
                VariableDeclarator devFinalCount_declarator = new VariableDeclarator(new NameID("__devFinalCount"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                specs.add(PointerSpecifier.UNQUALIFIED);
                devFinalCount_decl = new VariableDeclaration(specs, devFinalCount_declarator);
                combinerProc.getBody().addDeclaration(devFinalCount_decl);

                Declaration devOpVal_decl = null;
                VariableDeclarator devOpVal_declarator = new VariableDeclarator(new NameID("__devOpVal"));
                specs = new LinkedList<Specifier>();
                specs.add(c2cu.combValSpec);
                specs.add(PointerSpecifier.UNQUALIFIED);
                devOpVal_decl = new VariableDeclaration(specs, devOpVal_declarator);
                combinerProc.getBody().addDeclaration(devOpVal_decl);

                Declaration devOpKey_decl = null;
                VariableDeclarator devOpKey_declarator = new VariableDeclarator(new NameID("__devOpKey"));
                specs = new LinkedList<Specifier>();
                specs.add(c2cu.combKeySpec);
                specs.add(PointerSpecifier.UNQUALIFIED);
                devOpKey_decl = new VariableDeclaration(specs, devOpKey_declarator);
                combinerProc.getBody().addDeclaration(devOpKey_decl);

                Declaration combinerThreads_decl = null;
                VariableDeclarator combinerThreads_declarator = new VariableDeclarator(new NameID("__combinerThreads"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                combinerThreads_decl = new VariableDeclaration(specs, combinerThreads_declarator);
                combinerProc.getBody().addDeclaration(combinerThreads_decl);

                //Insert parameters for the function declaration
                Declaration devKey_decl = null;
                VariableDeclarator devKey_declarator = new VariableDeclarator(new NameID("__devKey"));
                specs = new LinkedList<Specifier>();
                specs.add(c2cu.keySpec);
                specs.add(PointerSpecifier.UNQUALIFIED);
                devKey_decl = new VariableDeclaration(specs, devKey_declarator);
                combinerProc.addDeclaration(devKey_decl);

                Declaration devVal_decl = null;
                VariableDeclarator devVal_declarator = new VariableDeclarator(new NameID("__devVal"));
                specs = new LinkedList<Specifier>();
                specs.add(c2cu.valSpec);
                specs.add(PointerSpecifier.UNQUALIFIED);
                devVal_decl = new VariableDeclaration(specs, devVal_declarator);
                combinerProc.addDeclaration(devVal_decl);

                Declaration devIndexArray_decl = null;
                VariableDeclarator devIndexArray_declarator = new VariableDeclarator(new NameID("__devIndexArray"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                specs.add(PointerSpecifier.UNQUALIFIED);
                devIndexArray_decl = new VariableDeclaration(specs, devIndexArray_declarator);
                combinerProc.addDeclaration(devIndexArray_decl);

                Declaration mapKVCount_decl = null;
                VariableDeclarator mapKVCount_declarator = new VariableDeclarator(new NameID("__mapKVCount"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                mapKVCount_decl = new VariableDeclaration(specs, mapKVCount_declarator);
                combinerProc.addDeclaration(mapKVCount_decl);

                Declaration finalCount_decl = null;
                VariableDeclarator finalCount_declarator = new VariableDeclarator(new NameID("__finalCount"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                specs.add(PointerSpecifier.UNQUALIFIED);
                specs.add(PointerSpecifier.UNQUALIFIED);
                finalCount_decl = new VariableDeclaration(specs, finalCount_declarator);
                combinerProc.addDeclaration(finalCount_decl);

                Declaration opVal_decl = null;
                VariableDeclarator opVal_declarator = new VariableDeclarator(new NameID("__opVal"));
                specs = new LinkedList<Specifier>();
                specs.add(c2cu.combValSpec);
                specs.add(PointerSpecifier.UNQUALIFIED);
                specs.add(PointerSpecifier.UNQUALIFIED);
                opVal_decl = new VariableDeclaration(specs, opVal_declarator);
                combinerProc.addDeclaration(opVal_decl);

                Declaration opKey_decl = null;
                VariableDeclarator opKey_declarator = new VariableDeclarator(new NameID("__opKey"));
                specs = new LinkedList<Specifier>();
                specs.add(c2cu.combKeySpec);
                specs.add(PointerSpecifier.UNQUALIFIED);
                specs.add(PointerSpecifier.UNQUALIFIED);
                opKey_decl = new VariableDeclaration(specs, opKey_declarator);
                combinerProc.addDeclaration(opKey_decl);

                Declaration mapKeyLength_decl = null;
                VariableDeclarator mapKeyLength_declarator = new VariableDeclarator(new NameID("__mapKeyLength"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                mapKeyLength_decl = new VariableDeclaration(specs, mapKeyLength_declarator);
                combinerProc.addDeclaration(mapKeyLength_decl);

                Declaration mapValLength_decl = null;
                VariableDeclarator mapValLength_declarator = new VariableDeclarator(new NameID("__mapValLength"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                mapValLength_decl = new VariableDeclaration(specs, mapValLength_declarator);
                combinerProc.addDeclaration(mapValLength_decl);

                Declaration combKeyLength_decl = null;
                VariableDeclarator combKeyLength_declarator = new VariableDeclarator(new NameID("__combKeyLength"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                combKeyLength_decl = new VariableDeclaration(specs, combKeyLength_declarator);
                combinerProc.addDeclaration(combKeyLength_decl);


                Declaration combValLength_decl = null;
                VariableDeclarator combValLength_declarator = new VariableDeclarator(new NameID("__combValLength"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                combValLength_decl = new VariableDeclaration(specs, combValLength_declarator);
                combinerProc.addDeclaration(combValLength_decl);

                Declaration combineBlocks_decl = null;
                VariableDeclarator combineBlocks_declarator = new VariableDeclarator(new NameID("__combineBlocks"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                combineBlocks_decl = new VariableDeclaration(specs, combineBlocks_declarator);
                combinerProc.addDeclaration(combineBlocks_decl);

                Declaration combineBlockSize_decl = null;
                VariableDeclarator combineBlockSize_declarator = new VariableDeclarator(new NameID("__combineBlockSize"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.INT);
                combineBlockSize_decl = new VariableDeclaration(specs, combineBlockSize_declarator);
                combinerProc.addDeclaration(combineBlockSize_decl);

                Expression devFinalCount = (Expression)devFinalCount_decl.getDeclaredIDs().get(0);
                Expression devOpVal = (Expression)devOpVal_decl.getDeclaredIDs().get(0);
                Expression devOpKey = (Expression)devOpKey_decl.getDeclaredIDs().get(0);
                Expression combineBlocks = (Expression)combineBlocks_decl.getDeclaredIDs().get(0);
                Expression combineBlockSize = (Expression)combineBlockSize_decl.getDeclaredIDs().get(0);
                Expression combinerThreads = (Expression)combinerThreads_decl.getDeclaredIDs().get(0);
                Expression devKeyV = (Expression)devKey_decl.getDeclaredIDs().get(0);
                Expression devVal = (Expression)devVal_decl.getDeclaredIDs().get(0);
                Expression devIndexArray = (Expression)devIndexArray_decl.getDeclaredIDs().get(0);
                Expression mapKVCount =  (Expression)mapKVCount_decl.getDeclaredIDs().get(0);
                Expression mapKeyLength =  (Expression)mapKeyLength_decl.getDeclaredIDs().get(0);
                Expression mapValLength =  (Expression)mapValLength_decl.getDeclaredIDs().get(0);
                Expression combKeyLength1 =  (Expression)combKeyLength_decl.getDeclaredIDs().get(0);
                Expression combValLength1 =  (Expression)combValLength_decl.getDeclaredIDs().get(0);
                Expression finalCount =  (Expression)finalCount_decl.getDeclaredIDs().get(0);
                Expression opKey =  (Expression)opKey_decl.getDeclaredIDs().get(0);
                Expression opVal =  (Expression)opVal_decl.getDeclaredIDs().get(0);
                /*
                allocateCombinerData((void **)&devFinalCount, (void **)&devOpKey, (void **)&devOpVal, (void **)finalCount,
                (void **)opKey, (void **)opVal, combineBlockSize, combineBlocks, combinerThreads,  mapKeyLength,
                mapValLength, combKeyLength, combValLength, mapKVCount);
                */

                FunctionCall allocData = new FunctionCall(new NameID("allocateCombinerData"));
                specs = new LinkedList<Specifier>();
                specs.add(Specifier.VOID);
                specs.add(PointerSpecifier.UNQUALIFIED);
                specs.add(PointerSpecifier.UNQUALIFIED);
                allocData.addArgument(new Typecast(specs, new UnaryExpression(UnaryOperator.ADDRESS_OF, devFinalCount.clone())));
                allocData.addArgument(new Typecast(specs, new UnaryExpression(UnaryOperator.ADDRESS_OF, devOpKey.clone())));
                allocData.addArgument(new Typecast(specs, new UnaryExpression(UnaryOperator.ADDRESS_OF, devOpVal.clone())));
                allocData.addArgument(new Typecast(specs, finalCount.clone()));
                allocData.addArgument(new Typecast(specs, opKey.clone()));
                allocData.addArgument(new Typecast(specs, opVal.clone()));
                allocData.addArgument(combineBlockSize.clone());
                allocData.addArgument(combineBlocks.clone());
                allocData.addArgument(combinerThreads.clone());
                allocData.addArgument(mapKeyLength.clone());
                allocData.addArgument(mapValLength.clone());
                allocData.addArgument(combKeyLength.clone());
                allocData.addArgument(combValLength.clone());
                allocData.addArgument(mapKVCount.clone());
                allocData.addArgument(new NameID("_" + c2cu.keySpec + "_"));
                allocData.addArgument(new NameID("_" + c2cu.valSpec + "_"));

                ExpressionStatement allocDataCall = new ExpressionStatement(allocData);
                combinerProc.getBody().addStatementBefore(IRTools.getFirstNonDeclarationStatement(combinerProc.getBody()), allocDataCall);

                //combinerProc.getDeclaration().add
                TranslationUnit tuMain = (TranslationUnit) c2cu.mainProc.getParent();
                specs.clear();
                specs.add(Specifier.EXTERN);
                specs.addAll(combinerProc.getReturnType());
                VariableDeclaration combDecl = new VariableDeclaration(specs, combinerProc.getDeclarator());
                tuMain.addDeclarationBefore(tuMain.getFirstDeclaration(), combDecl);
                /*
                cudaMemcpy(*opKey, devOpKey, mapKVCount*combKeyLength*sizeof(char), cudaMemcpyDeviceToHost);
                cudaMemcpy(*opVal, devOpVal, mapKVCount*combValLength*sizeof(int), cudaMemcpyDeviceToHost);

                cudaMemcpy(*finalCount, devFinalCount, (combinerThreads/32) *sizeof(int), cudaMemcpyDeviceToHost);

                //printf("combiner done\n");
                cudaFree(devFinalCount);
                //cudaFree(devOpStore);
                cudaFree(devOpKey);
                cudaFree(devOpVal);
                */

                //Insert memory copy calls
                CompoundStatement parent = (CompoundStatement) pAnnot.getAnnotatable().getParent();
                BinaryExpression b1 = new BinaryExpression(mapKVCount.clone(), BinaryOperator.MULTIPLY, combKeyLength.clone());
                specs.clear();
                specs.add(c2cu.combKeySpec);
                BinaryExpression b2 = new BinaryExpression(b1, BinaryOperator.MULTIPLY, new SizeofExpression(specs));
                FunctionCall memCpy = new FunctionCall(new NameID("cudaMemcpy"));
                memCpy.addArgument(new UnaryExpression(UnaryOperator.DEREFERENCE, opKey.clone()));
                memCpy.addArgument(devOpKey.clone());
                memCpy.addArgument(b2);
                memCpy.addArgument(new NameID("cudaMemcpyDeviceToHost"));
                ExpressionStatement memcpyCall = new ExpressionStatement(memCpy);
                parent.addStatementAfter((Statement)pAnnot.getAnnotatable(), memcpyCall);

                BinaryExpression b6 = new BinaryExpression(mapKVCount.clone(), BinaryOperator.MULTIPLY, combValLength.clone());
                specs.clear();
                specs.add(c2cu.combValSpec);
                BinaryExpression b3 = new BinaryExpression(b6.clone(), BinaryOperator.MULTIPLY, new SizeofExpression(specs));
                FunctionCall memCpy2 = new FunctionCall(new NameID("cudaMemcpy"));
                memCpy2.addArgument(new UnaryExpression(UnaryOperator.DEREFERENCE, opVal.clone()));
                memCpy2.addArgument(devOpVal.clone());
                memCpy2.addArgument(b3);
                memCpy2.addArgument(new NameID("cudaMemcpyDeviceToHost"));
                ExpressionStatement memcpyCall2 = new ExpressionStatement(memCpy2);
                parent.addStatementAfter(memcpyCall, memcpyCall2);

                specs.clear();
                specs.add(Specifier.INT);
                BinaryExpression b4 = new BinaryExpression(combinerThreads.clone(), BinaryOperator.DIVIDE,
                        new IntegerLiteral(32));
                BinaryExpression b5 = new BinaryExpression(b4 , BinaryOperator.MULTIPLY, new SizeofExpression(specs));
                FunctionCall memCpy3 = new FunctionCall(new NameID("cudaMemcpy"));
                memCpy3.addArgument(new UnaryExpression(UnaryOperator.DEREFERENCE, finalCount.clone()));
                memCpy3.addArgument(devFinalCount.clone());
                memCpy3.addArgument(b5);
                memCpy3.addArgument(new NameID("cudaMemcpyDeviceToHost"));
                ExpressionStatement memcpyCall3 = new ExpressionStatement(memCpy3);
                parent.addStatementAfter(memcpyCall2, memcpyCall3);

                FunctionCall releaseComb = new FunctionCall(new NameID("releaseCombinerData"));
                releaseComb.addArgument(devFinalCount.clone());
                releaseComb.addArgument(devOpKey.clone());
                releaseComb.addArgument(devOpVal.clone());
                ExpressionStatement releaseCombCall = new ExpressionStatement(releaseComb);
                parent.addStatementAfter(memcpyCall3, releaseCombCall);
            }
        }

    }
}
