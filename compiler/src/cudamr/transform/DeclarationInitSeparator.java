package cudamr.transform;

/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/17/13
 * Time: 11:06 AM
 * To change this template use File | Settings | File Templates.
 */

import cetus.transforms.TransformPass;
import cetus.hir.*;
import java.util.*;

/**
 * This pass assumes that SingleDeclarator pass is executed before this.
 *
 * @author Amit Sabne <asabne@purdue.edu>
 *         Purdue University
 *
 */
public class DeclarationInitSeparator extends TransformPass {

    private static String pass_name = "[DeclarationInitSeparator]";
    private static Statement insertedInitStmt = null;

    public DeclarationInitSeparator(Program program) {
        super(program);
    }

    /* (non-Javadoc)
     * @see cetus.transforms.TransformPass#getPassName()
     */
    @Override
    public String getPassName() {
        return pass_name;
    }

    private void separateDeclInitialization(VariableDeclaration decl) {
        //This pass assumes each declaration contains only one declarator, which can be enforced by SingleDeclarator pass.
        if ( decl.getNumDeclarators() != 1 ) {
            return;
        }
        Declarator declr = decl.getDeclarator(0);
        if( !(declr instanceof VariableDeclarator) ) {
            return;
        }
        VariableDeclarator lsm_declarator = (VariableDeclarator)declr;
        Initializer lsm_init = lsm_declarator.getInitializer();
        if( lsm_init == null ) {
            return;
        }
        PrintTools.printlnStatus(4, pass_name, "separating the initialization of declaration, ",decl);
        CompoundStatement cStmt = null;
        Statement declStmt = null;
        Traversable parent = decl.getParent();
        if (parent instanceof SymbolTable) {
            //This declaration is for a global variable; the initialization of a global variable is not separated.
            return;
        } else if (parent instanceof DeclarationStatement) {
            declStmt = (Statement)parent;
            cStmt = (CompoundStatement)parent.getParent();
        } else {
            return;
        }
        /* now parent is a symbol table and child is either decl or declstmt. */
        List<Specifier> lspecs = new ChainedList<Specifier>();
        lspecs.addAll(decl.getSpecifiers());
        List<Specifier> declrSpecs = new ChainedList<Specifier>();
        declrSpecs.addAll(lsm_declarator.getSpecifiers());
        if( declrSpecs.contains(PointerSpecifier.CONST) || declrSpecs.contains(PointerSpecifier.CONST_RESTRICT) ||
                declrSpecs.contains(PointerSpecifier.CONST_RESTRICT_VOLATILE) || declrSpecs.contains(PointerSpecifier.CONST_VOLATILE) ||
                (lspecs.contains(Specifier.CONST)&&!SymbolTools.isPointer(lsm_declarator)) || (lspecs.contains(Specifier.STATIC)) ) {
            //Initialization of constant/static variable/pointer should not be separated.
            return;
        } else {
            int listSize = lsm_init.getChildren().size();
            if( listSize == 1 ) {
                Expression initValue = (Expression)lsm_init.getChildren().get(0);
                if( initValue instanceof Literal ) {
                    //We don't separate initialization if its value is constant.
                    return;
                } else {
                    lsm_declarator.setInitializer(null);
                    initValue.setParent(null);
                    AssignmentExpression lAssignExp = new AssignmentExpression(new Identifier(lsm_declarator),AssignmentOperator.NORMAL,
                            initValue);
                    Statement lAssignStmt = new ExpressionStatement(lAssignExp);
                    cStmt.addStatementAfter(declStmt, lAssignStmt);
					/*
					Statement fStmt = IRTools.getFirstNonDeclarationStatement(cStmt);
					if( fStmt == null ) {
						cStmt.addStatement(lAssignStmt);
					} else {
						if( insertedInitStmt == null ) {
							cStmt.addStatementBefore(fStmt, lAssignStmt);
						} else if(insertedInitStmt.getParent().equals(cStmt)) {
							cStmt.addStatementAfter(insertedInitStmt, lAssignStmt);
						} else {
							cStmt.addStatementBefore(fStmt, lAssignStmt);
						}
					}
					insertedInitStmt = lAssignStmt;
					*/
                }
            } else {
                //DEBUG: we don't know how to handle this case yet.
                return;
            }
        }
    }

    /* (non-Javadoc)
     * @see cetus.transforms.TransformPass#start()
     */
    @Override
    public void start() {
        DFIterator<Declaration> iter =
                new DFIterator<Declaration>(program, Declaration.class);
        while (iter.hasNext()) {
            Declaration d = iter.next();
            if (d instanceof Procedure) {
                PrintTools.printlnStatus(2, pass_name, "examining procedure",
                        "\"", ((Procedure)d).getName(), "\"");
            } else if (d instanceof VariableDeclaration) {
                separateDeclInitialization((VariableDeclaration)d);
            }
        }
    }

}
