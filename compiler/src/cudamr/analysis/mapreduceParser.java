/**
 *
 */
package cudamr.analysis;

import java.io.*;
import java.util.*;
import java.lang.String;
import cetus.hir.*;

/**
 * MapReduce annotation parser
 * CAUTION: current version stores start/length expressions of subarrays in data clauses as SomeExpression, which
 * is not analyzable by internal analyses passes. For mapreduceurate analysis, these should be parsed further by using
 * extra expression parser.
 *
 * @author Amit Sabne <asabne@purdue.edu>
 *         Purdue University
 *
 */
public class mapreduceParser {

    private static String [] token_array;
    private static int token_index;
    private	static HashMap mapreduce_map; // Used (key, value) mapping types:
    private int debug_level;
    private static final int infiniteLoopCheckTh = 1024; //used to detect possible infinite loop due to incorrect parsing.

    private static HashMap<String, String> macroMap = null;

    public static class ExpressionParser {
        static private String expr;
        static private int expr_pos;
        static private char expr_c;
        static private String token;

        public ExpressionParser()
        {
            expr = "";
            expr_pos = -1;
            expr_c = '\0';
        }

        /**
         * checks if the given char c is a letter or undersquare
         */
        static boolean isAlpha(final char c)
        {
            char cUpper = Character.toUpperCase(c);
            return "ABCDEFGHIJKLMNOPQRSTUVWXYZ_".indexOf(cUpper) != -1;
        }
        /**
         * checks if the given char c is a digit or dot
         */
        static boolean isDigitDot(final char c)
        {
            return "0123456789.".indexOf(c) != -1;
        }

        /**
         * checks if the given char c is a digit
         */
        static boolean isDigit(final char c)
        {
            return "0123456789".indexOf(c) != -1;
        }

        /**
         * Get the next character from the expression.
         * The character is stored into the char expr_c.
         * If the end of the expression is reached, the function puts zero ('\0')
         * in expr_c.
         */
        static private void getChar()
        {
            expr_pos++;
            if (expr_pos < expr.length())
            {
                expr_c = expr.charAt(expr_pos);
            }
            else
            {
                expr_c = '\0';
            }
        }

        /**
         * Current parser simply checks whether input string <b>new_expr</b> is integer literal,
         * a variable name, or else.
         *
         * @param new_expr input string to be parsed
         * @return Expression which is either IntegerLiteral, NameID, or SomeExpression type.
         */
        public static Expression parse (final String new_expr)
        {
            Expression parsedExpr = null;
            boolean containDigits = false;
            boolean containLetters = false;
            boolean containNonLetters = false;
            if( (new_expr == null) || (new_expr.length() == 0) ) {
                return parsedExpr;
            }
            expr = new_expr;
            expr_pos = -1;
            expr_c = '\0';
            getChar();
            while( expr_c != '\0' ) {
                if( isDigit(expr_c) ) {
                    containDigits = true;
                } else if( isAlpha(expr_c) ) {
                    containLetters = true;
                } else {
                    containNonLetters = true;
                }
                getChar();
            }
            if( containDigits && !containLetters && !containNonLetters ) {
                //this string is IntegerLiteral
                parsedExpr = new IntegerLiteral(expr);
            } else if( !containNonLetters ) {
                //this string is a simple variable
                parsedExpr = new NameID(expr);
            } else {
                //general expression
                List<Traversable> children = new ArrayList<Traversable>();
                //DEBUG: below does not print parenthesis for SomeExpression.
                //parsedExpr = new SomeExpression(expr, children);
                //parsedExpr.setParens(true);
                parsedExpr = new SomeExpression("("+expr+")", children);

            }
            return parsedExpr;
        }
    }

    public mapreduceParser() {
    }

    private static String get_token()
    {
        if( end_of_token() ) {
            MapReduceParserError("parsing ended unexpectedly.");
            return null;
        } else {
            String tok = token_array[token_index++];
            if( macroMap.containsKey(tok) ) {
                String macroV = macroMap.get(tok);
                String[] new_token_array = macroV.split("\\s+");
                tok = new_token_array[0];
                if( new_token_array.length > 1 ) {
                    String[] result = Arrays.copyOf(token_array, token_array.length + new_token_array.length - 1);
                    System.arraycopy(new_token_array, 0, result, token_index-1, new_token_array.length);
                    System.arraycopy(token_array, token_index, result, token_index-1+new_token_array.length, token_array.length-token_index);
                    token_array = result;
                }
                return tok;
            } else {
                return tok;
            }
        }
    }

    // get one token but do not consume the token
    private static String lookahead()
    {
        if( end_of_token() ) {
            return "";
        } else {
            String tok = token_array[token_index];
            if( macroMap.containsKey(tok) ) {
                String macroV = macroMap.get(tok);
                String[] new_token_array = macroV.split("\\s+");
                tok = new_token_array[0];
                if( new_token_array.length > 1 ) {
                    String[] result = Arrays.copyOf(token_array, token_array.length + new_token_array.length - 1);
                    System.arraycopy(new_token_array, 0, result, token_index, new_token_array.length);
                    System.arraycopy(token_array, token_index+1, result, token_index+new_token_array.length, token_array.length-token_index);
                    token_array = result;
                }
                return tok;
            } else {
                return tok;
            }
        }
    }

    // consume one token
    private static void eat()
    {
        token_index++;
    }

    // match a token with the given string
    private static boolean match(String istr)
    {
        boolean answer = check(istr);
        if (answer == false) {
            System.out.println("MapReduceParser Syntax Error: " + istr + " is expected");
            System.out.println(display_tokens());
/*			System.out.println("\nmacroMap\n");
			for( String key : macroMap.keySet() ) {
				System.out.println("key: " + key + ", value: " + macroMap.get(key));
			}*/
            System.exit(0);
        }
        token_index++;
        return answer;
    }

    // match a token with the given string, but do not consume a token
    private static boolean check(String istr)
    {
        if ( end_of_token() )
            return false;
        String tok = token_array[token_index];
        if( macroMap.containsKey(tok) ) {
            String macroV = macroMap.get(tok);
            String[] new_token_array = macroV.split("\\s+");
            tok = new_token_array[0];
            if( new_token_array.length > 1 ) {
                String[] result = Arrays.copyOf(token_array, token_array.length + new_token_array.length - 1);
                System.arraycopy(new_token_array, 0, result, token_index, new_token_array.length);
                System.arraycopy(token_array, token_index+1, result, token_index+new_token_array.length, token_array.length-token_index-1);
                token_array = result;
            }
        }
        return ( tok.compareTo(istr) == 0 ) ? true : false;
    }

    private static String display_tokens()
    {
        StringBuilder str = new StringBuilder(160);

        for (int i=0; i<token_array.length; i++)
        {
            str.append("token_array[" + i + "] = " + token_array[i] + "\n");
        }
        return str.toString();
    }

    private static boolean end_of_token()
    {
        return (token_index >= token_array.length) ? true : false;
    }

    public static void preprocess_mapreduce_pragma( String[] str_array, HashMap<String, String> macro_map) {
        token_array = str_array;
        token_index = 2; // "mapreduce" and "#" have already been matched
        macroMap = macro_map;
        String tok = get_token();
        String construct = "mapreduce_" + tok;
        try {
            switch (mapreduce_preprocessor.valueOf(construct)) {
                case mapreduce_define 		: parse_define_command(); break;
                default : MapReduceParserError("Not Supported Construct");
            }
        } catch( Exception e) {
            MapReduceParserError("Not Supported Construct" + tok);
        }
    }

    /*
     * For now, only simple macro variable is allowed, but not function-style one.
     *
     */
    private static void parse_define_command() {
        PrintTools.println("MapReduceParser is parsing [define] directive", 3);
        //String name = get_token(); //This will cause errors if the same macro is defined more than once.
        String name = token_array[token_index++];
        String value = "";
        if( !end_of_token() ) {
            value = get_token();
            while( !end_of_token() ) {
                value = value + " " + get_token();
            }
        }
        PrintTools.println("Macro name: " + name + ", value: " + value, 3);
        macroMap.put(name, value);
        macroMap.put("\\"+name, value);
        macroMap.put("\\{"+name+"}", value);

    }


    /**
     * Parse mapreduce pragmas, which are stored as raw text after Cetus parsing.
     *
     * @param input_map HashMap that will contain the parsed output pragmas
     * @param str_array input pragma string that will be parsed.
     * @return true if the pragma will be attached to the following non-pragma
     *         statement. Or, it returns false, if the pragma is stand-alone.
     */
    public static boolean parse_mapreduce_pragma(HashMap input_map, String [] str_array, HashMap<String, String>macro_map)
    {
        mapreduce_map = input_map;
        token_array = str_array;
        token_index = 1; // "mapreduce" has already been matched
        //token_index = 2; // If there is a leading space, use this one.
        macroMap = macro_map;

        PrintTools.println(display_tokens(), 9);

        String token = get_token();
        String construct = "mapreduce_" + token;
        try {
            switch (mapreduce_directives.valueOf(construct)) {
                case mapreduce_mapper 		: parse_mapreduce_mapper(); return true;
                case mapreduce_combiner		: parse_mapreduce_combiner(); return true;
                //		default : throw new NonOmpDirectiveException();
                default : MapReduceParserError("Not Supported Construct");
            }
        } catch( Exception e) {
            MapReduceParserError("unexpected or wrong token found (" + token + ")");
        }
        return true;		// meaningless return because it is unreachable
    }



    /** ---------------------------------------------------------------
     *		MapReduce mapper Construct
     *
     *		#pragma mapreduce mapper [clause[[,] clause]...] new-line
     *			structured-block
     *
---------------------------------- */
    private static void parse_mapreduce_mapper()
    {
        addToMap("mapper", "true");

        PrintTools.println("MapReduceParser is parsing [mapper] directive", 3);
        while (end_of_token() == false)
        {
            String tok = get_token();
            if( tok.equals("") ) continue; //Skip empty string, which may occur due to macro.
            if( tok.equals(",") ) continue; //Skip comma between clauses, if existing.
            String clause = "mapreduce_" + tok;
            PrintTools.println("clause=" + clause, 3);
            try {
                switch (mapreduce_clause.valueOf(clause)) {
                    case mapreduce_key:		parse_mapreduce_keyclause(tok); break;
                    case mapreduce_keylength:   parse_mapreduce_keylengthclause(tok); break;
                    case mapreduce_vallength:   parse_mapreduce_vallengthclause(tok); break;
                    case mapreduce_value:		parse_mapreduce_valueclause(tok); break;
                    case mapreduce_sharedRO 	:	parse_mapreduce_sharedROclause(tok); break;
                    case mapreduce_texture 	:	parse_mapreduce_textureclause(tok); break;
                    case mapreduce_kvpairs:   parse_mapreduce_kvpairsclause(tok); break;
                    case mapreduce_blocks: parse_mapreduce_blocksclause(tok); break;
                    case mapreduce_threads: parse_mapreduce_threadsclause(tok); break;
                    default : MapReduceParserError("NoSuchMapReduceConstruct : " + clause);
                }
            } catch( Exception e) {
                MapReduceParserError("unexpected or wrong token found on mapper (" + tok + ")");
            }
        }

    }

    private static void parse_mapreduce_combiner()
    {
        addToMap("combiner", "true");

        PrintTools.println("MapReduceParser is parsing [mapper] directive", 3);
        while (end_of_token() == false)
        {
            String tok = get_token();
            if( tok.equals("") ) continue; //Skip empty string, which may occur due to macro.
            if( tok.equals(",") ) continue; //Skip comma between clauses, if existing.
            String clause = "mapreduce_" + tok;
            PrintTools.println("clause=" + clause, 3);
            try {
                switch (mapreduce_clause.valueOf(clause)) {
                    case mapreduce_key:		parse_mapreduce_keyclause(tok); break;
                    case mapreduce_keylength:   parse_mapreduce_keylengthclause(tok); break;
                    case mapreduce_vallength:   parse_mapreduce_vallengthclause(tok); break;
                    case mapreduce_value:		parse_mapreduce_valueclause(tok); break;
                    case mapreduce_keyin		:	parse_mapreduce_keyinclause(tok); break;
                    case mapreduce_valuein 	:	parse_mapreduce_valueinclause(tok); break;
                    case mapreduce_sharedRO 	:	parse_mapreduce_sharedROclause(tok); break;
                    case mapreduce_texture 	:	parse_mapreduce_textureclause(tok); break;
                    default : MapReduceParserError("NoSuchMapReduceConstruct : " + clause);
                }
            } catch( Exception e) {
                MapReduceParserError("unexpected or wrong token found (" + tok + ")");
            }
        }

    }






    /**
     * This method parses input string into an Expression using ExpressionParser.parse() method.
     * This does not consume closingTok.
     *
     * @param closingTok
     * @param openingTok
     * @param initMatchCNT
     * @return
     */
    private static Expression parse_expression(String openingTok, String closingTok, int initMatchCNT)
    {
        StringBuilder sb = new StringBuilder(32);
        int matchingCNT = initMatchCNT;
        int counter = 0;
        String tok = lookahead();
        if( tok.equals(closingTok) ) {
            //No valid token to parse
            return null;
        } else if( tok.equals(openingTok) ) {
            matchingCNT++;
        }
//		try {
        for (;;) {
            sb.append(get_token());
            tok = lookahead();
            if ( tok.equals("") ) {
                break; //end of token
            } else if ( tok.equals(closingTok) ) {
                --matchingCNT;
                if( matchingCNT < 0 ) {
                    break;
                } else if( (initMatchCNT > 0) && (matchingCNT == 0) ) {
                    break;
                }
            } else if ( tok.equals(openingTok) ) {
                matchingCNT++;
            } else if ( counter > infiniteLoopCheckTh ) {
                MapReduceParserError("Can't find stopping token ("+closingTok+")");
                System.exit(0);
            }
            counter++;
        }
/*		} catch( Exception e) {
			MapReduceParserError("unexpected error in parsing expression (Error: " + e + ")");
		}*/
        return ExpressionParser.parse(sb.toString());
    }

    /**
     * This method parses input string into an Expression using ExpressionParser.parse() method.
     * This method assumes that stopTok is different from closingTok.
     * This does not consume stopTok or closingTok.
     *
     * @param openingTok
     * @param closingTok
     * @param stopTok
     * @param initMatchCNT
     * @return
     */
    private static Expression parse_expression(String openingTok, String closingTok, String stopTok, int initMatchCNT)
    {
        //System.err.println("Enter parse_expression()");
        StringBuilder sb = new StringBuilder(32);
        int matchingCNT = initMatchCNT;
        int counter = 0;
        String tok = lookahead();
        if( tok.equals(closingTok) || tok.equals(stopTok) ) {
            //No valid token to parse
            return null;
        } else if( tok.equals(openingTok) ) {
            matchingCNT++;
        }
//		try {
        for (;;) {
            sb.append(get_token());
            tok = lookahead();
            if ( tok.equals("") ) {
                break; //end of token
            } else if ( tok.equals(closingTok) ) {
                --matchingCNT;
                if( matchingCNT < 0 ) {
                    break;
                } else if( (initMatchCNT > 0) && (matchingCNT == 0) ) {
                    break;
                }
            } else if ( tok.equals(openingTok) ) {
                matchingCNT++;
            } else if ( tok.equals(stopTok) ) {
                break;
            } else if ( counter > infiniteLoopCheckTh ) {
                MapReduceParserError("Can't find stopping token ("+stopTok+")");
                System.exit(0);
            }
            counter++;
        }
/*		} catch( Exception e) {
			MapReduceParserError("unexpected error in parsing expression (Error: " + e + ")");
		}*/
        return ExpressionParser.parse(sb.toString());
    }

    /**
     * This method parses input string into an Expression using ExpressionParser.parse() method.
     * This method assumes that stopTok is different from closingTok.
     * This does not consume stopTok or closingTok.
     *
     * @param openingTok
     * @param closingTok
     * @param stopTokSet
     * @param initMatchCNT
     * @return
     */
    private static Expression parse_expression(String openingTok, String closingTok, Set<String> stopTokSet, int initMatchCNT)
    {
        StringBuilder sb = new StringBuilder(32);
        int matchingCNT = initMatchCNT;
        int counter = 0;
        String tok = lookahead();
        if( tok.equals(closingTok) ) {
            //No valid token to parse
            return null;
        } else if( tok.equals(openingTok) ) {
            matchingCNT++;
        } else {
            for( String stopTok : stopTokSet ) {
                if( tok.equals(stopTok) ) {
                    return null;
                }
            }
        }
//		try {
        for (;;) {
            sb.append(get_token());
            tok = lookahead();
            if ( tok.equals("") ) {
                break; //end of token
            } else if ( tok.equals(closingTok) ) {
                --matchingCNT;
                if( matchingCNT < 0 ) {
                    break;
                } else if( (initMatchCNT > 0) && (matchingCNT == 0) ) {
                    break;
                }
            } else if ( tok.equals(openingTok) ) {
                matchingCNT++;
            } else if ( counter > infiniteLoopCheckTh ) {
                MapReduceParserError("Can't find stopping token: " + stopTokSet);
                System.exit(0);
            } else {
                boolean foundStopTok = false;
                for( String stopTok : stopTokSet ) {
                    if( tok.equals(stopTok) ) {
                        foundStopTok = true;
                        break;
                    }
                }
                if( foundStopTok ) {
                    break;
                }

            }
            counter++;
        }
/*		} catch( Exception e) {
			MapReduceParserError("unexpected error in parsing expression (Error: " + e + ")");
		}*/
        return ExpressionParser.parse(sb.toString());
    }

    private static void parse_mapreduce_noargclause(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        addToMap(clause, "true");
    }

    private static void parse_mapreduce_optionalconfclause(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        if( check("(") ) {
            match("(");
            Expression exp = parse_expression("(", ")", 1);
            match(")");
            if( exp == null ) {
                //addToMap(clause, "true");
                MapReduceParserError("No valid argument is found for the clause, " + clause);
            } else {
                addToMap(clause, exp);
            }
        } else {
            addToMap(clause, "true");
        }
    }

    private static void parse_mapreduce_keyclause(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("(");
        Expression exp = parse_expression("(", ")", 1);
        match(")");
        if( exp == null ) {
            MapReduceParserError("No valid argument is found for the clause, " + clause);
        } else {
            addToMap(clause, exp);
        }
    }

    private static void parse_mapreduce_keyinclause(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("(");
        Expression exp = parse_expression("(", ")", 1);
        match(")");
        if( exp == null ) {
            MapReduceParserError("No valid argument is found for the clause, " + clause);
        } else {
            addToMap(clause, exp);
        }
    }

    private static void parse_mapreduce_keylengthclause(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("(");
        Expression exp = parse_expression("(", ")", 1);
        match(")");
        if( exp == null ) {
            MapReduceParserError("No valid argument is found for the clause, " + clause);
        } else {
            addToMap(clause, exp);
        }
    }


    private static void parse_mapreduce_kvpairsclause(String clause)
    {
      PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
      match("(");
      Expression exp = parse_expression("(", ")", 1);
      match(")");
      if( exp == null ) {
        MapReduceParserError("No valid argument is found for the clause, " + clause);
      } else {
        addToMap(clause, exp);
      }
    }

    private static void parse_mapreduce_blocksclause(String clause)
    {
      PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
      match("(");
      Expression exp = parse_expression("(", ")", 1);
      match(")");
      if( exp == null ) {
        MapReduceParserError("No valid argument is found for the clause, " + clause);
      } else {
        addToMap(clause, exp);
      }
    }

    private static void parse_mapreduce_threadsclause(String clause)
    {
      PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
      match("(");
      Expression exp = parse_expression("(", ")", 1);
      match(")");
      if( exp == null ) {
        MapReduceParserError("No valid argument is found for the clause, " + clause);
      } else {
        addToMap(clause, exp);
      }
    }

    private static void  parse_mapreduce_vallengthclause(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("(");
        Expression exp = parse_expression("(", ")", 1);
        match(")");
        if( exp == null ) {
            MapReduceParserError("No valid argument is found for the clause, " + clause);
        } else {
            addToMap(clause, exp);
        }
    }

    private static void parse_mapreduce_valueclause(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("(");
        Expression exp = parse_expression("(", ")", 1);
        match(")");
        if( exp == null ) {
            MapReduceParserError("No valid argument is found for the clause, " + clause);
        } else {
            addToMap(clause, exp);
        }
    }

    private static void parse_mapreduce_valueinclause(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("(");
        Expression exp = parse_expression("(", ")", 1);
        match(")");
        if( exp == null ) {
            MapReduceParserError("No valid argument is found for the clause, " + clause);
        } else {
            addToMap(clause, exp);
        }
    }


    private static void parse_mapreduce_sharedROclause(String clause)
    {
      PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
      match("(");
      Set<Expression> set = new HashSet<Expression>();
      parse_commaSeparatedExpressionList(set);
      match(")");
      addToMap(clause, set);
    }

    private static void parse_mapreduce_textureclause(String clause)
    {
      PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
      match("(");
      Set<Expression> set = new HashSet<Expression>();
      parse_commaSeparatedExpressionList(set);
      match(")");
      addToMap(clause, set);
    }

    private static void parse_mapreduce_confclause(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("(");
        Expression exp = parse_expression("(", ")", 1);
        match(")");
        if( exp == null ) {
            MapReduceParserError("No valid argument is found for the clause, " + clause);
        } else {
            addToMap(clause, exp);
        }
    }



    private static void parse_mapreduce_stringargclause(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+ clause + "] clause", 3);
        match("(");
        String str = get_token();
        match(")");
        addToMap(clause, str);
    }

    private static void parse_conf_stringset(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("(");
        Set<String> set = new HashSet<String>();
        parse_commaSeparatedStringList(set);
        match(")");
        addToMap(clause, set);
    }

    private static void parse_conf_expressionset(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("(");
        Set<Expression> set = new HashSet<Expression>();
        parse_commaSeparatedExpressionList(set);
        match(")");
        addToMap(clause, set);
    }


    private static void parse_conf_expression(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("=");
        Expression exp = ExpressionParser.parse(get_token());
        if( exp == null ) {
            MapReduceParserError("No valid argument is found for the clause, " + clause);
        } else {
            addToMap(clause, exp);
        }
    }

    private static void parse_expressionlist(String clause)
    {
        PrintTools.println("MapReduceParser is parsing ["+clause+"] clause", 3);
        match("(");
        List<Expression> list = new LinkedList<Expression>();
        parse_commaSeparatedExpressionList(list);
        match(")");
        addToMap(clause, list);
    }

    /**
     *	This function reads a list of comma-separated string variables
     * It checks the right parenthesis to end the parsing, but does not consume it.
     */
    private static void parse_commaSeparatedStringList(Set<String> set)
    {
        for (;;) {
            set.add(get_token());
            if ( check(")") )
            {
                break;
            }
            else if ( match(",") == false )
            {
                MapReduceParserError("comma expected in comma separated list");
            }
        }
    }

    /**
     *	This function reads a list of comma-separated variables
     * It checks the right parenthesis to end the parsing, but does not consume it.
     */
    private static void parse_commaSeparatedExpressionList(Set<Expression> set)
    {
        int initMatchCNT = 1;
        boolean firstItr = true;
//		try {
        for (;;) {
            String tok = lookahead();
            if( tok.equals(")") || tok.equals(",") ) {
                MapReduceParserError("valid list is missing in comma separated list");
            }
            //Expression exp = ExpressionParser.parse(tok);
            Expression exp = parse_expression("(", ")", ",", initMatchCNT);
            set.add(exp);
            if ( check(")") )
            {
                break;
            }
            else if ( match(",") == false )
            {
                MapReduceParserError("comma expected in comma separated list");
            }
            if( firstItr ) {
                initMatchCNT = 0;
                firstItr = false;
            }
        }
/*		} catch( Exception e) {
			MapReduceParserError("unexpected error in parsing comma-separated Expression list");
		}*/
    }

    /**
     *	This function reads a list of comma-separated variables
     * It checks the right parenthesis to end the parsing, but does not consume it.
     */
    private static void parse_commaSeparatedExpressionList(List<Expression> list)
    {
        int initMatchCNT = 1;
        boolean firstItr = true;
//		try {
        for (;;) {
            String tok = lookahead();
            if( tok.equals(")") || tok.equals(",") ) {
                MapReduceParserError("valid list is missing in comma separated list");
            }
            //Expression exp = ExpressionParser.parse(tok);
            Expression exp = parse_expression("(", ")", ",", initMatchCNT);
            list.add(exp);
            if ( check(")") )
            {
                break;
            }
            else if ( match(",") == false )
            {
                MapReduceParserError("comma expected in comma separated list");
            }
            if( firstItr ) {
                initMatchCNT = 0;
                firstItr = false;
            }
        }
/*		} catch( Exception e) {
			MapReduceParserError("unexpected error in parsing comma-separated Expression list");
		}*/
    }

    private static void notSupportedWarning(String text)
    {
        System.out.println("Not Supported MapReduce Annotation: " + text);
    }

    private static void MapReduceParserError(String text)
    {
        System.out.println("Syntax Error in MapReduce Directive Parsing: " + text);
        System.out.println(display_tokens());
        System.out.println("Exit the MapReduce translator!!");
        System.exit(0);
    }

    private static void addToMap(String key, String new_str)
    {
        if (mapreduce_map.keySet().contains(key))
            Tools.exit("[ERROR] MapReduce Parser detected duplicate pragma: " + key);
        else
            mapreduce_map.put(key, new_str);
    }

    private static void addToMap(String key, Expression expr)
    {
        if (mapreduce_map.keySet().contains(key))
            Tools.exit("[ERROR] MapReduce Parser detected duplicate pragma: " + key);
        else
            mapreduce_map.put(key, expr);
    }


    // When a key already exists in the map
    // clauses may be repeated as needed.
    private static void addToMap(String key, Set new_set)
    {
        if (mapreduce_map.keySet().contains(key))
        {
            Set set = (Set)mapreduce_map.get(key);
            set.addAll(new_set);
        }
        else
        {
            mapreduce_map.put(key, new_set);
        }
    }

    // When a key already exists in the map
    // clauses may be repeated as needed.
    private static void addToMap(String key, List new_list)
    {
        if (mapreduce_map.keySet().contains(key))
        {
            List list = (List)mapreduce_map.get(key);
            list.addAll(new_list);
        }
        else
        {
            mapreduce_map.put(key, new_list);
        }
    }

    private static void addToMap(String key, Map new_map)
    {
        if (mapreduce_map.keySet().contains(key))
        {
            Map orig_map = (Map)mapreduce_map.get(key);
            for ( String new_str : (Set<String>)new_map.keySet() )
            {
                Set new_set = (Set)new_map.get(new_str);
                if (orig_map.keySet().contains(new_str))
                {
                    Set orig_set = (Set)orig_map.get(new_str);
                    orig_set.addAll(new_set);
                }
                else
                {
                    orig_map.put(new_str, new_set);
                }
            }
        }
        else
        {
            mapreduce_map.put(key, new_map);
        }
    }

    public static enum mapreduce_preprocessor
    {
        mapreduce_define
    }

    public static enum mapreduce_directives
    {
        mapreduce_mapper,
        mapreduce_combiner,
        mapreduce_loop,
        mapreduce_data,
        mapreduce_host_data,
        mapreduce_declare,
        mapreduce_update,
        mapreduce_ainfo,
        mapreduce_cuda,
        mapreduce_cache,
        mapreduce_wait,
        mapreduce_routine,
        mapreduce_resilience,
        mapreduce_ftregion,
        mapreduce_ftinject,
        mapreduce_barrier,
        mapreduce_profile,
        mapreduce_enter,
        mapreduce_exit
    }

    public static enum mapreduce_clause
    {
        mapreduce_key,
        mapreduce_value,
        mapreduce_keylength,
        mapreduce_vallength,
        mapreduce_keyin,
        mapreduce_valuein,
        mapreduce_sharedRO,
        mapreduce_blocks,
        mapreduce_threads,
        mapreduce_texture,
        mapreduce_kvpairs,
    }

    public static enum cuda_clause
    {
        token_registerRO,
        token_registerRW,
        token_noregister,
        token_sharedRO,
        token_sharedRW,
        token_noshared,
        token_texture,
        token_notexture,
        token_constant,
        token_noconstant,
        token_global,
        token_noreductionunroll,
        token_procname,
        token_kernelid,
        token_noploopswap,
        token_noloopcollapse,
        token_multisrccg,
        token_multisrcgc,
        token_conditionalsrc,
        token_enclosingloops,
        token_permute
    }


}
