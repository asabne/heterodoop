package cudamr.analysis;

/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/11/13
 * Time: 1:39 AM
 * To change this template use File | Settings | File Templates.
 */

import cetus.analysis.LoopTools;
import cetus.hir.*;

import java.util.*;
import java.util.Enumeration;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeMap;
import java.util.LinkedList;


import cetus.exec.Driver;
import cetus.hir.*;
import cudamr.hir.*;
import cetus.analysis.LoopTools;


/**
 * <b>AnalysisTools</b> provides tools that perform various analyses for C-to-CUDA translation.
 *
 * @author Amit Sabne <asabne@purdue.edu>
 *         Purdue University
 */
public abstract class AnalysisTools {
    /**
     * Java doesn't allow a class to be both abstract and final,
     * so this private constructor prevents any derivations.
     */
    private AnalysisTools()
    {
    }

    static private final String[] predefinedCVars = {"stdin", "stdout",
            "stderr"};

    static List predefinedCVarList = new LinkedList(Arrays.asList(predefinedCVars));

    /**
     * check whether the input variable is a member of a class.
     *
     * @param varSym input variable symbol
     * @return true if input variable is a member of a class
     */
    public static boolean isClassMember(Declarator varSym) {
        Traversable t = varSym.getParent();
        boolean foundParentClass = false;
        while( t != null ) {
            if( t instanceof ClassDeclaration) {
                foundParentClass = true;
                break;
            } else {
                t = t.getParent();
            }
        }
        return foundParentClass;
    }

    /**
     * Return a set of static symbols from the input symbol set, iset
     *
     * @param iset input symbol set
     * @return a set of static symbols
     */
    static public HashSet<Symbol> getStaticVariables(Set<Symbol> iset)
    {
        HashSet<Symbol> ret = new HashSet<Symbol> ();
        for (Symbol sym : iset)
        {
            if(SymbolTools.containsSpecifier(sym, Specifier.STATIC)) {
                ret.add(sym);
            }
        }
        return ret;
    }

    /**
     * Returns the set of global symbols accessed in the input Traversable object, st.
     *
     * @param st input Traversable object to be searched.
     * @return the set of accessed global symbols, visible in the current scope.
     */
    public static Set<Symbol> getAccessedGlobalSymbols(Traversable st, Map<String, Symbol> visibleGSymMap) {
        Map<String, Symbol> gSymMap;
        Set<Symbol> rSet = new HashSet<Symbol>();
        Set<Symbol> aSet = AnalysisTools.getAccessedVariables(st, true);
        Set<Symbol> lSet = SymbolTools.getLocalSymbols(st);
        // Remove procedure symbols from aSet. //
        Set<Symbol> tSet = new HashSet<Symbol>();
        for( Symbol sm : aSet ) {
            if( (sm instanceof Procedure) || (sm instanceof ProcedureDeclarator) ) {
                tSet.add(sm);
            }
        }
        aSet.removeAll(tSet);
        // Remove local symbols from aSet.
        aSet.removeAll(lSet);
        // Find global symbols visible in the current scope
        // (distinguish extern variable from the original one.)
        if( visibleGSymMap == null ) {
            tSet = SymbolTools.getGlobalSymbols(st);
            gSymMap = new HashMap<String, Symbol>();
            for( Symbol gS : tSet ) {
                gSymMap.put(gS.getSymbolName(), gS);
            }
        } else {
            gSymMap = visibleGSymMap;
        }
        for( Symbol sym : aSet ) {
            if( SymbolTools.isGlobal(sym) ) {
                if( gSymMap.containsKey(sym.getSymbolName()) ) {
                    rSet.add(gSymMap.get(sym.getSymbolName()));
                } else {
                    Procedure tProc = IRTools.getParentProcedure(st);
                    String tProcName = "Unknown";
                    if( tProc != null ) {
                        tProcName = tProc.getSymbolName();
                    }
                    PrintTools.println("\n[WARNING] a global symbol (" + sym.getSymbolName() + ") accessed in a procedure, " +
                            tProcName + ", is not visible in the enclosing compute region; this may result in incorrect translation.\n", 0);
                }
            }
        }
        return rSet;
    }

    /**
     * Returns the set of global symbols accessed in the input Traversable object, st.
     * If st contains function calls, each function is recursively checked.
     *
     * @param st input Traversable object to be searched interprocedurally.
     * @return the set of accessed global symbols, visible in the current scope.
     */
    public static Set<Symbol> getIpAccessedGlobalSymbols(Traversable st, Map<String, Symbol> visibleGSymMap) {
        Map<String, Symbol> gSymMap;
        Set<Symbol> rSet = new HashSet<Symbol>();
        Set<Symbol> aSet = AnalysisTools.getAccessedVariables(st, true);
        Set<Symbol> lSet = SymbolTools.getLocalSymbols(st);
        // Remove procedure symbols from aSet. //
        Set<Symbol> tSet = new HashSet<Symbol>();
        for( Symbol sm : aSet ) {
            if( (sm instanceof Procedure) || (sm instanceof ProcedureDeclarator) ) {
                tSet.add(sm);
            }
        }
        aSet.removeAll(tSet);
        // Remove local symbols from aSet.
        aSet.removeAll(lSet);
        // Find global symbols visible in the current scope
        // (distinguish extern variable from the original one.)
        if( visibleGSymMap == null ) {
            tSet = SymbolTools.getGlobalSymbols(st);
            gSymMap = new HashMap<String, Symbol>();
            for( Symbol gS : tSet ) {
                gSymMap.put(gS.getSymbolName(), gS);
            }
        } else {
            gSymMap = visibleGSymMap;
        }
        for( Symbol sym : aSet ) {
            if( SymbolTools.isGlobal(sym) ) {
                if( gSymMap.containsKey(sym.getSymbolName()) ) {
                    rSet.add(gSymMap.get(sym.getSymbolName()));
                } else {
                    Procedure tProc = IRTools.getParentProcedure(st);
                    String tProcName = "Unknown";
                    if( tProc != null ) {
                        tProcName = tProc.getSymbolName();
                    }
                    PrintTools.println("\n[WARNING] a global symbol (" + sym.getSymbolName() + ") accessed in a procedure, " +
                            tProcName + ", is not visible in the enclosing compute region; this may result in incorrect translation.\n", 0);
                }
            }
        }
        List<FunctionCall> calledFuncs = IRTools.getFunctionCalls(st);
        for( FunctionCall call : calledFuncs ) {
            Procedure called_procedure = call.getProcedure();
            if( called_procedure != null ) {
                CompoundStatement body = called_procedure.getBody();
                Set<Symbol> procAccessedSymbols = getIpAccessedGlobalSymbols(body, gSymMap);
                rSet.addAll(procAccessedSymbols);
            }
        }
        return rSet;
    }


    /**
     * returns a set of all accessed symbols except Procedure symbols and 
     * member symbols of an enumeration. if {@code IRSymbolOnly} is true, only IR symbols are 
     * included.
     */
    public static Set<Symbol> getAccessedVariables(Traversable st, boolean IRSymbolOnly)
    {
        Set<Symbol> set = SymbolTools.getAccessedSymbols(st);
        HashSet<Symbol> ret = new HashSet<Symbol> ();
        for (Symbol symbol : set)
        {
            if( symbol.getDeclaration() instanceof cetus.hir.Enumeration) {
                continue; //Skip member symbols of an enumeration.
            }
            if( symbol instanceof VariableDeclarator ) {
                if( IRSymbolOnly ) {
                    if( !isClassMember((VariableDeclarator)symbol) ) {
                        ret.add(symbol);
                    }
                } else {
                    ret.add(symbol);
                }
            } else if( symbol instanceof AccessSymbol ) {
                //DEBUG: this case will not be executed since set does not include this case.
                if( IRSymbolOnly ) {
                    Symbol base = ((AccessSymbol)symbol).getIRSymbol();
                    if( base != null ) {
                        ret.add(base);
                    }
                } else {
                    ret.add(symbol);
                }
            } else if( symbol instanceof NestedDeclarator ){
                //Add it if it is a pointer to a chunk of arrays (ex: int (*B)[SIZE];)
                if( !((NestedDeclarator)symbol).isProcedure() ) {
                    ret.add(symbol);
                }
            } else if( symbol instanceof DerefSymbol ) {
                //FIXME: how to handle this?
                PrintTools.println("\n[WARNING] AnalysisTools.getAccessedVariables() will ignore the following symbol, " +
                        symbol + ".\n", 0);
            }
        }
        return ret;
    }

    /**
     * For each symbol in input set, iset,
     *     if it is an AccessSymbol, base symbol is added to output set
     *     else if it is not a class member, the symbol itself is added to the output set.
     *
     * @param iset
     * @return output set of base symbols
     */
    static public HashSet<Symbol> getIRSymbols(Set<Symbol> iset) {
        HashSet<Symbol> ret = new HashSet<Symbol>();
        for( Symbol sym : iset ) {
            if( sym instanceof VariableDeclarator ) {
                if( !isClassMember((VariableDeclarator)sym) ) {
                    ret.add(sym);
                }
            } else if( sym instanceof AccessSymbol ) {
                Symbol base = ((AccessSymbol)sym).getIRSymbol();
                if( base != null ) {
                    ret.add(base);
                }
            } else if( sym != null ){
                ret.add(sym);
            }
        }
        return ret;
    }


    /**
     * collect loop index variables within the input traversable object {@code tr}
     */
    public static	HashSet<Symbol> getLoopIndexVarSet(Traversable tr)
    {
        HashSet<Symbol> ret = new HashSet<Symbol> ();
        DFIterator<ForLoop> iter = new DFIterator<ForLoop>(tr, ForLoop.class);
        while(iter.hasNext())
        {
            Expression ivar_expr = LoopTools.getIndexVariable(iter.next());
            Symbol ivar = SymbolTools.getSymbolOf(ivar_expr);
            if (ivar==null)
                Tools.exit("[getLoopIndexVariables] Cannot find symbol:" + ivar_expr.toString());
            else
                ret.add(ivar);
        }
        return ret;
    }


    /**
     * Generate an access expression of a form, a.b, from an AccessSymbol.
     *
     * @param accSym
     * @return
     */
    public static AccessExpression accessSymbolToExpression( AccessSymbol accSym, List<Expression> indices ) {
        Symbol base = accSym.getBaseSymbol();
        Symbol member = accSym.getMemberSymbol();
        Expression lhs = null, rhs = null;
        if (base instanceof DerefSymbol) {
            lhs = ((DerefSymbol)base).toExpression();
        } else if (base instanceof AccessSymbol) {
            lhs = accessSymbolToExpression((AccessSymbol)base, indices);
        } else if (base instanceof Identifier) {
            lhs = new Identifier(base);
        } else {
            PrintTools.printlnStatus(0,
                    "\n[WARNING] Unexpected access expression type\n");
            return null;
        }
        if( (indices == null) || indices.isEmpty() ) {
            rhs = new Identifier(member);
        } else {
            rhs = new ArrayAccess(new Identifier(member), indices);
        }
        return new AccessExpression(lhs, AccessOperator.MEMBER_ACCESS, rhs);
    }

    /**
     * Converts a collection of symbols to a string of symbol names with the given separator.
     *
     * @param symbols the collection of Symbols to be converted.
     * @param separator the separating string.
     * @return the converted string, which is a list of symbol names
     */
    public static String symbolsToString(Collection<Symbol> symbols, String separator)
    {
        if ( symbols == null || symbols.size() == 0 )
            return "";

        StringBuilder str = new StringBuilder(80);

        Iterator<Symbol> iter = symbols.iterator();
        if ( iter.hasNext() )
        {
            str.append(iter.next().getSymbolName());
            while ( iter.hasNext() ) {
                str.append(separator+iter.next().getSymbolName());
            }
        }

        return str.toString();
    }

    /**
     * Converts a collection of symbols to a set of strings of symbol names.
     *
     * @param symbols the collection of Symbols to be converted.
     * @return a set of strings, which contains symbol names
     */
    public static Set<String> symbolsToStringSet(Collection<Symbol> symbols)
    {
        HashSet<String> strSet = new HashSet<String>();
        if ( symbols == null || symbols.size() == 0 )
            return strSet;


        Iterator<Symbol> iter = symbols.iterator();
        if ( iter.hasNext() )
        {
            strSet.add(iter.next().getSymbolName());
            while ( iter.hasNext() ) {
                strSet.add(iter.next().getSymbolName());
            }
        }

        return strSet;
    }

    /**
     * Converts a collection of expressions to a set of strings.
     *
     * @param exprs the collection of expressions to be converted.
     * @return a set of strings
     */
    public static Set<String> expressionsToStringSet(Collection<Expression> exprs)
    {
        HashSet<String> strSet = new HashSet<String>();
        if ( exprs == null || exprs.size() == 0 )
            return strSet;


        Iterator<Expression> iter = exprs.iterator();
        if ( iter.hasNext() )
        {
            strSet.add(iter.next().toString());
            while ( iter.hasNext() ) {
                strSet.add(iter.next().toString());
            }
        }

        return strSet;
    }




    /**
     * Returns true if pragma annotations of the given type exists
     * in the annotatable objects within the traversable object
     * {@code t}. 
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @return true if pragmas exist; otherwise, return false
     */
    public static <T extends PragmaAnnotation> boolean containsPragma(
            Traversable t, Class<T> pragma_cls) {
        boolean foundPragma = false;
        DFIterator<Annotatable> iter =
                new DFIterator<Annotatable>(t, Annotatable.class);
        while (iter.hasNext()) {
            Annotatable at = iter.next();
            List<T> pragmas = at.getAnnotations(pragma_cls);
            if( (pragmas != null) && !pragmas.isEmpty() ) {
                foundPragma = true;
                break;
            }
        }
        return foundPragma;
    }

    /**
     * Returns a list of pragma annotations of the given type 
     * and are attached to annotatable objects within the traversable object
     * {@code t}.
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @return the list of matching pragma annotations.
     */
    public static <T extends PragmaAnnotation> List<T> collectPragmas(
            Traversable t, Class<T> pragma_cls) {
        List<T> ret = new ArrayList<T>();
        DFIterator<Annotatable> iter =
                new DFIterator<Annotatable>(t, Annotatable.class);
        while (iter.hasNext()) {
            Annotatable at = iter.next();
            List<T> pragmas = at.getAnnotations(pragma_cls);
            ret.addAll(pragmas);
        }
        return ret;
    }

    /**
     * Returns a list of pragma annotations that contain the specified string
     * keys and are attached to annotatable objects within the traversable object
     * {@code t}. For example, it can collect list of OpenMP pragmas having
     * a work-sharing directive {@code for} within a specific procedure.
     * If {@code includeAll} is true, check whether all keys in the {@code searchKeys} are included,
     * and otherwise, check whether any key in the {@code searchKeys} is included. 
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @param searchKeys the keywords to be searched for.
     * @param includeAll if true, search pragmas containing all keywords; otherwise search pragma containing any keywords
     * in the {@code searchKeys} set.
     * @return the list of matching pragma annotations.
     */
    public static <T extends PragmaAnnotation> List<T> collectPragmas(
            Traversable t, Class<T> pragma_cls, Set<String> searchKeys, boolean includeAll) {
        List<T> ret = new ArrayList<T>();
        DFIterator<Annotatable> iter =
                new DFIterator<Annotatable>(t, Annotatable.class);
        while (iter.hasNext()) {
            Annotatable at = iter.next();
            List<T> pragmas = at.getAnnotations(pragma_cls);
            for (int i = 0; i < pragmas.size(); i++) {
                T pragma = pragmas.get(i);
                boolean found = false;
                for( String key: searchKeys ) {
                    if ( pragma.containsKey(key) ) {
                        found = true;
                    } else {
                        found = false;
                    }
                    if( includeAll ) {
                        if( !found ) {
                            break;
                        }
                    } else if( found ) {
                        break;
                    }
                }
                if( found ) {
                    ret.add(pragma);
                }
            }
        }
        return ret;
    }

    /**
     * Returns true if a pragma annotation exists that contain the specified string key
     * and are attached to annotatable objects within the traversable object
     * {@code t} interprocedurally. 
     * If functions are called within the traversable object (@code t), 
     * the called functions are recursively searched.
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @param key the keyword to be searched for.
     * @return true if a matching pragma annotation exists.
     */
    public static <T extends PragmaAnnotation> boolean
    ipContainPragmas(Traversable t, Class<T> pragma_cls, String key)
    {
        DFIterator<Traversable> iter = new DFIterator<Traversable>(t);
        while ( iter.hasNext() )
        {
            Object o = iter.next();
            if ( o instanceof Annotatable )
            {
                Annotatable at = (Annotatable)o;
                List<T> pragmas = at.getAnnotations(pragma_cls);
                if( pragmas != null ) {
                    for ( T pragma : pragmas )
                        if ( pragma.containsKey(key) ) {
                            return true;
                        }
                }
            } else if( o instanceof FunctionCall ) {
                FunctionCall funCall = (FunctionCall)o;
                if( !StandardLibrary.contains(funCall) ) {
                    Procedure calledProc = funCall.getProcedure();
                    if( calledProc != null ) {
                        if( ipContainPragmas(calledProc, pragma_cls, key) ) return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Returns true if a pragma annotation exists that contain the specified string keys
     * and are attached to annotatable objects within the traversable object
     * {@code t} interprocedurally. 
     * If {@code includeAll} is true, check whether all keys in the {@code searchKeys} are included,
     * and otherwise, check whether any key in the {@code searchKeys} is included. 
     * If functions are called within the traversable object (@code t), 
     * the called functions are recursively searched.
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @param searchKeys the keywords to be searched for.
     * @param includeAll if true, search pragmas containing all keywords; otherwise search pragma containing any keywords
     * in the {@code searchKeys} set.
     * @return true if a matching pragma annotation exists.
     */
    public static <T extends PragmaAnnotation> boolean
    ipContainPragmas(Traversable t, Class<T> pragma_cls, Set<String> searchKeys, boolean includeAll)
    {
        DFIterator<Traversable> iter = new DFIterator<Traversable>(t);
        while ( iter.hasNext() )
        {
            Object o = iter.next();
            if ( o instanceof Annotatable )
            {
                Annotatable at = (Annotatable)o;
                List<T> pragmas = at.getAnnotations(pragma_cls);
                if( pragmas != null ) {
                    for ( T pragma : pragmas ) {
                        boolean found = false;
                        for( String key: searchKeys ) {
                            if ( pragma.containsKey(key) ) {
                                found = true;
                            } else {
                                found = false;
                            }
                            if( includeAll ) {
                                if( !found ) {
                                    break;
                                }
                            } else if( found ) {
                                break;
                            }
                        }
                        if( found ) {
                            return true;
                        }
                    }
                }
            } else if( o instanceof FunctionCall ) {
                FunctionCall funCall = (FunctionCall)o;
                if( !StandardLibrary.contains(funCall) ) {
                    Procedure calledProc = funCall.getProcedure();
                    if( calledProc != null ) {
                        if( ipContainPragmas(calledProc, pragma_cls, searchKeys, includeAll) ) return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Returns a list of pragma annotations that contain the specified string keys
     * and are attached to annotatable objects within the traversable object
     * {@code t} interprocedurally. 
     * If functions are called within the traversable object (@code t), 
     * the called functions are recursively searched.
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @param key the keyword to be searched for.
     * @return the list of matching pragma annotations.
     */
    public static <T extends PragmaAnnotation> List<T>
    ipCollectPragmas(Traversable t, Class<T> pragma_cls, String key)
    {
        List<T> ret = new LinkedList<T>();

        DFIterator<Traversable> iter = new DFIterator<Traversable>(t);
        while ( iter.hasNext() )
        {
            Object o = iter.next();
            if ( o instanceof Annotatable )
            {
                Annotatable at = (Annotatable)o;
                List<T> pragmas = at.getAnnotations(pragma_cls);
                if( pragmas != null ) {
                    for ( T pragma : pragmas )
                        if ( pragma.containsKey(key) )
                            ret.add(pragma);
                }
            } else if( o instanceof FunctionCall ) {
                FunctionCall funCall = (FunctionCall)o;
                if( !StandardLibrary.contains(funCall) ) {
                    Procedure calledProc = funCall.getProcedure();
                    if( calledProc != null ) {
                        ret.addAll(ipCollectPragmas(calledProc, pragma_cls, key));
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Returns a list of pragma annotations that contain the specified string keys
     * and are attached to annotatable objects within the traversable object
     * {@code t} interprocedurally. 
     * If {@code includeAll} is true, check whether all keys in the {@code searchKeys} are included,
     * and otherwise, check whether any key in the {@code searchKeys} is included. 
     * If functions are called within the traversable object (@code t), 
     * the called functions are recursively searched.
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @param searchKeys the keywords to be searched for.
     * @param includeAll if true, search pragmas containing all keywords; otherwise search pragma containing any keywords
     * in the {@code searchKeys} set.
     * @return the list of matching pragma annotations.
     */
    public static <T extends PragmaAnnotation> List<T>
    ipCollectPragmas(Traversable t, Class<T> pragma_cls, Set<String> searchKeys, boolean includeAll)
    {
        List<T> ret = new LinkedList<T>();

        DFIterator<Traversable> iter = new DFIterator<Traversable>(t);
        while ( iter.hasNext() )
        {
            Object o = iter.next();
            if ( o instanceof Annotatable )
            {
                Annotatable at = (Annotatable)o;
                List<T> pragmas = at.getAnnotations(pragma_cls);
                if( pragmas != null ) {
                    for ( T pragma : pragmas ) {
                        boolean found = false;
                        for( String key: searchKeys ) {
                            if ( pragma.containsKey(key) ) {
                                found = true;
                            } else {
                                found = false;
                            }
                            if( includeAll ) {
                                if( !found ) {
                                    break;
                                }
                            } else if( found ) {
                                break;
                            }
                        }
                        if( found ) {
                            ret.add(pragma);
                        }
                    }
                }
            } else if( o instanceof FunctionCall ) {
                FunctionCall funCall = (FunctionCall)o;
                if( !StandardLibrary.contains(funCall) ) {
                    Procedure calledProc = funCall.getProcedure();
                    if( calledProc != null ) {
                        ret.addAll(ipCollectPragmas(calledProc, pragma_cls, searchKeys, includeAll));
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Returns the first pragma annotation that contains the specified string keys
     * and are attached to annotatable objects in the parent of traversable object
     * {@code t}. 
     * If the function enclosing the traversable object (@code t) is called in
     * other functions, the calling functions are recursively searched.
     *
     * @param input the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @param key the keyword to be searched for.
     * @return a matching pragma annotation found first.
     */
    public static <T extends PragmaAnnotation> T
    ipFindFirstPragmaInParent(Traversable input, Class<T> pragma_cls, String key, List<FunctionCall> funcCallList)
    {
        T ret = null;
        if( input == null ) {
            return ret;
        }
        Traversable t = input.getParent();
        while( t != null ) {
            if( t instanceof Annotatable ) {
                Annotatable at  = (Annotatable)t;
                T tAnnot = at.getAnnotation(pragma_cls, key);
                if( tAnnot != null ) {
                    ret = tAnnot;
                    break;
                }
            }
            if( t instanceof Procedure ) {
                Procedure tProc = (Procedure)t;
                List<FunctionCall> fCallList = funcCallList;
                if( fCallList == null ) {
                    while( t != null ) {
                        if( t instanceof Program ) {
                            break;
                        }
                        t = t.getParent();
                    }
                    if( t instanceof Program ) {
                        fCallList = IRTools.getFunctionCalls(t);
                    }
                }
                if( fCallList != null ) {
                    for( FunctionCall fCall : fCallList ) {
                        if( fCall.getName().equals(tProc.getName()) ) {
                            T tAnnot = fCall.getStatement().getAnnotation(pragma_cls, key);
                            if( tAnnot != null ) {
                                ret = tAnnot;
                                break;
                            } else {
                                tAnnot = ipFindFirstPragmaInParent(fCall.getStatement(), pragma_cls, key, fCallList);
                                if( tAnnot != null ) {
                                    ret = tAnnot;
                                    break;
                                }
                            }
                        }
                    }
                }
                break;
            }
            t = t.getParent();
        }
        return ret;
    }

    /**
     * Returns the first pragma annotation that contains the specified string keys
     * and are attached to annotatable objects in the parent of traversable object
     * {@code t}. 
     * If the function enclosing the traversable object (@code t) is called in
     * other functions, the calling functions are recursively searched.
     * If {@code includeAll} is true, check whether all keys in the {@code searchKeys} are included,
     * and otherwise, check whether any key in the {@code searchKeys} is included. 
     *
     * @param input the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @param searchKeys the keywords to be searched for.
     * @param includeAll if true, search pragmas containing all keywords; otherwise search pragma containing any keywords
     * in the {@code searchKeys} set.
     * @return a matching pragma annotation found first.
     */
    public static <T extends PragmaAnnotation> T
    ipFindFirstPragmaInParent(Traversable input, Class<T> pragma_cls, Set<String> searchKeys, boolean includeAll,
                              List<FunctionCall> funcCallList)
    {
        T ret = null;
        if( input == null ) {
            return ret;
        }
        Traversable t = input.getParent();
        while( t != null ) {
            if( t instanceof Annotatable ) {
                Annotatable at  = (Annotatable)t;
                T tAnnot = null;
                boolean found = false;
                for( String key: searchKeys ) {
                    tAnnot = at.getAnnotation(pragma_cls, key);
                    if( tAnnot != null ) {
                        found = true;
                    } else {
                        found = false;
                    }
                    if( includeAll ) {
                        if( !found ) {
                            break;
                        }
                    } else if( found ) {
                        break;
                    }
                }
                if( found ) {
                    ret = tAnnot;
                    break;
                }
            }
            if( t instanceof Procedure ) {
                Procedure tProc = (Procedure)t;
                List<FunctionCall> fCallList = funcCallList;
                if( fCallList == null ) {
                    while( t != null ) {
                        if( t instanceof Program ) {
                            break;
                        }
                        t = t.getParent();
                    }
                    if( t instanceof Program ) {
                        fCallList = IRTools.getFunctionCalls(t);
                    }
                }
                if( fCallList != null ) {
                    for( FunctionCall fCall : fCallList ) {
                        if( fCall.getName().equals(tProc.getName()) ) {
                            Annotatable at  = (Annotatable)fCall.getStatement();
                            T tAnnot = null;
                            boolean found = false;
                            for( String key: searchKeys ) {
                                tAnnot = at.getAnnotation(pragma_cls, key);
                                if( tAnnot != null ) {
                                    found = true;
                                } else {
                                    found = false;
                                }
                                if( includeAll ) {
                                    if( !found ) {
                                        break;
                                    }
                                } else if( found ) {
                                    break;
                                }
                            }
                            if( found ) {
                                ret = tAnnot;
                                break;
                            } else {
                                tAnnot = ipFindFirstPragmaInParent(at, pragma_cls, searchKeys, includeAll,
                                        fCallList);
                                if( tAnnot != null ) {
                                    ret = tAnnot;
                                    break;
                                }
                            }
                        }
                    }
                }
                break;
            }
            t = t.getParent();
        }
        return ret;
    }

    /**
     * Returns a list of FunctionCall expressions within the traversable object
     * interprocedurally.
     *
     * @param t the traversable object to be searched.
     * @return the list of function calls that appear in {@code t}.
     */
    public static Set<Procedure> ipGetCalledProcedures(Traversable t) {
        Set<Procedure> procList = new HashSet<Procedure>();
        if (t == null) {
            return null;
        }
        List<FunctionCall> fCallList = ((new DFIterator<FunctionCall>(t, FunctionCall.class)).getList());
        for( FunctionCall fCall : fCallList ) {
            if( !StandardLibrary.contains(fCall) ) {
                Procedure proc = fCall.getProcedure();
                if( proc != null ) {
                    procList.add(proc);
                    Set<Procedure> ipList = ipGetCalledProcedures(proc.getBody());
                    if( ipList != null ) {
                        procList.addAll(ipList);
                    }
                }
            }
        }
        return procList;
    }

    /**
     * Returns the innermost pragma annotation that contain the specified string
     * keys and are attached to annotatable objects within the traversable object
     * {@code t}. For example, it can return the innermost worker loop annotation
     * within specific nested loops.
     * CAUTION: if multiple innermost pragmas exist, this method returns the last
     * found one; it does not compare nesting levels.
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @param searchKey the keywords to be searched for.
     * @return the innermost matching pragma annotation.
     */
    public static <T extends PragmaAnnotation> T findInnermostPragma(
            Traversable t, Class<T> pragma_cls, String searchKey ) {
        T ret = null;
        BreadthFirstIterator iter =
                new BreadthFirstIterator(t);
        for (;;)
        {
            Annotatable at = null;

            try {
                at = (Annotatable)iter.next(Annotatable.class);
            } catch (NoSuchElementException e) {
                break;
            }
            T tAnnot = at.getAnnotation(pragma_cls, searchKey);
            if( tAnnot != null ) {
                ret = tAnnot;
            }
        }
        return ret;
    }

    /**
     * Returns the innermost pragma annotation that contain the specified string
     * keys and are attached to annotatable objects within the traversable object
     * {@code t}. For example, it can return the innermost worker loop annotation
     * within specific nested loops.
     * If {@code includeAll} is true, check whether all keys in the {@code searchKeys} are included,
     * and otherwise, check whether any key in the {@code searchKeys} is included. 
     * CAUTION: if multiple innermost pragmas exist, this method returns the last
     * found one; it does not compare nesting levels.
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @param searchKeys the keywords to be searched for.
     * @param includeAll if true, search pragmas containing all keywords; otherwise search pragma containing any keywords
     * in the {@code searchKeys} set.
     * @return the innermost matching pragma annotation.
     */
    public static <T extends PragmaAnnotation> T findInnermostPragma(
            Traversable t, Class<T> pragma_cls, Set<String> searchKeys, boolean includeAll) {
        T ret = null;
        BreadthFirstIterator iter =
                new BreadthFirstIterator(t);
        for (;;)
        {
            Annotatable at = null;

            try {
                at = (Annotatable)iter.next(Annotatable.class);
            } catch (NoSuchElementException e) {
                break;
            }
            List<T> pragmas = at.getAnnotations(pragma_cls);
            for (int i = 0; i < pragmas.size(); i++) {
                T pragma = pragmas.get(i);
                boolean found = false;
                for( String key: searchKeys ) {
                    if ( pragma.containsKey(key) ) {
                        found = true;
                    } else {
                        found = false;
                    }
                    if( includeAll ) {
                        if( !found ) {
                            break;
                        }
                    } else if( found ) {
                        break;
                    }
                }
                if( found ) {
                    ret = pragma;
                }
            }
        }
        return ret;
    }

    /**
     * Returns a list of outermost pragma annotations that contain the specified string
     * keys and are attached to annotatable objects within the traversable object
     * {@code t}. For example, it can return the outermost worker loop annotations
     * within specific nested loops.
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be searched for.
     * @param searchKey the keyword to be searched for.
     * @return the list of outermost matching pragma annotations.
     */
    public static <T extends PragmaAnnotation> List<T> collectOutermostPragmas(
            Traversable t, Class<T> pragma_cls, String searchKey ) {
        List<T> ret = new ArrayList<T>();
        DFIterator<Annotatable> iter =
                new DFIterator<Annotatable>(t, Annotatable.class);
        while (iter.hasNext()) {
            Annotatable at = iter.next();
            T tAnnot = at.getAnnotation(pragma_cls, searchKey);
            if( tAnnot != null ) {
                Traversable tt = at.getParent();
                boolean isOutermost = true;
                while( (tt != null) ) {
                    if( ((Annotatable)tt).containsAnnotation(pragma_cls, searchKey) ) {
                        isOutermost = false;
                        break;
                    }
                    tt = tt.getParent();
                }
                if( isOutermost ) {
                    ret.add(tAnnot);
                }
            }
        }
        return ret;
    }

    /**
     * Remove pragma annotations of the given type 
     * that are attached to annotatable objects within the traversable object
     * {@code t}.
     *
     * @param t the traversable object to be searched.
     * @param pragma_cls the type of pragmas to be removed.
     */
    public static <T extends PragmaAnnotation> void removePragmas(
            Traversable t, Class<T> pragma_cls) {
        DFIterator<Annotatable> iter =
                new DFIterator<Annotatable>(t, Annotatable.class);
        while (iter.hasNext()) {
            Annotatable at = iter.next();
            at.removeAnnotations(pragma_cls);
        }
    }


    /**
     * Replace old IR symbol ({@code oldIRSym}) in {@code oldPSym} with new one ({@code newIRSym}).
     *
     * @param oldPSym PseudoSymbol contains the old IR symbol ({@code oldIRSym})
     * @param oldIRSym old IR symbol to be replaced
     * @param newIRSym new IR symbol
     * @return PseudoSymbol with the new IR symbol ({@code newIRSym})
     */
    protected static PseudoSymbol replaceIRSymbol(PseudoSymbol oldPSym, Symbol oldIRSym, Symbol newIRSym) {
        PseudoSymbol newPSym = null;
        if( (oldPSym == null) || (oldIRSym == null) || (newIRSym == null) ) {
            return null;
        }
        if( oldPSym instanceof AccessSymbol ) {
            Symbol base = ((AccessSymbol) oldPSym).getBaseSymbol();
            Symbol member = ((AccessSymbol) oldPSym).getMemberSymbol();
            if( base.equals(oldIRSym) ) {
                newPSym = new AccessSymbol(newIRSym, member);
            } else if (base instanceof PseudoSymbol ) {
                Symbol newBase = replaceIRSymbol((PseudoSymbol)base, oldIRSym, newIRSym);
                newPSym = new AccessSymbol(newBase, member);
            } else {
                PrintTools.println("\n[WARNING in AnalysisTools.replaceIRSymbol()] cannot find old IR symbol to be replaced:\n" +
                        "Old IR symbol: " + oldIRSym + "\nPseudo symbol: " + oldPSym + ".\n", 0);
            }
        } else if( oldPSym instanceof DerefSymbol ) {
            Symbol refSym = ((DerefSymbol)oldPSym).getRefSymbol();
            if( refSym.equals(oldIRSym) ) {
                newPSym = new DerefSymbol(newIRSym);
            } else if( refSym instanceof PseudoSymbol ) {
                Symbol newRefSym = replaceIRSymbol((PseudoSymbol)refSym, oldIRSym, newIRSym);
                newPSym = new DerefSymbol(newRefSym);
            } else {
                PrintTools.println("\n[WARNING in AnalysisTools.replaceIRSymbol()] cannot find old IR symbol to be replaced:\n" +
                        "Old IR symbol: " + oldIRSym + "\nPseudo symbol: " + oldPSym + ".\n", 0);
            }
        }
        return newPSym;
    }

    /**
     * Find the original symbol that the input extern symbol, inSym, refers to.
     * If the symbol refers to predefined C variables (stdin, stdout, stderr), 
     * return it as it is.
     *
     * @param inSym input extern symbol
     * @param prog program where the input symbol belongs.
     * @return returns the original symbol that the input extern symbol refers to.
     *         If the input symbol is not an extern symbol, return input symbol itself.
     *         If no original symbol is found, return null.
     */
    public static Symbol getOrgSymbolOfExternOne(Symbol inSym, Program prog) {
        Symbol orgSym = null;
        Symbol IRSym = null;
        if( inSym == null ) {
            return orgSym;
        }
        if( inSym instanceof PseudoSymbol ) {
            IRSym = ((PseudoSymbol)inSym).getIRSymbol();
        } else {
            IRSym = inSym;
        }
        if(!SymbolTools.containsSpecifier(IRSym, Specifier.EXTERN)) {
            return inSym;
        }
        String extSName = IRSym.getSymbolName();
        //Return predefined C variable as it is.
        if( predefinedCVarList.contains(extSName) ) {
            return inSym;
        }
        for ( Traversable tt : prog.getChildren() )
        {
            if( orgSym != null ) {
                break;
            }
            Set<Symbol> gSymbols = SymbolTools.getGlobalSymbols(tt);
            for( Symbol gSym : gSymbols ) {
                if(gSym.getSymbolName().equals(extSName)) {
                    if(!SymbolTools.containsSpecifier(gSym, Specifier.EXTERN)) {
                        orgSym = gSym;
                        break;
                    }
                }
            }
        }
        if( orgSym == null ) {
            return null;
        }
        if( inSym instanceof PseudoSymbol ) {
            orgSym = replaceIRSymbol((PseudoSymbol)inSym, IRSym, orgSym);
        }
        return orgSym;
    }

    static public class SymbolStatus {
        public static int ExternGlobal = 4;
        public static int Global = 3;
        public static int Parameter = 2;
        public static int Local = 1;
        public static int Invisible = 0;
        public static int MultipleArguments = -1;
        public static int ComplexArgument = -2;
        public static int NotSearchable = -3;
        public static int UnknownError = -4;
        public static boolean OrgSymbolFound(int status) {
            return (Invisible < status);
        }
        public static boolean isGlobal(int status) {
            return (Global <= status);
        }
    }

    /**
     * Find the original symbol of the input symbol, inSym.
     * If symbol scope, {@code symScope}, is given, and if the input symbol
     * is a function parameter, the original symbol is searched only upto the
     * symbol scope.
     * CAVEAT: This method assumes that {@code inSym} is visible to the traversable {@code t}
     *
     * If the input symbol is a global variable,
     *     - return a list of the symbol and the parent TranslationUnit
     * Else if the input symbol is a function parameter
     *     If the parent function is the same as the {@code symScope}
     *         - return a list of the symbol and the parent Procedure
     *     Else if the input symbol is a value-passed parameter of the function
     *         - If {@code symScope} == null
     *             - return a list of the symbol and the parent Procedure
     *         - Else
     *             - return an empty list
     *     Else if the argument for the parameter is unique, 
     *         - call this method again with the argument to keep searching.
     *     Else if the argument for the parameter is complex, 
     *         - return a list of the input symbol only.
     *     Else 
     *         - return an empty list.
     * Else if the input symbol is a (static) local variable 
     *     If the symbol is visible in the {@code symScope} or if {@code symScope} is null
     *         - return a list of the symbol and the parent Procedure
     *     Else
     *         - return an empty list
     *
     *
     * @param inSym input symbol
     * @param t traversable from which the original symbol is searched.
     * @param noExtern set true to find the original symbol without extern specifier
     * @param symScope procedure which sets the scope of the original symbol search
     * @param symbolInfo a list of found symbol and enclosing function/translation unit
     * @param fCallList a list of function calls in the program
     * @return status of this search result
     */
    public static int findOrgSymbol(Symbol inSym, Traversable t, boolean noExtern, Procedure symScope,
                                    List symbolInfo, List<FunctionCall> fCallList) {
        int symbolStatus = SymbolStatus.UnknownError;
        Symbol tSym = null;
        Symbol IRSym = null;
        Set<Symbol> symSet = null;
        Procedure p_proc = null;
        TranslationUnit t_unit = null;
        Program program = null;
        if( (inSym == null) || (t == null) ) {
            return SymbolStatus.NotSearchable;
        }
        // Find a parent Procedure.
        while (true) {
            if (t instanceof Procedure) break;
            t = t.getParent();
        }
        p_proc = (Procedure)t;
        // Find a parent TranslationUnit.
        while (true) {
            if (t instanceof TranslationUnit) break;
            t = t.getParent();
        }
        t_unit = (TranslationUnit)t;
        program = (Program)t_unit.getParent();
        if( inSym instanceof PseudoSymbol ) {
            IRSym = ((PseudoSymbol) inSym).getIRSymbol();
        } else {
            IRSym = inSym;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // If the input symbol is a global variable, return a list of the symbol and the parent TranslationUnit. //
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        if( SymbolTools.isGlobal(inSym) ) {
            tSym = getOrgSymbolOfExternOne(inSym, program);
            if( noExtern ) {
                if( tSym == null ) {
                    PrintTools.println("\n[WARNING in findOrgSymbol()] Can not find the original symbol" +
                            " that the extern symbol (" + inSym + ") refers to.\n", 1);
                    tSym = inSym;
                    symbolStatus = SymbolStatus.ExternGlobal;
                } else {
                    symbolStatus = SymbolStatus.Global;
                }
            }else {
                if( (tSym == null) || (!tSym.equals(inSym)) ) {
                    symbolStatus = SymbolStatus.ExternGlobal;
                } else {
                    symbolStatus = SymbolStatus.Global;
                }
                tSym = inSym;
            }
            symbolInfo.add(tSym);
            symbolInfo.add(t_unit);
            return symbolStatus;
        } else if( SymbolTools.isFormal(IRSym) ) {
            //The input symbol is a formal function parameter.
            String pName = p_proc.getSymbolName();
            symSet = SymbolTools.getParameterSymbols(p_proc);
            if( symSet.contains(IRSym) ) {
                if( (symScope !=null) && (symScope.getSymbolName().equals(p_proc.getSymbolName())) ) {
                    //Stop searching since this procedure is the same as {@code symScope}.
                    symbolInfo.add(inSym);
                    symbolInfo.add(p_proc);
                    return SymbolStatus.Parameter;
                } else if( pName.equals("main") || pName.equals("MAIN__") ) {
                    //We don't have to check a formal parameter of main function.
                    if( symScope == null ) {
                        symbolStatus = SymbolStatus.Parameter;
                    } else {
                        symbolStatus = SymbolStatus.Invisible;
                    }
                    symbolInfo.add(inSym);
                    symbolInfo.add(p_proc);
                    return symbolStatus;
                } else if( SymbolTools.isScalar(IRSym) && !SymbolTools.isPointer(IRSym) && !(IRSym instanceof NestedDeclarator) ){
                    //The parameter is a non-pointer, scalar variable; stop searching.
                    if( symScope == null ) {
                        symbolStatus = SymbolStatus.Parameter;
                    } else {
                        symbolStatus = SymbolStatus.Invisible;
                    }
                    symbolInfo.add(inSym);
                    symbolInfo.add(p_proc);
                    return symbolStatus;
                }else {
                    //Find corresponding argument of this formal parameter.
                    List<FunctionCall> funcCallList;
                    if( fCallList == null ) {
                        funcCallList = IRTools.getFunctionCalls(program);
                    } else {
                        funcCallList = fCallList;
                    }
                    // Find the caller procedure that called this procedure.
                    List paramList = p_proc.getParameters();
                    int list_size = paramList.size();
                    if( list_size == 1 ) {
                        Object obj = paramList.get(0);
                        String paramS = obj.toString();
                        // Remove any leading or trailing whitespace.
                        paramS = paramS.trim();
                        if( paramS.equals(Specifier.VOID.toString()) ) {
                            list_size = 0;
                        }
                    }
                    Procedure t_proc = null;
                    Symbol currArgSym = null;
                    Symbol prevArgSym = null;
                    boolean foundArg = false;
                    for( FunctionCall funcCall : funcCallList ) {
                        if(p_proc.getName().equals(funcCall.getName())) {
                            List argList = funcCall.getArguments();
                            for( int i=0; i<list_size; i++ ) {
                                ////////////////////////////////////////////////////////////////////////
                                // DEBUG: IRTools.containsSymbol() works only for searching           //
                                // symbols in expression tree; it internally compare Identifier only, //
                                // but VariableDeclarator contains NameID instead of Identifier.      //
                                ////////////////////////////////////////////////////////////////////////
                                //if( IRTools.containsSymbol((Declaration)paramList.get(i), inSym)) 
                                List declaredSyms = ((Declaration)paramList.get(i)).getDeclaredIDs();
                                if( declaredSyms.contains(new Identifier(IRSym)) ) {
                                    // Found an actual argument for the inSym. 
                                    foundArg = true;
                                    t = funcCall.getStatement();
                                    while( (t != null) && !(t instanceof Procedure) ) {
                                        t = t.getParent();
                                    }
                                    t_proc = (Procedure)t;
                                    Expression exp = (Expression)argList.get(i);
                                    ///////////////////////////////////////////////////////////////////
                                    // LIMIT: if the passed argument is complex, current translator  //
                                    // can not calculate the region accessed in the called function. //
                                    // Therefore, only the following expressions are handled for now.//
                                    //     - Simple pointer identifier (ex: a)                       //
                                    //     - Simple unary expression (ex: &b)                        //
                                    //     - Simple binary expression where only one symbol exists   //
                                    //       (ex: b + 1)                                             //
                                    ///////////////////////////////////////////////////////////////////
                                    if( exp instanceof BinaryExpression ) {
                                        BinaryExpression biExp = (BinaryExpression)exp;
                                        Symbol lsym = SymbolTools.getSymbolOf(biExp.getLHS());
                                        Symbol rsym = SymbolTools.getSymbolOf(biExp.getRHS());
                                        if( (lsym == null) && (rsym == null) ) {
                                            currArgSym = null;
                                        } else if( (lsym != null) && (rsym == null) ) {
                                            currArgSym = lsym;
                                        } else if( (lsym == null) && (rsym != null) ) {
                                            currArgSym = rsym;
                                        } else {
                                            currArgSym = null;
                                        }
                                    } else {
                                        currArgSym = SymbolTools.getSymbolOf(exp);
                                    }
                                    if( currArgSym == null ) {
                                        symbolInfo.add(inSym);
                                        symbolInfo.add(p_proc);
                                        if( !(exp instanceof Literal) ) {
                                            PrintTools.println("\n[WARNING in findOrgSymbol()] argument (" + exp +
                                                    ") passed for " + "the parameter symbol (" + IRSym +
                                                    ") of a procedure, " + p_proc.getName() + ", has complex expression; " +
                                                    "failed to analyze it.\n",0);
                                            return SymbolStatus.ComplexArgument;
                                        } else {
                                            if( symScope == null ) {
                                                symbolStatus = SymbolStatus.Parameter;
                                            } else {
                                                symbolStatus = SymbolStatus.Invisible;
                                            }
                                            return symbolStatus;
                                        }
                                    } else if( (prevArgSym != null) && (!currArgSym.equals(prevArgSym)) ) {
                                        Symbol pSym = getOrgSymbolOfExternOne(prevArgSym, program);
                                        Symbol cSym = getOrgSymbolOfExternOne(currArgSym, program);
                                        if( (pSym == null) || !pSym.equals(cSym) ) {
                                            // Multiple argument symbols are found.
                                            PrintTools.println("\n[WARNING in findOrgSymbol()] multiple argments exist " +
                                                    "for the parameter symbol (" + IRSym + ") of procedure ("
                                                    + p_proc.getSymbolName() + "); can't find the original symbol.\n", 1);
                                            symbolInfo.add(inSym);
                                            symbolInfo.add(p_proc);
                                            return SymbolStatus.MultipleArguments;
                                        }
                                    }
                                    prevArgSym = currArgSym;
                                }
                            }
                        }
                    } //end of funcCallList loop
                    if( foundArg ) { //found a unique argument symbol for the current parameter symbol.
                        symbolStatus = findOrgSymbol(currArgSym, t_proc, noExtern, symScope, symbolInfo, funcCallList);
                        if( inSym instanceof PseudoSymbol ) {
                            if( !symbolInfo.isEmpty() ) {
                                Symbol newIRSym = (Symbol)symbolInfo.remove(0);
                                inSym = replaceIRSymbol((PseudoSymbol)inSym, IRSym, newIRSym);
                                symbolInfo.add(0, inSym);
                            }
                        }
                        return symbolStatus;
                    } else { //not corresponding argument symbol is found (implementation bug)
                        PrintTools.println("\n[WARNING in findOrgSymbol()] Input symbol (" + IRSym + ") is a formal function parameter," +
                                " but corresponding argument is not found in the current scope (enclosing procedure: "+ p_proc.getSymbolName() +
                                ").\n", 1);
                        symbolInfo.add(inSym);
                        symbolInfo.add(p_proc);
                        return SymbolStatus.UnknownError;
                    }
                }
            } else {
                PrintTools.println("\n[WARNING in findOrgSymbol()] Input symbol (" + IRSym + ") is a formal function parameter," +
                        " but it is not visible in the current scope (enclosing procedure: "+ p_proc.getSymbolName() +
                        ").\n", 1);
                symbolInfo.add(inSym);
                symbolInfo.add(p_proc);
                return SymbolStatus.Invisible;
            }
        } else if (SymbolTools.isLocal(IRSym) ) {
            Traversable tt = (Traversable)IRSym;
            while ( (tt != null) && !(tt instanceof Procedure) ){
                tt = tt.getParent();
            }
            if( (tt instanceof Procedure) && ((Procedure)tt).getSymbolName().equals(p_proc.getSymbolName()) ) {
                if( (symScope == null) || p_proc.getSymbolName().equals(symScope.getSymbolName()) ) {
                    symbolStatus = SymbolStatus.Local;
                } else {
                    symbolStatus = SymbolStatus.Invisible;
                }
            } else {
                PrintTools.println("\n[WARNING in findOrgSymbol()] Input symbol (" + IRSym + ") is a local symbol," +
                        " but it is not visible in the current scope (enclosing procedure: "+ p_proc.getSymbolName() +
                        ").\n", 1);
                symbolStatus = SymbolStatus.Invisible;
            }
            symbolInfo.add(inSym);
            symbolInfo.add(p_proc);
            return symbolStatus;
        } else {
            PrintTools.println("\n[WARNING in findOrgSymbol()] Can not determine the type of the input symbol (" + inSym +
                    " in the procedure, " + p_proc.getSymbolName() + ");" +
                    " return an empty list.\n", 1);
            symbolInfo.add(inSym);
            symbolInfo.add(p_proc);
            return SymbolStatus.UnknownError;
        }
    }

    /**
     * Searches the symbol set and returns the first symbol whose name is the specified string
     * @param sset		Symbol set being searched
     * @param symName	symbol name being searched for
     * @return the first symbol amaong the symbol set whose name is the same as the specified string
     */
    public static Symbol findsSymbol(Set<Symbol> sset, String symName)
    {
        if ( sset == null )
            return null;

        for( Symbol sym : sset ) {
            if( sym.getSymbolName().equals(symName) ) {
                return sym;
            }
        }

        return null;
    }



    public static boolean isCudaCall(FunctionCall fCall) {
        if ( fCall == null )
            return false;

        Set<String> cudaCalls = new HashSet<String>(Arrays.asList(
                "CUDA_SAFE_CALL","cudaFree","cudaMalloc","cudaMemcpy",
                "cudaMallocPitch","tex1Dfetch","cudaBindTexture", "cudaMemcpy2D"
        ));

        if ( cudaCalls.contains((fCall.getName()).toString()) ) {
            return true;
        }
        return false;
    }

    /**
     * Return a statement before the ref_stmt in the parent CompoundStatement
     *
     * @param parent parent CompoundStatement containing the ref_stmt as a child
     * @param ref_stmt
     * @return statement before the ref_stmt in the parent CompoundStatement. If ref_stmt 
     * is not a child of parent or if there is no previous statement, return null.
     */
    public static Statement getStatementBefore(CompoundStatement parent, Statement ref_stmt) {
        List<Traversable> children = parent.getChildren();
        int index = Tools.indexByReference(children, ref_stmt);
        if( index <= 0 ) {
            return null;
        }
        return (Statement)children.get(index-1);
    }

    /**
     * Return a statement after the ref_stmt in the parent CompoundStatement
     *
     * @param parent parent CompoundStatement containing the ref_stmt as a child
     * @param ref_stmt
     * @return statement after the ref_stmt in the parent CompoundStatement. If ref_stmt 
     * is not a child of parent or if there is no previous statement, return null.
     */
    public static Statement getStatementAfter(CompoundStatement parent, Statement ref_stmt) {
        List<Traversable> children = parent.getChildren();
        int index = Tools.indexByReference(children, ref_stmt);
        if( (index == -1) || (index == children.size()-1) ) {
            return null;
        }
        return (Statement)children.get(index+1);
    }


    /**
     * Convert general set into a sorted set; this is mainly for consistent code generation, since
     * HashSet element order can be changed randomly.
     *
     * @param inSet
     * @return sorted set
     */
    public static Collection getSortedCollection(Set inSet) {
        TreeMap<String, Object> sortedMap = new TreeMap<String, Object>();
        for( Object obj : inSet ) {
            String objString;
            if( obj instanceof Procedure ) {
                objString = ((Procedure)obj).getSymbolName();
            } else {
                objString = obj.toString();
            }
            sortedMap.put(objString, obj);
        }
        return sortedMap.values();
    }

    /**
     * Returns true if the symbol set contains a symbol whose name is the specified string.
     * @param sset		Symbol set being searched
     * @param symName	symbol name being searched for
     */
    public static boolean containsSymbol(Set<Symbol> sset, String symName)
    {
        if ( sset == null )
            return false;

        for( Symbol sym : sset ) {
            if( sym.getSymbolName().equals(symName) ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Find a symbol table containing the input IR symbol, {@code sym}, searching 
     * from the input traversable, {@code tt}.
     *
     * @param sym input symbol
     * @param tt traversable from which search starts
     * @return symbol table containing the symbol, {@code sym}; return null if not found.
     */
    public static SymbolTable getIRSymbolScope(Symbol sym, Traversable tt) {
        Symbol IRSym = sym;
        if( sym instanceof PseudoSymbol ) {
            IRSym = ((PseudoSymbol)sym).getIRSymbol();
        }
        SymbolTable  targetSymbolTable = null;
        while( tt != null ) {
            if( tt instanceof SymbolTable ) {
                if( ((SymbolTable)tt).containsSymbol(IRSym) ) {
                    break;
                }
            }
            tt = tt.getParent();
        }
        if( (tt != null) && (tt instanceof SymbolTable) && ((SymbolTable)tt).containsSymbol(IRSym) ) {
            targetSymbolTable = (SymbolTable)tt;
        }
        return targetSymbolTable;
    }


    /**
     * Check if the input symbol, sym, is passed by reference in the function call, fCall.
     *
     * @param sym input symbol to check
     * @param fCall function call of interest
     * @return -2 if procedure definition of the function call can not be found
     *         or -1 if the input symbol is not passed by reference
     *         or -3 if an error occurs 
     *         or index of the first argument where the input symbol is passed by reference
     */
    public static int isCalledByRef(Symbol sym, FunctionCall fCall) {
        int status = -1;
        Procedure calledProc = fCall.getProcedure();
        if( calledProc == null ) {
            status = -2; //Can't find the procedure for the function call.
        } else {
            boolean isPointerTypeArg = false;
            if( SymbolTools.isArray(sym) || SymbolTools.isPointer(sym) ) {
                isPointerTypeArg = true;
            }
            List argList = fCall.getArguments();
            List paramList = calledProc.getParameters();
            int list_size = argList.size();
            for( int i=0; i<list_size; i++ ) {
                Object arg = argList.get(i);
                Symbol paramSym = (Symbol)((VariableDeclaration)paramList.get(i)).getDeclarator(0);
                boolean isPointerTypeParam = false;
                if( SymbolTools.isArray(paramSym) || SymbolTools.isPointer(paramSym) ) {
                    isPointerTypeParam = true;
                }
                if( arg instanceof Traversable ) {
                    Set<Symbol> usedSyms = DataFlowTools.getUseSymbol((Traversable)arg);
                    if( isPointerTypeParam && usedSyms.contains(sym) ) {
                        if( isPointerTypeArg ) {
                            status = i;
                            break;
                        } else if ( arg instanceof UnaryExpression ) {
                            UnaryExpression uexp = (UnaryExpression)arg;
                            if( uexp.getOperator().equals(UnaryOperator.ADDRESS_OF) ) {
                                status = i;
                                break;
                            }
                        }
                    }
                } else {
                    status = -3;
                    break;
                }
            }
        }
        return status;
    }

    /**
     * Interprocedurally check whether input symbol, sym, is defined in the Traversable, t.
     *
     * @param sym input symbol
     * @param t traversable to check
     * @return true if the input symbol is defined in the traversable.
     */
    public static boolean ipaIsDefined(Symbol sym, Traversable t ) {
        boolean isDefined = false;
        Set<Symbol> defSet = DataFlowTools.getDefSymbol(t);
        if( defSet.contains(sym)) {
            isDefined = true;
        } else {
            List<FunctionCall> fCallList = IRTools.getFunctionCalls(t);
            for( FunctionCall fCall : fCallList ) {
                if( StandardLibrary.contains(fCall) ) {
                    if( StandardLibrary.isSideEffectFree(fCall) ) {
                        continue;
                    } else {
                        Set<Symbol> usedSyms = DataFlowTools.getUseSymbol(fCall);
                        if( usedSyms.contains(sym) ) {
                            isDefined = true;
                            break;
                        }
                    }
                } else {
                    Procedure proc = fCall.getProcedure();
                    if( proc != null ) {
                        int index = isCalledByRef(sym, fCall);
                        if( index >= 0 ) {
                            Symbol paramSym = (Symbol)((VariableDeclaration)proc.getParameter(index)).getDeclarator(0);
                            isDefined = ipaIsDefined(paramSym, proc.getBody());
                        } else if ( SymbolTools.isGlobal(sym) ) {
                            isDefined = ipaIsDefined(sym, proc.getBody());
                        }
                        if( isDefined ) {
                            break;
                        }
                    }
                }

            }
        }
        return isDefined;
    }


    /**
     * Insert barriers before and after each compute region, so that other analysis can
     * easily distinguish GPU compute regions from CPU regions. 
     */
    public static void markIntervalForComputeRegions(Program program) {
		/* iterate to search for all Procedures */
        List<Procedure> procedureList = IRTools.getProcedureList(program);
        CompoundStatement target_parent;
        for (Procedure proc : procedureList)
        {
			/* Search for all compute regions in a given Procedure */
            List<mapreduceAnnotation> compAnnots = AnalysisTools.collectPragmas(proc, mapreduceAnnotation.class, mapreduceAnnotation.mapreduceDirectiveSet, false);
            if( compAnnots != null ) {
                for ( mapreduceAnnotation annot : compAnnots )
                {
                    Statement target_stmt = (Statement)annot.getAnnotatable();
                    target_parent = (CompoundStatement)target_stmt.getParent();
                    mapreduceAnnotation barrierAnnot = new mapreduceAnnotation("barrier", "S2P");
                    Statement bAStmt = new AnnotationStatement(barrierAnnot);
                    target_parent.addStatementBefore(target_stmt, bAStmt);
                    barrierAnnot = new mapreduceAnnotation("barrier", "P2S");
                    bAStmt = new AnnotationStatement(barrierAnnot);
                    target_parent.addStatementAfter(target_stmt, bAStmt);
                }
            }
        }
    }

    static public void deleteBarriers( Traversable t ) {
        List<mapreduceAnnotation> barrList = new LinkedList<mapreduceAnnotation>();
        DFIterator<AnnotationStatement> iter =
                new DFIterator<AnnotationStatement>(t, AnnotationStatement.class);
        while (iter.hasNext()) {
            AnnotationStatement at = iter.next();
            mapreduceAnnotation br_annot = at.getAnnotation(mapreduceAnnotation.class, "barrier");
            if ( br_annot != null ) {
                barrList.add(br_annot);
            }
        }
        for( mapreduceAnnotation o_annot : barrList ) {
            Statement astmt = (Statement)o_annot.getAnnotatable();
            if( astmt != null ) {
                Traversable parent = astmt.getParent();
                if( parent != null )
                    parent.removeChild(astmt);
                else
                    PrintTools.println("[Error in deleteBarriers()] parent is null!", 0);
            }
        }
    }


    public static boolean isInHeaderFile(Declaration decl, TranslationUnit trUnt) {
        boolean isInHeader = false;
        if( trUnt != null ) {
            Traversable tt = decl.getParent();
            while ( (tt != null) && !(tt instanceof TranslationUnit) ) {
                tt = tt.getParent();
            }
            if( (tt == null) || (tt != trUnt) ) {
                Tools.exit("[ERROR in AnalysisTools.isInHeaderFile()] input declaration does not belong to the input " +
                        "file:\nInput file: " + trUnt.getInputFilename() + "\nInput declaration: \n" + decl + "\n");
            }
            Declaration firstdecl = trUnt.getFirstDeclaration();
            if( decl != firstdecl ) {
                boolean foundFirstDecl = false;
                List<Traversable> children = trUnt.getChildren();
                for( Traversable child : children ) {
                    if( !foundFirstDecl ) {
                        if( child == decl ) {
                            isInHeader = true;
                            break;
                        } else if( child == firstdecl ) {
                            foundFirstDecl = true;
                            break;
                        }
                    }
                }
            }

        }
        return isInHeader;
    }

    /**
     * Find the first procedure in the input file ({@code trUnt}).
     *
     * @param trUnt input Translation Unit (input file)
     * @return the first procedure in the input file {@code trUnt}
     */
    public static Procedure findFirstProcedure(TranslationUnit trUnt) {
        Procedure firstProc = null;
        if( trUnt != null ) {
            Declaration firstdecl = trUnt.getFirstDeclaration();
            if( firstdecl instanceof Procedure ) {
                firstProc = (Procedure)firstdecl;
            } else {
                boolean foundFirstDecl = false;
                List<Traversable> children = trUnt.getChildren();
                for( Traversable child : children ) {
                    if( foundFirstDecl ) {
                        if( child instanceof Procedure ) {
                            firstProc = (Procedure)child;
                            break;
                        }
                    } else if( child == firstdecl ) {
                        foundFirstDecl = true;
                    }
                }
            }
        }
        return firstProc;
    }

    /**
     *  Return the list of procedure declarations belonging to the translation
     *  unit that the input tr belongs to. If the input tr is Program, this will
     *  return all procedure declarations in the whole program.
     * @param tr
     * @return
     */
    public static List<ProcedureDeclarator> getProcedureDeclarators(Traversable tr) {
        List<ProcedureDeclarator> retList = new ArrayList<ProcedureDeclarator>();
        while( (tr != null) && !(tr instanceof TranslationUnit) &&
                !(tr instanceof Program) ) {
            tr = tr.getParent();
        }
        if( tr != null ) {
            List<TranslationUnit> trUnts = new ArrayList<TranslationUnit>();
            if( tr instanceof TranslationUnit ) {
                trUnts.add((TranslationUnit)tr);
            } else if( tr instanceof Program ) {
                for( Traversable tt : ((Program)tr).getChildren() ) {
                    trUnts.add((TranslationUnit)tt);
                }
            }
            for( TranslationUnit trU : trUnts ) {
                BreadthFirstIterator iter = new BreadthFirstIterator(trU);
                iter.pruneOn(ProcedureDeclarator.class);
                for (;;)
                {
                    ProcedureDeclarator procDeclr = null;

                    try {
                        procDeclr = (ProcedureDeclarator)iter.next(ProcedureDeclarator.class);
                        retList.add(procDeclr);
                    } catch (NoSuchElementException e) {
                        break;
                    }
                }
            }
        }
        return retList;
    }

    /**
     * Find a specifier type whose bit length is the same as the that of input symbol {@code inSym}
     * FIXME: bitlength of a specifier type is dependent on the target system, and thus this should 
     * be changeable using a compiler option.
     *
     * @param inSym
     * @return
     */
    public static Specifier getBitVecType(Symbol inSym) {
        Specifier uType = null;
        List<Specifier> typespecs = inSym.getTypeSpecifiers();
        if( typespecs.contains(Specifier.CHAR) ) {
            uType = new UserSpecifier(new NameID("type8b"));
        } else if( typespecs.contains(Specifier.SHORT) ) {
            uType = new UserSpecifier(new NameID("type16b"));
        } else if( typespecs.contains(Specifier.INT) ) {
            if( typespecs.contains(Specifier.LONG) ) {
                //Assume sizeof(long int) = 8. 
                uType = new UserSpecifier(new NameID("type64b"));
            } else {
                //Assume sizeof(int) = 4. 
                uType = new UserSpecifier(new NameID("type32b"));
            }
        } else if( typespecs.contains(Specifier.FLOAT) ) {
            uType = new UserSpecifier(new NameID("type32b"));
        } else if( typespecs.contains(Specifier.DOUBLE) ) {
            uType = new UserSpecifier(new NameID("type64b"));
        }
        return uType;
    }

    /**
     * Search input expression and return an ArrayAccess expression if existing;
     * if there are multiple ArrayAccess expressions, the first one will be returned.
     * If there is no ArrayAccess, return null.
     *
     * @param iExp input expression to be searched.
     * @return ArrayAccess expression 
     */
    public static ArrayAccess getArrayAccess(Expression iExp) {
        ArrayAccess aAccess = null;
        DepthFirstIterator iter = new DepthFirstIterator(iExp);
        while(iter.hasNext()) {
            Object o = iter.next();
            if(o instanceof ArrayAccess)
            {
                aAccess = (ArrayAccess)o;
                break;
            }
        }
        return aAccess;
    }

    /**
     * Search input expression and return a list of ArrayAccess expressions if existing;
     * If there is no ArrayAccess, return empty list.
     *
     * @param iExp input expression to be searched.
     * @return list of ArrayAccess expressions 
     */
    public static List<ArrayAccess> getArrayAccesses(Expression iExp) {
        List<ArrayAccess> aList = new LinkedList<ArrayAccess>();
        DepthFirstIterator iter = new DepthFirstIterator(iExp);
        while(iter.hasNext()) {
            Object o = iter.next();
            if(o instanceof ArrayAccess)
            {
                aList.add((ArrayAccess)o);
            }
        }
        return aList;
    }


}

