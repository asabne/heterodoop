package cudamr.hir;

import cetus.hir.IntegerLiteral;
import cetus.hir.NameID;
import cetus.hir.PrintTools;
import cetus.hir.Specifier;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

public class TextureSpecifier extends CUDASpecifier
{
  private List<Specifier> specs;
  private IntegerLiteral dim;
  private NameID readMode;

  public TextureSpecifier()
  {
    specs = new LinkedList<Specifier>();
  }

  public TextureSpecifier(Specifier type)
  {
    specs = new LinkedList<Specifier>();
    specs.add(type);
    dim = new IntegerLiteral(1);
    readMode = new NameID("cudaReadModeElementType");
  }

  public TextureSpecifier(List<Specifier> ispecs)
  {
    specs = new LinkedList<Specifier>();
    specs.addAll(ispecs);
    dim = new IntegerLiteral(1);
    readMode = new NameID("cudaReadModeElementType");
  }

  public TextureSpecifier(List<Specifier> ispecs, int idim)
  {
    specs = new LinkedList<Specifier>();
    specs.addAll(ispecs);
    dim = new IntegerLiteral(idim);
    readMode = new NameID("cudaReadModeElementType");
  }

  public TextureSpecifier(List<Specifier> ispecs, int idim, String ireadMode)
  {
    specs = new LinkedList<Specifier>();
    specs.addAll(ispecs);
    dim = new IntegerLiteral(idim);
    readMode = new NameID(ireadMode);
  }

  /** Prints the specifier to the print writer. */
  public void print(PrintWriter o)
  {
    o.print("texture<");
    o.print(PrintTools.listToString(specs, " "));
    o.print(", " + dim + ", " + readMode);
    o.print(">");
  }

  /** Returns a string representation of the specifier. */
  @Override
  public String toString()
  {
    StringWriter sw = new StringWriter(16);
    print(new PrintWriter(sw));
    return sw.toString();
  }

}
