package cudamr.hir;

import cetus.hir.Annotatable;
import cetus.hir.PragmaAnnotation;
import cetus.hir.PrintTools;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: asabne
 * Date: 10/10/13
 * Time: 4:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class mapreduceAnnotation extends PragmaAnnotation {
    private static final Set<String> no_values =
            new HashSet<String>(Arrays.asList("mapper", "compiler", "reducer"));

    private static final Set<String> collection_values =
            new HashSet<String>(Arrays.asList("key", "value", "keylength", "valuein", "keyin", "vallength", "sharedRO", "texture", "kvpairs", "blocks", "threads"));

    public static final Set<String> mapreduceDirectiveSet = new HashSet(Arrays.asList("mapper", "combiner", "reducer"));

    private static final List<String> print_order =
            new ArrayList<String>(Arrays.asList("mapper", "compiler", "reducer", "key", "value", "keylength", "valuein",
                    "keyin", "vallength", "sharedRO", "texture", "kvpairs", "blocks", "threads"));

    public mapreduceAnnotation()
    {
        super("mapreduce");
    }

    /**
     * Constructs a mapreduce annotation with the given key-value pair.
     */
    public mapreduceAnnotation(String key, Object value)
    {
        super("mapreduce");
        put(key, value);
    }

    public String toString()
    {
        if ( skip_print )
            return "";

        StringBuilder str = new StringBuilder(80);

        str.append(super.toString());

        Set<String> directiveSet = new HashSet<String>();
        directiveSet.addAll(keySet());
        directiveSet.remove("pragma");

        for( String key : print_order ) {
            if( directiveSet.contains(key) ) {
                printDirective(key, str);
                directiveSet.remove(key);
            }
        }
        if( !directiveSet.isEmpty() ) {
            for( String key: directiveSet ) {
                printDirective(key, str);
            }
        }

        return str.toString();
    }

    private void printDirective(String key, StringBuilder str) {
        if ( no_values.contains(key) )
            str.append(" "+key);
        else if ( collection_values.contains(key) )
        {
            str.append(" "+key+"(");
            Object value = get(key);
            if ( value instanceof Collection )
                if( value instanceof List ) {
                    str.append(PrintTools.listToString((List)value, ", "));
                } else {
                    str.append(PrintTools.collectionToString((Collection)value, ", "));
                }
            else // e.g., num_gangs
                str.append(value);
            str.append(")");
        }
        else
        {
            //If this annotation contains annotatable object as value,
            //printing the value will cause infinite recursion; skip
            //printing annotatable object.
            Object tObj = get(key);
            if( !(tObj instanceof Annotatable) ) {
                str.append(" "+key);
                if ( (tObj != null) && (!"true".equals(tObj)) )
                    str.append(" ("+tObj+")");
            }
        }
    }

}
