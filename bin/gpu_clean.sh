#!/bin/bash
source ~/vars.sh

if [ $# -ne 1 ]
then
  echo "Usage: ./gpu_clean.sh [option]"
  exit 1
fi

function cleanAllgpu() {
	#remove all GPU logs
	rm -f ~/cuda_rt/log_* < /dev/null
	while read line
	do
		name=$line
		ssh -n $name " killall -SIGTERM driver0;" < /dev/null
		if [[ $HOSTNAME = *kid* ]] 
		then 
			ssh -n $name " killall -SIGTERM driver1; killall -SIGTERM driver2;" < /dev/null 
		fi
	
		echo "Deleting locks & logs : Node $name"
		ssh -n $name rm -f $HADOOP_TMP_DIR/mapred/local/taskTracker/asabne/jobcache/gpuLocks/* < /dev/null
		#ssh -n $name rm -f ~/cuda_rt/log_$name* < /dev/null
		#for the worst
		#ssh -n $name " killall -9 driver0; killall -9 hdfsClient;" < /dev/null
		ssh -n $name " killall -9 requester0;" < /dev/null
		if [[ $HOSTNAME = *kid* ]] 
		then
			ssh -n $name " killall -9 requester1; killall -9 requester2;" < /dev/null
		fi
	done < ${HADOOP_CONF_DIR}slaves
}


function cleanThisgpu() {
	name=`hostname`
	ssh -n $name " killall -SIGTERM driver0;" < /dev/null
	if [[ $HOSTNAME = *kid* ]] 
	then 
		ssh -n $name " killall -SIGTERM driver1; killall -SIGTERM driver2;" < /dev/null 
	fi
	echo "Deleting locks & logs : Node $name"
	ssh -n $name rm -f $HADOOP_TMP_DIR/mapred/local/taskTracker/asabne/jobcache/gpuLocks/* < /dev/null
	ssh -n $name rm -f ~/cuda_rt/log_$name* < /dev/null
	#for the worst
	#ssh -n $name " killall -9 driver0; killall -9 hdfsClient;" < /dev/null
	ssh -n $name " killall -9 requester0;" < /dev/null
	if [[ $HOSTNAME = *kid* ]] 
	then
		ssh -n $name " killall -9 requester1; killall -9 requester2;" < /dev/null
	fi
}

function startAllgpu() {
	while read line
	do
		name=$line
		#echo "ssh $name ~/cuda_rt/launcher0.sh </dev/null >/dev/null 2>&1 &"
		ssh -n $name "~/cuda_rt/launcher0.sh </dev/null >/dev/null 2>&1 &" < /dev/null

		#start the other GPU drivers only on kids
		if [[ $HOSTNAME = *kid* ]] 
		then 
			ssh -n $name "~/cuda_rt/launcher1.sh </dev/null >/dev/null 2>&1 &" < /dev/null
			ssh -n $name "~/cuda_rt/launcher2.sh </dev/null >/dev/null 2>&1 &" < /dev/null
		fi
	done < ${HADOOP_CONF_DIR}slaves
}

function findFaultyMachines() {
	while read line
	do
		name=$line
		#hdfs=`ssh -n $name "ps -A | grep -e hdfs" < /dev/null`
		#if [ -z "$hdfs" ]; then
			#echo "HDFS client failure on node $line";
		#fi
		driver=`ssh -n $name "ps -A | grep -c driver" < /dev/null`
		if [ $driver -ne 3 ]; then
			echo "Driver failure on node $line";
		fi
	done < ${HADOOP_CONF_DIR}slaves

}


function checkLogs() {
	while read line
	do
		name=$line
		file=~/cuda_rt/log_${name}_0
		lockCreate=`ssh -n $name "grep Creating $file*" < /dev/null`
		if [ -z "$lockCreate" ]; then
			echo "Lock creation not found on $name";
		fi
	done < ${HADOOP_CONF_DIR}slaves

}


function killLaunchers() {
	while read line
	do
		name=$line
		ssh -n $name "killall -9 launcher0.sh" < /dev/null
		if [[ $HOSTNAME = *kid* ]] 
		then 
			ssh -n $name "killall -9 launcher1.sh" < /dev/null
			ssh -n $name "killall -9 launcher2.sh" < /dev/null
		fi
	done < ${HADOOP_CONF_DIR}slaves

}


function resetHadoop() {
	stop-mapred.sh
	stop-dfs.sh
	#remove all logs
	rm -f $HADOOP_LOGS/*

	while read line
	do
		name=$line
		ssh -n $name "rm -fr $HADOOP_TMP_DIR/*" < /dev/null
		ssh -n $name "killall -9 java" < /dev/null
	done < ${HADOOP_CONF_DIR}slaves

	while read line
	do
		name=$line
		ssh -n $name "rm -fr $HADOOP_TMP_DIR/*" < /dev/null
		ssh -n $name "killall -9 java" < /dev/null
	done < ${HADOOP_CONF_DIR}masters

	hadoop namenode -format
	start-dfs.sh
	start-mapred.sh
}

case "$1" in

'cleanallgpu')
		cleanAllgpu;
		;;
'cleanthisgpu')
		cleanThisgpu;
		;;
'resetallgpu')
		cleanAllgpu;
		startAllgpu;
		;;
'findfaulty')
		findFaultyMachines;
		;;
'resethadoop') 
		resetHadoop;
		;;
'checklogs') 
		checkLogs;
		;;
'killlaunchers') 
		killLaunchers;
		;;
*)
	echo "Invalid Application Name specified"
		;;
esac
