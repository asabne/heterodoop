#!/bin/bash

source ~/vars.sh

#remove previous logs
rm -fr $HADOOP_LOGS
rm -f ~/cuda_rt/log*



if [ ! -d ${HADOOP_CONF_DIR} ]; then 
mkdir -p ${HADOOP_CONF_DIR}
cp ${HADOOP_HOME}/conf/* ${HADOOP_CONF_DIR}
sed -ie "s|USERNAME|$USER|g" ${HADOOP_CONF_DIR}mapred-site.xml.default 
sed -ie "s|USERNAME|$USER|g" ${HADOOP_CONF_DIR}hdfs-site.xml.default 
cp ${HADOOP_CONF_DIR}hdfs-site.xml.default ${HADOOP_CONF_DIR}hdfs-site.xml
fi


cat $PBS_NODEFILE | uniq | tail -n+2 > ${HADOOP_CONF_DIR}slaves
cat $PBS_NODEFILE | uniq  | head -1 > ${HADOOP_CONF_DIR}masters

namenode=`cat $PBS_NODEFILE | uniq  | head -1`
jobnode=$namenode
sed -e "s|localhost|$jobnode|g" ${HADOOP_CONF_DIR}mapred-site.xml.default > ${HADOOP_CONF_DIR}mapred-site.xml
sed -e "s|localhost|$namenode|g" ${HADOOP_CONF_DIR}core-site.xml.default > ${HADOOP_CONF_DIR}core-site.xml



echo "Starting Hadoop Daemons"

#remove old hadoop tmp
while read line
do
    name=$line
	#remove old hadoop tmp
	ssh -n $name rm -fr $HADOOP_TMP_DIR/ < /dev/null
done < ${HADOOP_CONF_DIR}slaves

#start hadoop daemons on master
while read line
do
    name=$line
		#remove old hadoop tmp
		ssh -n $name rm -fr $HADOOP_TMP_DIR/ < /dev/null
		ssh -n $name ${HADOOP_HOME}/bin/hadoop namenode -format < /dev/null
		ssh -n $name ${HADOOP_HOME}/bin/start-dfs.sh < /dev/null
		ssh -n $name ${HADOOP_HOME}/bin/start-mapred.sh < /dev/null
done < ${HADOOP_CONF_DIR}masters

#clean-up the gpus
gpu_clean.sh resetallgpu
gpu_clean.sh killlaunchers
gpu_clean.sh resetallgpu




